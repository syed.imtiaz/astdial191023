<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$con = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($con,$dbName);

if ($con->connect_error) {
 die("Connection failed: " . $con->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
$cur_date = date("Y-m-d");

// CAMPAIGN
$select_camp = "SELECT campaignname,campaignid FROM vtiger_campaign INNER JOIN vtiger_crmentity ON vtiger_campaign.campaignid=vtiger_crmentity.crmid WHERE deleted='0' AND campaignstatus='Active'";	
$query_camp = mysqli_query($con,$select_camp);				
while($row_camp = mysqli_fetch_assoc($query_camp)){
	$campaigns[] = $row_camp['campaignname'];
}

// MAINDISPO
$select_dispo = mysqli_query($con, "SELECT name, maindispoid FROM vtiger_maindispo INNER JOIN vtiger_crmentity ON vtiger_maindispo.maindispoid=vtiger_crmentity.crmid WHERE deleted='0'");
while($row_dispo = mysqli_fetch_assoc($select_dispo)){
	$main_dispo[] = $row_dispo['name'];
}

//SUB DISPO
$select_subdispo = mysqli_query($con, "SELECT name, subdispoid FROM vtiger_subdispo INNER JOIN vtiger_crmentity ON vtiger_subdispo.subdispoid=vtiger_crmentity.crmid WHERE deleted='0'");
while($row_subdispo = mysqli_fetch_assoc($select_subdispo)){
	$sub_dispo[] = $row_subdispo['name'];
}
$realtime_contents = array(
		"campaigns"=>$campaigns,
		"main_dispo"=>$main_dispo,
		"sub_dispo"=>$sub_dispo,
		);
echo json_encode($realtime_contents);	

?>
