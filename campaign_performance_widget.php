<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$conn = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($conn,$dbName);

if ($conn->connect_error) {
 die("Connection failed: " . $conn->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)

$yesterday = date('Y-m-d',strtotime("-1 days"));
$last_seventh_day = date('Y-m-d',strtotime("-7 days"));
$today = date('Y-m-d');
// $seventh_day = date('Y-m-d',strtotime("-7 days"));

	if($_POST['post_data'] == 1){
		$post = '1';

		$sdate = $_POST['sdate'];
		$edate = $_POST['edate'];
		$campaign = $_POST['camp_sel'];

		$start_date = date("Y-m-d", strtotime($sdate));
		$end_date = date("Y-m-d", strtotime($edate));

	$start_dates = date("d-m-Y", strtotime($sdate));
	$end_dates = date("d-m-Y", strtotime($edate));

			if($campaign != ''){
				$camp = " AND campaign LIKE '%".$campaign."%' " ;
				$campa = " AND a.campaign LIKE '%".$campaign."%' " ;
				$campaignname = " AND vtiger_campaign.campaignname LIKE '%".$campaign."%' " ;
				$campaign_contname = " AND b.campaignname LIKE '%".$campaign."%' " ;
				$camp_logged = " AND campaign_logged_in LIKE '%".$campaign."%' " ;

			}else{
				$camp = "";
				$acamp = "";
				$campaignname = "";
				$camp_logged = "" ;
				$campaign_contname = "" ;
			}
		// connected
		$select_connect = "SELECT count(*), campaign from campaign_dial_status where status='1' and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." GROUP BY campaign ";

		// not connected
		$select_notconnect = "SELECT count(*), campaign from campaign_dial_status where status='2' and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." GROUP BY campaign ";
		
	//	echo $select_notconnect;
		// campaign login and talk
		$camp_login = mysqli_query($conn, "SELECT SUM(login_time) as login_time, SUM(pause_time) as pause_time, campaign_logged_in FROM user_callsummary WHERE DATE(`current_eventtime`)<='$end_date' AND DATE(`current_eventtime`)>='$start_date' ".$camp_logged." GROUP BY campaign_logged_in ");

		// active campaigns
		$camp_active = mysqli_query($conn, "SELECT COUNT(*) AS camp_total, a.campaign AS campaignname, b.campaignname FROM campaign_dial_status a LEFT JOIN vtiger_campaign b ON a.campaign = b.campaignname LEFT JOIN vtiger_crmentity c ON b.campaignid = c.crmid WHERE DATE(a.modify_date) >= '$start_date' AND DATE(a.modify_date) <= '$end_date' AND c.setype = 'Campaigns' AND a.status!='' ".$campa." GROUP BY a.campaign ");

		while($row_camp = mysqli_fetch_assoc($camp_active)){
			$act_camp_name[] = $row_camp['campaignname'];
			$act_camp_total[] = $row_camp['camp_total'];
		}

	// dialed
	
		foreach ($act_camp_name as $campaign) {
			
			$camp_active_dialed = mysqli_query($conn, "SELECT COUNT(*) AS camp_total FROM campaign_dial_status WHERE campaign LIKE '%$campaign%' AND DATE(modify_date) >= '$start_date' AND DATE(modify_date) <= '$end_date' AND status='1' ".$camp." GROUP BY campaign ");
			// campaign active dialed
			//echo "SELECT COUNT(*) AS camp_total FROM campaign_dial_status WHERE campaign LIKE '%$campaign%' AND DATE(modify_date) >= '$start_date' AND DATE(modify_date) <= '$end_date' AND status='1' ".$campa." GROUP BY campaign ";
			while($row_camp_active_dialed = mysqli_fetch_assoc($camp_active_dialed)){
				$act_camp_total_dialed[] = $row_camp_active_dialed['camp_total'];
			}
		
		}
		
		// not dialed
		foreach ($act_camp_name as $campaign) {
			
			$camp_active_notdialed = mysqli_query($conn, "SELECT COUNT(*) AS camp_total FROM campaign_dial_status WHERE campaign LIKE '%$campaign%' AND DATE(modify_date) >= '$start_date' AND DATE(modify_date) <= '$end_date' AND status='2' ".$camp." GROUP BY campaign ");
			
			//echo "SELECT COUNT(*) AS camp_total FROM campaign_dial_status WHERE campaign LIKE '%$campaign%' AND DATE(modify_date) >= '$start_date' AND DATE(modify_date) <= '$end_date' AND status='2' " . $campa . " GROUP BY campaign ";
			// campaign active notdialed
			while($row_camp_active_notdialed = mysqli_fetch_assoc($camp_active_notdialed)){
				$act_camp_total_notdialed[] = $row_camp_active_notdialed['camp_total'];
			}
		}

		$total_sum_notcon = 0;
		foreach($act_camp_total_notdialed as $total_sum_notdialed){
			$total_sum_notcon = $total_sum_notcon + $total_sum_notdialed;
		}

		// pending calls
		$camp_active_pending = mysqli_query($conn, "SELECT COUNT(*) camp_total_pen, b.campaignname FROM vtiger_campaigncontrel a LEFT JOIN vtiger_campaign b on a.campaignid=b.campaignid LEFT JOIN vtiger_crmentity c on b.campaignid = c.crmid WHERE c.setype = 'Campaigns' AND c.deleted = '0' and a.ast_update = '0' and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$campaign_contname." GROUP BY a.campaignid ");

	}else{
		$post = '0';
		$camp = "";
		$acamp = "";
		$campaignname = "";
		$camp_logged = "" ;
		$campaign_contname = "" ;

		$start_date = $last_seventh_day;
		$end_date = $today;

		$start_dates = date("d-m-Y", strtotime($last_seventh_day));
		$end_dates = date("d-m-Y", strtotime($today));

		// connected
		$select_connect = "SELECT count(*), campaign from campaign_dial_status where status='1' and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." GROUP BY campaign ";

		
		// not connected
		$select_notconnect = "SELECT count(*), campaign from campaign_dial_status where status='2' and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." GROUP BY campaign ";

	
		// campaign login and talk
		$camp_login = mysqli_query($conn, "SELECT SUM(login_time) as login_time, SUM(pause_time) as pause_time, campaign_logged_in FROM user_callsummary WHERE DATE(`current_eventtime`)<='$end_date' AND DATE(`current_eventtime`)>='$start_date' ".$camp_logged." GROUP BY campaign_logged_in ");

		// active campaigns
		
		$camp_active = mysqli_query($conn, "SELECT COUNT(*) AS camp_total,a.campaign AS campaignname, b.campaignname FROM campaign_dial_status a LEFT JOIN vtiger_campaign b ON a.campaign = b.campaignname LEFT JOIN vtiger_crmentity c ON b.campaignid = c.crmid WHERE DATE(a.modify_date) >= '$start_date' AND DATE(a.modify_date) <= '$end_date' AND c.setype = 'Campaigns' AND a.status!='' GROUP BY a.campaign ");
		while($row_camp = mysqli_fetch_assoc($camp_active)){
			$act_camp_name[] = $row_camp['campaignname'];
			$act_camp_total[] = $row_camp['camp_total'];
			
		}

		//total sum of calls 
		$total_sum = 0;
		foreach($act_camp_total as $total_sum_calls){
			$total_sum = $total_sum + $total_sum_calls;

		}
	//end total sum of calls

	// dialed
	//$act_camp_total_dialed[] = '';
		 foreach ($act_camp_name as $campaign) {
			
			$camp_active_dialed = mysqli_query($conn, "SELECT COUNT(*) AS camp_total FROM campaign_dial_status WHERE campaign LIKE '%$campaign%' AND DATE(modify_date) >= '$start_date' AND DATE(modify_date) <= '$end_date' AND status='1' GROUP BY campaign ");
			while($row_camp_active_dialed = mysqli_fetch_assoc($camp_active_dialed)){
				$act_camp_total_dialed[] = $row_camp_active_dialed['camp_total'];
			}
		 }

		 //
		 $total_sum_conn= 0;
		 foreach($act_camp_total_dialed as $total_sum_connected){
			$total_sum_conn = $total_sum_conn + $total_sum_connected;
		 }

	// not dialed
		
		foreach ($act_camp_name as $campaign) {

			$camp_active_notdialed = mysqli_query($conn, "SELECT COUNT(*) AS camp_total FROM campaign_dial_status WHERE campaign LIKE '%$campaign%' AND DATE(modify_date) >= '$start_date' AND DATE(modify_date) <= '$end_date' AND status='2' GROUP BY campaign ");
			// campaign active notdialed
			while($row_camp_active_notdialed = mysqli_fetch_assoc($camp_active_notdialed)){
				$act_camp_total_notdialed[] = $row_camp_active_notdialed['camp_total'];	
			}
		}

		//total sum not connected
		$t = 0;
		foreach($act_camp_total_notdialed as $total_not_dialed){
			$t = $t + $total_not_dialed;
		}
		//end

		// pending calls
		$camp_active_pending = mysqli_query($conn, "SELECT COUNT(*) camp_total_pen, b.campaignname FROM vtiger_campaigncontrel a LEFT JOIN vtiger_campaign b on a.campaignid=b.campaignid LEFT JOIN vtiger_crmentity c on b.campaignid = c.crmid WHERE c.setype = 'Campaigns' AND c.deleted = '0' and a.ast_update = '0' and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$campaign_contname." GROUP BY a.campaignid ");
	}

	// campaign pending
	while($row_camp_pending = mysqli_fetch_assoc($camp_active_pending)){
		$pend_name[] = $row_camp_pending['campaignname'];
		$pend_total[] = $row_camp_pending['camp_total_pen'];
	}

	// campaign login
	while($row_camp_login = mysqli_fetch_assoc($camp_login)){
		if($row_camp_login['login_time'] < 0){
			$login_time[] = 0;
		}else{
			$login_time[] = floor($row_camp_login['login_time']/60);
		}
		if($row_camp_login['pause_time'] < 0){
			$pause_time[] = 0;
		}else{
			$pause_time[] = floor($row_camp_login['pause_time']/60);
		}
		
		$campaign_logged_in[] = $row_camp_login['campaign_logged_in'];
	}

	//connect
	$query_connect = mysqli_query($conn,$select_connect);
	while($row_connect = mysqli_fetch_array($query_connect)){
		$connect[] = $row_connect[0];
		$campaigns[] = $row_connect[1];
	}

//notconnect
  //  $notconnect = 0;
	$query_notconnect = mysqli_query($conn,$select_notconnect);
	while($row_notconnect = mysqli_fetch_array($query_notconnect)){
		$notconnect[] = $row_notconnect[0];
	}

// get campaigns
$campid=array();
$select_camp = "SELECT campaignname,campaignid FROM vtiger_campaign INNER JOIN vtiger_crmentity ON vtiger_campaign.campaignid=vtiger_crmentity.crmid WHERE deleted='0' AND campaignstatus='Active'";
$query_camp = mysqli_query($conn,$select_camp);
while($row_camp = mysqli_fetch_assoc($query_camp)){
        $campaigns[] = $row_camp['campaignname'];
        $campid[] = $row_camp['campaignid'];
}
// end campaigns
// print_r($campid);

$campdialarray=array();
foreach($campid as $campaignid){
        //$totcamplist="SELECT * FROM `vtiger_campaigncontrel` WHERE `campaignrelstatusid` = '1' AND `campaignid` = '$campaignid'";
        //$res_totlist=mysqli_query($con,$totcamplist);
        //$totlist=mysqli_num_rows($res_totlist);

$totcamplist = "SELECT COUNT(DISTINCT vtiger_crmentity.crmid) AS count FROM vtiger_contactdetails INNER JOIN vtiger_campaigncontrel ON vtiger_campaigncontrel.contactid = vtiger_contactdetails.contactid INNER JOIN vtiger_contactaddress ON vtiger_contactdetails.contactid = vtiger_contactaddress.contactaddressid INNER JOIN vtiger_contactsubdetails ON vtiger_contactdetails.contactid = vtiger_contactsubdetails.contactsubscriptionid INNER JOIN vtiger_customerdetails ON vtiger_contactdetails.contactid = vtiger_customerdetails.customerid INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_contactdetails.contactid LEFT JOIN vtiger_contactscf ON vtiger_contactdetails.contactid = vtiger_contactscf.contactid LEFT JOIN vtiger_groups ON vtiger_groups.groupid=vtiger_crmentity.smownerid LEFT JOIN vtiger_users ON vtiger_crmentity.smownerid=vtiger_users.id LEFT JOIN vtiger_account ON vtiger_account.accountid = vtiger_contactdetails.accountid LEFT JOIN vtiger_campaignrelstatus ON vtiger_campaignrelstatus.campaignrelstatusid = vtiger_campaigncontrel.campaignrelstatusid WHERE vtiger_campaigncontrel.campaignid = '$campaignid' AND vtiger_crmentity.deleted=0";

        $res_totlist=mysqli_query($conn,$totcamplist);
        $totlist_row=mysqli_fetch_assoc($res_totlist);
        $totlist=$totlist_row['count'];

	 $totcampdiallist="SELECT * FROM `vtiger_campaigncontrel` WHERE `campaignrelstatusid` = '1' AND (status !='0' or dispo !='0') AND `campaignid` = '$campaignid'";
	
	$totcampdiallist = "SELECT COUNT(DISTINCT vtiger_crmentity.crmid) AS count FROM vtiger_contactdetails INNER JOIN vtiger_campaigncontrel ON vtiger_campaigncontrel.contactid = vtiger_contactdetails.contactid INNER JOIN vtiger_contactaddress ON vtiger_contactdetails.contactid = vtiger_contactaddress.contactaddressid INNER JOIN vtiger_contactsubdetails ON vtiger_contactdetails.contactid = vtiger_contactsubdetails.contactsubscriptionid INNER JOIN vtiger_customerdetails ON vtiger_contactdetails.contactid = vtiger_customerdetails.customerid INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_contactdetails.contactid LEFT JOIN vtiger_contactscf ON vtiger_contactdetails.contactid = vtiger_contactscf.contactid LEFT JOIN vtiger_groups ON vtiger_groups.groupid=vtiger_crmentity.smownerid LEFT JOIN vtiger_users ON vtiger_crmentity.smownerid=vtiger_users.id LEFT JOIN vtiger_account ON vtiger_account.accountid = vtiger_contactdetails.accountid LEFT JOIN vtiger_campaignrelstatus ON vtiger_campaignrelstatus.campaignrelstatusid = vtiger_campaigncontrel.campaignrelstatusid WHERE vtiger_campaigncontrel.campaignid = '$campaignid' AND vtiger_crmentity.deleted=0 AND vtiger_campaigncontrel.`campaignrelstatusid` = '1' AND (vtiger_campaigncontrel.status !='0' or vtiger_campaigncontrel.dispo !='0')";

	$res_totdiallist=mysqli_query($conn,$totcampdiallist);
	$list = mysqli_fetch_assoc($res_totdiallist);
	$totdiallist = $list['count'];

	//$totdiallist=mysqli_num_rows($res_totdiallist);

	$totnondiallist=$totlist - $totdiallist;
        $campname="select campaignname FROM `vtiger_campaign` where campaignid='$campaignid'";
        $res_campname=mysqli_query($conn,$campname);
        while($rowcamp=mysqli_fetch_array($res_campname)){
                $cName=$rowcamp['campaignname'];
       }
        $campdialarray[]=$cName.'@_@'.$totlist.'@_@'.$totdiallist.'@_@'.$totnondiallist.'@_@'.$campaignid;
}
$widget_contents = array(
   		"connect"=>$connect,
   		"notconnect"=>$notconnect,
		"campaigns"=>$campaigns,
		"campaign_logged_in"=>$campaign_logged_in,
		"pause_time"=>$pause_time,
		"login_time"=>$login_time,
		"act_camp_name"=>$act_camp_name,
		"act_camp_total"=>$act_camp_total,
		"act_camp_total_dialed"=>$act_camp_total_dialed,
		"act_camp_total_notdialed"=>$act_camp_total_notdialed,
		"pend_name"=>$pend_name,
		"pend_total"=>$pend_total,
		"start_dates"=>$start_dates,
		"end_dates"=>$end_dates,
		"total_sum_calls"=> $total_sum,
		"total_sum_not_conneted"=>$t,
		"total_sum_conn"=> $total_sum_conn,
		"campaignDetails"=>$campdialarray,
		);
echo json_encode($widget_contents);	
?>

