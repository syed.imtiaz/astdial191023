<?php
include 'config.inc.php';
$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$conn = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($conn,$dbName);

if ($conn->connect_error) {
 die("Connection failed: " . $conn->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)

$yesterday = date('Y-m-d',strtotime("-1 days"));
$last_seventh_day = date('Y-m-d',strtotime("-7 days"));
$today = date('Y-m-d');
// $seventh_day = date('Y-m-d',strtotime("-7 days"));

	if($_POST['post_data'] == 1){
		$post = '1';
		$sdate = $_POST['sdate'];
		$edate = $_POST['edate'];
		$campaign = $_POST['camp_sel'];
		$start_date = date("Y-m-d", strtotime($sdate));
		$end_date = date("Y-m-d", strtotime($edate));

			if($campaign != ''){
				$camp = " AND campaign LIKE '%".$campaign."%' " ;
				$campaignname = " AND vtiger_campaign.campaignname LIKE '%".$campaign."%' " ;
				$camp_logged = " AND campaign_logged_in LIKE '%".$campaign."%' " ;

			}else{
				$camp = "";
				$campaignname = "";
				$camp_logged = "" ;
			}

	}else{
		$post = '0';
		$camp = "";
		$campaignname = "";
		$camp_logged = "" ;
		$start_date = $last_seventh_day;
		$end_date = $today;
		$start_dates = date("d-m-Y", strtotime($last_seventh_day));
		$end_dates = date("d-m-Y", strtotime($today));
	}

	//connect
	$select_connect = "SELECT count(*) from campaign_dial_status where status='1' and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." ";
	$query_connect = mysqli_query($conn,$select_connect);
	$row_connect = mysqli_fetch_array($query_connect);
	$connect = $row_connect[0];

	//notconnect
	$select_notconnect = "SELECT count(*) from campaign_dial_status where status='2' and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." ";
	$query_notconnect = mysqli_query($conn,$select_notconnect);
	$row_notconnect = mysqli_fetch_array($query_notconnect);
	$notconnect = $row_notconnect[0];
	$dial = $connect + $notconnect;

	// YESTERDAY TOTAL CALLS
	$call_yesterday_sql = mysqli_query($conn, "SELECT COUNT(a.calllogsid) as total_yesterday_calls, a.source, b.first_name, b.last_name FROM vtiger_calllogs a LEFT JOIN vtiger_users b ON a.source = b.user_name WHERE a.date <= '".$end_date."' AND a.date >= '".$start_date."' ".$camp." GROUP BY a.source");
	while($call_yesterday_row = mysqli_fetch_assoc($call_yesterday_sql)){
		$yes_total[] = $call_yesterday_row['total_yesterday_calls'];
		$yes_source[] = $call_yesterday_row['source'];
		$yes_user[] = $call_yesterday_row['first_name'].' '.$call_yesterday_row['last_name'];
	}

	// YESTERDAY CONNECTED CALLS
	foreach ($yes_source as $lguser){
		$yes_conn_sql = mysqli_query($conn, "SELECT  COUNT(a.calllogsid) as conn_yesterday_calls FROM vtiger_calllogs a LEFT JOIN vtiger_maindispo b ON a.dispo = b.name LEFT JOIN vtiger_maindispocf c ON b.maindispoid = c.maindispoid LEFT JOIN vtiger_users d ON a.source = d.user_name WHERE a.source='$lguser' and c.cf_918 = 'Connected' and a.date <= '".$end_date."' AND a.date >= '".$start_date."' ".$camp." GROUP BY a.source");
		if(mysqli_num_rows($yes_conn_sql)>0){
		while($row_yes_con = mysqli_fetch_assoc($yes_conn_sql)){
			$yes_call_conn[] = $row_yes_con['conn_yesterday_calls'];
		}
	}
	else {
		$yes_call_conn[] = "0";
	}
	}

		// YESTERDAY NOT CONNECTED CALLS
		foreach ($yes_source as $lgnuser){
		$yes_conn_sqln = mysqli_query($conn, "SELECT  COUNT(a.calllogsid) as conn_yesterday_calls FROM vtiger_calllogs a LEFT JOIN vtiger_maindispo b ON a.dispo = b.name LEFT JOIN vtiger_maindispocf c ON b.maindispoid = c.maindispoid LEFT JOIN vtiger_users d ON a.source = d.user_name WHERE a.source='$lgnuser' and c.cf_918 = 'Not Connected' and a.date <= '".$end_date."' AND a.date >= '".$start_date."' ".$camp." GROUP BY a.source");
		if(mysqli_num_rows($yes_conn_sqln)>0){
		while($row_yes_conn = mysqli_fetch_assoc($yes_conn_sqln)){
			$yes_call_nconn[] = $row_yes_conn['conn_yesterday_calls'];
		}
	}
	else {
		$yes_call_nconn[] = "0";
	}
	}

	// // YESTERDAY CONNECTED CALLS
	// $yes_conn_sql = mysqli_query($conn, "SELECT  COUNT(a.calllogsid) as conn_yesterday_calls FROM vtiger_calllogs a LEFT JOIN vtiger_maindispo b ON a.dispo = b.name LEFT JOIN vtiger_maindispocf c ON b.maindispoid = c.maindispoid LEFT JOIN vtiger_users d ON a.source = d.user_name WHERE c.cf_918 = 'Connected' and a.date <= '".$end_date."' AND a.date >= '".$start_date."' ".$camp." GROUP BY a.source");
	// while($row_yes_con = mysqli_fetch_assoc($yes_conn_sql)){
	// 	$yes_call_conn[] = $row_yes_con['conn_yesterday_calls'];
	// }

	// // YESTERDAY NOT CONNECTED CALLS
	// $yes_conn_sqln = mysqli_query($conn, "SELECT  COUNT(a.calllogsid) as conn_yesterday_calls FROM vtiger_calllogs a LEFT JOIN vtiger_maindispo b ON a.dispo = b.name LEFT JOIN vtiger_maindispocf c ON b.maindispoid = c.maindispoid LEFT JOIN vtiger_users d ON a.source = d.user_name WHERE c.cf_918 = 'Not Connected' and a.date <= '".$end_date."' AND a.date >= '".$start_date."' ".$camp." GROUP BY a.source");
	// while($row_yes_conn = mysqli_fetch_assoc($yes_conn_sqln)){
	// 	$yes_call_nconn[] = $row_yes_conn['conn_yesterday_calls'];
	// }

	// AGENT LOGIN TIME (PAUSE,WAIT,BREAK)
	$agent_login = mysqli_query($conn, "SELECT SUM(a.login_time) as login_time,SUM(a.pause_time) as pause_time, SUM(a.break_time) as break_time, SUM(a.talk_time) as talk_time, b.first_name, b.last_name FROM user_callsummary a LEFT JOIN vtiger_users b ON a.userid = b.user_name  WHERE DATE(`current_eventtime`) <= '".$end_date."' AND DATE(`current_eventtime`) >= '".$start_date."' ".$camp_logged." GROUP BY a.userid ");
	while($row_login = mysqli_fetch_assoc($agent_login)){
		$login_name[] = $row_login['first_name'].' '.$row_login['last_name'];
		if($row_login['pause_time'] < 0){
			$pause_time[] = 0;
		}else{

		$pause_time[] = floor($row_login['pause_time']/60);
		$pause_time_tbl = floor($row_login['pause_time']);
		$hours   = floor(($pause_time_tbl - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
		$minuts  = floor(($pause_time_tbl - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
		$seconds = floor(($pause_time_tbl - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minuts * 60));
		$pause_time_hour[] = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);
		}
		if($row_login['break_time'] < 0){
			$break_time[] = 0;
		}else{
			$break_time[] = floor($row_login['break_time']/60);

		$break_time_tbl = floor($row_login['break_time']);
		$hours   = floor(($break_time_tbl - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
		$minuts  = floor(($break_time_tbl - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
		$seconds = floor(($break_time_tbl - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minuts * 60));
		$break_time_hour[] = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);
		}
		if($row_login['talk_time'] < 0){
			$talk_time[] = 0;
		}else{
			$talk_time[] = floor($row_login['talk_time']/60);

		$talk_time_tbl = floor($row_login['talk_time']);
		$hours   = floor(($talk_time_tbl - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
		$minuts  = floor(($talk_time_tbl - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
		$seconds = floor(($talk_time_tbl - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minuts * 60));
		$talk_time_hour[] = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);
		}
		if($row_login['login_time'] < 0){
			$login_time[] = 0;
		}else{

		$login_time = floor($row_login['login_time']);
		$hours   = floor(($login_time - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
		$minuts  = floor(($login_time - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
		$seconds = floor(($login_time - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minuts * 60));
		$login_time_hour[] = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);

		}
	}

	// AGENT ATTENDANCE
	$agent_atten = mysqli_query($conn, "SELECT COUNT(*) as cnt_atten, DATE(current_eventtime) as date FROM user_callsummary WHERE DATE(`current_eventtime`) <= '".$end_date."' AND DATE(`current_eventtime`) >= '".$start_date."' ".$camp_logged." GROUP BY DATE(`current_eventtime`) ");
	while($row_atten = mysqli_fetch_assoc($agent_atten)){
		$atten_cnt[] = $row_atten['cnt_atten'];
		$atten_date[] = date('d-m-Y', strtotime($row_atten['date']));
	}

	// AGENT CALLS
	$agent_calls = mysqli_query($conn, "SELECT COUNT(*) as call_cnt, date FROM vtiger_calllogs WHERE date <= '".$end_date."' AND date >= '".$start_date."' ".$camp." GROUP BY date ");
	while($row_calls = mysqli_fetch_assoc($agent_calls)){
		$call_date[] = date('d-m-Y', strtotime($row_calls['date']));
		$call_cnt[] = $row_calls['call_cnt'];
	}

$test = "SELECT COUNT(*) as call_cnt, date FROM vtiger_calllogs WHERE date <= '".$end_date."' AND date >= '".$start_date."' ".$camp." GROUP BY date";
$widget_contents = array(
		"dial"=>$dial,
		"connect"=>$connect,
		"notconnect"=>$notconnect,
		"yes_user"=>$yes_user ,
		"yes_totconn_call"=>$yes_total ,
		"yes_conn_call"=>$yes_call_conn ,
		"yes_nconn_call"=>$yes_call_nconn, 
		"login_name"=>$login_name, 
		"pause_time"=> $pause_time, 
		"break_time"=>$break_time, 
		"talk_time"=>$talk_time,
		"atten_cnt"=>$atten_cnt, 
		"atten_date"=>$atten_date,
		"call_date"=>$call_date,  
		"call_cnt"=>$call_cnt,    
		"start_dates"=>$start_dates,
		"end_dates"=>$end_dates,
		"pause_time_hour"=>$pause_time_hour,
		"login_time_hour"=> $login_time_hour,
		"break_time_hour"=> $break_time_hour,
		"talk_time_hour"=> $talk_time_hour
		);
echo json_encode($widget_contents);	
?>
