<?php /* Smarty version Smarty-3.1.7, created on 2022-03-25 14:09:19
         compiled from "/var/www/xdial.astcrm.com/html/includes/runtime/../../layouts/v7/modules/RealTime/ListViewContents.tpl" */ ?>
<?php /*%%SmartyHeaderCode:776881921623d7fb7b63fb9-08973361%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3b117caa9aeb83b501b7d4b00cfadffb7f18067b' => 
    array (
      0 => '/var/www/xdial.astcrm.com/html/includes/runtime/../../layouts/v7/modules/RealTime/ListViewContents.tpl',
      1 => 1646199973,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '776881921623d7fb7b63fb9-08973361',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_623d7fb7b7144',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_623d7fb7b7144')) {function content_623d7fb7b7144($_smarty_tpl) {?>

    <!-- Optional JavaScript -->

  <style type="text/css">
   .badge{
    width: 100%;
    font-size: 16px;
    padding: 5px;
    margin-top: 2px;
    border-radius: 2px;
   }
   /*refresh switch starts*/
   .switch-field {
      display: flex;
      margin-bottom: 36px;
      overflow: hidden;
    }

    .switch-field input {
      position: absolute !important;
      clip: rect(0, 0, 0, 0);
      height: 1px;
      width: 1px;
      border: 0;
      overflow: hidden;
    }

    .switch-field label {
      background-color: #ffffff;
      color: rgba(0, 0, 0, 0.6);
      font-size: 14px;
      line-height: 1;
      text-align: center;
      padding: 8px 16px;
      margin-right: -1px;
      border: 1px solid rgba(0, 0, 0, 0.2);
      transition: all 0.1s ease-in-out;
    }

    .switch-field label:hover {
      cursor: pointer;
    }

    .switch-field input:checked + label {
      background-color: #35aa47;
      box-shadow: none;
      color : #ffffff;
    }

    .switch-field label:first-of-type {
      border-radius: 4px 0 0 4px;
    }

    .switch-field label:last-of-type {
      border-radius: 0 4px 4px 0;
    }

    p {
      font-size: 16px;
      margin-bottom: 8px;
    }
    /*refresh switch ends*/

    /*countdown div starts*/

    #countdown {
      position: relative;
      /*margin: auto;*/
      margin-bottom: 15px;
      height: 40px;
      width: 40px;
      text-align: center;
    }

    #countdown-number {
      color: #17a2b8;
      display: inline-block;
      line-height: 40px;
      font-size: 18px;
    }

    svg {
      position: absolute;
      top: 0;
      right: 0;
      width: 40px;
      height: 40px;
      transform: rotateY(-180deg) rotateZ(-90deg);
    }

/*    svg circle {
      stroke-dasharray: 113px;
      stroke-dashoffset: 0px;
      stroke-linecap: round;
      stroke-width: 4px;
      stroke: white;
      fill: none;
      stroke:#17a2b8;
      animation: countdown 10s linear infinite forwards;
    }*/

    @keyframes countdown {
      from {
        stroke-dashoffset: 0px;
      }
      to {
        stroke-dashoffset: 113px;
      }
    }
    /*countdown div ends*/

    </style>
      <div class="row" style="padding: 30px; margin-left: 2%;">
        <div class="col-md-3" >
          <div class="form-group">
            <div id="countdown">
              <div id="countdown-number"></div>
              <svg id="disp_timer">
                <circle r="18" cx="20" cy="20" ></circle>
              </svg>
            </div>
            <label><b>SELECT CAMPAIGN</b></label>
            <select id="campaign" name="camapign"  style="width: 70%; padding:5px 20px; display: inline-block; border: 1px solid #ccc; border-radius: 4px; box-sizing: border-box; background-color: #ffffff;">
     
            </select>
          </div>
        </div>
        <div class="col-md-1">
  
        </div>
        <div class="col-md-4">
            <div class="card">
              <div class="card-header" style="background-color: #E5E8E8; padding: 10px;">
                <b>LIVE STATUS</b>
              </div>
              <ul class="list-group list-group-flush">
                <li class="list-group-item">
                  <span class="badge badge-primary" style="background-color: #007bff">Total : <span id="total_live"></span></span>
                </li>
                <li class="list-group-item">
                  <span class="badge badge-success" style="background-color: #28a745">Talk : <span id="talk_live"></span></span>
                </li>
                <li class="list-group-item">
                  <span class="badge badge-danger" style="background-color: #dc3545">Pause : <span id="pause_live"></span></span>
                </li>
                <li class="list-group-item">
                  <span class="badge badge-secondary" style="background-color: #ffc107">Break : <span id="break_live"></span></span>
                </li>
                <li class="list-group-item">
                  <span class="badge badge-info badge" style="background-color: #17a2b8">Logout : <span id="logout_live"></span></span>
                </li>
              </ul>
            </div>
        </div>
        <div class="col-md-4">
          <div class="card" style="border: solid 1px #f7f7f7;">
            <div class="card-header" style="background-color: #E5E8E8; padding: 10px;">
              <b>LEAD STATUS</b>
            </div>
              <ul class="list-group list-group-flush">
                <li class="list-group-item">
                  <span class="badge badge-primary" style="background-color: #007bff">Total Leads : <span id="total_leads"></span></span>
                </li>
                <li class="list-group-item">
                  <span class="badge badge-success" style="background-color: #28a745">Assigned : <span id="assign"></span></span>
                </li>
                <li class="list-group-item">
                  <span class="badge badge-danger" style="background-color: #dc3545">Unassigned : <span id="notassign"></span></span>
                </li>
                <li class="list-group-item">
                  <span class="badge badge-secondary" style="background-color: #ffc107">Dialed : <span id="dial_leads"></span></span>
                </li>
                <li class="list-group-item">
                  <span class="badge badge-info badge" style="background-color: #17a2b8">Manual Dialed : <span id="manualdial_cont"></span></span>
                </li>
                <li class="list-group-item">
                  <span class="badge badge-warning badge" style="background-color: #6c757d">Not Dialed : <span id="notdial"></span></span>
                </li>
              </ul>
          </div>
        </div>
      </div>

        <div class="card" style="margin-left: 4%; margin-right: 10px; ">
              <div class="card-header" style="background-color: #E5E8E8; padding: 10px;">
                <b>AGENT STATUS</b>
              </div>
              <br>
              <table id="myTable" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%"></table>
        </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
        <!-- javascript here -->
    <script type="text/javascript">

      $(document).ready(function () {
        var i, assign, table_row;
        var countdown_time = 10;
        var in_seconds = countdown_time+'000';

        document.getElementById('disp_timer').innerHTML = '<circle r="18" cx="20" cy="20" style="stroke-dasharray: 113px; stroke-dashoffset: 0px; stroke-linecap: round; stroke-width: 4px; stroke: white; fill: none; stroke:#17a2b8; animation: countdown '+countdown_time+'s linear infinite forwards;"></circle>';

         $("#campaign").change(function (event) {
          var camp_selected = $(this).val();
          // filter load
            $.ajax(
            {
                type: "POST",
                url: "fetch_realtime.php",
                dataType:'json',
                data: {
                  'camp_selected': camp_selected,
                },
                success: function(response)
                {
                   document.getElementById('total_live').innerHTML = response.total;
                   document.getElementById('talk_live').innerHTML = response.dial;
                   document.getElementById('pause_live').innerHTML = response.pause;
                   document.getElementById('break_live').innerHTML = response.break;
                   document.getElementById('logout_live').innerHTML = response.logout;

                   document.getElementById('total_leads').innerHTML = response.total_leads;
                   document.getElementById('assign').innerHTML = response.assign;
                   document.getElementById('notassign').innerHTML = response.notassign;
                   document.getElementById('dial_leads').innerHTML = response.dial_leads;
                   document.getElementById('notdial').innerHTML = response.notdial;
                   document.getElementById('manualdial_cont').innerHTML = response.manualdial_cont;


                // table data starts
                   table_row ='<thead style="background-color: #2471A3; color: #ffffff;"> <tr> <th class="th-sm">Name</th> <th class="th-sm">Campaign</th> <th class="th-sm">Connected Calls</th> <th class="th-sm">Not Connected Calls</th> <th class="th-sm">Current Event</th> <th class="th-sm">Current Event Duration</th> <th class="th-sm">Info</th> <th class="th-sm">Login Time</th> <th class="th-sm"><span class="badge badge-success" style="width: 30%; margin-right: 2px; background-color: #28a745; padding: 1px;">Talk</span><span class="badge badge-danger" style="width: 30%; margin-right: 2px; background-color: #dc3545; padding: 1px;">Pause</span><span class="badge badge-secondary" style="width: 30%; margin-right: 2px; background-color: #ffc107; padding: 1px;">Break</span></th> </tr> </thead><tbody>';
                    for(i in response.username){
                    var td_data, evt_time, status_table;

                    status_table = "<table class='table table-striped table-bordered'><tr  style='width:"+response.total_percentage[i]+"%'>";
                    if(response.talk_percentage[i]>0){
                      status_table +="<td style='width:"+response.talk_percentage[i]+"%;background-color: #28a745;' title="+response.show_talk_time[i]+"></td>";
                    }

                    if(response.pause_time_per[i]>0){
                      status_table +="<td style='width:"+response.pause_time_per[i]+"%;background-color: #dc3545;' title="+response.show_pause_time[i]+"></td>"; 
                    }

                    if(response.break_time_per[i]>0){
                      status_table +="<td style='width:"+response.break_time_per[i]+"%;background-color: #ffc107;' title="+response.show_break_time[i]+"></td>";
                    }
                    status_table +="</tr></table>";

                      // dispo check
                      if(response.dispo_status[i] == 'PAUSE'){
                        td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545;">PAUSE</span></td>';
                      }else if(response.dispo_status[i] == 'DIAL'){
                        td_data = '<td><span class="badge badge-success" style="background-color: #28a745">INCALL</span></td>';
                      }else if(response.dispo_status[i] == 'LOGOUT'){
                        td_data = '<td><span class="badge badge-info badge" style="background-color: #17a2b8">LOGOUT</span></td>';
                      }else if(response.dispo_status[i] == 'LOGIN'){
                        td_data = '<td><span class="badge badge-primary" style="background-color: #007bff;">LOGIN</span></td>';
                      }else if(response.dispo_status[i] == 'BREAK'){
                        td_data = '<td><span class="badge badge-secondary" style="background-color: #ffc107">BREAK</span></td>';
                      }else if(response.dispo_status[i] == 'userdispo'){
                        td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545">PAUSE</span></td>';
                      }else{
                        td_data = '<td><span class="badge badge-primary" style="background-color: #007bff">'+response.dispo_status[i]+'</span></td>';
                      }
                      // check event time greater than 1 hour
                      if(response.event_check[i] > response.diff_check[i]){
                        evt_time = '<td style="background-color:red;">'+response.event_time[i]+'</td>';
                      }else{
                        evt_time = '<td>'+response.event_time[i]+'</td>';
                      }

                      table_row +='<tr><td style="background-color:'+response.column_background[i]+'">'+response.username[i]+'</td><td>'+response.campaign_name[i]+'</td><td>'+response.successful_calls[i]+'</td><td>'+response.failed_calls[i]+'</td>'+td_data+evt_time+'<td>'+response.info[i]+'</td><td>'+response.login_total[i]+'</td><td>'+status_table+'</td></tr>';
                    }
                   // table data ends
                   table_row +='</tbody><tfoot> <tr> <th></th> <th></th> <th>Total: '+response.success_total_call+'</th> <th>Total: '+response.unsuccess_total_call+'</th> <th></th> <th></th> <th></th> <th></th> <th></th> </tr> </tfoot>';

                    document.getElementById('myTable').innerHTML = table_row;
                }
            });
          });
   
        function refresh_div(){
        var e = document.getElementById("campaign");
        var sel_value = e.options[e.selectedIndex].value;
        
          // refresh load
          $.ajax(
          {
              type: "POST",
              url: "fetch_realtime.php",
              dataType:'json',
              data: {
                'camp_selected': sel_value,
              },
              success: function(response)
              {
                 document.getElementById('total_live').innerHTML = response.total;
                 document.getElementById('talk_live').innerHTML = response.dial;
                 document.getElementById('pause_live').innerHTML = response.pause;
                 document.getElementById('break_live').innerHTML = response.break;
                 document.getElementById('logout_live').innerHTML = response.logout;

                 document.getElementById('total_leads').innerHTML = response.total_leads;
                 document.getElementById('assign').innerHTML = response.assign;
                 document.getElementById('notassign').innerHTML = response.notassign;
                 document.getElementById('dial_leads').innerHTML = response.dial_leads;
                 document.getElementById('notdial').innerHTML = response.notdial;
                 document.getElementById('manualdial_cont').innerHTML = response.manualdial_cont;


              // table data starts
                 table_row ='<thead style="background-color: #2471A3; color: #ffffff;"> <tr> <th class="th-sm">Name</th> <th class="th-sm">Campaign</th> <th class="th-sm">Connected Calls</th> <th class="th-sm">Not Connected Calls</th> <th class="th-sm">Current Event</th> <th class="th-sm">Current Event Duration</th> <th class="th-sm">Info</th> <th class="th-sm">Login Time</th> <th class="th-sm"><span class="badge badge-success" style="width: 30%; margin-right: 2px; background-color: #28a745; padding: 1px;">Talk</span><span class="badge badge-danger" style="width: 30%; margin-right: 2px; background-color: #dc3545; padding: 1px;">Pause</span><span class="badge badge-secondary" style="width: 30%; margin-right: 2px; background-color: #ffc107; padding: 1px;">Break</span></th> </tr> </thead><tbody>';
                  for(i in response.username){
                    var td_data, evt_time, status_table;

                    status_table = "<table class='table table-striped table-bordered'><tr  style='width:"+response.total_percentage[i]+"%'>";
                    if(response.talk_percentage[i]>0){
                      status_table +="<td style='width:"+response.talk_percentage[i]+"%;background-color: #28a745;' title="+response.show_talk_time[i]+"></td>";
                    }

                    if(response.pause_time_per[i]>0){
                      status_table +="<td style='width:"+response.pause_time_per[i]+"%;background-color: #dc3545;' title="+response.show_pause_time[i]+"></td>"; 
                    }

                    if(response.break_time_per[i]>0){
                      status_table +="<td style='width:"+response.break_time_per[i]+"%;background-color: #ffc107;' title="+response.show_break_time[i]+"></td>";
                    }
                    status_table +="</tr></table>";

                    // dispo check
                    if(response.dispo_status[i] == 'PAUSE'){
                      td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545;">PAUSE</span></td>';
                    }else if(response.dispo_status[i] == 'DIAL'){
                      td_data = '<td><span class="badge badge-success" style="background-color: #28a745">INCALL</span></td>';
                    }else if(response.dispo_status[i] == 'LOGOUT'){
                      td_data = '<td><span class="badge badge-info badge" style="background-color: #17a2b8">LOGOUT</span></td>';
                    }else if(response.dispo_status[i] == 'LOGIN'){
                      td_data = '<td><span class="badge badge-primary" style="background-color: #007bff;">LOGIN</span></td>';
                    }else if(response.dispo_status[i] == 'BREAK'){
                      td_data = '<td><span class="badge badge-secondary" style="background-color: #ffc107">BREAK</span></td>';
                    }else if(response.dispo_status[i] == 'userdispo'){
                      td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545">PAUSE</span></td>';
                    }else{
                      td_data = '<td><span class="badge badge-primary" style="background-color: #007bff">'+response.dispo_status[i]+'</span></td>';
                    }
                    // check event time greater than 1 hour
                    if(response.event_check[i] > response.diff_check[i]){
                      evt_time = '<td style="background-color:red;">'+response.event_time[i]+'</td>';
                    }else{
                      evt_time = '<td>'+response.event_time[i]+'</td>';
                    }

                    table_row +='<tr><td style="background-color:'+response.column_background[i]+'">'+response.username[i]+'</td><td>'+response.campaign_name[i]+'</td><td>'+response.successful_calls[i]+'</td><td>'+response.failed_calls[i]+'</td>'+td_data+evt_time+'<td>'+response.info[i]+'</td><td>'+response.login_total[i]+'</td><td>'+status_table+'</td></tr>';
                  }
                 // table data ends
                  table_row +='</tbody><tfoot> <tr> <th></th> <th></th> <th>Total: '+response.success_total_call+'</th> <th>Total: '+response.unsuccess_total_call+'</th> <th></th> <th></th> <th></th> <th></th> <th></th> </tr> </tfoot>';

                 document.getElementById('myTable').innerHTML = table_row;

              }
          });
        }
        t = setInterval(refresh_div,in_seconds);

        // default load
          $.ajax(
          {
              type: "POST",
              url: "fetch_realtime.php",
              dataType:'json',
              data: {
                'postdata': 1,
              },
              success: function(response)
              {
                console.log(response.test);
                assign += '<option value="ALL">All</option>';

                for(i in response.campaigns){

                  assign += "<option value='"+response.campaigns[i]+"'>"+response.campaigns[i]+"</option>";
                }
                 document.getElementById('campaign').innerHTML = assign;

                 document.getElementById('total_live').innerHTML = response.total;
                 document.getElementById('talk_live').innerHTML = response.dial;
                 document.getElementById('pause_live').innerHTML = response.pause;
                 document.getElementById('break_live').innerHTML = response.break;
                 document.getElementById('logout_live').innerHTML = response.logout;

                 document.getElementById('total_leads').innerHTML = response.total_leads;
                 document.getElementById('assign').innerHTML = response.assign;
                 document.getElementById('notassign').innerHTML = response.notassign;
                 document.getElementById('dial_leads').innerHTML = response.dial_leads;
                 document.getElementById('notdial').innerHTML = response.notdial;
                 document.getElementById('manualdial_cont').innerHTML = response.manualdial_cont;

                 
                 // table data starts
                  table_row ='<thead style="background-color: #2471A3; color: #ffffff;"> <tr> <th class="th-sm">Name</th> <th class="th-sm">Campaign</th> <th class="th-sm">Connected Calls</th> <th class="th-sm">Not Connected Calls</th> <th class="th-sm">Current Event</th> <th class="th-sm">Current Event Duration</th> <th class="th-sm">Info</th> <th class="th-sm">Login Time</th> <th class="th-sm"><span class="badge badge-success" style="width: 30%; margin-right: 2px; background-color: #28a745; padding: 1px;">Talk</span><span class="badge badge-danger" style="width: 30%; margin-right: 2px; background-color: #dc3545; padding: 1px;">Pause</span><span class="badge badge-secondary" style="width: 30%; margin-right: 2px; background-color: #ffc107; padding: 1px;">Break</span></th> </tr> </thead><tbody>';

                  for(i in response.username){
                    var td_data, evt_time, status_table;

                    status_table = "<table class='table table-striped table-bordered'><tr  style='width:"+response.total_percentage[i]+"%'>";
                    if(response.talk_percentage[i]>0){
                      status_table +="<td style='width:"+response.talk_percentage[i]+"%;background-color: #28a745;' title="+response.show_talk_time[i]+"></td>";
                    }

                    if(response.pause_time_per[i]>0){
                      status_table +="<td style='width:"+response.pause_time_per[i]+"%;background-color: #dc3545;' title="+response.show_pause_time[i]+"></td>"; 
                    }

                    if(response.break_time_per[i]>0){
                      status_table +="<td style='width:"+response.break_time_per[i]+"%;background-color: #ffc107;' title="+response.show_break_time[i]+"></td>";
                    }
                    status_table +="</tr></table>";

                    // dispo check
                    if(response.dispo_status[i] == 'PAUSE'){
                      td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545;">PAUSE</span></td>';
                    }else if(response.dispo_status[i] == 'DIAL'){
                      td_data = '<td><span class="badge badge-success" style="background-color: #28a745">INCALL</span></td>';
                    }else if(response.dispo_status[i] == 'LOGOUT'){
                      td_data = '<td><span class="badge badge-info badge" style="background-color: #17a2b8">LOGOUT</span></td>';
                    }else if(response.dispo_status[i] == 'LOGIN'){
                      td_data = '<td><span class="badge badge-primary" style="background-color: #007bff;">LOGIN</span></td>';
                    }else if(response.dispo_status[i] == 'BREAK'){
                      td_data = '<td><span class="badge badge-secondary" style="background-color: #ffc107">BREAK</span></td>';
                    }else if(response.dispo_status[i] == 'userdispo'){
                      td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545">PAUSE</span></td>';
                    }else{
                      td_data = '<td><span class="badge badge-primary" style="background-color: #007bff">'+response.dispo_status[i]+'</span></td>';
                    }
                    // check event time greater than 1 hour
                    if(response.event_check[i] > response.diff_check[i]){
                      evt_time = '<td style="background-color:red;">'+response.event_time[i]+'</td>';
                    }else{
                      evt_time = '<td>'+response.event_time[i]+'</td>';
                    }

                    table_row +='<tr><td style="background-color:'+response.column_background[i]+'">'+response.username[i]+'</td><td>'+response.campaign_name[i]+'</td><td>'+response.successful_calls[i]+'</td><td>'+response.failed_calls[i]+'</td>'+td_data+evt_time+'<td>'+response.info[i]+'</td><td>'+response.login_total[i]+'</td><td>'+status_table+'</td></tr>';
                  }
                 // table data ends
                 table_row +='</tbody><tfoot> <tr> <th></th> <th></th> <th>Total: '+response.success_total_call+'</th> <th>Total: '+response.unsuccess_total_call+'</th> <th></th> <th></th> <th></th> <th></th> <th></th> </tr> </tfoot>';
                 document.getElementById('myTable').innerHTML = table_row;
              }
          });

          // countdown
            var countdownNumberEl = document.getElementById('countdown-number');
            var countdown = countdown_time;

            countdownNumberEl.textContent = countdown;

            setInterval(function() {
              countdown = --countdown <= 0 ? 10 : countdown;

              countdownNumberEl.textContent = countdown;
            }, 1000);
            // 

      });
    </script>
<?php }} ?>