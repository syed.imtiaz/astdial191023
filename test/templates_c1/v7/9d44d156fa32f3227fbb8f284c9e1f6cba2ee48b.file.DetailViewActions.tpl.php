<?php /* Smarty version Smarty-3.1.7, created on 2022-03-25 14:09:27
         compiled from "/var/www/xdial.astcrm.com/html/includes/runtime/../../layouts/v7/modules/Vtiger/DetailViewActions.tpl" */ ?>
<?php /*%%SmartyHeaderCode:317083131623d7fbf7a3c98-15318220%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9d44d156fa32f3227fbb8f284c9e1f6cba2ee48b' => 
    array (
      0 => '/var/www/xdial.astcrm.com/html/includes/runtime/../../layouts/v7/modules/Vtiger/DetailViewActions.tpl',
      1 => 1646199973,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '317083131623d7fbf7a3c98-15318220',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'RECORD' => 0,
    'MODULE' => 0,
    'MODULE_MODEL' => 0,
    'STARRED' => 0,
    'DETAILVIEW_LINKS' => 0,
    'MODULE_NAME' => 0,
    'DETAIL_VIEW_BASIC_LINK' => 0,
    'SELECTED_MENU_CATEGORY' => 0,
    'DETAIL_VIEW_LINK' => 0,
    'NO_PAGINATION' => 0,
    'PREVIOUS_RECORD_URL' => 0,
    'NEXT_RECORD_URL' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_623d7fbf7c7bd',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_623d7fbf7c7bd')) {function content_623d7fbf7c7bd($_smarty_tpl) {?>
<input id="recordId" type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['RECORD']->value->getId();?>
" /><div class="col-lg-6 detailViewButtoncontainer"><div class="pull-right btn-toolbar"><!--astcrm recycle and tranfer the leads based on dispostion or campaign --><?php if ($_smarty_tpl->tpl_vars['MODULE']->value=='Campaigns'){?><!-- modal recycle starts--><form method="POST" id="post_recycle"><div id="recycle_modal" class="modal fade"><div class="modal-dialog" ><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title">Campaign From: <span id="c_name" style="color: #ffffff; font-style: italic;"></span></h4></div><div class="modal-body"><div class="row" style="margin-left: 10px;"><div class="col-md-3"><label><b>Disposition</b></label><select class="form-control" id="dispo"></select></div></div><br><h5 style="color: #04B431; text-align: center;" id="recycle_msg">Please wait...</h5></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal">Close</button><button type="button" class="btn btn-success" style="margin-right: 5px;" id="sub_recycle">Apply</button></div></div></div></div></form><!-- modal recycle ends --><!-- modal transfer starts--><form method="POST" id="post_transfer"><div id="transfer_modal" class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title">Campaign From: <span id="c_name1" style="color: #ffffff; font-style: italic;"></span></h4></div><div class="modal-body"><div class="row" style="margin-left: 10px;"><div class="col-md-3"><label><b>Disposition</b></label><select class="form-control" id="dispo_ext"></select></div><div class="col-md-3"><label><b>Campaign To</b></label><select class="form-control" id="campaign"></select></div></div><br><h5 style="color: #04B431; text-align: center;" id="trans_msg">Transferring Leads <span id="trans_cont"></span><span id="tt_lead"></span></h5><h5 style="text-align: center;" id="resp_msg">Transferred <span id="c_leads" style="color: #1560bd;"></span> Leads from Campaign <span id="from_camp" style="color: #1560bd;"></span><span id="disp_status"></span> to <span id="to_camp" style="color: #1560bd;"></span></h5></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal">Close</button><button type="button" class="btn btn-success" style="margin-right: 5px;" id="sub_transfer">Apply</button></div></div></div></div></form><!-- modal transfer ends --><!-- astcrm recycle and transfer leads --><div class="btn-group"><button class="btn btn-success show-recycle" type="button" id="recycle" style="margin-right: 5px;"><strong>Recycle</strong></button><button class="btn btn-success show-transfer" type="button" id="transfer" style="margin-right: 5px;"><strong>Transfer Leads</strong></button></div><!-- astcrm recycle and transfer leads ends --><?php }?><div class="btn-group"><?php $_smarty_tpl->tpl_vars['STARRED'] = new Smarty_variable($_smarty_tpl->tpl_vars['RECORD']->value->get('starred'), null, 0);?><?php if ($_smarty_tpl->tpl_vars['MODULE_MODEL']->value->isStarredEnabled()){?><button class="btn btn-default markStar <?php if ($_smarty_tpl->tpl_vars['STARRED']->value){?> active <?php }?>" id="starToggle" style="width:100px;"><div class='starredStatus' title="<?php echo vtranslate('LBL_STARRED',$_smarty_tpl->tpl_vars['MODULE']->value);?>
"><div class='unfollowMessage'><i class="fa fa-star-o"></i> &nbsp;<?php echo vtranslate('LBL_UNFOLLOW',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</div><div class='followMessage'><i class="fa fa-star active"></i> &nbsp;<?php echo vtranslate('LBL_FOLLOWING',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</div></div><div class='unstarredStatus' title="<?php echo vtranslate('LBL_NOT_STARRED',$_smarty_tpl->tpl_vars['MODULE']->value);?>
"><?php echo vtranslate('LBL_FOLLOW',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</div></button><?php }?><?php  $_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['DETAILVIEW_LINKS']->value['DETAILVIEWBASIC']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->key => $_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->value){
$_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->_loop = true;
?><button class="btn btn-default" id="<?php echo $_smarty_tpl->tpl_vars['MODULE_NAME']->value;?>
_detailView_basicAction_<?php echo Vtiger_Util_Helper::replaceSpaceWithUnderScores($_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->value->getLabel());?>
"<?php if ($_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->value->isPageLoadLink()){?>onclick="window.location.href = '<?php echo $_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->value->getUrl();?>
&app=<?php echo $_smarty_tpl->tpl_vars['SELECTED_MENU_CATEGORY']->value;?>
'"<?php }else{ ?>onclick="<?php echo $_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->value->getUrl();?>
"<?php }?><?php if ($_smarty_tpl->tpl_vars['MODULE_NAME']->value=='Documents'&&$_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->value->getLabel()=='LBL_VIEW_FILE'){?>data-filelocationtype="<?php echo $_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->value->get('filelocationtype');?>
" data-filename="<?php echo $_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->value->get('filename');?>
"<?php }?>><?php echo vtranslate($_smarty_tpl->tpl_vars['DETAIL_VIEW_BASIC_LINK']->value->getLabel(),$_smarty_tpl->tpl_vars['MODULE_NAME']->value);?>
</button><?php } ?><?php if (count($_smarty_tpl->tpl_vars['DETAILVIEW_LINKS']->value['DETAILVIEW'])>0){?><button class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><?php echo vtranslate('LBL_MORE',$_smarty_tpl->tpl_vars['MODULE_NAME']->value);?>
&nbsp;&nbsp;<i class="caret"></i></button><ul class="dropdown-menu dropdown-menu-right"><?php  $_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['DETAILVIEW_LINKS']->value['DETAILVIEW']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK']->key => $_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK']->value){
$_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK']->_loop = true;
?><?php if ($_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK']->value->getLabel()==''){?><li class="divider"></li><?php }else{ ?><li id="<?php echo $_smarty_tpl->tpl_vars['MODULE_NAME']->value;?>
_detailView_moreAction_<?php echo Vtiger_Util_Helper::replaceSpaceWithUnderScores($_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK']->value->getLabel());?>
"><?php if (strstr($_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK']->value->getUrl(),"javascript")){?><a href='<?php echo $_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK']->value->getUrl();?>
'><?php echo vtranslate($_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK']->value->getLabel(),$_smarty_tpl->tpl_vars['MODULE_NAME']->value);?>
</a><?php }else{ ?><a href='<?php echo $_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK']->value->getUrl();?>
&app=<?php echo $_smarty_tpl->tpl_vars['SELECTED_MENU_CATEGORY']->value;?>
' ><?php echo vtranslate($_smarty_tpl->tpl_vars['DETAIL_VIEW_LINK']->value->getLabel(),$_smarty_tpl->tpl_vars['MODULE_NAME']->value);?>
</a><?php }?></li><?php }?><?php } ?></ul><?php }?></div><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['NO_PAGINATION']->value;?>
<?php $_tmp1=ob_get_clean();?><?php if (!$_tmp1){?><div class="btn-group pull-right"><button class="btn btn-default " id="detailViewPreviousRecordButton" <?php if (empty($_smarty_tpl->tpl_vars['PREVIOUS_RECORD_URL']->value)){?> disabled="disabled" <?php }else{ ?> onclick="window.location.href = '<?php echo $_smarty_tpl->tpl_vars['PREVIOUS_RECORD_URL']->value;?>
&app=<?php echo $_smarty_tpl->tpl_vars['SELECTED_MENU_CATEGORY']->value;?>
'" <?php }?> ><i class="fa fa-chevron-left"></i></button><button class="btn btn-default  " id="detailViewNextRecordButton"<?php if (empty($_smarty_tpl->tpl_vars['NEXT_RECORD_URL']->value)){?> disabled="disabled" <?php }else{ ?> onclick="window.location.href = '<?php echo $_smarty_tpl->tpl_vars['NEXT_RECORD_URL']->value;?>
&app=<?php echo $_smarty_tpl->tpl_vars['SELECTED_MENU_CATEGORY']->value;?>
'" <?php }?>><i class="fa fa-chevron-right"></i></button></div><?php }?></div><input type="hidden" name="record_id" value="<?php echo $_smarty_tpl->tpl_vars['RECORD']->value->getId();?>
"></div>

<script type="text/javascript">
    $(document).ready(function(){

        $('#recycle').click(function(){
            $("#recycle_modal").modal('show');
        });

        $('#transfer').click(function(){
            $("#transfer_modal").modal('show');
        });

        var record_id = $("#recordId").val();
        var loggedin_user = $('#loggedin_user').val();

        $('#trans_msg').hide();
        $('#resp_msg').hide();
        $('#recycle_msg').hide();

        $("#transfer").click(function(){  
            $('#resp_msg').hide();
            $('select option:selected').removeAttr('selected');
        });
        // post recycle
        $("#sub_recycle").click(function(){     

           var sel_main = $('#dispo').find(":selected").text();

           if(sel_main == "Please Select"){
            var sel_mains = "";
           }else if(sel_main == "No disposition"){
            var sel_mains = "NULL";
           }else if(sel_main == "SKIPPED"){
            var sel_mains = "Skipped";
           }
           else{
            var sel_mains = sel_main;
           }
           
           if(sel_mains == ''){
            alert('Please select the disposition');
           }else{

            $.ajax({
                  type: "POST",
                  url: "fetch_recycle_transfer.php",
                  dataType:'json',
                  data: {
                    'post_filter': "post_recycle",
                    'main_selected': sel_mains,
                    'record_id': record_id,
                    'loggedin_user': loggedin_user,
                  },
                  beforeSend : function (){
                    $('#recycle_msg').show();
                  },
                  success: function(response)
                  {
                    $('#recycle_msg').hide();
                    if(response.status == 1){
                        alert(response.msg);    
                        $("#recycle_modal").modal('hide');
                        location.reload(true);
                    }
                  }
              });
           }
             //
         });

        // post transfer
        $("#sub_transfer").click(function(){      

           var sel_camp = $('#campaign').find(":selected").text();
           var sel_main = $('#dispo_ext').find(":selected").text();

           $('#trans_msg').hide();
           $('#resp_msg').hide();

           if(sel_main == "Please Select"){
            var sel_mains = "";
           }else{
            var sel_mains = sel_main;
           }

           if((sel_camp != "Please Select") && (sel_mains != '')  ){

                    $.ajax(
                      {
                      type: "POST",
                      url: "fetch_dropdown_recycle.php",
                      dataType:'json',
                      data: {
                        'post_filter': "post_transfer",
                        'camp_select': sel_camp,
                        'main_selected': sel_mains,
                        'record_id': record_id,
                      },
                      success: function(responses)
                      { 
                        // alert(response.count_leads);
                        if (responses.count_leads>10) {
                            document.getElementById('trans_cont').innerHTML = '1,<span>'+responses.f_value+',</span><span>'+responses.s_value+'...</span>';
                        }
                        
                        document.getElementById('tt_lead').innerHTML = responses.count_leads;

                        $('#trans_msg').show();

                        setTimeout(function(){
                         $.ajax(
                          {
                              type: "POST",
                              url: "fetch_recycle_transfer.php",
                              dataType:'json',
                              data: {
                                'post_filter': "post_transfer",
                                'camp_select': sel_camp,
                                'main_selected': sel_mains,
                                'record_id': record_id,
                              },
                              // beforeSend : function (){
                              //    $('#trans_msg').show();
                              // },
                              success: function(response)
                              {
                                $('select option:selected').removeAttr('selected');
                                $('#trans_msg').hide();

                                document.getElementById('c_leads').innerHTML = response.count_leads;
                                document.getElementById('from_camp').innerHTML = response.from_campaign; 
                                document.getElementById('to_camp').innerHTML = response.to_campaign; 
                                if (response.main_disp != '') {
                                    document.getElementById('disp_status').innerHTML = ' with Disposition <span style="color: #1560bd; ">'+response.main_disp+'</span>'; 
                                }

                                $('#resp_msg').show();
                              }
                          });
                        }, 2000);
                       }
                    });

           }else{
            alert('please select all the fields');
            return false;
           }
           //
         });

        // fetch dropdown values
        var camp, main_dispo, sub_dispo, main_dispo_trans;
        $.ajax(
          {
          type: "POST",
          url: "fetch_dropdown_recycle.php",
          dataType:'json',
          data: {
            'postdata': 1,
            'record_id':record_id,
          },
          success: function(response)
          { 
            // alert(response.campaign_name);
            // CAMPAIGN
            camp += '<option value="ALL">Please Select</option>';
            for(i in response.campaigns){
              camp += "<option value='"+response.campaigns[i]+"'>"+response.campaigns[i]+"</option>";
            }
             document.getElementById('campaign').innerHTML = camp;
             
             // MAIN DISPO
            main_dispo += '<option value="ALL" selected>Please Select</option>';
            for(i in response.main_dispo){
              main_dispo += "<option value='"+response.main_dispo[i]+"'>"+response.main_dispo[i]+"</option>";
            }
            main_dispo += '<option value="SKIPPED" style="">SKIPPED</option>';
             document.getElementById('dispo').innerHTML = main_dispo;
            main_dispo += '<option value="NULL" style="color:red;">No disposition</option>';
             document.getElementById('dispo').innerHTML = main_dispo;

                 // MAIN DISPO
            main_dispo_trans += '<option value="ALL" selected>Please Select</option>';
            for(i in response.main_dispo){
              main_dispo_trans += "<option value='"+response.main_dispo[i]+"'>"+response.main_dispo[i]+"</option>";
            }
             document.getElementById('dispo_ext').innerHTML = main_dispo_trans;

             document.getElementById('c_name').innerHTML = response.campaign_name;
             document.getElementById('c_name1').innerHTML = response.campaign_name;
           }
        });


        });

</script>
<?php }} ?>