<?php /* Smarty version Smarty-3.1.7, created on 2023-09-14 14:38:53
         compiled from "/var/www/xdial.astcrm.com/html/includes/runtime/../../layouts/v7/modules/CampaignPerformance/List.tpl" */ ?>
<?php /*%%SmartyHeaderCode:200052600662bbf405ed58c4-40712404%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5e680613a3909bf4a00316c7ae3274b74db1b819' => 
    array (
      0 => '/var/www/xdial.astcrm.com/html/includes/runtime/../../layouts/v7/modules/CampaignPerformance/List.tpl',
      1 => 1694608795,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '200052600662bbf405ed58c4-40712404',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_62bbf406004d8',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62bbf406004d8')) {function content_62bbf406004d8($_smarty_tpl) {?><!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Campaign Performance</title>

    <link rel="stylesheet" href="analytics.css">
    <style type="text/css">

    .navbar{
      padding: 0px;
    }

    h6{
        text-align: center;
    }

        #col_border{
            margin-top: -15px;
        }

        .order-card {
            color: #fff;
        }

        .bg-c-blue {
            background: linear-gradient(45deg,#a2774a,#a2774a);
        }

        .bg-c-green {
            background: linear-gradient(45deg,#796a8c,#796a8c);
        }

      /* .bg-c-yellow {
            background: linear-gradient(45deg,#FFB64D,#ffcb80);
        }*/

        .bg-c-pink {
            background: linear-gradient(45deg,#9c605a,#9c605a);
        }


        .card {
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2.94px 0.06px rgba(4,26,55,0.16);
            box-shadow: 0 1px 2.94px 0.06px rgba(4,26,55,0.16);
   
            margin-bottom: 30px;
            -webkit-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            margin-right: -20px;
            background-color: #ffffff;
	    
        }

        .card .card-block {
            padding: 0px;
        }

        .order-card i {
            font-size: 10px;
        }

        .f-left {
            float: left;
        }

        .f-right {
            float: right;
        }

      div #scroll { 
          height: 330px; 
          overflow-x: hidden; 
          overflow-y: auto; 
      }
    </style>

</head>

<body>

    <div class="wrapper">
       <!-- Page Content  -->
        <div id="content">
            <div class="card" style="margin-top: -10px; margin-bottom: 24px;">
              <div class="card-body">
                <!-- <h6 style="text-align: left; color: #800080;">LEAD PERFORMANCE</h6> -->
                <div class="row" style="padding: 10px;">
                    <div class="col-md-2">
                    <span></span>
                    <label>Date&nbsp;</label>
                    <input type="text" name="closingdate" id="reportrange" class="form-control listSearchContributor inputElement dateField" data-date-format="yyyy-mm-dd" data-calendar-type="range" value="" data-field-type="date" placeholder="choose date">
                    </div>
                    <div class="col-md-2" >
                        <label>Campaign&nbsp;</label>
                        <select name="camapign" id="campaign" class="form-control">
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-primary btn-sm" id="submit_filter" style="margin-bottom: -54px;">Submit</button>
                    </div>
                </div>
              </div>
            </div>
		<br>
		<br>
            <!-- charts starts here -->
            <div class="row">
              <!-- attendance camp -->
                <div class="col-md-6" id="col_border">	
                    <div class="card" id="hide_campaign" style="border: solid 3px #e2e2e4;">
	            <div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold; font-family:'OpenSans-Regular', sans-serif;">Campaign wise Calls</div>
                        <div class="card-body" id="graph-campaign">			
		         <canvas id="campaign_wise"width="320" height="269"></canvas>
                        </div>
                    </div>
                    <div class="card" id="show_campaign">
                        <div class="card-body" id="graph-campaignPie ">
                            <canvas id="campaign_wise_pie" width="320" height="130" ></canvas>
                        </div>
                    </div>
                </div>
                <!-- login camp -->
                <div class="col-md-6" id="col_border">
                    <div class="card" id="hide_campaign_login" style="border : solid 3px #e2e2e4;">
		    <div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold; font-family:'OpenSans-Regular', sans-serif;">Campaign Login(in minutes)</div>
                        <div class="card-body" id="graph-login_pause">
                            <canvas id="camp_login_pause"width="269" height="270"</canvas>
                        </div>
                    </div>
			<br>
			<br>
                    <div class="card" id="show_campaign_login">
                        <div class="card-body" id="graph-campaignLoginPie">
                            <canvas id="campaign_wise_Loginpie" width="320" height="130" ></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-6" id="col_border">
                  
                    <a id="export_camp" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Export" style="float: left; margin-bottom: 5px;"><i class="fa fa-download " aria-hidden="true"></i>&nbsp;Export</a>
                      <table class="table table-striped" id="table-id">
                        <thead style="background: #505f6b; color: #ffffff;">
                          <tr>
                            <th>Campaign Name</th>
                            <th>Total</th>
                            <th>Connected</th>
                            <th>Not Connected</th>
                          </tr>
                        </thead>
                        <tbody id="camp_active">
                 
                        </tbody>

                        <tfoot id="table_footer" style="background: #505f6b; color: #ffffff;">
                        	
                        </tfoot>
                      </table>
                    
                </div>
                <div class="col-md-6" id="col_border">
                    <div class="card" style="border : solid 3px #e2e2e4;">
		    <div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold; font-family:'OpenSans-Regular', sans-serif;">Pending Calls</div>
                     <br>
		     <br> 
                        <div class="card-body" id="graph-pendingCalls">                                
                            <canvas id="pendingCalls" width="320" height="130"></canvas>
                        </div>
                    </div>
                </div>
		<br>
		<br>
		<div class="col-md-12" id="col_border"></div>
  	  <table width="100%" style="margin-top:5%"><tr><th style="background-color:#E5E8E8;color:black;font-weight:bold;text-align:center;width:100%">Campaign Current Status</th><th style="text-align:right;background-color:#E5E8E8;color:black;font-weigth:bold"><button style="background-color:#f0b64c;color:white;border:0;padding:5px" id="refresh">Refresh</button></th><tr></table>
            <table id="myCampTable" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
		</table>
		</div>
            </div>
        </div>
    </div>

    <script src="Chartjs/Chart.js"></script>
    <script type="text/javascript" src="export_to_excel.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $("#export_camp").click(function(){

              var today = new Date();
              var dd = String(today.getDate()).padStart(2, '0');
              var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
              var yyyy = today.getFullYear();
              today = dd + '_' + mm + '_' + yyyy;

              var html = document.querySelector("#export_camp").outerHTML;
              export_table_to_csv(html, "CampaignReport_"+today+".csv");
            });

          $('#show_campaign').hide();
          $('#show_campaign_login').hide();

          $("#submit_filter").click(function(){  

           // campaign attendance chart
           document.getElementById("campaign_wise").remove(); //canvas
           div = document.querySelector("#graph-campaign"); //canvas parent element
           div.insertAdjacentHTML("afterbegin", "<canvas id='campaign_wise' height='269' ></canvas>"); //adding the canvas again   

           // campaign attendance Piechart
           document.getElementById("campaign_wise_pie").remove(); //canvas
           div = document.querySelector("#graph-campaignPie"); //canvas parent element
           div.insertAdjacentHTML("afterbegin", "<canvas id='campaign_wise_pie' width='320' height='130' ></canvas>"); //adding the canvas again  

           // campaign login/pause chart
           document.getElementById("camp_login_pause").remove(); //canvas
           div = document.querySelector("#graph-login_pause"); //canvas parent element
           div.insertAdjacentHTML("afterbegin", "<canvas id='camp_login_pause' height='269' ></canvas>"); //adding the canvas again   

           // campaign login/pause chart LOGIN
           document.getElementById("campaign_wise_Loginpie").remove(); //canvas
           div = document.querySelector("#graph-campaignLoginPie"); //canvas parent element
           div.insertAdjacentHTML("afterbegin", "<canvas id='campaign_wise_Loginpie' width='320' height='130' ></canvas>"); //adding the canvas again  

            // campaign pending calls
           document.getElementById("pendingCalls").remove(); //canvas
           div = document.querySelector("#graph-pendingCalls"); //canvas parent element
           div.insertAdjacentHTML("afterbegin", "<canvas id='pendingCalls' width='320' height='130' ></canvas>"); //adding the canvas again 

         //  document.getElementById('table_footer').remove();

                var full_dates = $('#reportrange').val();
                var array = full_dates.split(",");
                var sdate = array[0];
                var edate = array[1];

                var sel_camp = $('#campaign').find(":selected").text();
                if(sel_camp == "ALL"){
                    var camp_sel = "";
                     $('#hide_campaign').show();
                     $('#hide_campaign_login').show();
                     $('#show_campaign').hide();
                     $('#show_campaign_login').hide();
                     $('#table_footer').show();
                }else{
                    var camp_sel = sel_camp;
                     $('#table_footer').hide();
                }

              if(full_dates != ''){
                // fetching graph data
                $.ajax(
                {
                    type: "POST",
                    url: "campaign_performance_widget.php",
                    dataType:'json',
                    data: {
                      'post_data': 1,
                      'sdate': sdate,
                      'edate': edate,
                      'camp_sel': camp_sel,
                    },
                    success: function(response)
                    {
                      var startDate = sdate;
                      var endDate = edate;

                      var tbody = '';

                      for(var i in response.act_camp_name){

                         tbody += '<tr><td>'+response.act_camp_name[i]+'</td><td>'+response.act_camp_total[i]+'</td><td>'+response.act_camp_total_dialed[i]+'</td><td>'+response.act_camp_total_notdialed[i]+'</td></tr>';
                      }

                      $('#camp_active').html(tbody);

		ktable_row = '<thead style="background-color: #505f6b; color: #ffffff;"> <tr> <th class="th-sm">Campaign Name</th> <th class="th-sm">Total</th> <th class="th-sm">Dialed</th> <th class="th-sm">In Queue</th></tr> </thead><tbody>';
              for (var j in response.campaignDetails) {

                var splitdetails=(response.campaignDetails[j]).split('@_@');

                ktable_row += '<tr><td>a href="index.php?module=Campaigns&view=Detail&record='+ splitdetails[4] +'&app=MARKETING" target="_blank" style="color:#80bfff;cursor:pointer">' + splitdetails[0] + '</a></td><td>' + splitdetails[1] + '</td><td>' + splitdetails[2] + '</td><td>' + splitdetails[3] + '</td></tr>';
              }
              // table data ends
              ktable_row += '</tbody></table>';
              document.getElementById('myCampTable').innerHTML = ktable_row;



                     // campaign calls
                      var camp_connect = response.connect;
                      var camp_notconnect = response.notconnect;
                      var camp_name = response.campaigns;
                      camp_conn_notconn(camp_connect, camp_notconnect, camp_name, startDate, endDate);

                      // campaign login
                      var name_camp = response.campaign_logged_in;
                      var login_camp = response.login_time;
                      var pause_camp = response.pause_time;
                      campaign_login_time(name_camp, login_camp, pause_camp);

                      // campaign pending
                      var pending_camp = response.pend_name;
                      var pending_camp_tot = response.pend_total;
                      pendingCamp(pending_camp, pending_camp_tot);

                          if(sel_camp == "ALL"){
                              // var camp_sel = "";
                          }else{
                              $('#show_campaign').show();
                              $('#show_campaign_login').show();
                              $('#hide_campaign').hide();
                              $('#hide_campaign_login').hide();

                              camp_conn_notconnPie(camp_connect, camp_notconnect, camp_name);
                              camp_LoginPie(name_camp, login_camp, pause_camp);
                          }

                    }
                });

              }else{
                alert('Please select the date');
                location.reload();
              }
            
            });


					//refresh button
            $("#refresh").click(function(){  
              $.ajax(
            {
              type: "POST",
                url: "campaign_performance_widget.php",
                dataType:'json',
                data: {
                  'post_data': 0,
                },
                success: function(response)
                {

                  ktable_row = '<thead style="background-color: #505f6b; color: #ffffff;"> <tr> <th class="th-sm">Campaign Name</th> <th class="th-sm">Total</th> <th class="th-sm">Dialed</th> <th class="th-sm">In Queue</th></tr> </thead><tbody>';
                  for (var j in response.campaignDetails) {

                  var splitdetails=(response.campaignDetails[j]).split('@_@');

                  ktable_row += '<tr><td><a href="index.php?module=Campaigns&view=Detail&record='+ splitdetails[4] +'&app=MARKETING" target="_blank" style="color:#80bfff;cursor:pointer">' + splitdetails[0] + '</a></td><td>' + splitdetails[1] + '</td><td>' + splitdetails[2] + '</td><td>' + splitdetails[3] + '</td></tr>';
                  }
                  // table data ends
                  ktable_row += '</tbody></table>';
                  document.getElementById('myCampTable').innerHTML = ktable_row;

                }
              });
            });


            // without filter
            $.ajax(
            {
                type: "POST",
                url: "campaign_performance_widget.php",
                dataType:'json',
                data: {
                  'post_data': 0,
                },
                success: function(response)
                {

                  $('#reportrange').val(response.start_dates+','+response.end_dates);
                  var startDate = response.start_dates;
                  var endDate = response.end_dates;

                  var tbody = '';
                  var row_cnt = 0;

                  for(var i in response.act_camp_name){

                        if(response.act_camp_total_dialed[i] === undefined || response.act_camp_total_dialed[i] === null){
                      var total_camp_connected = 0;
                    }else{
                      var total_camp_connected = response.act_camp_total_dialed[i]
                    }

                    if(response.act_camp_total_notdialed[i] === undefined || response.act_camp_total_notdialed[i] === null){
                      var total_camp_not_connected = 0;
                    }else{
                      var total_camp_not_connected = response.act_camp_total_notdialed[i]
                    }

                      tbody += '<tr><td>'+response.act_camp_name[i]+'</td><td>'+response.act_camp_total[i]+'</td><td>'+total_camp_connected+'</td><td>'+total_camp_not_connected+'</td></tr>';
                     row_cnt++;
                  }

                    //footer sum
                  var total_sum_calls = response.total_sum_calls;
                  var total_sum_conn = response.total_sum_conn;
                  var total_sum_not_conneted = response.total_sum_not_conneted;

                  var tfoot = '';
                  tfoot += '<tr><td>Total</td><td>'+total_sum_calls+'</td><td>'+total_sum_conn+'</td><td>'+total_sum_not_conneted+'</td></tr>';

                    $('#table_footer').html(tfoot);

                  //end footer count

                  if(row_cnt != 0){
                    
                  }

                  $('#camp_active').html(tbody);
		
		ktable_row = '<thead style="background-color: #505f6b; color: #ffffff;"> <tr> <th class="th-sm">Campaign Name</th> <th class="th-sm">Total</th> <th class="th-sm">Dialed</th> <th class="th-sm">In Queue</th></tr> </thead><tbody>';
              for (var j in response.campaignDetails) {

                var splitdetails=(response.campaignDetails[j]).split('@_@');

                ktable_row += '<tr><td><a href="index.php?module=Campaigns&view=Detail&record='+ splitdetails[4] +'&app=MARKETING" target="_blank" style="color:#80bfff;cursor:pointer">' + splitdetails[0] + '</a></td><td>' + splitdetails[1] + '</td><td>' + splitdetails[2] + '</td><td>' + splitdetails[3] + '</td></tr>';
              }
              // table data ends
              ktable_row += '</tbody></table>';
              document.getElementById('myCampTable').innerHTML = ktable_row;


              
		  // campaign calls
                  var camp_connect = response.connect;
                  var camp_notconnect = response.notconnect;
                  var camp_name = response.campaigns;
                  camp_conn_notconn(camp_connect, camp_notconnect, camp_name, startDate, endDate);

                  // campaign login
                  var name_camp = response.campaign_logged_in;
                  var login_camp = response.login_time;
                  var pause_camp = response.pause_time;
                  campaign_login_time(name_camp, login_camp, pause_camp);

                  // campaign pendingCalls by Duration

                  var pending_camp = response.pend_name;
                  var pending_camp_tot = response.pend_total;
                  pendingCamp(pending_camp, pending_camp_tot);

                }
            });

            var camp;
            $.ajax(
              {
              type: "POST",
              url: "fetch_dropdown.php",
              dataType:'json',
              data: {
                'postdata': 1,
              },
              success: function(response)
              {
                // CAMPAIGN
                camp += '<option value="ALL">ALL</option>';
                for(i in response.campaigns){
                  camp += "<option value='"+response.campaigns[i]+"'>"+response.campaigns[i]+"</option>";
                }
                 document.getElementById('campaign').innerHTML = camp;
               }
            });

        });

function camp_conn_notconn(camp_connect, camp_notconnect, camp_name, startDate, endDate){
  var ctx = document.getElementById("campaign_wise").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: camp_name,
      datasets: [{
        label: 'Not Connected',
        backgroundColor: "#9c605a",
        data: camp_notconnect,
      }, {
        label: 'Connected',
        backgroundColor: "#796a8c",
        data: camp_connect,
      }],
    },
    options: {
      title: {
            display: true,
            
            position: 'top'
                },
        tooltips: {
          displayColors: true,
          callbacks:{
            mode: 'x',
          },
        },
        scales: {
          xAxes: [{
            stacked: true,
            gridLines: {
              display: false,
            },
		    display:true,
                    scaleLabel: {
                    display: true,
                    labelString: 'Campaign'
          }

          }],
          yAxes: [{
            stacked: true,
            ticks: {
              beginAtZero: true,
            },
            	type: 'linear',
		display:true,
                scaleLabel: {
                display: true,
                labelString: 'Count'
          }

          }]
        },
        responsive: true,
        maintainAspectRatio: false,
        legend: { position: 'bottom' },  
        onHover: (event, chartElement) => {
          event.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
        },   
        onClick: function (e) {
                var campaign = this.getElementsAtEvent(e)[0]._model.label;
                var campaignName = campaign.replace(" ", "+");
                  window.open(
            'index.php?module=CallLogs&parent=&page=1&view=List&viewname=50&orderby=&sortorder=&app=TOOLS&search_params=%5B%5B%5B"date"%2C"bw"%2C"'+startDate+'%2C'+endDate+'"%5D%2C%5B"campaign"%2C"c"%2C"'+campaignName+'"%5D%5D%5D&tag_params=%5B%5D&nolistcache=0&list_headers=%5B"username"%2C"source"%2C"destination"%2C"date"%2C"time"%2C"campaign"%2C"dispo"%2C"duration"%2C"agent_duration"%2C"customer_duration"%2C"voice_files"%2C"callback"%2C"info1"%2C"info2"%2C"info3"%2C"info4"%5D&tag=',
            '_blank' // <- This is what makes it open in a new window.
          );

        }

      }
    });
}

function campaign_login_time(name_camp, login_camp, pause_camp){

  var ctx = document.getElementById("camp_login_pause").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: name_camp,
      datasets: [{
        label: 'Login Time',
        backgroundColor: "#80bfff",
        data: login_camp,
      }, {
        label: 'Pause Time',
        backgroundColor: "#ffd24d",
        data: pause_camp,
      }],
    },
    options: {
      title: {
            display: true,
            
            position: 'top'
                },
        tooltips: {
          displayColors: true,
          callbacks:{
            mode: 'x',
          },
        },
        scales: {
          xAxes: [{
            stacked: true,
            gridLines: {
              display: false,
            },
  		display:true,
                scaleLabel: {
                display: true,
                labelString: 'Campaign'
          }

          }],
          yAxes: [{
            stacked: true,
            ticks: {
              beginAtZero: true,
            },
            type: 'linear',
		display:true,
                scaleLabel: {
                display: true,
                labelString: 'Count'
          }

          }]
        },
        responsive: true,
        maintainAspectRatio: false,
        legend: { position: 'bottom' },
      }
    });
}

function camp_LoginPie(name_camp, login_camp, pause_camp){
    Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

    var helpers = Chart.helpers;
    var defaults = Chart.defaults;

    Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
      updateElement: function(arc, index, reset) {
        var _this = this;
        var chart = _this.chart,
            chartArea = chart.chartArea,
            opts = chart.options,
            animationOpts = opts.animation,
            arcOpts = opts.elements.arc,
            centerX = (chartArea.left + chartArea.right) / 2,
            centerY = (chartArea.top + chartArea.bottom) / 2,
            startAngle = opts.rotation, // non reset case handled later
            endAngle = opts.rotation, // non reset case handled later
            dataset = _this.getDataset(),
            circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
            innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
            outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
            custom = arc.custom || {},
            valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

        helpers.extend(arc, {
          // Utility
          _datasetIndex: _this.index,
          _index: index,

          // Desired view properties
          _model: {
            x: centerX + chart.offsetX,
            y: centerY + chart.offsetY,
            startAngle: startAngle,
            endAngle: endAngle,
            circumference: circumference,
            outerRadius: outerRadius,
            innerRadius: innerRadius,
            label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
          },

          draw: function () {
            var ctx = this._chart.ctx,
                vm = this._view,
                sA = vm.startAngle,
                eA = vm.endAngle,
                opts = this._chart.config.options;
            
              var labelPos = this.tooltipPosition();
              var segmentLabel = vm.circumference / opts.circumference * 100;
              
              ctx.beginPath();
              
              ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
              ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);
              
              ctx.closePath();
              ctx.strokeStyle = vm.borderColor;
              ctx.lineWidth = vm.borderWidth;
              
              ctx.fillStyle = vm.backgroundColor;
              
              ctx.fill();
              ctx.lineJoin = 'bevel';
              
              if (vm.borderWidth) {
                ctx.stroke();
              }
              
              if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
                ctx.beginPath();
                ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
                ctx.fillStyle = "#fff";
                ctx.textBaseline = "top";
                ctx.textAlign = "center";
                
                // Round percentage in a way that it always adds up to 100%
                ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
              }
          }
        });

        var model = arc._model;
        model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
        model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
        model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
        model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

        // Set correct angles if not resetting
        if (!reset || !animationOpts.animateRotate) {
          if (index === 0) {
            model.startAngle = opts.rotation;
          } else {
            model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
          }

          model.endAngle = model.startAngle + model.circumference;
        }

        arc.pivot();
      }
    });

    var config = {
      type: 'doughnutLabels',
      data: {
        datasets: [{
          data:[login_camp,pause_camp],
          backgroundColor: ['#80bfff','#ffd24d'],
          label: 'Dataset 1'
        }],
        labels:['Login TIme (in minutes)', 'Pause Time (in minutes)']
      },
      options: {
        responsive: true,
        legend: {
          position: 'right',
        },
        title: {
          display: true,
          text: 'Campaign Login'
        },
        animation: {
          animateScale: true,
          animateRotate: true
        }
      }
    };

    var ctx = document.getElementById("campaign_wise_Loginpie").getContext("2d");
    new Chart(ctx, config);
}

function camp_conn_notconnPie(camp_connect, camp_notconnect, camp_name){
    Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

    var helpers = Chart.helpers;
    var defaults = Chart.defaults;

    Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
      updateElement: function(arc, index, reset) {
        var _this = this;
        var chart = _this.chart,
            chartArea = chart.chartArea,
            opts = chart.options,
            animationOpts = opts.animation,
            arcOpts = opts.elements.arc,
            centerX = (chartArea.left + chartArea.right) / 2,
            centerY = (chartArea.top + chartArea.bottom) / 2,
            startAngle = opts.rotation, // non reset case handled later
            endAngle = opts.rotation, // non reset case handled later
            dataset = _this.getDataset(),
            circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
            innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
            outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
            custom = arc.custom || {},
            valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

        helpers.extend(arc, {
          // Utility
          _datasetIndex: _this.index,
          _index: index,

          // Desired view properties
          _model: {
            x: centerX + chart.offsetX,
            y: centerY + chart.offsetY,
            startAngle: startAngle,
            endAngle: endAngle,
            circumference: circumference,
            outerRadius: outerRadius,
            innerRadius: innerRadius,
            label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
          },

          draw: function () {
            var ctx = this._chart.ctx,
                vm = this._view,
                sA = vm.startAngle,
                eA = vm.endAngle,
                opts = this._chart.config.options;
            
              var labelPos = this.tooltipPosition();
              var segmentLabel = vm.circumference / opts.circumference * 100;
              
              ctx.beginPath();
              
              ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
              ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);
              
              ctx.closePath();
              ctx.strokeStyle = vm.borderColor;
              ctx.lineWidth = vm.borderWidth;
              
              ctx.fillStyle = vm.backgroundColor;
              
              ctx.fill();
              ctx.lineJoin = 'bevel';
              
              if (vm.borderWidth) {
                ctx.stroke();
              }
              
              if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
                ctx.beginPath();
                ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
                ctx.fillStyle = "#fff";
                ctx.textBaseline = "top";
                ctx.textAlign = "center";
                
                // Round percentage in a way that it always adds up to 100%
                ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
              }
          }
        });

        var model = arc._model;
        model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
        model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
        model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
        model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

        // Set correct angles if not resetting
        if (!reset || !animationOpts.animateRotate) {
          if (index === 0) {
            model.startAngle = opts.rotation;
          } else {
            model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
          }

          model.endAngle = model.startAngle + model.circumference;
        }

        arc.pivot();
      }
    });

    var config = {
      type: 'doughnutLabels',
      data: {
        datasets: [{
          data:[camp_connect,camp_notconnect],
          backgroundColor: ['#1DE9B6','#2196F3'],
          label: 'Dataset 1'
        }],
        labels:['Connected', 'Not Connected']
      },
      options: {
        responsive: true,
        legend: {
          position: 'right',
        },
        title: {
          display: true,
          text: 'Campaign Calls'
        },
        animation: {
          animateScale: true,
          animateRotate: true
        }
      }
    };

    var ctx = document.getElementById("campaign_wise_pie").getContext("2d");
    new Chart(ctx, config);
}

// pending camp
function pendingCamp(pending_camp, pending_camp_tot){

// colors
  var chart_colors = ["#1DE9B6","#2196F3","#F50057","#5E35B1","#B10358","#F1C40F","#7D3C98","#E74C3C","#FE3A06","#03D5E7","#04B014","#BE4B05","#BE0521","#EA7385"];
  var dynamic_colors = [];
      // for (var i = 0; i < pending_camp.length; i++) {
        for(var i in pending_camp){
          dynamic_colors.push(chart_colors[i])
      }

Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

var helpers = Chart.helpers;
var defaults = Chart.defaults;

Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
  updateElement: function(arc, index, reset) {
    var _this = this;
    var chart = _this.chart,
        chartArea = chart.chartArea,
        opts = chart.options,
        animationOpts = opts.animation,
        arcOpts = opts.elements.arc,
        centerX = (chartArea.left + chartArea.right) / 2,
        centerY = (chartArea.top + chartArea.bottom) / 2,
        startAngle = opts.rotation, // non reset case handled later
        endAngle = opts.rotation, // non reset case handled later
        dataset = _this.getDataset(),
        circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
        innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
        outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
        custom = arc.custom || {},
        valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

    helpers.extend(arc, {
      // Utility
      _datasetIndex: _this.index,
      _index: index,

      // Desired view properties
      _model: {
        x: centerX + chart.offsetX,
        y: centerY + chart.offsetY,
        startAngle: startAngle,
        endAngle: endAngle,
        circumference: circumference,
        outerRadius: outerRadius,
        innerRadius: innerRadius,
        label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
      },

      draw: function () {
        var ctx = this._chart.ctx,
            vm = this._view,
            sA = vm.startAngle,
            eA = vm.endAngle,
            opts = this._chart.config.options;
        
          var labelPos = this.tooltipPosition();
          var segmentLabel = vm.circumference / opts.circumference * 100;
          
          ctx.beginPath();
          
          ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
          ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);
          
          ctx.closePath();
          ctx.strokeStyle = vm.borderColor;
          ctx.lineWidth = vm.borderWidth;
          
          ctx.fillStyle = vm.backgroundColor;
          
          ctx.fill();
          ctx.lineJoin = 'bevel';
          
          if (vm.borderWidth) {
            ctx.stroke();
          }
          
          if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
            ctx.beginPath();
            ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
            ctx.fillStyle = "#fff";
            ctx.textBaseline = "top";
            ctx.textAlign = "center";
            
            // Round percentage in a way that it always adds up to 100%
            ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
          }
      }
    });

    var model = arc._model;
    model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
    model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
    model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
    model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

    // Set correct angles if not resetting
    if (!reset || !animationOpts.animateRotate) {
      if (index === 0) {
        model.startAngle = opts.rotation;
      } else {
        model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
      }

      model.endAngle = model.startAngle + model.circumference;
    }

    arc.pivot();
  }
});

var config = {
  type: 'doughnutLabels',
  data: {
    datasets: [{
      data:pending_camp_tot,
      backgroundColor: chart_colors,
      label: 'Dataset 1'
    }],
    labels: pending_camp
  },
  options: {
    responsive: true,
    legend: {
      position: 'right',
    },
    title: {
      display: true,
     
    },
    animation: {
      animateScale: true,
      animateRotate: true
    }
  }
};

var ctx = document.getElementById("pendingCalls").getContext("2d");
new Chart(ctx, config);

}
// end pending camp

</script>

</body>

</html>
<?php }} ?>