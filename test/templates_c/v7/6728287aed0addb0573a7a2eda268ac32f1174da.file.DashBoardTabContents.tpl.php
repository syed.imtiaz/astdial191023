<?php /* Smarty version Smarty-3.1.7, created on 2023-09-14 10:13:01
         compiled from "/var/www/xdial.astcrm.com/html/includes/runtime/../../layouts/v7/modules/Vtiger/dashboards/DashBoardTabContents.tpl" */ ?>
<?php /*%%SmartyHeaderCode:567990396629ee9db9f1429-88495482%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6728287aed0addb0573a7a2eda268ac32f1174da' => 
    array (
      0 => '/var/www/xdial.astcrm.com/html/includes/runtime/../../layouts/v7/modules/Vtiger/dashboards/DashBoardTabContents.tpl',
      1 => 1694610701,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '567990396629ee9db9f1429-88495482',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_629ee9dba2532',
  'variables' => 
  array (
    'MODULE_NAME' => 0,
    'MODULE' => 0,
    'TABID' => 0,
    'WIDGETS' => 0,
    'WIDGET' => 0,
    'WIDGETDOMID' => 0,
    'COLUMNS' => 0,
    'ROW' => 0,
    'ROWCOUNT' => 0,
    'COLCOUNT' => 0,
    'CHARTWIDGETDOMID' => 0,
    'WIDGETID' => 0,
    'CURRENT_USER' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_629ee9dba2532')) {function content_629ee9dba2532($_smarty_tpl) {?>

<style type="text/css">b{font-family: 'OpenSans-Semibold', 'ProximaNova-Semibold', sans-serif;font-weight: normal;font-size: 16px;margin-left: 20px;}.flex-container {display: flex;}.flex-container > div {background-color: #fff;margin: 10px;padding-bottom:30px;font-size: 30px;width: 33.3%;height: 360px;border: solid 3px #e2e2e4;}/*	.grid-item {box-shadow: 0 10px 20px rgba(0,0,0,0.30), 0 5px 7px rgba(0,0,0,0.22);}*/111</style><!--	<div><iframe width="100%" height="1020px" src="https://datastudio.google.com/embed/reporting/59fc71cc-528e-4587-b31a-425b2c6cd74d/page/T6o3C" frameborder="0" style="border:0" allowfullscreen></iframe></div>--><div class='dashBoardTabContainer'><?php echo $_smarty_tpl->getSubTemplate (vtemplate_path("dashboards/DashBoardHeader.tpl",$_smarty_tpl->tpl_vars['MODULE_NAME']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('DASHBOARDHEADER_TITLE'=>vtranslate($_smarty_tpl->tpl_vars['MODULE']->value,$_smarty_tpl->tpl_vars['MODULE']->value)), 0);?>
<br><div class="dashboardBanner"></div><div class="dashBoardTabContents clearfix"><!-- astcrm dashboards custom widgets --><div class="flex-container"><div class="grid-item"><div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px; font-weight:Bold;">7 Days Calls</div><canvas id="chart-weekcalls"></canvas></div><div class="grid-item"><div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold;">Last Day Calls</div><canvas id="chart-yestercalls"></canvas></div><div class="grid-item"><div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold;">Yesterday Call by Duration</div><canvas id="yesterday_duration" width="150" height="100" style="padding:10px;"></canvas></div></div><div class="flex-container"><div class="grid-item" style="text-align:center"><div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold;">Last 7 Days Disposition</div><canvas id="pieChart-dispoSeven" width="33.3%" height="22px" style="padding: 10px;"></canvas><b style="font-size: 12px;">Total Calls: <span id="dispo_lastSeven"></span> </b></div><div class="grid-item" style="text-align:center"><div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold;">Yesterday  Disposition</div><canvas id="pieChart-expenses" width="360" height="220" style="padding: 10px;"></canvas><b style="font-size: 12px;">Total Calls: <span id="expenses_total"></span> </b></div><div class="grid-item" style="text-align:center"><div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold;">Last 7 Days Logins </div><canvas id="chart-loginseven" height="220" width="300" style="padding: 10px;"></canvas><b style="font-size: 12px;">Total Users: <span id="login_users"></span></b></div></div><!-- astcrm --><div class="gridster_<?php echo $_smarty_tpl->tpl_vars['TABID']->value;?>
"><ul><?php $_smarty_tpl->tpl_vars['COLUMNS'] = new Smarty_variable(2, null, 0);?><?php $_smarty_tpl->tpl_vars['ROW'] = new Smarty_variable(1, null, 0);?><?php  $_smarty_tpl->tpl_vars['WIDGET'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['WIDGET']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['WIDGETS']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['count']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['WIDGET']->key => $_smarty_tpl->tpl_vars['WIDGET']->value){
$_smarty_tpl->tpl_vars['WIDGET']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['count']['index']++;
?><?php $_smarty_tpl->tpl_vars['WIDGETDOMID'] = new Smarty_variable($_smarty_tpl->tpl_vars['WIDGET']->value->get('linkid'), null, 0);?><?php if ($_smarty_tpl->tpl_vars['WIDGET']->value->getName()=='MiniList'){?><?php $_smarty_tpl->tpl_vars['WIDGETDOMID'] = new Smarty_variable(($_smarty_tpl->tpl_vars['WIDGET']->value->get('linkid')).('-').($_smarty_tpl->tpl_vars['WIDGET']->value->get('widgetid')), null, 0);?><?php }elseif($_smarty_tpl->tpl_vars['WIDGET']->value->getName()=='Notebook'){?><?php $_smarty_tpl->tpl_vars['WIDGETDOMID'] = new Smarty_variable(($_smarty_tpl->tpl_vars['WIDGET']->value->get('linkid')).('-').($_smarty_tpl->tpl_vars['WIDGET']->value->get('widgetid')), null, 0);?><?php }?><?php if ($_smarty_tpl->tpl_vars['WIDGETDOMID']->value){?><li id="<?php echo $_smarty_tpl->tpl_vars['WIDGETDOMID']->value;?>
" <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['count']['index']%$_smarty_tpl->tpl_vars['COLUMNS']->value==0&&$_smarty_tpl->getVariable('smarty')->value['foreach']['count']['index']!=0){?> <?php $_smarty_tpl->tpl_vars['ROWCOUNT'] = new Smarty_variable($_smarty_tpl->tpl_vars['ROW']->value+1, null, 0);?> data-row="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getPositionRow($_smarty_tpl->tpl_vars['ROWCOUNT']->value);?>
" <?php }else{ ?> data-row="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getPositionRow($_smarty_tpl->tpl_vars['ROW']->value);?>
" <?php }?><?php $_smarty_tpl->tpl_vars['COLCOUNT'] = new Smarty_variable(($_smarty_tpl->getVariable('smarty')->value['foreach']['count']['index']%$_smarty_tpl->tpl_vars['COLUMNS']->value)+1, null, 0);?> data-col="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getPositionCol($_smarty_tpl->tpl_vars['COLCOUNT']->value);?>
" data-sizex="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getSizeX();?>
" data-sizey="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getSizeY();?>
" <?php if ($_smarty_tpl->tpl_vars['WIDGET']->value->get('position')==''){?> data-position="false"<?php }?>class="dashboardWidget dashboardWidget_<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['count']['index'];?>
" data-url="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getUrl();?>
" data-mode="open" data-name="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getName();?>
"></li><?php }else{ ?><?php $_smarty_tpl->tpl_vars['CHARTWIDGETDOMID'] = new Smarty_variable($_smarty_tpl->tpl_vars['WIDGET']->value->get('reportid'), null, 0);?><?php $_smarty_tpl->tpl_vars['WIDGETID'] = new Smarty_variable($_smarty_tpl->tpl_vars['WIDGET']->value->get('id'), null, 0);?><li id="<?php echo $_smarty_tpl->tpl_vars['CHARTWIDGETDOMID']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['WIDGETID']->value;?>
" <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['count']['index']%$_smarty_tpl->tpl_vars['COLUMNS']->value==0&&$_smarty_tpl->getVariable('smarty')->value['foreach']['count']['index']!=0){?> <?php $_smarty_tpl->tpl_vars['ROWCOUNT'] = new Smarty_variable($_smarty_tpl->tpl_vars['ROW']->value+1, null, 0);?> data-row="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getPositionRow($_smarty_tpl->tpl_vars['ROWCOUNT']->value);?>
" <?php }else{ ?> data-row="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getPositionRow($_smarty_tpl->tpl_vars['ROW']->value);?>
" <?php }?><?php $_smarty_tpl->tpl_vars['COLCOUNT'] = new Smarty_variable(($_smarty_tpl->getVariable('smarty')->value['foreach']['count']['index']%$_smarty_tpl->tpl_vars['COLUMNS']->value)+1, null, 0);?> data-col="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getPositionCol($_smarty_tpl->tpl_vars['COLCOUNT']->value);?>
" data-sizex="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getSizeX();?>
" data-sizey="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getSizeY();?>
" <?php if ($_smarty_tpl->tpl_vars['WIDGET']->value->get('position')==''){?> data-position="false"<?php }?>class="dashboardWidget dashboardWidget_<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['count']['index'];?>
" data-url="<?php echo $_smarty_tpl->tpl_vars['WIDGET']->value->getUrl();?>
" data-mode="open" data-name="ChartReportWidget"></li><?php }?><?php } ?></ul><input type="hidden" id=row value="<?php echo $_smarty_tpl->tpl_vars['ROWCOUNT']->value;?>
" /><input type="hidden" id=col value="<?php echo $_smarty_tpl->tpl_vars['COLCOUNT']->value;?>
" /><input type="hidden" id="userDateFormat" value="<?php echo $_smarty_tpl->tpl_vars['CURRENT_USER']->value->get('date_format');?>
" /></div></div></div>

<!-- chartJS -->
<script src="Chartjs/Chart.js"></script>
<script type="text/javascript">

	 $(document).ready(function(){ 
         	// fetching graph data
        	$.ajax(
            {
                type: "POST",
                url: "dashboard_custom_widget.php",
                dataType:'json',
                data: {
                  'post_data': 1,
                },
                success: function(response)
                {

                  var chart_total = response.chart_total;
                  var chart_lastweekpause_time = response.chart_lastweekpause_time;
                  var chart_lastweekbreak_time = response.chart_lastweekbreak_time;
                  var chart_lastweektalk_time = response.chart_lastweektalk_time;
                  var yesterDate = response.yesterDate;

                  // yesterday disposition status
                    var chart_dispo_status =response.chart_dispo_status;
  		            // yesterday disposition count
  		            var chart_dispo = response.chart_dispo;

  		            // last week dialed calls
  		            var chart_lastweekcall = response.chart_lastweekcall;
  	

  		 		// last week dialed calls connected/not connected
                  var chart_lastweekdate = response.chart_lastweekdate;

                  var colors_conn = [];
                  var colors_nconn = [];

                  for(var i in response.chart_lastweekdate){
                  	if(response.chart_lastweekdate[i] == response.check_today){

                  		colors_conn.push("#796a8c");
                  		colors_nconn.push("#9c605a");

                  	}else{

                  		colors_conn.push("#796a8c");
                  		colors_nconn.push("#9c605a");
                  	}
                  }

                var yesterday_dmy = response.yesterday_dmy;
	            var last_seventh_day_dmy = response.last_seventh_day_dmy;

                  var last_week_connected = response.conn_lastweek;
                  var last_week_notconnected = response.not_conn_seven;
  		          lastWeekCalls(chart_lastweekcall,chart_lastweekdate,last_week_connected,last_week_notconnected,colors_conn,colors_nconn);

  		          // yesterday dialed calls connected/not connected
                  var yes_users = response.yes_user;
                  var yes_conn_calls = response.yes_conn_call;
                  var yes_nconn_calls = response.yes_nconn_call;
                  var l_d = response.l_d;
                  calls_yesterday(yes_users, yes_conn_calls, yes_nconn_calls,yesterDate,l_d);

  		            // last week login
  		          var date_last_logins = response.date_last_login;
                  var cnt_last_logins = response.cnt_last_login;
                  var total_users = response.res_users;

  		            // yesterday call by duration
  		            var thiry = response.res_thirty;
                	var sixty = response.res_sixty;
                	var one_twenty = response.res_one_twenty;
                	var three_hundred = response.res_three_hundred;
                	var three_hundred_more = response.res_three_hundred_more;


                	// today connected/not connected
                	var todal_conn = response.count_total_connected;
                	var today_conn = response.count_connected;
                	var today_not_conn = response.count_not_connected;

                	// LAST SEVEN DAYS DISPOSITION
                	var dispoCnt_lastSeven = response.chart_dispo_seven;
	            	var dispoStatus_lastSeven = response.chart_dispo_status_seven;
	            	var chart_total_sevens = response.chart_total_seven;

	                last_seven_dispo(dispoCnt_lastSeven, dispoStatus_lastSeven, chart_total_sevens,yesterday_dmy,last_seventh_day_dmy);

                	// function will call graphs
                	yesterday_duration(thiry, sixty, one_twenty, three_hundred, three_hundred_more,yesterday_dmy);
  		           	lastWeekLogin(date_last_logins,cnt_last_logins,total_users);
                  	lastDaySchedule(chart_total, chart_dispo, chart_dispo_status, yesterday_dmy,l_d);
                  	lastWeekPerformance(chart_lastweekpause_time,chart_lastweekbreak_time,chart_lastweektalk_time);

                }
            });

        });


// Last 7 days calls
function lastWeekCalls(chart_lastweekcall,chart_lastweekdate,last_week_connected,last_week_notconnected,colors_conn,colors_nconn){

    var ctx = document.getElementById("chart-weekcalls").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: chart_lastweekdate,
			datasets: [{
				label: 'Not Connected',
				backgroundColor: colors_nconn,
				data: last_week_notconnected,
			}, {
				label: 'Connected',
				backgroundColor: colors_conn,
				data: last_week_connected,
			}],
		},
		options: {
		tooltips: {
		      displayColors: true,
		      callbacks:{
		        mode: 'x',
		      },
		    },
		    scales: {
		      xAxes: [{
		        stacked: true,
		        gridLines: {
		          display: false,
		        },
			display:true,
           	        scaleLabel: {
               		 display: true,
               		 labelString: 'Date'
		          }

		      }],
		      yAxes: [{
		        stacked: true,
		        ticks: {
		          beginAtZero: true,
		        },
		        type: 'linear',
			display:true,
	                scaleLabel: {
	                display: true,
	                labelString: 'Count'
		          }

		      }]
		    },
		    onHover: (event, chartElement) => {
			    event.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
			},
			responsive: true,
			maintainAspectRatio: false,
			legend: { position: 'bottom' },
			onClick: function (e) {
                var callDate = this.getElementsAtEvent(e)[0]._model.label;

                window.open(
				  'index.php?module=CallLogs&parent=&page=1&view=List&viewname=50&orderby=&sortorder=&app=TOOLS&search_params=%5B%5B%5B%22date%22%2C%22bw%22%2C%22'+callDate+'%2C'+callDate+'%22%5D%5D%5D&tag_params=%5B%5D&nolistcache=0&list_headers=%5B%22username%22%2C%22source%22%2C%22destination%22%2C%22date%22%2C%22time%22%2C%22campaign%22%2C%22dispo%22%2C%22duration%22%2C%22agent_duration%22%2C%22customer_duration%22%2C%22voice_files%22%2C%22callback%22%2C%22info1%22%2C%22info2%22%2C%22info3%22%2C%22info4%22%5D&tag=',
				  '_blank' // <- This is what makes it open in a new window.
				);
            }

			}
		});

	}
// end

// yesterday calls 
function calls_yesterday(yes_users, yes_conn_calls, yes_nconn_calls,yesterDate,l_d){

    var ctx = document.getElementById("chart-yestercalls").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: yes_users,
			datasets: [{
				label: 'Not Connected',
				backgroundColor: "#9c605a",
				data: yes_nconn_calls,
			}, {
				label: 'Connected',
				backgroundColor: "#796a8c",
				data: yes_conn_calls,

			}],
		},
		options: {
		    tooltips: {
		      displayColors: true,
		      callbacks:{
		        mode: 'x',
		      },
		    },onHover: (event, chartElement) => {
			    event.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
			},
		    scales: {
		      xAxes: [{
		        stacked: true,
		        gridLines: {
		          display: false,
		        },
			display:true,
	                scaleLabel: {
	                display: true,
	                labelString: 'Agents'
		          }

		      }],
		      yAxes: [{
		        stacked: true,
		        ticks: {
		          beginAtZero: true,
		        },
		        type: 'linear',
			display:true,
	                scaleLabel: {
	                display: true,
	                labelString: 'Count'
          }

		      }]
		    },
				responsive: true,
				maintainAspectRatio: false,
				legend: { position: 'bottom' },
				onClick: function (e) {
                var yesterdayAgents = this.getElementsAtEvent(e)[0]._model.label;
                var yesterdayAgentsName = yesterdayAgents.replace(" ", "+");
                
	                window.open(
					  'index.php?module=CallLogs&parent=&page=1&view=List&viewname=50&orderby=&sortorder=&app=TOOLS&search_params=%5B%5B%5B%22assigned_user_id%22%2C%22c%22%2C%22'+yesterdayAgentsName+'%22%5D%2C%5B%22date%22%2C%22bw%22%2C%22'+yesterDate+'%2C'+yesterDate+'%22%5D%5D%5D&tag_params=%5B%5D&nolistcache=0&list_headers=%5B%22assigned_user_id%22%2C%22date%22%2C%22time%22%2C%22destination%22%2C%22campaign%22%2C%22dispo%22%2C%22callback%22%2C%22source%22%5D&tag=',
					  '_blank' // <- This is what makes it open in a new window.
					);
            	}
			}
		});

	}
// end

// yesterday call by duration
function yesterday_duration(thiry, sixty, one_twenty, three_hundred, three_hundred_more, yesterday_dmy){

	// colors
	var chart_colors = ["#1DE9B6","#2196F3","#F50057","#5E35B1","#B10358","#F1C40F","#7D3C98","#E74C3C","#FE3A06","#03D5E7","#04B014","#BE4B05","#BE0521","#EA7385"];

	Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.pie);

	var helpers = Chart.helpers;
	var defaults = Chart.defaults;

	Chart.controllers.doughnutLabels = Chart.controllers.pie.extend({
		updateElement: function(arc, index, reset) {
	    var _this = this;
	    var chart = _this.chart,
	        chartArea = chart.chartArea,
	        opts = chart.options,
	        animationOpts = opts.animation,
	        arcOpts = opts.elements.arc,
	        centerX = (chartArea.left + chartArea.right) / 2,
	        centerY = (chartArea.top + chartArea.bottom) / 2,
	        startAngle = opts.rotation, // non reset case handled later
	        endAngle = opts.rotation, // non reset case handled later
	        dataset = _this.getDataset(),
	        circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
	        innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
	        outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
	        custom = arc.custom || {},
	        valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

	    helpers.extend(arc, {
	      // Utility
	      _datasetIndex: _this.index,
	      _index: index,

	      // Desired view properties
	      _model: {
	        x: centerX + chart.offsetX,
	        y: centerY + chart.offsetY,
	        startAngle: startAngle,
	        endAngle: endAngle,
	        circumference: circumference,
	        outerRadius: outerRadius,
	        innerRadius: innerRadius,
	        label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
	      },

	      draw: function () {
	      	var ctx = this._chart.ctx,
							vm = this._view,
							sA = vm.startAngle,
							eA = vm.endAngle,
							opts = this._chart.config.options;
					
						var labelPos = this.tooltipPosition();
						var segmentLabel = vm.circumference / opts.circumference * 100;
						
						ctx.beginPath();
						
						ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
						ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);
						
						ctx.closePath();
						ctx.strokeStyle = vm.borderColor;
						ctx.lineWidth = vm.borderWidth;
						
						ctx.fillStyle = vm.backgroundColor;
						
						ctx.fill();
						ctx.lineJoin = 'bevel';
						
						if (vm.borderWidth) {
							ctx.stroke();
						}
						
						if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
							ctx.beginPath();
							ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
							ctx.fillStyle = "#fff";
							ctx.textBaseline = "top";
							ctx.textAlign = "center";
	            
	            // Round percentage in a way that it always adds up to 100%
							ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
						}
	      }
	    });

	    var model = arc._model;
	    model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
	    model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
	    model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
	    model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

	    // Set correct angles if not resetting
	    if (!reset || !animationOpts.animateRotate) {
	      if (index === 0) {
	        model.startAngle = opts.rotation;
	      } else {
	        model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
	      }

	      model.endAngle = model.startAngle + model.circumference;
	    }

	    arc.pivot();
	  }
	});

	var config = {
	  type: 'doughnutLabels',
	  data: {
	    datasets: [{
	      data:[thiry, sixty, one_twenty, three_hundred, three_hundred_more],
	      backgroundColor: chart_colors,
	      label: 'Dataset 1'
	    }],
	    labels: ['<=30 Sec', '>30 and <=60 Sec', '>60 Sec and <=2 Min', '>2 Min and <=5 Min', '>5 Min'],
	  },
	  options: {
	    responsive: true,
	    legend: {
	      position: 'right',
	    },
	    title: {
	      display: true,

	    },
	    animation: {
	      animateScale: true,
	      animateRotate: true
	    }
	  }
	};

	var ctx = document.getElementById("yesterday_duration").getContext("2d");
	var myChart = new Chart(ctx, config);

}
// ends

// last week login
function lastWeekLogin(date_last_logins,cnt_last_logins,total_users){

document.getElementById('login_users').innerHTML = total_users;

   var canvas = document.getElementById("chart-loginseven");
	var ctx = canvas.getContext('2d');

	// Data with datasets options
	var data = {
	    labels: date_last_logins,
	      datasets: [
	        {
	            label: "Login",
	            fill: false,
	            backgroundColor: "#80bfff",
	            borderColor: "#80bfff",
	            data: cnt_last_logins
	        }
	    ]
	};

	// Notice how nested the beginAtZero is
	var options = {
	        title: {
	                  display: true,
	                       },
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                },
			type: 'linear',
	                display:true,
	                scaleLabel: {
	                display: true,
	                labelString: 'Count'
		          }

	            }],
			xAxes: [{
	                stacked: true,
		        gridLines: {
	                display: false,
            },
	                display:true,
	                scaleLabel: {
	                display: true,
	                labelString: 'Date'
          }
          }],
	        },
	        legend: { position: 'bottom' },			
	};

	// Chart declaration:
	var myBarChart = new Chart(ctx, {
	    type: 'line',
	    data: data,
	    options: options
	});
}
// 

// Last week performance
function lastWeekPerformance(chart_lastweekpause_time,chart_lastweekbreak_time,chart_lastweektalk_time){

	new Chart(document.getElementById("chart-weekperformance"), {
	    type: 'horizontalBar',
	    data: {
	      labels: ["Talk Time", "Pause Time", "Break Time"],
	      datasets: [
	        {
	          label: "Time (in minutes)",
	          backgroundColor: ["#66a984", "#ffd24d","#b19781"],
	          data: [chart_lastweektalk_time,chart_lastweekpause_time,chart_lastweekbreak_time]
	        }
	      ]
	    },

	    options: {
	      legend: { display: false },
	      title: {
	        display: false,
	        text: 'Last Week Performance'
	      },
	      responsive: true,
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero: true
	                }
	            }]
	        }
	    }
	});

	}


// yesterday disposition
function lastDaySchedule(chart_total, chart_dispo, chart_dispo_status,yesterday_dmy,l_d){
$("#expenses_total").html(chart_total);

// colors
	var chart_colors = ["#2196F3","#7D3C98","#F50057","#5E35B1","#B10358","#F1C40F","1DE9B6","#E74C3C","#FE3A06","#03D5E7","#04B014","#BE4B05","#BE0521","#EA7385"];

Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

var helpers = Chart.helpers;
var defaults = Chart.defaults;

Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
	updateElement: function(arc, index, reset) {
    var _this = this;
    var chart = _this.chart,
        chartArea = chart.chartArea,
        opts = chart.options,
        animationOpts = opts.animation,
        arcOpts = opts.elements.arc,
        centerX = (chartArea.left + chartArea.right) / 2,
        centerY = (chartArea.top + chartArea.bottom) / 2,
        startAngle = opts.rotation, // non reset case handled later
        endAngle = opts.rotation, // non reset case handled later
        dataset = _this.getDataset(),
        circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
        innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
        outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
        custom = arc.custom || {},
        valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

    helpers.extend(arc, {
      // Utility
      _datasetIndex: _this.index,
      _index: index,

      // Desired view properties
      _model: {
        x: centerX + chart.offsetX,
        y: centerY + chart.offsetY,
        startAngle: startAngle,
        endAngle: endAngle,
        circumference: circumference,
        outerRadius: outerRadius,
        innerRadius: innerRadius,
        label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
      },

      draw: function () {
      	var ctx = this._chart.ctx,
		vm = this._view,
		sA = vm.startAngle,
		eA = vm.endAngle,
		opts = this._chart.config.options;
	
		var labelPos = this.tooltipPosition();
		var segmentLabel = vm.circumference / opts.circumference * 100;
		
		ctx.beginPath();
		
		ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
		ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);
		
		ctx.closePath();
		ctx.strokeStyle = vm.borderColor;
		ctx.lineWidth = vm.borderWidth;
		
		ctx.fillStyle = vm.backgroundColor;
		
		ctx.fill();
		ctx.lineJoin = 'bevel';
		
		if (vm.borderWidth) {
			ctx.stroke();
		}
		
		if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
			ctx.beginPath();
			ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
			ctx.fillStyle = "#fff";
			ctx.textBaseline = "top";
			ctx.textAlign = "center";

		// Round percentage in a way that it always adds up to 100%
			ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
		}
      }
    });

    var model = arc._model;
    model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
    model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
    model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
    model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

    // Set correct angles if not resetting
    if (!reset || !animationOpts.animateRotate) {
      if (index === 0) {
        model.startAngle = opts.rotation;
      } else {
        model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
      }

      model.endAngle = model.startAngle + model.circumference;
    }

    arc.pivot();
  }
});

var config = {
  type: 'doughnutLabels',
  data: {
    datasets: [{
      data:chart_dispo,
      backgroundColor: chart_colors,
      label: 'Dataset 1'
    }],
    labels: chart_dispo_status
  },
  options: {
    responsive: true,
	    onClick:function(e){
	    var activePoints = myChart.getElementsAtEvent(e);
	    var selectedIndex = activePoints[0]._index;
	    var chartValue = this.data.datasets[0].data[selectedIndex];
	    var chartLabel = this.data.labels[selectedIndex];

        var chartLabelDisp = chartLabel.replace(" ", "+");

	       window.open(
			  'index.php?module=CallLogs&parent=&page=1&view=List&viewname=50&orderby=&sortorder=&app=TOOLS&search_params=%5B%5B%5B%22date%22%2C%22bw%22%2C%22'+yesterday_dmy+'%2C'+yesterday_dmy+'%22%5D%2C%5B%22dispo%22%2C%22c%22%2C%22'+chartLabelDisp+'%22%5D%5D%5D&tag_params=%5B%5D&nolistcache=0&list_headers=%5B%22username%22%2C%22source%22%2C%22destination%22%2C%22date%22%2C%22time%22%2C%22campaign%22%2C%22dispo%22%2C%22duration%22%2C%22agent_duration%22%2C%22customer_duration%22%2C%22voice_files%22%2C%22callback%22%2C%22info1%22%2C%22info2%22%2C%22info3%22%2C%22info4%22%5D&tag=',
			  '_blank' // <- This is what makes it open in a new window.
			);

		},
    legend: {
      position: 'right',
    },onHover: (event, chartElement) => {
	    event.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
	},
    title: {
      display: true,
    
    },
    animation: {
      animateScale: true,
      animateRotate: true
    }
  }
};

var ctx = document.getElementById("pieChart-expenses").getContext("2d");
var myChart = new Chart(ctx, config);


}
// end yesterday dispo

// LAST SEVEN DAYS DISPOSITION
function last_seven_dispo(dispoCnt_lastSeven, dispoStatus_lastSeven, chart_total_sevens,yesterday_dmy,last_seventh_day_dmy){

	$("#dispo_lastSeven").html(chart_total_sevens);

// colors
	var chart_colors = ["#1DE9B6","#2196F3","#F50057","#5E35B1","#B10358","#F1C40F","#7D3C98","#E74C3C","#FE3A06","#03D5E7","#04B014","#BE4B05","#BE0521","#EA7385"];

	Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

	var helpers = Chart.helpers;
	var defaults = Chart.defaults;

	Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
		updateElement: function(arc, index, reset) {
	    var _this = this;
	    var chart = _this.chart,
	        chartArea = chart.chartArea,
	        opts = chart.options,
	        animationOpts = opts.animation,
	        arcOpts = opts.elements.arc,
	        centerX = (chartArea.left + chartArea.right) / 2,
	        centerY = (chartArea.top + chartArea.bottom) / 2,
	        startAngle = opts.rotation, // non reset case handled later
	        endAngle = opts.rotation, // non reset case handled later
	        dataset = _this.getDataset(),
	        circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
	        innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
	        outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
	        custom = arc.custom || {},
	        valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

	    helpers.extend(arc, {
	      // Utility
	      _datasetIndex: _this.index,
	      _index: index,

	      // Desired view properties
	      _model: {
	        x: centerX + chart.offsetX,
	        y: centerY + chart.offsetY,
	        startAngle: startAngle,
	        endAngle: endAngle,
	        circumference: circumference,
	        outerRadius: outerRadius,
	        innerRadius: innerRadius,
	        label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
	      },

	      draw: function () {
	      	var ctx = this._chart.ctx,
				vm = this._view,
				sA = vm.startAngle,
				eA = vm.endAngle,
				opts = this._chart.config.options;
		
			var labelPos = this.tooltipPosition();
			var segmentLabel = vm.circumference / opts.circumference * 100;
			
			ctx.beginPath();
			
			ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
			ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);
			
			ctx.closePath();
			ctx.strokeStyle = vm.borderColor;
			ctx.lineWidth = vm.borderWidth;
			
			ctx.fillStyle = vm.backgroundColor;
			
			ctx.fill();
			ctx.lineJoin = 'bevel';
			
			if (vm.borderWidth) {
				ctx.stroke();
			}
			
			if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
				ctx.beginPath();
				ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
				ctx.fillStyle = "#fff";
				ctx.textBaseline = "top";
				ctx.textAlign = "center";
    
    			// Round percentage in a way that it always adds up to 100%
				ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
			}
	      }
	    });

	    var model = arc._model;
	    model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
	    model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
	    model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
	    model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

	    // Set correct angles if not resetting
	    if (!reset || !animationOpts.animateRotate) {
	      if (index === 0) {
	        model.startAngle = opts.rotation;
	      } else {
	        model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
	      }

	      model.endAngle = model.startAngle + model.circumference;
	    }

	    arc.pivot();
	  }
	});

	var config = {
	  type: 'doughnutLabels',
	  data: {
	    datasets: [{
	      data:dispoCnt_lastSeven,
	      backgroundColor: chart_colors,
	      label: 'Dataset 1'
	    }],
	    labels: dispoStatus_lastSeven
	  },
	  options: {
	    responsive: true,
	    onClick:function(e){
		    var activePoints = myChart.getElementsAtEvent(e);
		    var selectedIndex = activePoints[0]._index;
		    var chartValue = this.data.datasets[0].data[selectedIndex];
		    var chartLabel = this.data.labels[selectedIndex];

            var chartLabelDisp = chartLabel.replace(" ", "+");

		       window.open(
				  'index.php?module=CallLogs&parent=&page=1&view=List&viewname=50&orderby=&sortorder=&app=TOOLS&search_params=%5B%5B%5B%22date%22%2C%22bw%22%2C%22'+last_seventh_day_dmy+'%2C'+yesterday_dmy+'%22%5D%2C%5B%22dispo%22%2C%22c%22%2C%22'+chartLabelDisp+'%22%5D%5D%5D&tag_params=%5B%5D&nolistcache=0&list_headers=%5B%22username%22%2C%22source%22%2C%22destination%22%2C%22date%22%2C%22time%22%2C%22campaign%22%2C%22dispo%22%2C%22duration%22%2C%22agent_duration%22%2C%22customer_duration%22%2C%22voice_files%22%2C%22callback%22%2C%22info1%22%2C%22info2%22%2C%22info3%22%2C%22info4%22%5D&tag=',
				  '_blank' // <- This is what makes it open in a new window.
				);

		},
	    legend: {
	      position: 'right',
	    },onHover: (event, chartElement) => {
		    event.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
		},
	    title: {
	      display: true,
	  
	    },
	    animation: {
	      animateScale: true,
	      animateRotate: true
	    }
	  }
	};

	var ctx = document.getElementById("pieChart-dispoSeven").getContext("2d");
	var myChart = new Chart(ctx, config);
}
// LAST SEVEN DAYS DISPOSITION ENDS
</script>

<!-- astcrm -->
<?php }} ?>