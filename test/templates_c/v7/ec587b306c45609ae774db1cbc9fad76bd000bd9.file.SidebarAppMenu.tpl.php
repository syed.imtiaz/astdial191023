<?php /* Smarty version Smarty-3.1.7, created on 2023-01-06 10:02:22
         compiled from "/var/www/xdial.astcrm.com/html/includes/runtime/../../layouts/v7/modules/Vtiger/partials/SidebarAppMenu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:832272106629ee91ba074f8-58726934%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ec587b306c45609ae774db1cbc9fad76bd000bd9' => 
    array (
      0 => '/var/www/xdial.astcrm.com/html/includes/runtime/../../layouts/v7/modules/Vtiger/partials/SidebarAppMenu.tpl',
      1 => 1672921093,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '832272106629ee91ba074f8-58726934',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.7',
  'unifunc' => 'content_629ee91ba356c',
  'variables' => 
  array (
    'DASHBOARD_MODULE_MODEL' => 0,
    'USER_PRIVILEGES_MODEL' => 0,
    'HOME_MODULE_MODEL' => 0,
    'MODULE' => 0,
    'APP_LIST' => 0,
    'APP_NAME' => 0,
    'APP_GROUPED_MENU' => 0,
    'APP_MENU_MODEL' => 0,
    'FIRST_MENU_MODEL' => 0,
    'APP_IMAGE_MAP' => 0,
    'moduleModel' => 0,
    'moduleName' => 0,
    'translatedModuleLabel' => 0,
    'USER_MODEL' => 0,
    'MAILMANAGER_MODULE_MODEL' => 0,
    'DOCUMENTS_MODULE_MODEL' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_629ee91ba356c')) {function content_629ee91ba356c($_smarty_tpl) {?>

<div class="app-menu hide" id="app-menu">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2 col-xs-2 cursorPointer app-switcher-container">
				<div class="row app-navigator">
					<span id="menu-toggle-action" class="app-icon fa fa-bars"></span>
				</div>
			</div>
		</div>
		<?php $_smarty_tpl->tpl_vars['USER_PRIVILEGES_MODEL'] = new Smarty_variable(Users_Privileges_Model::getCurrentUserPrivilegesModel(), null, 0);?>
		<?php $_smarty_tpl->tpl_vars['HOME_MODULE_MODEL'] = new Smarty_variable(Vtiger_Module_Model::getInstance('Home'), null, 0);?>
		<?php $_smarty_tpl->tpl_vars['DASHBOARD_MODULE_MODEL'] = new Smarty_variable(Vtiger_Module_Model::getInstance('Dashboard'), null, 0);?>
		<div class="app-list row">
			<?php if ($_smarty_tpl->tpl_vars['USER_PRIVILEGES_MODEL']->value->hasModulePermission($_smarty_tpl->tpl_vars['DASHBOARD_MODULE_MODEL']->value->getId())){?>
				<div class="menu-item app-item dropdown-toggle" data-default-url="<?php echo $_smarty_tpl->tpl_vars['HOME_MODULE_MODEL']->value->getDefaultUrl();?>
">
					<div class="menu-items-wrapper">
						<span class="app-icon-list fa fa-dashboard"></span>
						<span class="app-name textOverflowEllipsis"> <?php echo vtranslate('LBL_DASHBOARD',$_smarty_tpl->tpl_vars['MODULE']->value);?>
</span>
					</div>
				</div>
			<?php }?>
			<?php $_smarty_tpl->tpl_vars['APP_GROUPED_MENU'] = new Smarty_variable(Settings_MenuEditor_Module_Model::getAllVisibleModules(), null, 0);?>
			<?php $_smarty_tpl->tpl_vars['APP_LIST'] = new Smarty_variable(Vtiger_MenuStructure_Model::getAppMenuList(), null, 0);?>

				<!-- realtime astcrm -->
				<div class="menu-item app-item dropdown-toggle" data-default-url="index.php?module=ASTDialRealTime&view=List&app=TOOLS">
					<div class="menu-items-wrapper">
						 <a href="index.php?module=ASTDialRealTime&view=List&app=TOOLS" title="RealTime">
						 	<span class="app-icon-list fa fa-clock-o" style="font-size:25px;"></span>
						<span class="app-name textOverflowEllipsis">
								<span class="module-name textOverflowEllipsis">Real Time</span>
							</a></span>
					</div>
				</div>
				<!-- realtime astcrm ends -->


			<?php  $_smarty_tpl->tpl_vars['APP_NAME'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['APP_NAME']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['APP_LIST']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['APP_NAME']->key => $_smarty_tpl->tpl_vars['APP_NAME']->value){
$_smarty_tpl->tpl_vars['APP_NAME']->_loop = true;
?>
				<?php if ($_smarty_tpl->tpl_vars['APP_NAME']->value=='ANALYTICS'){?> <?php continue 1?><?php }?>
				<?php if (count($_smarty_tpl->tpl_vars['APP_GROUPED_MENU']->value[$_smarty_tpl->tpl_vars['APP_NAME']->value])>0){?>
					<div class="dropdown app-modules-dropdown-container">
						<?php  $_smarty_tpl->tpl_vars['APP_MENU_MODEL'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['APP_MENU_MODEL']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['APP_GROUPED_MENU']->value[$_smarty_tpl->tpl_vars['APP_NAME']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['APP_MENU_MODEL']->key => $_smarty_tpl->tpl_vars['APP_MENU_MODEL']->value){
$_smarty_tpl->tpl_vars['APP_MENU_MODEL']->_loop = true;
?>
							<?php $_smarty_tpl->tpl_vars['FIRST_MENU_MODEL'] = new Smarty_variable($_smarty_tpl->tpl_vars['APP_MENU_MODEL']->value, null, 0);?>
							<?php if ($_smarty_tpl->tpl_vars['APP_MENU_MODEL']->value){?>
								<?php break 1?>
							<?php }?>
						<?php } ?>
						<div class="menu-item app-item dropdown-toggle app-item-color-<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
" data-app-name="<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
" id="<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
_modules_dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-default-url="<?php echo $_smarty_tpl->tpl_vars['FIRST_MENU_MODEL']->value->getDefaultUrl();?>
&app=<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
">
							<div class="menu-items-wrapper app-menu-items-wrapper">
								<span class="app-icon-list fa <?php echo $_smarty_tpl->tpl_vars['APP_IMAGE_MAP']->value[$_smarty_tpl->tpl_vars['APP_NAME']->value];?>
"></span>
								<span class="app-name textOverflowEllipsis"> <?php echo vtranslate("LBL_".($_smarty_tpl->tpl_vars['APP_NAME']->value));?>
</span>
								<span class="fa fa-chevron-right pull-right"></span>
							</div>
						</div>

					
					<ul class="dropdown-menu app-modules-dropdown" aria-labelledby="<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
_modules_dropdownMenu">


							<?php  $_smarty_tpl->tpl_vars['moduleModel'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['moduleModel']->_loop = false;
 $_smarty_tpl->tpl_vars['moduleName'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['APP_GROUPED_MENU']->value[$_smarty_tpl->tpl_vars['APP_NAME']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['moduleModel']->key => $_smarty_tpl->tpl_vars['moduleModel']->value){
$_smarty_tpl->tpl_vars['moduleModel']->_loop = true;
 $_smarty_tpl->tpl_vars['moduleName']->value = $_smarty_tpl->tpl_vars['moduleModel']->key;
?>
								<?php $_smarty_tpl->tpl_vars['translatedModuleLabel'] = new Smarty_variable(vtranslate($_smarty_tpl->tpl_vars['moduleModel']->value->get('label'),$_smarty_tpl->tpl_vars['moduleName']->value), null, 0);?>

								
								<!-- astcrm calling realtime php-->
					<?php if ($_smarty_tpl->tpl_vars['translatedModuleLabel']->value!='Lead Reports'&&$_smarty_tpl->tpl_vars['translatedModuleLabel']->value!='Agent Performance'&&$_smarty_tpl->tpl_vars['translatedModuleLabel']->value!='Campaign Reports'&&$_smarty_tpl->tpl_vars['translatedModuleLabel']->value!='RealTime'){?>  



								<li>

									<a href="<?php echo $_smarty_tpl->tpl_vars['moduleModel']->value->getDefaultUrl();?>
&app=<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['translatedModuleLabel']->value;?>
">

										<?php if ($_smarty_tpl->tpl_vars['translatedModuleLabel']->value=='RealTime Report'){?>
										<span class="fa fa-bar-chart hide"></span>
										<?php }elseif($_smarty_tpl->tpl_vars['translatedModuleLabel']->value!='CallLogs'){?>
										<span class="module-icon"><?php echo $_smarty_tpl->tpl_vars['moduleModel']->value->getModuleIcon();?>
</span>
										<?php }?>

										<?php if ($_smarty_tpl->tpl_vars['translatedModuleLabel']->value!='RealTime Report'&&$_smarty_tpl->tpl_vars['translatedModuleLabel']->value!='CallLogs'){?>
										<span class="module-name textOverflowEllipsis"> <?php echo $_smarty_tpl->tpl_vars['translatedModuleLabel']->value;?>
</span>
										<?php }?>

									</a>
								</li>
								<?php }?>
								<!-- astcrm -->
							<?php } ?>
						</ul>
					</div>
				<?php }?>
			<?php } ?>
			<!-- astcrm -->
			<!-- astcrm CALLREPORT-->
				<?php if ($_smarty_tpl->tpl_vars['USER_MODEL']->value->isAdminUser()){?>

				<!-- astcrm analytic reports -->
				<div class="dropdown app-modules-dropdown-container">
					<div class="menu-item app-item dropdown-toggle app-item-color-CALLREPORT" data-app-name="CALLREPORT" id="CALLREPORT_modules_dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-default-url="#">
						<div class="menu-items-wrapper app-menu-items-wrapper">
							<span class="module-icon">
								<span class="custom-module" title="callReports" style="background-color:#2C3B49;color:white;"><i class="fa fa-line-chart" style="font-size:24px"></i></span>
							</span>
							<span class="app-name textOverflowEllipsis">ANALYTICS</span>
							<span class="fa fa-chevron-right pull-right"></span>
						</div>
					</div>
					<ul class="dropdown-menu app-modules-dropdown" aria-labelledby="<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
_modules_dropdownMenu">

						

							<li>
							<a href="index.php?module=Analytics&view=List&app=TOOLS" title="CallLogs">
								<span class="module-icon"><span class="custom-module" title="leadPerformance">LP</span></span>
								<span class="module-name textOverflowEllipsis">Lead Report</span>
							</a>
						</li>


						<li>
							<a href="index.php?module=AgentPerformance&view=List&app=TOOLS" title="CallLogs">
								<span class="module-icon"><span class="custom-module" title="agentPerformance">AR</span></span>
								<span class="module-name textOverflowEllipsis">Agent Report</span>
							</a>
						</li>

						<li>
							<a href="index.php?module=CampaignPerformance&view=List&app=TOOLS" title="CallLogs">
								<span class="module-icon"><span class="custom-module" title="campaignPerformance">CP</span></span>
								<span class="module-name textOverflowEllipsis">Campaign Report</span>
							</a>
						</li>

						<li>
							<a href="index.php?module=CallLogs&view=List&app=TOOLS" title="CallLogs">
								<span class="module-icon"><span class="custom-module" title="leadPerformance">CA</span></span>
								<span class="module-name textOverflowEllipsis">Call Logs</span>
							</a>
						</li>

				


					

					</ul>
				</div>
				<!-- astcrm analytic reports ends-->

				<!-- astcrm call reports -->
				<div class="dropdown app-modules-dropdown-container">
						<div class="menu-item app-item dropdown-toggle app-item-color-CALLREPORT" data-app-name="CALLREPORT" id="CALLREPORT_modules_dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-default-url="#">
							<div class="menu-items-wrapper app-menu-items-wrapper" onclick="callreport()">
							<span class="module-icon">
									<span class="custom-module" title="callReports" style="background-color:#2C3B49;color:white;"><i class="fa fa-bar-chart"></i></span>
							</span>
								<span class="app-name textOverflowEllipsis">CALL REPORTS</span>
							<!-- 	<span class="fa fa-chevron-right pull-right"></span> -->
							</div>
						</div>

					<!-- 	<ul class="dropdown-menu app-modules-dropdown" aria-labelledby="<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
_modules_dropdownMenu">
 -->



						<!-- 	<li>
								<a href="index.php?module=MainDispo&view=List&type=dial" title="CallLogs">
									<span class="module-icon"><span class="custom-module" title="dialstatus">DS</span></span>
									<span class="module-name textOverflowEllipsis">Dial Status</span>
								</a>
							</li> -->



						<!-- 	<li>
								<a href="index.php?module=MainDispo&view=List&type=agent" title="CallLogs">
									<span class="module-icon"><span class="custom-module" title="agentperformance">AP</span></span>
									<span class="module-name textOverflowEllipsis">Agent Performance</span>
								</a>
							</li> -->

						<!-- 	<li>
								<a href="index.php?module=MainDispo&view=List&type=dialing" title="CallLogs">
									<span class="module-icon"><span class="custom-module" title="dialingstatus">DI</span></span>
									<span class="module-name textOverflowEllipsis">Dialling</span>
								</a>
							</li>

							<li>
								<a href="index.php?module=MainDispo&view=List&type=leads" title="CallLogs">
									<span class="module-icon"><span class="custom-module" title="leadstatus">LS</span></span>
									<span class="module-name textOverflowEllipsis">Leads Status</span>
								</a>
							</li> -->
							
					<!-- 	</ul> -->
					</div>
					<!-- astcrm call reports ends -->
		<!-- 			<div class="menu-item app-item dropdown-toggle" onclick="NewTab()">
						<div class="menu-items-wrapper" style="padding-left: 2px !important;">
							<span class="module-icon"><span class="custom-module" title="dailyReports">DR</span></span>
							<span class="app-name textOverflowEllipsis">DAILY REPORTS</span>
						</div>
					</div> -->
				<?php }?>
			<!-- astcrm -->
			<!-- astcrm -->
			<div class="app-list-divider"></div>
			<?php $_smarty_tpl->tpl_vars['MAILMANAGER_MODULE_MODEL'] = new Smarty_variable(Vtiger_Module_Model::getInstance('MailManager'), null, 0);?>
			<?php if ($_smarty_tpl->tpl_vars['USER_PRIVILEGES_MODEL']->value->hasModulePermission($_smarty_tpl->tpl_vars['MAILMANAGER_MODULE_MODEL']->value->getId())){?>
				<div class="menu-item app-item app-item-misc" data-default-url="index.php?module=MailManager&view=List">
					<div class="menu-items-wrapper">
						<span class="app-icon-list fa"><?php echo $_smarty_tpl->tpl_vars['MAILMANAGER_MODULE_MODEL']->value->getModuleIcon();?>
</span>
						<span class="app-name textOverflowEllipsis"> <?php echo vtranslate('MailManager');?>
</span>
					</div>
				</div>
			<?php }?>
			<?php $_smarty_tpl->tpl_vars['DOCUMENTS_MODULE_MODEL'] = new Smarty_variable(Vtiger_Module_Model::getInstance('Documents'), null, 0);?>
			<?php if ($_smarty_tpl->tpl_vars['USER_PRIVILEGES_MODEL']->value->hasModulePermission($_smarty_tpl->tpl_vars['DOCUMENTS_MODULE_MODEL']->value->getId())){?>
				<div class="menu-item app-item app-item-misc" data-default-url="index.php?module=Documents&view=List">
					<div class="menu-items-wrapper">
						<span class="app-icon-list fa"><?php echo $_smarty_tpl->tpl_vars['DOCUMENTS_MODULE_MODEL']->value->getModuleIcon();?>
</span>
						<span class="app-name textOverflowEllipsis"> <?php echo vtranslate('Documents');?>
</span>
					</div>
				</div>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['USER_MODEL']->value->isAdminUser()){?>
				<?php if (vtlib_isModuleActive('ExtensionStore')){?>
					<!-- <div class="menu-item app-item app-item-misc" data-default-url="index.php?module=ExtensionStore&parent=Settings&view=ExtensionStore">
						<div class="menu-items-wrapper">
							<span class="app-icon-list fa fa-shopping-cart"></span>
							<span class="app-name textOverflowEllipsis"> <?php echo vtranslate('LBL_EXTENSION_STORE','Settings:Vtiger');?>
</span>
						</div>
					</div> -->
					<div></div>
				<?php }?>
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['USER_MODEL']->value->isAdminUser()){?>
				<div class="dropdown app-modules-dropdown-container dropdown-compact">
					<div class="menu-item app-item dropdown-toggle app-item-misc" data-app-name="TOOLS" id="TOOLS_modules_dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-default-url="<?php if ($_smarty_tpl->tpl_vars['USER_MODEL']->value->isAdminUser()){?>index.php?module=Vtiger&parent=Settings&view=Index<?php }else{ ?>index.php?module=Users&view=Settings<?php }?>">
						<div class="menu-items-wrapper app-menu-items-wrapper">
							<span class="app-icon-list fa fa-cog"></span>
							<span class="app-name textOverflowEllipsis"> <?php echo vtranslate('LBL_SETTINGS','Settings:Vtiger');?>
</span>
							<?php if ($_smarty_tpl->tpl_vars['USER_MODEL']->value->isAdminUser()){?>
								<span class="fa fa-chevron-right pull-right"></span>
							<?php }?>
						</div>
					</div>
					<ul class="dropdown-menu app-modules-dropdown dropdown-modules-compact" aria-labelledby="<?php echo $_smarty_tpl->tpl_vars['APP_NAME']->value;?>
_modules_dropdownMenu" data-height="0.27">
						<li>
							<a href="?module=Vtiger&parent=Settings&view=Index">
								<span class="fa fa-cog module-icon"></span>
								<span class="module-name textOverflowEllipsis"> <?php echo vtranslate('LBL_CRM_SETTINGS','Vtiger');?>
</span>
							</a>
						</li>
						<li>
							<a href="?module=Users&parent=Settings&view=List">
								<span class="fa fa-user module-icon"></span>
								<span class="module-name textOverflowEllipsis"> <?php echo vtranslate('LBL_MANAGE_USERS','Vtiger');?>
</span>
							</a>
						</li>
					</ul>
				</div>
			<?php }?>
		</div>
	</div>
</div>
<!--     <script> 
        function NewTab() { 
            window.open("daily_reports/dailyReport.php", "_blank"); 
        } 
    </script>  -->

    <script>
    	function callreport(){
    		window.location.assign('index.php?module=Reports&view=List')
    	}
    </script><?php }} ?>