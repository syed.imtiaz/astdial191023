<?php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$moduleName = 'Campaigns'; // Replace with your module name
$fieldInstance = Vtiger_Field::getInstance('dispo', $moduleName);
if ($fieldInstance === false) {
    $fieldInstance = new Vtiger_Field();
    $fieldInstance->name = 'dispo';
    $fieldInstance->table = $moduleName;
    $fieldInstance->label = 'Select Disposition';
    $fieldInstance->column = 'dispo';
    $fieldInstance->columntype = 'VARCHAR(255)';
    $fieldInstance->uitype = 15; // Uitype for multi-select dropdown
    $fieldInstance->typeofdata = 'V~M'; // V - Varchar, M - Multi Select
    $fieldInstance->displaytype = 1; // Display as dropdown
    $fieldInstance->presence = 0; // 0 - Not mandatory, 2 - Mandatory
    $fieldInstance->masseditable = 1; // Allow mass edit
    $fieldInstance->quickcreate = 1; // Show in quick create form
    $fieldInstance->sequence = $fieldInstance->getNewSequence();
    $fieldInstance->save();
}

// Set picklist values for the multi-select dropdown
$picklistValues = array(
    'Option 1' => 'RingingNoResponse',
    'Option 2' => 'SwitchOff',
    'Option 3' => 'Interested',
    'Option 4' => 'Not Interested',
    'Option 5' => 'Callback',
    'Option 6' => 'Number Busy',
    'Option 7' => 'Wrong Number',
    'Option 8' => 'Promise to Pay',
    'Option 9' => 'Invalid',
    'Option 10' => 'DND',
    // Add more options as needed
);
Vtiger_Picklist::updateValues($moduleName, $fieldInstance->name, $picklistValues);
?>
