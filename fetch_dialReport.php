<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$con = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($con,$dbName);

if ($con->connect_error) {
 die("Connection failed: " . $con->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
$cur_date = date("Y-m-d");
// $cur_date = '2020-06-09';

if($_POST['post_filter']){

	$campaign = $_POST['camp_select'];
	$date_sub = $_POST['date'];
	$name = $_POST['name'];
	$phone = $_POST['number'];
	$cname = $_POST['cname'];
	$cnumber = $_POST['cnumber'];
	$main_disp = $_POST['main_selected'];
	$sub = $_POST['sub_selected'];
	$status_dial = $_POST['status_selected'];

	if($status_dial == ''){
		$dial_status = "";
	}else{
		if($status_dial == 'Connected'){
			$dial_status = 1;
		}else{
			$dial_status = 2;
		}
	}

	if($campaign != '' || $campaign != 'ALL'){
		$camp = "AND campaign LIKE '%".$campaign."%'  " ;
	}else{
		$camp = "" ;
	}

	if($date_sub != ''){
		$dates = "AND modify_date LIKE '".$date_sub."%'  " ;
	}else{
		$dates = "" ;
	}

	if($name != ''){
		$names = "AND user_name LIKE '%".$name."%'  " ;
	}else{
		$names = "" ;
	}

	if($phone != ''){
		$phoneno = "AND user LIKE '%".$phone."%'  " ;
	}else{
		$phoneno = "" ;
	}

	if($cname != ''){
		$cnames = "AND customer_name LIKE '%".$cname."%'  " ;
	}else{
		$cnames = "" ;
	}

	if($cnumber != ''){
		$cnumbers = "AND phone LIKE '%".$cnumber."%'  " ;
	}else{
		$cnumbers = "" ;
	}

	if($main_disp != ''){
		$main_disps = "AND dispo LIKE '%".$main_disp."%'  " ;
	}else{
		$main_disps = "" ;
	}

	if($sub != ''){
		$subs = "AND subdispo LIKE '%".$sub."%'  " ;
	}else{
		$subs = "" ;
	}

	if($dial_status != ''){
		$dial_statuss = "AND status LIKE '%".$dial_status."%'  " ;
	}else{
		$dial_statuss = "" ;
	}

	$fetch_report = mysqli_query($con, "SELECT dial_id, modify_date, user_name, user, customer_name, phone, status, dispo, subdispo, campaign FROM campaign_dial_status WHERE user_name != '' ".$camp.$dates.$names.$phoneno.$cnames.$cnumbers.$main_disps.$subs.$subs.$dial_statuss." ORDER BY modify_date DESC");

}else{

	$campaign = $_POST['camp_selected'];
	// $where_clause =" ";
	if($campaign != '' || $campaign != 'ALL'){
		$camp = "AND campaign LIKE '%".$campaign."%'  " ;
	}else{
		$camp = "" ;
	}

	if($campaign == '' || $campaign == 'ALL'){
	$fetch_report = mysqli_query($con, "SELECT dial_id, modify_date, user_name, user, customer_name, phone, status, dispo, subdispo, campaign FROM campaign_dial_status WHERE modify_date LIKE '".$cur_date."%' ");
	}else{
		$fetch_report = mysqli_query($con, "SELECT dial_id, modify_date, user_name, user, customer_name, phone, status, dispo, subdispo, campaign FROM campaign_dial_status WHERE user_name != '' ".$camp." AND modify_date LIKE '".$cur_date."%' ");
	}
		
}
$count = 0;
	while($row_report = mysqli_fetch_assoc($fetch_report)){

		$count++;

		// get call type
		$qry_camp_type = mysqli_query($con, "SELECT campaigntype FROM vtiger_campaign WHERE campaignname LIKE '%".$row_report['campaign']."%' ");
		$row_camp_type = mysqli_fetch_assoc($qry_camp_type);

		$camp_type = explode('-', $row_camp_type['campaigntype']);
		$attempt[] = strtoupper($camp_type[0]);

			$modify_date= explode(' ', $row_report['modify_date']);
			$date[] = $modify_date[0];
			$time[] = $modify_date[1];
			$user_name[] = $row_report['user_name'];
			$user_phone[] = $row_report['user'];
			$customer_name[] = $row_report['customer_name'];
			$customer_phone[] = $row_report['phone'];
			$customer_campaign[] = $row_report['campaign'];

			if($row_report['status'] == 1){
				$status[] = "Connected";
			}else{
				$status[] = "Not Connected";
			}
			
			$dispo[] = $row_report['dispo'];
			$subdispo[] = $row_report['subdispo'];
		}

	$realtime_contents = array(
		"attempt"=>$attempt,
		"date"=>$date,
		"time"=>$time,
		"user_name"=>$user_name,
		"user_phone"=>$user_phone,
		"customer_name"=>$customer_name,
		"customer_phone"=>$customer_phone,
		"status"=>$status,
		"dispo"=>$dispo,
		"subdispo"=>$subdispo,
		"count"=>$count,
		"campaign"=>$customer_campaign,
		);
	echo json_encode($realtime_contents);

?>