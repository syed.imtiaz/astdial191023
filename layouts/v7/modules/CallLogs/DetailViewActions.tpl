{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
<input id="recordId" type="hidden" value="{$RECORD->getId()}" />
    <div class="col-lg-6 detailViewButtoncontainer">
        <div class="pull-right btn-toolbar">
        <!--astcrm recycle and tranfer the leads based on dispostion or campaign -->
        {if $MODULE eq 'Campaigns'}
        <!-- modal recycle starts-->
            <form method="POST" id="post_recycle">
                <div id="recycle_modal" class="modal fade">
                    <div class="modal-dialog" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Campaign From: <span id="c_name" style="color: #ffffff; font-style: italic;"></span></h4>
                            </div>
                            <div class="modal-body">
                              <div class="row" style="margin-left: 10px;">
                                <div class="col-md-3">
                                    <label><b>Disposition</b></label>
                                    <select class="form-control" id="dispo"></select>
                                </div>
                              </div>
                              <br>
                              <h5 style="color: #04B431; text-align: center;" id="recycle_msg">Please wait...</h5>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success" style="margin-right: 5px;" id="sub_recycle">Apply</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- modal recycle ends -->
            <!-- modal transfer starts-->
            <form method="POST" id="post_transfer">
                <div id="transfer_modal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Campaign From: <span id="c_name1" style="color: #ffffff; font-style: italic;"></span></h4>
                            </div>
                            <div class="modal-body">
                              <div class="row" style="margin-left: 10px;">
                                <div class="col-md-3">
                                    <label><b>Disposition</b></label>
                                    <select class="form-control" id="dispo_ext"></select>
                                </div>
                                <div class="col-md-3">
                                    <label><b>Campaign To</b></label>
                                    <select class="form-control" id="campaign"></select>
                                </div>

                              </div>
                              <br>
                              <h5 style="color: #04B431; text-align: center;" id="trans_msg">Transferring Leads <span id="trans_cont"></span><span id="tt_lead"></span></h5>
                              <h5 style="text-align: center;" id="resp_msg">Transferred <span id="c_leads" style="color: #1560bd;"></span> Leads from Campaign <span id="from_camp" style="color: #1560bd;"></span><span id="disp_status"></span> to <span id="to_camp" style="color: #1560bd;"></span></h5>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success" style="margin-right: 5px;" id="sub_transfer">Apply</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- modal transfer ends -->
            <!-- astcrm recycle and transfer leads -->
            <div class="btn-group">
                <button class="btn btn-success show-recycle" type="button" id="recycle" style="margin-right: 5px;"><strong>Recycle</strong></button>
                <button class="btn btn-success show-transfer" type="button" id="transfer" style="margin-right: 5px;"><strong>Transfer Leads</strong></button>
            </div>
        <!-- astcrm recycle and transfer leads ends -->
        {/if}
            <div class="btn-group">
            {assign var=STARRED value=$RECORD->get('starred')}
            {if $MODULE_MODEL->isStarredEnabled()}
                <!--button class="btn btn-default markStar {if $STARRED} active {/if}" id="starToggle" style="width:100px;">
                    <div class='starredStatus' title="{vtranslate('LBL_STARRED', $MODULE)}">
                        <div class='unfollowMessage'>
                            <i class="fa fa-star-o"></i> &nbsp;{vtranslate('LBL_UNFOLLOW',$MODULE)}
                        </div>
                        <div class='followMessage'>
                            <i class="fa fa-star active"></i> &nbsp;{vtranslate('LBL_FOLLOWING',$MODULE)}
                        </div>
                    </div>
                    <div class='unstarredStatus' title="{vtranslate('LBL_NOT_STARRED', $MODULE)}">
                        {vtranslate('LBL_FOLLOW',$MODULE)}
                    </div>
                </button-->
            {/if}
            {foreach item=DETAIL_VIEW_BASIC_LINK from=$DETAILVIEW_LINKS['DETAILVIEWBASIC']}
                <!--button class="btn btn-default" id="{$MODULE_NAME}_detailView_basicAction_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_BASIC_LINK->getLabel())}"
                        {if $DETAIL_VIEW_BASIC_LINK->isPageLoadLink()}
                            onclick="window.location.href = '{$DETAIL_VIEW_BASIC_LINK->getUrl()}&app={$SELECTED_MENU_CATEGORY}'"
                        {else}
                            onclick="{$DETAIL_VIEW_BASIC_LINK->getUrl()}"
                        {/if}
                        {if $MODULE_NAME eq 'Documents' && $DETAIL_VIEW_BASIC_LINK->getLabel() eq 'LBL_VIEW_FILE'}
                            data-filelocationtype="{$DETAIL_VIEW_BASIC_LINK->get('filelocationtype')}" data-filename="{$DETAIL_VIEW_BASIC_LINK->get('filename')}"
                        {/if}>
                    {vtranslate($DETAIL_VIEW_BASIC_LINK->getLabel(), $MODULE_NAME)}
                </button-->
            {/foreach}
            {if $DETAILVIEW_LINKS['DETAILVIEW']|@count gt 0}
                <!--button class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                   {vtranslate('LBL_MORE', $MODULE_NAME)}&nbsp;&nbsp;<i class="caret"></i>
                </button-->
                <ul class="dropdown-menu dropdown-menu-right">
                    {foreach item=DETAIL_VIEW_LINK from=$DETAILVIEW_LINKS['DETAILVIEW']}
                        {if $DETAIL_VIEW_LINK->getLabel() eq ""} 
                            <li class="divider"></li>   
                            {else}
                            <li id="{$MODULE_NAME}_detailView_moreAction_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($DETAIL_VIEW_LINK->getLabel())}">
                                {if $DETAIL_VIEW_LINK->getUrl()|strstr:"javascript"} 
                                    <a href='{$DETAIL_VIEW_LINK->getUrl()}'>{vtranslate($DETAIL_VIEW_LINK->getLabel(), $MODULE_NAME)}</a>
                                {else}
                                    <a href='{$DETAIL_VIEW_LINK->getUrl()}&app={$SELECTED_MENU_CATEGORY}' >{vtranslate($DETAIL_VIEW_LINK->getLabel(), $MODULE_NAME)}</a>
                                {/if}
                            </li>
                        {/if}
                    {/foreach}
                </ul>
            {/if}
            </div>
            {if !{$NO_PAGINATION}}
            <div class="btn-group pull-right">
                <button class="btn btn-default " id="detailViewPreviousRecordButton" {if empty($PREVIOUS_RECORD_URL)} disabled="disabled" {else} onclick="window.location.href = '{$PREVIOUS_RECORD_URL}&app={$SELECTED_MENU_CATEGORY}'" {/if} >
                    <i class="fa fa-chevron-left"></i>
                </button>
                <button class="btn btn-default  " id="detailViewNextRecordButton"{if empty($NEXT_RECORD_URL)} disabled="disabled" {else} onclick="window.location.href = '{$NEXT_RECORD_URL}&app={$SELECTED_MENU_CATEGORY}'" {/if}>
                    <i class="fa fa-chevron-right"></i>
                </button>
            </div>
            {/if}        
        </div>
        <input type="hidden" name="record_id" value="{$RECORD->getId()}">
    </div>
{strip}
{literal}

<script type="text/javascript">
    $(document).ready(function(){

        $('#recycle').click(function(){
            $("#recycle_modal").modal('show');
        });

        $('#transfer').click(function(){
            $("#transfer_modal").modal('show');
        });

        var record_id = $("#recordId").val();
        var loggedin_user = $('#loggedin_user').val();

        $('#trans_msg').hide();
        $('#resp_msg').hide();
        $('#recycle_msg').hide();

        $("#transfer").click(function(){  
            $('#resp_msg').hide();
            $('select option:selected').removeAttr('selected');
        });
        // post recycle
        $("#sub_recycle").click(function(){     

           var sel_main = $('#dispo').find(":selected").text();

           if(sel_main == "Please Select"){
            var sel_mains = "";
           }else if(sel_main == "No disposition"){
            var sel_mains = "NULL";
           }else if(sel_main == "SKIPPED"){
            var sel_mains = "Skipped";
           }
           else{
            var sel_mains = sel_main;
           }
           
           if(sel_mains == ''){
            alert('Please select the disposition');
           }else{

            $.ajax({
                  type: "POST",
                  url: "fetch_recycle_transfer.php",
                  dataType:'json',
                  data: {
                    'post_filter': "post_recycle",
                    'main_selected': sel_mains,
                    'record_id': record_id,
                    'loggedin_user': loggedin_user,
                  },
                  beforeSend : function (){
                    $('#recycle_msg').show();
                  },
                  success: function(response)
                  {
                    $('#recycle_msg').hide();
                    if(response.status == 1){
                        alert(response.msg);    
                        $("#recycle_modal").modal('hide');
                        location.reload(true);
                    }
                  }
              });
           }
             //
         });

        // post transfer
        $("#sub_transfer").click(function(){      

           var sel_camp = $('#campaign').find(":selected").text();
           var sel_main = $('#dispo_ext').find(":selected").text();

           $('#trans_msg').hide();
           $('#resp_msg').hide();

           if(sel_main == "Please Select"){
            var sel_mains = "";
           }else{
            var sel_mains = sel_main;
           }

           if((sel_camp != "Please Select") && (sel_mains != '')  ){

                    $.ajax(
                      {
                      type: "POST",
                      url: "fetch_dropdown_recycle.php",
                      dataType:'json',
                      data: {
                        'post_filter': "post_transfer",
                        'camp_select': sel_camp,
                        'main_selected': sel_mains,
                        'record_id': record_id,
                      },
                      success: function(responses)
                      { 
                        // alert(response.count_leads);
                        if (responses.count_leads>10) {
                            document.getElementById('trans_cont').innerHTML = '1,<span>'+responses.f_value+',</span><span>'+responses.s_value+'...</span>';
                        }
                        
                        document.getElementById('tt_lead').innerHTML = responses.count_leads;

                        $('#trans_msg').show();

                        setTimeout(function(){
                         $.ajax(
                          {
                              type: "POST",
                              url: "fetch_recycle_transfer.php",
                              dataType:'json',
                              data: {
                                'post_filter': "post_transfer",
                                'camp_select': sel_camp,
                                'main_selected': sel_mains,
                                'record_id': record_id,
                              },
                              // beforeSend : function (){
                              //    $('#trans_msg').show();
                              // },
                              success: function(response)
                              {
                                $('select option:selected').removeAttr('selected');
                                $('#trans_msg').hide();

                                document.getElementById('c_leads').innerHTML = response.count_leads;
                                document.getElementById('from_camp').innerHTML = response.from_campaign; 
                                document.getElementById('to_camp').innerHTML = response.to_campaign; 
                                if (response.main_disp != '') {
                                    document.getElementById('disp_status').innerHTML = ' with Disposition <span style="color: #1560bd; ">'+response.main_disp+'</span>'; 
                                }

                                $('#resp_msg').show();
                              }
                          });
                        }, 2000);
                       }
                    });

           }else{
            alert('please select all the fields');
            return false;
           }
           //
         });

        // fetch dropdown values
        var camp, main_dispo, sub_dispo, main_dispo_trans;
        $.ajax(
          {
          type: "POST",
          url: "fetch_dropdown_recycle.php",
          dataType:'json',
          data: {
            'postdata': 1,
            'record_id':record_id,
          },
          success: function(response)
          { 
            // alert(response.campaign_name);
            // CAMPAIGN
            camp += '<option value="ALL">Please Select</option>';
            for(i in response.campaigns){
              camp += "<option value='"+response.campaigns[i]+"'>"+response.campaigns[i]+"</option>";
            }
             document.getElementById('campaign').innerHTML = camp;
             
             // MAIN DISPO
            main_dispo += '<option value="ALL" selected>Please Select</option>';
            for(i in response.main_dispo){
              main_dispo += "<option value='"+response.main_dispo[i]+"'>"+response.main_dispo[i]+"</option>";
            }
            main_dispo += '<option value="SKIPPED" style="">SKIPPED</option>';
             document.getElementById('dispo').innerHTML = main_dispo;
            main_dispo += '<option value="NULL" style="color:red;">No disposition</option>';
             document.getElementById('dispo').innerHTML = main_dispo;

                 // MAIN DISPO
            main_dispo_trans += '<option value="ALL" selected>Please Select</option>';
            for(i in response.main_dispo){
              main_dispo_trans += "<option value='"+response.main_dispo[i]+"'>"+response.main_dispo[i]+"</option>";
            }
             document.getElementById('dispo_ext').innerHTML = main_dispo_trans;

             document.getElementById('c_name').innerHTML = response.campaign_name;
             document.getElementById('c_name1').innerHTML = response.campaign_name;
           }
        });


        });

</script>
{/literal}
