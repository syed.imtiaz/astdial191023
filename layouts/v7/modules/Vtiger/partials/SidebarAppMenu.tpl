{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************}

<div class="app-menu hide" id="app-menu">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-2 col-xs-2 cursorPointer app-switcher-container">
				<div class="row app-navigator">
					<span id="menu-toggle-action" class="app-icon fa fa-bars"></span>
				</div>
			</div>
		</div>
		{assign var=USER_PRIVILEGES_MODEL value=Users_Privileges_Model::getCurrentUserPrivilegesModel()}
		{assign var=HOME_MODULE_MODEL value=Vtiger_Module_Model::getInstance('Home')}
		{assign var=DASHBOARD_MODULE_MODEL value=Vtiger_Module_Model::getInstance('Dashboard')}
		<div class="app-list row">
			{if $USER_PRIVILEGES_MODEL->hasModulePermission($DASHBOARD_MODULE_MODEL->getId())}
				<div class="menu-item app-item dropdown-toggle" data-default-url="{$HOME_MODULE_MODEL->getDefaultUrl()}">
					<div class="menu-items-wrapper">
						<span class="app-icon-list fa fa-dashboard"></span>
						<span class="app-name textOverflowEllipsis"> {vtranslate('LBL_DASHBOARD',$MODULE)}</span>
					</div>
				</div>
			{/if}
			{assign var=APP_GROUPED_MENU value=Settings_MenuEditor_Module_Model::getAllVisibleModules()}
			{assign var=APP_LIST value=Vtiger_MenuStructure_Model::getAppMenuList()}

				<!-- realtime astcrm -->
				<div class="menu-item app-item dropdown-toggle" data-default-url="index.php?module=ASTDialRealTime&view=List&app=TOOLS">
					<div class="menu-items-wrapper">
						 <a href="index.php?module=ASTDialRealTime&view=List&app=TOOLS" title="RealTime">
						 	<span class="app-icon-list fa fa-clock-o" style="font-size:25px;"></span>
						<span class="app-name textOverflowEllipsis">
								<span class="module-name textOverflowEllipsis">Real Time</span>
							</a></span>
					</div>
				</div>
				<!-- realtime astcrm ends -->


			{foreach item=APP_NAME from=$APP_LIST}
				{if $APP_NAME eq 'ANALYTICS'} {continue}{/if}
				{if count($APP_GROUPED_MENU.$APP_NAME) gt 0}
					<div class="dropdown app-modules-dropdown-container">
						{foreach item=APP_MENU_MODEL from=$APP_GROUPED_MENU.$APP_NAME}
							{assign var=FIRST_MENU_MODEL value=$APP_MENU_MODEL}
							{if $APP_MENU_MODEL}
								{break}
							{/if}
						{/foreach}
						<div class="menu-item app-item dropdown-toggle app-item-color-{$APP_NAME}" data-app-name="{$APP_NAME}" id="{$APP_NAME}_modules_dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-default-url="{$FIRST_MENU_MODEL->getDefaultUrl()}&app={$APP_NAME}">
							<div class="menu-items-wrapper app-menu-items-wrapper">
								<span class="app-icon-list fa {$APP_IMAGE_MAP.$APP_NAME}"></span>
								<span class="app-name textOverflowEllipsis"> {vtranslate("LBL_$APP_NAME")}</span>
								<span class="fa fa-chevron-right pull-right"></span>
							</div>
						</div>

					
					<ul class="dropdown-menu app-modules-dropdown" aria-labelledby="{$APP_NAME}_modules_dropdownMenu">


							{foreach item=moduleModel key=moduleName from=$APP_GROUPED_MENU[$APP_NAME]}
								{assign var='translatedModuleLabel' value=vtranslate($moduleModel->get('label'),$moduleName )}

								
								<!-- astcrm calling realtime php-->
					{if $translatedModuleLabel neq 'Lead Reports' && $translatedModuleLabel neq 'Agent Performance' && $translatedModuleLabel neq 'Campaign Reports' && $translatedModuleLabel neq 'RealTime'}  



								<li>

									<a href="{$moduleModel->getDefaultUrl()}&app={$APP_NAME}" title="{$translatedModuleLabel}">

										{if $translatedModuleLabel eq 'RealTime Report'}
										<span class="fa fa-bar-chart hide"></span>
										{else if $translatedModuleLabel neq 'CallLogs'}
										<span class="module-icon">{$moduleModel->getModuleIcon()}</span>
										{/if}

										{if $translatedModuleLabel neq 'RealTime Report' && $translatedModuleLabel neq 'CallLogs' }
										<span class="module-name textOverflowEllipsis"> {$translatedModuleLabel}</span>
										{/if}

									</a>
								</li>
								{/if}
								<!-- astcrm -->
							{/foreach}
						</ul>
					</div>
				{/if}
			{/foreach}
			<!-- astcrm -->
			<!-- astcrm CALLREPORT-->
				{if $USER_MODEL->isAdminUser()}

				<!-- astcrm analytic reports -->
				<div class="dropdown app-modules-dropdown-container">
					<div class="menu-item app-item dropdown-toggle app-item-color-CALLREPORT" data-app-name="CALLREPORT" id="CALLREPORT_modules_dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-default-url="#">
						<div class="menu-items-wrapper app-menu-items-wrapper">
							<span class="module-icon">
								<span class="custom-module" title="callReports" style="background-color:#2C3B49;color:white;"><i class="fa fa-line-chart" style="font-size:24px"></i></span>
							</span>
							<span class="app-name textOverflowEllipsis">ANALYTICS</span>
							<span class="fa fa-chevron-right pull-right"></span>
						</div>
					</div>
					<ul class="dropdown-menu app-modules-dropdown" aria-labelledby="{$APP_NAME}_modules_dropdownMenu">

						

							<li>
							<a href="index.php?module=Analytics&view=List&app=TOOLS" title="CallLogs">
								<span class="module-icon"><span class="custom-module" title="leadPerformance">LP</span></span>
								<span class="module-name textOverflowEllipsis">Lead Report</span>
							</a>
						</li>


						<li>
							<a href="index.php?module=AgentPerformance&view=List&app=TOOLS" title="CallLogs">
								<span class="module-icon"><span class="custom-module" title="agentPerformance">AR</span></span>
								<span class="module-name textOverflowEllipsis">Agent Report</span>
							</a>
						</li>

						<li>
							<a href="index.php?module=CampaignPerformance&view=List&app=TOOLS" title="CallLogs">
								<span class="module-icon"><span class="custom-module" title="campaignPerformance">CP</span></span>
								<span class="module-name textOverflowEllipsis">Campaign Report</span>
							</a>
						</li>

						<li>
							<a href="index.php?module=CallLogs&view=List&app=TOOLS" title="CallLogs">
								<span class="module-icon"><span class="custom-module" title="leadPerformance">CA</span></span>
								<span class="module-name textOverflowEllipsis">Call Logs</span>
							</a>
						</li>

				


					

					</ul>
				</div>
				<!-- astcrm analytic reports ends-->

				<!-- astcrm call reports -->
				<div class="dropdown app-modules-dropdown-container">
						<div class="menu-item app-item dropdown-toggle app-item-color-CALLREPORT" data-app-name="CALLREPORT" id="CALLREPORT_modules_dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-default-url="#">
							<div class="menu-items-wrapper app-menu-items-wrapper" onclick="callreport()">
							<span class="module-icon">
									<span class="custom-module" title="callReports" style="background-color:#2C3B49;color:white;"><i class="fa fa-bar-chart"></i></span>
							</span>
								<span class="app-name textOverflowEllipsis">CALL REPORTS</span>
							<!-- 	<span class="fa fa-chevron-right pull-right"></span> -->
							</div>
						</div>

					<!-- 	<ul class="dropdown-menu app-modules-dropdown" aria-labelledby="{$APP_NAME}_modules_dropdownMenu">
 -->



						<!-- 	<li>
								<a href="index.php?module=MainDispo&view=List&type=dial" title="CallLogs">
									<span class="module-icon"><span class="custom-module" title="dialstatus">DS</span></span>
									<span class="module-name textOverflowEllipsis">Dial Status</span>
								</a>
							</li> -->



						<!-- 	<li>
								<a href="index.php?module=MainDispo&view=List&type=agent" title="CallLogs">
									<span class="module-icon"><span class="custom-module" title="agentperformance">AP</span></span>
									<span class="module-name textOverflowEllipsis">Agent Performance</span>
								</a>
							</li> -->

						<!-- 	<li>
								<a href="index.php?module=MainDispo&view=List&type=dialing" title="CallLogs">
									<span class="module-icon"><span class="custom-module" title="dialingstatus">DI</span></span>
									<span class="module-name textOverflowEllipsis">Dialling</span>
								</a>
							</li>

							<li>
								<a href="index.php?module=MainDispo&view=List&type=leads" title="CallLogs">
									<span class="module-icon"><span class="custom-module" title="leadstatus">LS</span></span>
									<span class="module-name textOverflowEllipsis">Leads Status</span>
								</a>
							</li> -->
							
					<!-- 	</ul> -->
					</div>
					<!-- astcrm call reports ends -->
		<!-- 			<div class="menu-item app-item dropdown-toggle" onclick="NewTab()">
						<div class="menu-items-wrapper" style="padding-left: 2px !important;">
							<span class="module-icon"><span class="custom-module" title="dailyReports">DR</span></span>
							<span class="app-name textOverflowEllipsis">DAILY REPORTS</span>
						</div>
					</div> -->
				{/if}
			<!-- astcrm -->
			<!-- astcrm -->
			<div class="app-list-divider"></div>
			{assign var=MAILMANAGER_MODULE_MODEL value=Vtiger_Module_Model::getInstance('MailManager')}
			{if $USER_PRIVILEGES_MODEL->hasModulePermission($MAILMANAGER_MODULE_MODEL->getId())}
				<div class="menu-item app-item app-item-misc" data-default-url="index.php?module=MailManager&view=List">
					<div class="menu-items-wrapper">
						<span class="app-icon-list fa">{$MAILMANAGER_MODULE_MODEL->getModuleIcon()}</span>
						<span class="app-name textOverflowEllipsis"> {vtranslate('MailManager')}</span>
					</div>
				</div>
			{/if}
			{assign var=DOCUMENTS_MODULE_MODEL value=Vtiger_Module_Model::getInstance('Documents')}
			{if $USER_PRIVILEGES_MODEL->hasModulePermission($DOCUMENTS_MODULE_MODEL->getId())}
				<div class="menu-item app-item app-item-misc" data-default-url="index.php?module=Documents&view=List">
					<div class="menu-items-wrapper">
						<span class="app-icon-list fa">{$DOCUMENTS_MODULE_MODEL->getModuleIcon()}</span>
						<span class="app-name textOverflowEllipsis"> {vtranslate('Documents')}</span>
					</div>
				</div>
			{/if}
			{if $USER_MODEL->isAdminUser()}
				{if vtlib_isModuleActive('ExtensionStore')}
					<!-- <div class="menu-item app-item app-item-misc" data-default-url="index.php?module=ExtensionStore&parent=Settings&view=ExtensionStore">
						<div class="menu-items-wrapper">
							<span class="app-icon-list fa fa-shopping-cart"></span>
							<span class="app-name textOverflowEllipsis"> {vtranslate('LBL_EXTENSION_STORE', 'Settings:Vtiger')}</span>
						</div>
					</div> -->
					<div></div>
				{/if}
			{/if}
			{if $USER_MODEL->isAdminUser()}
				<div class="dropdown app-modules-dropdown-container dropdown-compact">
					<div class="menu-item app-item dropdown-toggle app-item-misc" data-app-name="TOOLS" id="TOOLS_modules_dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-default-url="{if $USER_MODEL->isAdminUser()}index.php?module=Vtiger&parent=Settings&view=Index{else}index.php?module=Users&view=Settings{/if}">
						<div class="menu-items-wrapper app-menu-items-wrapper">
							<span class="app-icon-list fa fa-cog"></span>
							<span class="app-name textOverflowEllipsis"> {vtranslate('LBL_SETTINGS', 'Settings:Vtiger')}</span>
							{if $USER_MODEL->isAdminUser()}
								<span class="fa fa-chevron-right pull-right"></span>
							{/if}
						</div>
					</div>
					<ul class="dropdown-menu app-modules-dropdown dropdown-modules-compact" aria-labelledby="{$APP_NAME}_modules_dropdownMenu" data-height="0.27">
						<li>
							<a href="?module=Vtiger&parent=Settings&view=Index">
								<span class="fa fa-cog module-icon"></span>
								<span class="module-name textOverflowEllipsis"> {vtranslate('LBL_CRM_SETTINGS','Vtiger')}</span>
							</a>
						</li>
						<li>
							<a href="?module=Users&parent=Settings&view=List">
								<span class="fa fa-user module-icon"></span>
								<span class="module-name textOverflowEllipsis"> {vtranslate('LBL_MANAGE_USERS','Vtiger')}</span>
							</a>
						</li>
					</ul>
				</div>
			{/if}
		</div>
	</div>
</div>
<!--     <script> 
        function NewTab() { 
            window.open("daily_reports/dailyReport.php", "_blank"); 
        } 
    </script>  -->

    <script>
    	function callreport(){
    		window.location.assign('index.php?module=Reports&view=List')
    	}
    </script>