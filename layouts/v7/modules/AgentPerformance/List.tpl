<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Agent Performance</title>
    <link rel="stylesheet" href="analytics.css">

    <style type="text/css">

    .navbar{
      padding: 0px;
    }

    h6{
        text-align: center;#9c605a
    }

        #col_border{
            margin-top: -15px;
        }

        .order-card {
            color: #fff;
        }

        .bg-c-blue {
            background: linear-gradient(45deg,#a2774a,#a2774a);
        }

        .bg-c-green {
            background: linear-gradient(45deg,#796a8c,#796a8c);
        }

      /*  .bg-c-yellow {
            background: linear-gradient(45deg,#FFB64D,#ffcb80);
        }*/

        .bg-c-pink {
            background: linear-gradient(45deg,#9c605a,#9c605a);
        }


        .card {
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2.94px 0.06px rgba(4,26,55,0.16);
            box-shadow: 0 1px 2.94px 0.06px rgba(4,26,55,0.16);
            border: none;
            margin-bottom: 30px;
            -webkit-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            margin-right: -20px;
            background-color: #ffffff;
        }

        .card .card-block {
            padding: 0px;
        }

        .order-card i {
            font-size: 10px;
        }

        .f-left {
            float: left;
        }

        .f-right {
            float: right;
        }

        #multiLineChart{
  
  border-radius: 6px;
}
    </style>

</head>

<body>

    <div class="wrapper">
       <!-- Page Content  -->
        <div id="content">
            <div class="card" style="margin-top: 10px; margin-bottom: 5px;">
              <div class="card-body">
                <!-- <h6 style="text-align: left; color: #800080;">LEAD PERFORMANCE</h6> -->
                <div class="row" style="padding: 10px;">
                    <div class="col-md-2">
                    <span></span>
                    <label>Date&nbsp;</label>
                    <input type="text" name="closingdate" id="reportrange" class="form-control listSearchContributor inputElement dateField" data-date-format="yyyy-mm-dd" data-calendar-type="range" value="" data-field-type="date" placeholder="choose date">
                    </div>
                    <div class="col-md-2" >
                        <label>Campaign&nbsp;</label>
                        <select name="camapign" id="campaign" class="form-control">
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-primary btn-sm" id="submit_filter" style="margin-bottom: -54px;">Submit</button>
                    </div>
                </div>
              </div>
            </div>

            <div class="row">
                <div class="col-md-2" style="margin-bottom: -20px; margin-right: -5px;">
                    <div class="card bg-c-blue order-card">
                        <div class="card-block">
                            <h6 class="m-b-20 text-center" style="padding-top: 5px;">Total Calls</h6>
                            <h3 class="text-center" style="margin-top: 10px;"><span id="dialed"></span></h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="margin-right: -5px;">
                    <div class="card bg-c-green order-card">
                        <div class="card-block">
                            <h6 class="m-b-20 text-center" style="padding-top: 5px;" >Connected</h6>
                            <h3 class="text-center" style="margin-top: 10px;"><span id="connected"></span></h3>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-2" style="margin-right: -5px;">
                    <div class="card bg-c-pink order-card">
                        <div class="card-block">
                            <h6 class="m-b-20 text-center" style="padding-top: 5px;">Not Connected</h6>
                            <h3 class="text-center" style="margin-top: 10px;"><span id="not_connected"></span></h3>
                        </div>
                    </div>
                </div>
          </div>
	 <br>

            <!-- charts starts here -->
            <div class="row">
                <div class="col-md-6" id="col_border">
                    <div class="card" style="border: solid 3px #e2e2e4;">
                       <div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold; font-family:'OpenSans-Regular', sans-serif;">Agent wise Calls</div>
			 <div class="card-body" id="graph-agent">
                            <canvas id="agent_wise" width="320" height="225"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" id="col_border">
                    <div class="card"style="border: solid 3px #e2e2e4;">
                      <div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold; font-family:'OpenSans-Regular', sans-serif;">Login Time(in minutes)</div>
                        <div class="card-body" id="graph-login">                                
                            <canvas id="login_time" width="320" height="225"</canvas>
                        </div>
                    </div>
                </div>
            </div>
		<br>
	
            <div class="row">
                <div class="col-md-6" id="col_border">
                    <div class="card" style="border: solid 3px #e2e2e4;">
                      <div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold; font-family:'OpenSans-Regular', sans-serif;">Attendance</div>
                        <div class="card-body" id="graph-attendance">
                            <canvas id="agent_attendance" width="320" height="225" ></canvas>
                        </div>
                    </div>
                </div>
               <div class="col-md-6" id="col_border">
                    <div class="card"style="border: solid 3px #e2e2e4;">
                      <div style="background-color:#E5E8E8; font-size:12px; text-align:center; padding:10px;font-weight:Bold; font-family:'OpenSans-Regular', sans-serif;">Calls</div>
                        <div class="card-body" id="graph-calls">                                
                            <canvas id="agent_calls" width="320" height="225"></canvas>
                        </div>
                    </div>
                </div>
            </div>	
	     <br>      
              <div class="card">
                <div class="card-body">
                  <table id="table-id" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%"></table>
                </div>
              </div>
          <div class="card">
          <div class="card-body">
          <table class="table table-striped table-bordered table-sm" width="100%" cellspacing="0">
          <thead style="background-color: #505f6b; color: #ffffff;">
          <tr>
          <th>Report</th>
          <th>Export</th>
          </tr>
          </thead>

          <tbody>
          <tr>
          <td>Agent Login Events</td>
          <td>
          <button type="button" id="agent_login" class="btn btn-warning" data-toggle="tooltip" title=""><i class="fa fa-download " aria-hidden="true">&nbsp;Export</i></button></td>
          </tr>
          </tbody>
          </table>
          </div>
          </div>


        </div>  
          <!--div class="container">
            <br />
            <div class="row">
             
              <div class="col-md-10">

                <canvas id="multiLineChart"></canvas>
              </div>
              
            </div>
          </div-->
    </div>



    
<link rel="stylesheet" type="text/css" href="Chartjs/Chart.min.css">
<script src="Chartjs/Chart.js"></script>
<script src="Chartjs/Chart.min.js"></script>
<script src="Chartjs/Chart.bundle.js"></script>
<script src="Chartjs/Chart.bundle.min.js"></script>
     <script type="text/javascript">
        $(document).ready(function () {


    

        $("#agent_login").click(function(){

          var full_dates = $('#reportrange').val();
             var array = full_dates.split(",");
        var sdate = array[0];
        var edate = array[1];

        window.location.href = 'get_agent_events.php?sdate='+sdate+'&edate='+edate;
         //alert('get_agent_events.php?sdate='+sdate+'&edate='+edate);
      });

          $("#submit_filter").click(function(){  

           // agentwise chart
           document.getElementById("agent_wise").remove(); //canvas
           div = document.querySelector("#graph-agent"); //canvas parent element
           div.insertAdjacentHTML("afterbegin", "<canvas id='agent_wise' height='250' ></canvas>"); //adding the canvas again   

           // login chart
           document.getElementById("login_time").remove(); //canvas
           div = document.querySelector("#graph-login"); //canvas parent element
           div.insertAdjacentHTML("afterbegin", "<canvas id=login_time' height='250' ></canvas>"); //adding the canvas again   

           // attendance chart
           document.getElementById("agent_attendance").remove(); //canvas
           div = document.querySelector("#graph-attendance"); //canvas parent element
           div.insertAdjacentHTML("afterbegin", "<canvas id='agent_attendance' height='270' ></canvas>"); //adding the canvas again   

           // attendance calls
           document.getElementById("agent_calls").remove(); //canvas
           div = document.querySelector("#graph-calls"); //canvas parent element
           div.insertAdjacentHTML("afterbegin", "<canvas id='agent_calls' height='270' ></canvas>"); //adding the canvas again  

                var full_dates = $('#reportrange').val();
                var array = full_dates.split(",");
                var sdate = array[0];
                var edate = array[1];

                var sel_camp = $('#campaign').find(":selected").text();
                if(sel_camp == "ALL"){
                    var camp_sel = "";
                }else{
                    var camp_sel = sel_camp;
                }

                if(full_dates != ''){
                    // fetching graph data
                    $.ajax(
                    {
                        type: "POST",
                        url: "agent_performance_widget.php",
                        dataType:'json',
                        data: {
                          'post_data': 1,
                          'sdate': sdate,
                          'edate': edate,
                          'camp_sel': camp_sel,
                        },
                        success: function(response)
                        {
                          var startDate = sdate;
                          var endDate = edate;

                          $('#dialed').html(response.dial);
                          $('#connected').html(response.connect);
                          $('#not_connected').html(response.notconnect);

                          // yesterday dialed calls connected/not connected
                          var yes_users = response.yes_user;
                          var yes_conn_calls = response.yes_conn_call;
                          var yes_nconn_calls = response.yes_nconn_call;
                          calls_yesterday(yes_users, yes_conn_calls, yes_nconn_calls, startDate, endDate);

                          // login
                          var loginName = response.login_name;
                          var pauseTime = response.pause_time;
                          var breakTime = response.break_time;
                          var talkTime = response.talk_time;
                          pause_break_count(loginName, pauseTime, breakTime, talkTime);

                          // attendance
                          var agentAttendance = response.atten_cnt;
                          var agentDate = response.atten_date;
                          agent_attendance(agentAttendance, agentDate);

                          // agent calls
                          var agent_call = response.call_cnt;
                          var agent_call_date = response.call_date;
                          agent_calls(agent_call, agent_call_date, agentDate);

                            //table variable//
                  var pause_time_hour = response.pause_time_hour;
                  var login_time = response.login_time_hour;
                  var break_time_hour = response.break_time_hour;
                  var talk_time_hour = response.talk_time_hour;

                  //


                                //table display with filter

                  
                    tdata = '<thead style="background-color: #505f6b; color: #ffffff;text-align:center;"> <tr> <th class="th-sm">Name</th> <th class="th-sm">Total Login Time</th> <th class="th-sm">Total Pause Time</th> <th class="th-sm">Total Break Time</th> <th class="th-sm">Total Talk Time</th> <th class="th-sm">Connected Calls</th> <th class="th-sm">Not Connected Calls</th> </tr> </thead> <tbody>';
       
        	
                  for(i in response.login_name){

                  if(response.yes_conn_call[i] === undefined ){
                    var connected_call = 0;
                  }else{
                    var connected_call = response.yes_conn_call[i];
                  }

                  if(response.yes_nconn_call[i] === undefined ){
                    var not_connected_call = 0;
                  }else{
                    var not_connected_call = response.yes_nconn_call[i];
                  }

                  tdata +='<tr><td>'+response.login_name[i]+'</td><td>'+response.login_time_hour[i]+'</td><td>'+response.pause_time_hour[i]+'</td><td>'+response.break_time_hour[i]+'</td><td>'+response.talk_time_hour[i]+'</td><td>'+connected_call+'</td><td>'+not_connected_call+'</td></tr>';

                    } 

                tdata +='</tbody>';
                 document.getElementById('table-id').innerHTML = tdata;
                 //table end with filter

                        }
                    });
                }else{
                  alert('Please select the date');
                  location.reload();
                }
            
            });

            // without filter
            $.ajax(
            {
                type: "POST",
                url: "agent_performance_widget.php",
                dataType:'json',
                data: {
                  'post_data': 0,
                },
                success: function(response)
                {

                  $('#reportrange').val(response.start_dates+','+response.end_dates);
                  var startDate = response.start_dates;
                  var endDate = response.end_dates;
                  
                  $('#dialed').html(response.dial);
                  $('#connected').html(response.connect);
                  $('#not_connected').html(response.notconnect);

                  // yesterday dialed calls connected/not connected
                  var yes_users = response.yes_user;
                  var yes_conn_calls = response.yes_conn_call;
                  var yes_nconn_calls = response.yes_nconn_call;
                  calls_yesterday(yes_users, yes_conn_calls, yes_nconn_calls, startDate, endDate);

                  // login 
                  var loginName = response.login_name;
                  var pauseTime = response.pause_time;
                  var breakTime = response.break_time;
                  var talkTime = response.talk_time;
                  pause_break_count(loginName, pauseTime, breakTime, talkTime);

                  // attendance
                  var agentAttendance = response.atten_cnt;
                  var agentDate = response.atten_date;
                  agent_attendance(agentAttendance,agentDate);

                  // agent calls
                  var agent_call = response.call_cnt;
                  var agent_call_date = response.call_date;
                  agent_calls(agent_call, agent_call_date, agentDate);

                  //alert(response.login_name);

                  //table variable//
                  var pause_time_hour = response.pause_time_hour;
                  var login_time = response.login_time_hour;
                  var break_time_hour = response.break_time_hour;
                  var talk_time_hour = response.talk_time_hour;

                  //

                   //table without filter

                    tdata = '<thead style="background-color: #505f6b; color: #ffffff; text-align:center;"> <tr> <th class="th-sm">Name</th> <th class="th-sm">Total Login Time</th> <th class="th-sm">Total Pause Time</th> <th class="th-sm">Total Break Time</th> <th class="th-sm">Total Talk Time</th> <th class="th-sm">Connected Calls</th> <th class="th-sm">Not Connected Calls</th> </tr> </thead> <tbody>';
       
        	
                  for(i in response.login_name){

                  if(response.yes_conn_call[i] === undefined ){
                    var connected_call = 0;
                  }else{
                    var connected_call = response.yes_conn_call[i];
                  }

                  if(response.yes_nconn_call[i] === undefined ){
                    var not_connected_call = 0;
                  }else{
                    var not_connected_call = response.yes_nconn_call[i];
                  }

                  tdata +='<tr><td>'+response.login_name[i]+'</td><td>'+response.login_time_hour[i]+'</td><td>'+response.pause_time_hour[i]+'</td><td>'+response.break_time_hour[i]+'</td><td>'+response.talk_time_hour[i]+'</td><td>'+connected_call+'</td><td>'+not_connected_call+'</td></tr>';

                    } 

                tdata +='</tbody>';
                 document.getElementById('table-id').innerHTML = tdata;
                 //table end without filter

                }
            });


            var camp;
            $.ajax(
              {
              type: "POST",
              url: "fetch_dropdown.php",
              dataType:'json',
              data: {
                'postdata': 1,
              },
              success: function(response)
              {
                // CAMPAIGN
                camp += '<option value="ALL">ALL</option>';
                for(i in response.campaigns){
                  camp += "<option value='"+response.campaigns[i]+"'>"+response.campaigns[i]+"</option>";
                }
                 document.getElementById('campaign').innerHTML = camp;
                
               }
            });

        });


  function calls_yesterday(yes_users, yes_conn_calls, yes_nconn_calls, startDate, endDate){    

    var ctx = document.getElementById("agent_wise").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: yes_users,
        datasets: [{
          label: 'Not Connected',
          backgroundColor: "#9c605a",
          data: yes_nconn_calls,
        }, {
          label: 'Connected',
          backgroundColor: "#796a8c",
          data: yes_conn_calls,
        }],
      },
      options: {
        title: {
              display: true,
              position: 'top'
                  },
          tooltips: {
            displayColors: true,
            callbacks:{
              mode: 'x',
            },
          },
          scales: {
            xAxes: [{
              stacked: true,
              gridLines: {
                display: false,
              },
		display:true,
	scaleLabel: {
            display: true,
            labelString: 'Agents'
          }
            }],
            yAxes: [{
              stacked: true,
              ticks: {
                beginAtZero: true,
              },
              type: 'linear',
		display:true,
		scaleLabel: {
            display: true,
            labelString: 'Count'
          }

            }]
          },onHover: (event, chartElement) => {
            event.target.style.cursor = chartElement[0] ? 'pointer' : 'default';
          },
          onClick: function (e) {
                var yesterdayAgents = this.getElementsAtEvent(e)[0]._model.label;
                var yesterdayAgentsName = yesterdayAgents.replace(" ", "+");

                window.open(
              'index.php?module=CallLogs&parent=&page=1&view=List&viewname=50&orderby=&sortorder=&app=TOOLS&search_params=%5B%5B%5B"date"%2C"bw"%2C"'+startDate+'%2C'+endDate+'"%5D%2C%5B"assigned_user_id"%2C"c"%2C"'+yesterdayAgentsName+'"%5D%5D%5D&tag_params=%5B%5D&nolistcache=0&list_headers=%5B"date"%2C"time"%2C"destination"%2C"campaign"%2C"dispo"%2C"callback"%2C"source"%2C"assigned_user_id"%5D&tag=',
              '_blank' // <- This is what makes it open in a new window.
            );
          },
          responsive: true,
          maintainAspectRatio: false,
          legend: { position: 'bottom' },
        }
      });

  }

  function pause_break_count(loginName, pauseTime, breakTime, talkTime){
      var ctx = document.getElementById("login_time").getContext('2d');
      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: loginName,
          datasets: [{
            label: 'Talk',
            backgroundColor: "#66a984",
            data: talkTime,
          },{
            label: 'Pause',
            backgroundColor: "#ffd24d",
            data: pauseTime,
          },{
            label: 'Break',
            backgroundColor: "#b19781",
            data: breakTime,
          }],
        },
        options: {
              title: {
                        display: true,
                        text: 'Login Time (in minutes)',
                        position: 'top'
                    },
            tooltips: {
              displayColors: true,
              callbacks:{
                mode: 'x',
              },
            },
            scales: {
              xAxes: [{
                stacked: true,
                gridLines: {
                  display: false,
                },
		    display:true,
	            
scaleLabel: {
        	    display: true,
        	    labelString: 'Agents'
          }

              }],
              yAxes: [{
                stacked: true,
                ticks: {
                  beginAtZero: true,
                },
                type: 'linear',
		display:true,
	        scaleLabel: {
                display: true,
                labelString: 'Count'
          }

              }]
            },
            responsive: true,
            maintainAspectRatio: false,
            legend: { position: 'bottom' },
          }
        });
  }

  function agent_attendance(agentAttendance, agentDate){

      var ctx = document.getElementById("agent_attendance").getContext('2d');
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: agentDate,
          datasets: [{
            fill: false,
            label: 'Attendance',
            backgroundColor: "#859265",
            borderColor:"#859265",
            data: agentAttendance,
          }],
        },
        options: {
              title: {
                        display: true,
                        position: 'top'
                    },
            tooltips: {
              displayColors: true,
              callbacks:{
                mode: 'x',
              },
            },
            scales: {
              xAxes: [{
                stacked: true,
                gridLines: {
                  display: false,
                },
	        scaleLabel: {
                display: true,
                labelString: 'Date'
          }

              }],
              yAxes: [{
                stacked: true,
                ticks: {
                  beginAtZero: true,
                },
                type: 'linear',
		  scaleLabel: {
                display: true,
                labelString: 'Count'
          }

              }]
            },
            responsive: true,
            maintainAspectRatio: false,
            legend: { position: 'bottom' },
          }
        });

  }

  function agent_calls(agent_call, agent_call_date, agentDate){

      var ctx = document.getElementById("agent_calls").getContext('2d');
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: agent_call_date,
          datasets: [{
            fill: false,
            label: 'Calls',
            backgroundColor: "#1DE9B6",
            borderColor: "#1DE9B6",
            data: agent_call,
          }],
        },
        options: {
              title: {
                        display: true,
                        position: 'top'
                    },
            tooltips: {
              displayColors: true,
              callbacks:{
                mode: 'x',
              },
            },
            scales: {
              xAxes: [{
                stacked: true,
                gridLines: {
                  display: false,
                },
		display:true,
                scaleLabel: {
                display: true,
                labelString: 'Date'
          }
              }],
              yAxes: [{
                stacked: true,
                ticks: {
                  beginAtZero: true,
                },
                type: 'linear',
		display:true,
                scaleLabel: {
                display: true,
                labelString: 'Count'
          }

              }]
            },
            responsive: true,
            maintainAspectRatio: false,
            legend: { position: 'bottom' },
          }
        });
  }
	
//getLastSevenDayMeeting();
	//----------last day meeting graph------------//
  function getLastSevenDayMeeting(){
//	alert('Stack');
   // var result = JSON.parse(res);

    //var last_date = result.last_date;
    // console.log(result);
    //var getlastdaytickets = result.lastDayTickets;

    //console.log(getlastdaytickets);

    //accessing key 
    //var username = Object.keys(getlastdaytickets);
   
    //accessing values
    //var status = Object.values(getlastdaytickets);

   var early = [1,0,0,9,7,6,9,8];
    var mrg = [13,10,24,34,56,23,15,2];
    var aft = [5,12,24,35,23,12,7,8];
	var night = [12,34,21,22,11,4,2,7];
   /* for(var i = 0;i<status.length; i++){

        Open.push(status[i].Open);
        Closed.push(status[i].Closed);
        InProgress.push(status[i].InProgress);
        
    }*/

      var ctx = document.getElementById("multiLineChart");
      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ["7406273311", "7259433006", "9880657187", "7406273335","7406273336","7406273330","7406273332","7406273324"],
          datasets: [{
            label: '00 AM - 9 AM',
            backgroundColor: "#CDDD36",
            data: early,
          },{
            label: '9 AM - 1 PM',
            backgroundColor: "#61B146",
            data: mrg,
          },{
            label: '1 PM - 6 PM',
            backgroundColor: "#1595CD",
            data: aft,
          },{
            label: '6 PM - 12 PM',
            backgroundColor: "#370b54",
            data: night,
          }],
        },
        options: {
              title: {
                        display: true,
                        text:"Agent",
                        position: 'top'
                    },
            tooltips: {
              displayColors: true,
              callbacks:{
                mode: 'x',
              },
            },
            scales: {
              xAxes: [{
                stacked: true,
                gridLines: {
                  display: false,
                }
              }],
              yAxes: [{
                stacked: true,
                ticks: {
                  beginAtZero: true,
                },
                type: 'linear',
              }]
            },
            responsive: true,
            maintainAspectRatio: false,
            legend: { position: 'right' },
          }
        });
  }


    </script>

</body>

</html>
