{*+**********************************************************************************
* The contents of this file are subject to the vtiger CRM Public License Version 1.1
* ("License"); You may not use this file except in compliance with the License
* The Original Code is: vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
************************************************************************************}

<br>
<center>
	<footer class="noprint">
		<div class="vtFooter">
			<p>
				
			astCRM | Powered by astCRM Systems Private Limited&nbsp;
			&copy; {date('Y')}&nbsp&nbsp;|&nbsp;&nbsp;
				<a href="http://www.astcrm.com" target="_blank">www.astCRM.com</a>
			</p>
		</div>
	</footer>
</center>
{include file='JSResources.tpl'|@vtemplate_path}
</div>
