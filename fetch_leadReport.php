<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$con = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($con,$dbName);

if ($con->connect_error) {
 die("Connection failed: " . $con->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
$cur_date = date("Y-m-d");

if($_POST['post_filter']){

	$campaign = $_POST['camp_select'];
	$date_sub = $_POST['date'];
	$name = $_POST['name'];
	$phone = $_POST['number'];
	$cname = $_POST['cname'];
	$cnumber = $_POST['cnumber'];
	$main_disp = $_POST['main_selected'];
	$sub = $_POST['sub_selected'];
	$status_dial = $_POST['status_selected'];
	$status_assign = $_POST['asn_status_selected'];

	if($status_dial == ''){
		$dial_status = "";
	}else{
		if($status_dial == 'Connected'){
			$dial_status = 1;
		}else{
			$dial_status = 2;
		}
	}

	if($status_assign == ''){
		$assign_statuss = "";

	}elseif($status_assign == 'Assigned'){
		$assign_statuss = "AND vtiger_campaigncontrel.ast_update = '1' ";
	}else{
		$assign_statuss = "AND vtiger_campaigncontrel.ast_update = '0' ";
	}

	if($campaign != 'ALL'){
		$camp = " AND vtiger_campaign.campaignname LIKE '%".$campaign."%' " ;
		$camp_man = " AND campaign LIKE '%".$campaign."%' " ;
	}else{
		$camp = "" ;
		$camp_man = "";
	}

	if($date_sub != ''){
		$dates = "AND modify_date LIKE '".$date_sub."%'  " ;
	}else{
		$dates = "" ;
	}

	if($name != ''){
		$names = "AND CONCAT(first_name, ' ', last_name) LIKE '%".$name."%'  " ;
	}else{
		$names = "" ;
	}

	if($phone != ''){
		$phoneno = "AND vtiger_campaigncontrel.user LIKE '%".$phone."%'  " ;
	}else{
		$phoneno = "" ;
	}

	if($cname != ''){
		$cnames = "AND customer_name LIKE '%".$cname."%'  " ;
	}else{
		$cnames = "" ;
	}

	if($cnumber != ''){
		$cnumbers = "AND mobile LIKE '%".$cnumber."%'  " ;
	}else{
		$cnumbers = "" ;
	}

	if($main_disp != ''){
		$main_disps = "AND vtiger_campaigncontrel.dispo LIKE '%".$main_disp."%'  " ;
	}else{
		$main_disps = "" ;
	}

	if($sub != ''){
		$subs = "AND vtiger_campaigncontrel.subdispo LIKE '%".$sub."%'  " ;
	}else{
		$subs = "" ;
	}

	if($dial_status != ''){
		$dial_statuss = "AND vtiger_campaigncontrel.status LIKE '%".$dial_status."%'  " ;
	}else{
		$dial_statuss = "" ;
	}

	// count total
	$sql_total=mysqli_query($con, "SELECT count(*) AS total_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$camp." ");
	$cnt_total = mysqli_fetch_assoc($sql_total);
	$total_leads = $cnt_total['total_cnt'];

	//count dialed
	$count_dial = mysqli_query($con, "SELECT count(*) AS dial_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='1' AND vtiger_campaigncontrel.status NOT IN ('') INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$camp." ");
	$cnt_dial = mysqli_fetch_assoc($count_dial);
	$dial_leads = $cnt_dial['dial_cnt'];

	$not_dialed = $total_leads-$dial_leads;

	if ($not_dialed == '') {
		$not_dialed = 0;
	}

	// assigned
	$assign_sql= mysqli_query($con, "SELECT count(*) AS asn_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='1'  INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$camp." ") ;
	$cnt_assign = mysqli_fetch_assoc($assign_sql);
	$assign_leads = $cnt_assign['asn_cnt'];

	// unassigned
	$unassign_sql= mysqli_query($con, "SELECT count(*) AS unasn_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='0'  INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$camp." ") ;
	$cnt_unassign  = mysqli_fetch_assoc($unassign_sql);
	$unassign_leads = $cnt_unassign['unasn_cnt'];

	// manual dial
	$sel_manualdial = mysqli_query($con, "SELECT count(*) AS manl_cnt FROM campaign_dial_status WHERE dial_id='-1' AND status = '1' ".$camp_man." ");	
	$cnt_manual = mysqli_fetch_assoc($sel_manualdial);
	$manual_dialed = $cnt_manual['manl_cnt'];

	$lead_report = mysqli_query($con, "SELECT  vtiger_campaigncontrel.modify_date, vtiger_campaigncontrel.user,  vtiger_campaigncontrel.customer_name ,vtiger_campaigncontrel.dispo,vtiger_campaigncontrel.subdispo,vtiger_campaigncontrel.status,vtiger_campaigncontrel.ast_update,vtiger_campaigncontrel.comments,vtiger_contactdetails.firstname,vtiger_contactdetails.lastname,vtiger_contactdetails.mobile,vtiger_users.first_name as first_user,vtiger_users.last_name as last_user,vtiger_campaigncontrel.status  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' LEFT JOIN vtiger_contactdetails ON vtiger_contactdetails.contactid=vtiger_campaigncontrel.contactid LEFT JOIN vtiger_users ON vtiger_users.user_name = vtiger_campaigncontrel.user INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$camp.$dates.$names.$phoneno.$cnames.$cnumbers.$main_disps.$subs.$dial_statuss.$assign_statuss." ") ;

}else{

	$campaign = $_POST['camp_selected'];

	if($campaign != 'ALL'){
		$camp = " AND vtiger_campaign.campaignname LIKE '%".$campaign."%' " ;
		$camp_man = " AND campaign LIKE '%".$campaign."%' " ;
	}else{
		$camp = "" ;
		$camp_man = "";
	}

	// count total
	$sql_total=mysqli_query($con, "SELECT count(*) AS total_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$camp." ");
	$cnt_total = mysqli_fetch_assoc($sql_total);
	$total_leads = $cnt_total['total_cnt'];

	//count dialed
	$count_dial = mysqli_query($con, "SELECT count(*) AS dial_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='1' AND vtiger_campaigncontrel.status NOT IN ('') INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$camp." ");
	$cnt_dial = mysqli_fetch_assoc($count_dial);
	$dial_leads = $cnt_dial['dial_cnt'];

	$not_dialed = $total_leads-$dial_leads;

	if ($not_dialed == '') {
		$not_dialed = 0;
	}

	// assigned
	$assign_sql= mysqli_query($con, "SELECT count(*) AS asn_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='1'  INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$camp." ") ;
	$cnt_assign = mysqli_fetch_assoc($assign_sql);
	$assign_leads = $cnt_assign['asn_cnt'];

	// unassigned
	$unassign_sql= mysqli_query($con, "SELECT count(*) AS unasn_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='0'  INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$camp." ") ;
	$cnt_unassign  = mysqli_fetch_assoc($unassign_sql);
	$unassign_leads = $cnt_unassign['unasn_cnt'];

	// manual dial
	$sel_manualdial = mysqli_query($con, "SELECT count(*) AS manl_cnt FROM campaign_dial_status WHERE dial_id='-1' AND status = '1' ".$camp_man." ");	
	$cnt_manual = mysqli_fetch_assoc($sel_manualdial);
	$manual_dialed = $cnt_manual['manl_cnt'];

	$lead_report = mysqli_query($con, "SELECT  vtiger_campaigncontrel.modify_date, vtiger_campaigncontrel.user,  vtiger_campaigncontrel.customer_name ,vtiger_campaigncontrel.dispo,vtiger_campaigncontrel.subdispo,vtiger_campaigncontrel.status,vtiger_campaigncontrel.ast_update,vtiger_campaigncontrel.comments,vtiger_contactdetails.firstname,vtiger_contactdetails.lastname,vtiger_contactdetails.mobile,vtiger_users.first_name as first_user,vtiger_users.last_name as last_user,vtiger_campaigncontrel.status  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' LEFT JOIN vtiger_contactdetails ON vtiger_contactdetails.contactid=vtiger_campaigncontrel.contactid LEFT JOIN vtiger_users ON vtiger_users.user_name = vtiger_campaigncontrel.user INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$camp." ") ;
		
}

while($row_report = mysqli_fetch_assoc($lead_report)){

	$split_date = explode(' ', $row_report['modify_date']);
	$lead_date[] = $split_date[0];

	$user_name[] = $row_report['first_user'].' '.$row_report['last_user'];
	$user_number[] = $row_report['user'];
	$customers_name[] = $row_report['customer_name'];
	$customers_mobile[] = $row_report['mobile']; 
	
	if($row_report['status'] == 1){
		$call_status[] = 'Connected';
	}else{
		$call_status[] = 'Not Connected';
	}

	$main_dispo[] = $row_report['dispo'];
	$sub_dispo[] = $row_report['subdispo'];

	if($row_report['ast_update'] == 1){
		$assign_status[] = 'Assigned';
	}else{
		$assign_status[] = 'Unassigned';
	}
	
	$comments[] = $row_report['comments'];

	}

	$realtime_contents = array(
		"test"=>$camp,
		"total_leads"=>$total_leads,
		"dial_leads"=>$dial_leads,
		"assign_leads"=>$assign_leads,
		"unassign_leads"=>$unassign_leads,
		"not_dialed"=>$not_dialed,
		"manual_dialed"=>$manual_dialed,

		"lead_date"=>$lead_date,
		"user_name"=>$user_name,
		"user_number"=>$user_number,
		"customers_name"=>$customers_name,
		"customers_mobile"=>$customers_mobile,
		"call_status"=>$call_status,
		"main_dispo"=>$main_dispo,
		"sub_dispo"=>$sub_dispo,
		"assign_status"=>$assign_status,
		"comments"=>$comments,

		);
	echo json_encode($realtime_contents);

?>