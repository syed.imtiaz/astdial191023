<?php
include 'config.inc.php';
$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$database   = $dbconfig['db_name'];
$conn = new mysqli($hostname, $username, $password, $database);
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

date_default_timezone_set("Asia/Calcutta");
$date_today = date('Y-m-d');

$skip_rows = 30;
$deleted = 0;
global $conn;

// skip first '$skip_rows' rows
$query_lists = $conn->query("SELECT cvid FROM vtiger_customview WHERE entitytype = 'Contacts' AND viewname not like '%Null%' AND viewname != 'All' ORDER BY cvid DESC LIMIT 18446744073709551615 OFFSET ".$skip_rows);
if(($query_lists->num_rows) > 0){
	while($res_lists = $query_lists->fetch_assoc()){
		$list_id = $res_lists['cvid'];
		$deleted = deleteLists($list_id,$deleted);
	}
}

function deleteLists($list_id,$deleted){
	global $conn;
	$where_clause = "WHERE cvid = '$list_id' ";

	$del_customview = "DELETE FROM vtiger_customview ".$where_clause;
	if($conn->query($del_customview) === TRUE){

		$del_cvadvfilter_grouping = "DELETE FROM vtiger_cvadvfilter_grouping ".$where_clause;
		$conn->query($del_cvadvfilter_grouping);

		$del_cvcolumnlist = "DELETE FROM vtiger_cvcolumnlist ".$where_clause;
		$conn->query($del_cvcolumnlist);

		$del_cvadvfilter = "DELETE FROM vtiger_cvadvfilter ".$where_clause;
		$conn->query($del_cvadvfilter);
		$deleted++;
	}
	return $deleted;
}

echo '<br>deleted: '.$deleted.' rows';
?>