<?php
$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('Contacts');
$callLogsBlock = Vtiger_Block::getInstance('LBL_CONTACT_INFORMATION', $module);

$contact1 = Vtiger_Field::getInstance('info1', $module);
if (!$contact1) {
    $contact1 = new Vtiger_Field();
    $contact1->name = 'info1';
    $contact1->label = 'Info 1';
    $contact1->table = $module->basetable;
    $contact1->column = 'info1';
    $contact1->columntype = 'varchar(255)';
    $contact1->uitype = 2;
    $contact1->typeofdata = 'V~O';
    $callLogsBlock->addField($contact1);
     //$contact->setPicklistValues(array('Connected','Not Connected'));
    //$contact->setRelatedModules(array('Contacts'));
}


$contact2 = Vtiger_Field::getInstance('info2', $module);
if (!$contact2) {
    $contact2 = new Vtiger_Field();
    $contact2->name = 'info2';
    $contact2->label = 'Info 2';
    $contact2->table = $module->basetable;
    $contact2->column = 'info2';
    $contact2->columntype = 'varchar(255)';
    $contact2->uitype = 2;
    $contact2->typeofdata = 'V~O';
    $callLogsBlock->addField($contact2);
     //$contact->setPicklistValues(array('Connected','Not Connected'));
    //$contact->setRelatedModules(array('Contacts'));
}

$contact3 = Vtiger_Field::getInstance('info3', $module);
if (!$contact3) {
    $contact3 = new Vtiger_Field();
    $contact3->name = 'info3';
    $contact3->label = 'Info 3';
    $contact3->table = $module->basetable;
    $contact3->column = 'info3';
    $contact3->columntype = 'varchar(255)';
    $contact3->uitype = 2;
    $contact3->typeofdata = 'V~O';
    $callLogsBlock->addField($contact3);
     //$contact->setPicklistValues(array('Connected','Not Connected'));
    //$contact->setRelatedModules(array('Contacts'));
}