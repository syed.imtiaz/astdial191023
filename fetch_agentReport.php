<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$con = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($con,$dbName);

if ($con->connect_error) {
 die("Connection failed: " . $con->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
$cur_date = date("Y-m-d");

	$campaign = $_POST['camp_select'];
	$start = $_POST['start'];
	$end = $_POST['end'];

	if($campaign != "ALL"){
		$camp = "AND a.campaign_logged_in LIKE '%".$campaign."%'  " ;
	}else{
		$camp = "" ;
	}

	if($start != '' && $end !=''){
		$dates = "AND DATE(datetime) >= '".$start."' AND DATE(datetime) <= '".$end."'  " ;
		$dates_user = "AND DATE(modify_date) >='".$start."' AND DATE(modify_date) <='".$end."'  " ;
	}else{

		$dates = "AND DATE(datetime) >= '".$cur_date."' AND DATE(datetime) <= '".$cur_date."'  " ;
		$dates_user = "AND DATE(modify_date) >='".$cur_date."' AND DATE(modify_date) <='".$cur_date."'  " ;
	}

	// $fetch_report = mysqli_query($con, "SELECT userid,SUM(login_time),SUM(pause_time),SUM(break_time),SUM( talk_time) FROM user_callsummary WHERE userid != '' ".$camp.$dates." GROUP BY(userid) ORDER BY DATE(datetime)");
	$fetch_report = mysqli_query($con, "SELECT a.userid,SUM(a.login_time),SUM(a.pause_time),SUM(a.break_time),SUM(a.talk_time) FROM user_callsummary a LEFT JOIN vtiger_users b ON a.userid = b.user_name WHERE a.userid != '' ".$camp.$dates." AND b.status = 'Active' GROUP BY(a.userid) ORDER BY DATE(datetime)");

	$count = 0;
	while($row = mysqli_fetch_array($fetch_report)){
		$count++;
		
		$diff = $row[1];
		//calculate login time
		$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
		$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
		$login_time[] = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);

		$pause_time = $row[2];
		//calculate pause time
		$hours   = floor(($pause_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts  = floor(($pause_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
		$seconds = floor(($pause_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
		$pause_time_hour[] = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);

		$break_time = $row[3];
		//calculate break time
		$hours   = floor(($break_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts  = floor(($break_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
		$seconds = floor(($break_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
		$break_time_hour[] = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);
  
		$talk_time =  $row[4];
		//calculate talk time
		$hours   = floor(($talk_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts  = floor(($talk_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
		$seconds = floor(($talk_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
		$talk_time_hour[] = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);

		//total calls
	    //sucess calls
		$user = $row[0];
		$sql_sucess = "SELECT count(*) from campaign_dial_status where user = '".$user."' " .$dates_user." AND status = '1' ";
		$query_sucess = mysqli_query($con,$sql_sucess);
		$result_sucess = mysqli_fetch_array($query_sucess);
		$sucess_calls[] = $result_sucess[0];
		//end success

		//unsucess calls
		$sql_unsucess = "SELECT count(*) from campaign_dial_status where user = '".$user."' ".$dates_user."  AND status = '2' ";
		$query_unsucess = mysqli_query($con,$sql_unsucess);
		$result_unsucess = mysqli_fetch_array($query_unsucess);
		$unsucess_calls[] = $result_unsucess[0];
		//end unsucess
	   //end totalcalls

		//display name
	   	$get_name = "SELECT first_name,last_name from vtiger_users where user_name = '".$row[0]."' ";
		$query_name = mysqli_query($con,$get_name);
		$name = mysqli_fetch_array($query_name);
		$display_name[] = $name[0]." ".$name[1];
	   //end display name

		}

	$realtime_contents = array(
		"name"=>$display_name,
		"login_time"=>$login_time,
		"pause_time"=>$pause_time_hour,
		"break_time"=>$break_time_hour,
		"talk_time"=>$talk_time_hour,
		"success_calls"=>$sucess_calls,
		"fail_calls"=>$unsucess_calls,
		"count"=>$count,
		);
	echo json_encode($realtime_contents);

?>