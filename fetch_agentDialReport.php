<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$con = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($con,$dbName);

if ($con->connect_error) {
 die("Connection failed: " . $con->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
$cur_date = date("Y-m-d");

	$campaign = $_POST['camp_select'];
	$start = $_POST['start'];
	$end = $_POST['end'];

	$total_dial =0;
	$total_connect =0;
	$total_notconnect =0;

	if($campaign != "ALL"){
		$camp = "AND campaign LIKE '%".$campaign."%'  " ;
	}else{
		$camp = "" ;
	}

	if($start != '' && $end !=''){
		$dates_user = "AND DATE(modify_date) >='".$start."' AND DATE(modify_date) <='".$end."'  " ;
	}else{

		$dates_user = "" ;
	}

	$select_dial = "SELECT count(*) from campaign_dial_status where status!='' ".$camp.$dates_user." ";
	$query_dial = mysqli_query($con,$select_dial);			
	$row_dial = mysqli_fetch_array($query_dial);
	$dial = $row_dial[0];

	//connect
	$select_connect = "SELECT count(*) from campaign_dial_status where status='1' ".$camp.$dates_user." ";
	$query_connect = mysqli_query($con,$select_connect);
	$row_connect = mysqli_fetch_array($query_connect);
	$connect = $row_connect[0];

	//notconnect
	$select_notconnect = "SELECT count(*) from campaign_dial_status where status='2' ".$camp.$dates_user." ";
	$query_notconnect = mysqli_query($con,$select_notconnect);
	$row_notconnect = mysqli_fetch_array($query_notconnect);
	$notconnect = $row_notconnect[0];

	$realtime_contents = array(
		"campaign"=>$campaign,
		"dial"=>$dial,
		"connect"=>$connect,
		"notconnect"=>$notconnect,
		);
	echo json_encode($realtime_contents);

?>