<?php
$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('CallLogs');
$callLogsBlock = Vtiger_Block::getInstance('LBL_CALLLOGS_INFORMATION', $module);

//Contact field creation

$contact1 = Vtiger_Field::getInstance('system_status', $module);
if (!$contact1) {
    $contact1 = new Vtiger_Field();
    $contact1->name = 'system_status';
    $contact1->label = 'System Status';
    $contact1->table = $module->basetable;
    $contact1->column = 'system_status';
    $contact1->columntype = 'varchar(255)';
    $contact1->uitype = 2;
    $contact1->typeofdata = 'V~O';
    $callLogsBlock->addField($contact1);
     //$contact->setPicklistValues(array('Connected','Not Connected'));
    //$contact->setRelatedModules(array('Contacts'));
}


/*$contact2 = Vtiger_Field::getInstance('end_time', $module);
if (!$contact2) {
    $contact2 = new Vtiger_Field();
    $contact2->name = 'end_time';
    $contact2->label = 'End Time';
    $contact2->table = $module->basetable;
    $contact2->column = 'end_time';
    $contact2->columntype = 'datetime';
    $contact2->uitype = 70;
    $contact2->typeofdata = 'DT~O';
    $callLogsBlock->addField($contact2);
     //$contact->setPicklistValues(array('Connected','Not Connected'));
    //$contact->setRelatedModules(array('Contacts'));
}

$contact3 = Vtiger_Field::getInstance('time_duration', $module);
if (!$contact3) {
    $contact3 = new Vtiger_Field();
    $contact3->name = 'time_duration';
    $contact3->label = 'Time Duration';
    $contact3->table = $module->basetable;
    $contact3->column = 'time_duration';
    $contact3->columntype = 'time';
    $contact3->uitype = 14;
    $contact3->typeofdata = 'T~O';
    $callLogsBlock->addField($contact3);
     //$contact->setPicklistValues(array('Connected','Not Connected'));
    //$contact->setRelatedModules(array('Contacts'));
}


 $contact2 = Vtiger_Field::getInstance('cb_date', $module);
// if (!$contact2) {
//     $contact2 = new Vtiger_Field();
//     $contact2->name = 'cb_date';
//     $contact2->label = 'CB Date';
//     $contact2->table = $module->basetable;
//     $contact2->column = 'cb_date';
//     $contact2->columntype = 'varchar(255)';
//     $contact2->uitype = 2;
//     $contact2->typeofdata = 'D~O';
//     $callLogsBlock->addField($contact2);
//      //$contact->setPicklistValues(array('Connected','Not Connected'));
//     //$contact->setRelatedModules(array('Contacts'));
// }

// $contact3 = Vtiger_Field::getInstance('cb_time', $module);
// if (!$contact3) {
//     $contact3 = new Vtiger_Field();
//     $contact3->name = 'cb_time';
//     $contact3->label = 'CB Time';
//     $contact3->table = $module->basetable;
//     $contact3->column = 'cb_time';
//     $contact3->columntype = 'varchar(255)';
//     $contact3->uitype = 2;
//     $contact3->typeofdata = 'T~O';
//     $callLogsBlock->addField($contact3);
//      //$contact->setPicklistValues(array('Connected','Not Connected'));
//     //$contact->setRelatedModules(array('Contacts'));
// }

// $contact4 = Vtiger_Field::getInstance('manual_dial', $module);
// if (!$contact4) {
//     $contact4 = new Vtiger_Field();
//     $contact4->name = 'manual_dial';
//     $contact4->label = 'Manual Dial';
//     $contact4->table = $module->basetable;
//     $contact4->column = 'manual_dial';
//     $contact4->columntype = 'varchar(255)';
//     $contact4->uitype = 56;
//     $contact4->typeofdata = 'C~O';
//     $callLogsBlock->addField($contact4);
//      //$contact->setPicklistValues(array('Connected','Not Connected'));
//     //$contact->setRelatedModules(array('Contacts'));
// }*/

