<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$conn = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($conn,$dbName);

if ($conn->connect_error) {
 die("Connection failed: " . $conn->connect_error);
}else{
	 // echo "connected";
} 

// date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
// $cur_date = date("Y-m-d");


$filename = "Agents_event" . date('Y-m-d') . ".csv"; 
$delimiter = ","; 
 
// Create a file pointer 
$f = fopen('php://memory', 'w'); 
 
// Set column headers 
$fields = array('Date','Time', 'UserName', 'Event'); 
fputcsv($f, $fields, $delimiter); 


// $start_date = '2021-02-10';
// $end_date = '2021-02-25';

$start_date = $_GET['sdate'];
$end_date = $_GET['edate'];

$s_date = date("Y-m-d", strtotime($start_date));
$e_date = date("Y-m-d", strtotime($end_date));
 
// Get records from the database 
// $result = $conn->query("SELECT `username` , `ddate` , MIN( `time` ) AS first_login_in, MAX( `time` ) AS last_log_out FROM vtiger_location_route WHERE ddate <= '$e_date' AND ddate >= '$s_date' GROUP BY `phone` , `ddate` ORDER BY `id` DESC"); 
// $sql = $conn->query("SELECT `campaign_logged_in`,`current_eventtime`,`userid`,`currentevent` FROM `user_callsummary` WHERE Date(`datetime`) <= '$e_date' AND DATE(`datetime`) >= '$s_date' AND currentevent != 'DIAL'" );

$sql = $conn->query("SELECT current,activity,reference FROM camapign_activity_log WHERE Date(`current`) <= '$e_date' AND DATE(`current`) >= '$s_date' AND activity != 'CAMPAIGN' AND activity != 'getdispositions' AND activity != 'getting contacts' AND activity != 'getcampaigns' AND activity != 'UpdateContacts' AND activity != 'DIAL' AND activity != 'userdispo'");


if($sql->num_rows > 0){ 
    // Output each row of the data, format line as csv and write to file pointer 
    while($row = $sql->fetch_assoc()){ 

        $u_id = $row['reference'];
        $event = $row['activity'];
        $dt=explode(' ',$row['current']);
        $date=$dt[0];
        $time=$dt[1];


      

        $sql_get_username = "SELECT CONCAT(first_name,' ',last_name) AS name FROM vtiger_users WHERE `user_name` = '$u_id'";
        $res = $conn->query($sql_get_username);
        while($row_name = $res->fetch_assoc()){
            $name = $row_name['name'];
        }

        
        $lineData = array($date, $time, $name, $row['activity']); 
        
        
        fputcsv($f, $lineData, $delimiter); 
    } 
} 
 
// Move back to beginning of file 
fseek($f, 0); 
 
// Set headers to download file rather than displayed 
header('Content-Type: text/csv'); 
header('Content-Disposition: attachment; filename="' . $filename . '";'); 
 
// Output all remaining data on a file pointer 
fpassthru($f); 
 
// Exit from file 
exit();



?>