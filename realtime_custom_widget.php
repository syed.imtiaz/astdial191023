<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$conn = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($conn,$dbName);

if ($conn->connect_error) {
 die("Connection failed: " . $conn->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
$today = date('Y-m-d');



	if($_POST['post_data'] == 1){
		$post = '1';
		$campaign = $_POST['camp_selected'];
			if($campaign != 'ALL'){
                    if($campaign != ''){
				$campcondition = " AND campaign LIKE '%".$campaign."%' " ;
				$campaignname = " AND vtiger_campaign.campaignname LIKE '%".$campaign."%' " ;
				$camp_logged_condition = " AND campaign_logged_in LIKE '%".$campaign."%' " ;
			}else{
				$campcondition = "";
				$campaignname = "";
				$camp_logged_condition = "" ;
			}
                    
                }

	}else{
		$post = '0';
		$campcondition = "";
		$campaignname = "";
		$camp_logged_condition = "" ;
	}


// FETCH Today DISPOSITION
$query_dispoCount = mysqli_query($conn, "SELECT COUNT(dispo) as total_dispo, dispo FROM vtiger_calllogs WHERE date = '".$today."' ".$campcondition." AND dispo <> '' GROUP BY dispo ORDER BY date asc");
while($total_dispoCount = mysqli_fetch_assoc($query_dispoCount)) {
	$chart_dispo[] = $total_dispoCount['total_dispo'];
	$chart_dispo_status[] = $total_dispoCount['dispo'];
}

// FETCH Today Callduration
$qry_thirty = mysqli_query($conn, "SELECT COUNT(*) as total30 FROM `campaign_dial_status` WHERE `duration`<=30 and DATE(`modify_date`)='$today' ".$campcondition." ");
	$row_thirty = mysqli_fetch_assoc($qry_thirty);
	$res_thirty = $row_thirty['total30'];

	$qry_sixty = mysqli_query($conn, "SELECT COUNT(*) as total60 FROM `campaign_dial_status` WHERE `duration`>30 and `duration`<=60 and DATE(`modify_date`)='$today' ".$campcondition."");
	$row_sixty = mysqli_fetch_assoc($qry_sixty);
	$res_sixty = $row_sixty['total60'];

	$qry_one_twenty = mysqli_query($conn, "SELECT COUNT(*) as total120 FROM `campaign_dial_status` WHERE `duration`>60 and `duration`<=120 and DATE(`modify_date`)='$today' ".$campcondition."");
	$row_one_twenty = mysqli_fetch_assoc($qry_one_twenty);
	$res_one_twenty = $row_one_twenty['total120'];

	$qry_three_hundred = mysqli_query($conn, "SELECT COUNT(*) as total300 FROM `campaign_dial_status` WHERE `duration`>120 and `duration`<=300 and DATE(`modify_date`)='$today' ".$campcondition."");
	$row_three_hundred = mysqli_fetch_assoc($qry_three_hundred);
	$res_three_hundred = $row_three_hundred['total300'];

	$qry_three_hundred_more = mysqli_query($conn, "SELECT COUNT(*) as total300_greater FROM `campaign_dial_status` WHERE `duration`>300 and DATE(`modify_date`)='$today' ".$campcondition."");
	$row_three_hundred_more = mysqli_fetch_assoc($qry_three_hundred_more);
	$res_three_hundred_more = $row_three_hundred_more['total300_greater'];

         $callduration_status= array("<=30 Sec",">30 and <=60 Sec",">60 Sec and <=2 Min",">2 Min and <=5 Min",">5 Min");
         //$callduration_status= array('1','2','3','4','5');
         $callduration_count=array($res_thirty,$res_sixty,$res_one_twenty,$res_three_hundred,$res_three_hundred_more);

      //No of calls by hour (Time distribution)

      $query_timedisrtibution = mysqli_query($conn,"select count(*) as totalcalls,HOUR(time) as hours from vtiger_calllogs as a join vtiger_crmentity as b on (b.crmid=a.calllogsid) where date = '$today' ".$campcondition." and b.deleted=0 group by HOUR(time)");
       while($row_timedisrtibution = mysqli_fetch_assoc($query_timedisrtibution)){

		$timedisrtibution_hours[] = $row_timedisrtibution['hours'];
		if($row_timedisrtibution['totalcalls'] > 0){
			$timedisrtibution_totalcalls[] = $row_timedisrtibution['totalcalls'];
		}
		
	}
        $initialhrs_xaxis = array('00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00');
        $initialhrs = array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
        $intialcalls = array('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');
        $initialdata = array_combine($initialhrs, $intialcalls);
        $finaldata = array_combine($timedisrtibution_hours, $timedisrtibution_totalcalls);
        $mergedata = array_replace($initialdata,$finaldata);
        $final_timedistribution_values = array_values($mergedata);


        $query_totaltalk = mysqli_query($conn,"select sum(duration) as duration,HOUR(modify_date) as hours FROM `campaign_dial_status` WHERE  DATE(`modify_date`)='$today' ".$campcondition." and dispo != 'NO Contact' group by HOUR(modify_date)");
        while($row_totaltalk = mysqli_fetch_assoc($query_totaltalk)){

		$totaltalk_hours[] = $row_totaltalk['hours'];
		$totaltalk_duration[] = $row_totaltalk['duration'];
	}

        $initialdata_totaltalk = array_combine($initialhrs, $intialcalls);
        $finaldata_totaltalk = array_combine($totaltalk_hours, $totaltalk_duration);
        $mergedata_totaltalk = array_replace($initialdata_totaltalk,$finaldata_totaltalk);
        $final_totaltalk_values = array_values($mergedata_totaltalk);

        //LoginActivity
        $query_login = mysqli_query($conn,"select count(*) as logincount ,HOUR(first_login) as hours from user_callsummary where DATE(first_login) = '$today' ".$camp_logged_condition." group by HOUR(first_login)");
        while($row_login = mysqli_fetch_assoc($query_login)){

		$login_hours[] = $row_login['hours'];
		$login_count[] = $row_login['logincount'];
	}
        
        $initialdata_login = array_combine($initialhrs, $intialcalls);
        $finaldata_login = array_combine($login_hours, $login_count);
        $mergedata_login = array_replace($initialdata_login,$finaldata_login);
        $final_login_values = array_values($mergedata_login);

        //LogoutActivity

        $query_logout = mysqli_query($conn,"select count(*) as logoutcount ,HOUR(last_logout) as hours from user_callsummary where DATE(last_logout) = '$today' ".$camp_logged_condition." group by HOUR(last_logout)");
        while($row_logout = mysqli_fetch_assoc($query_logout)){

		$logout_hours[] = $row_logout['hours'];
		$logout_count[] = $row_logout['logoutcount'];
	}
        
        $initialdata_logout = array_combine($initialhrs, $intialcalls);
        $finaldata_logout = array_combine($logout_hours, $logout_count);
        $mergedata_logout = array_replace($initialdata_logout,$finaldata_logout);
        $final_logout_values = array_values($mergedata_logout);
       

        //ActiveAgents

        //$query_active_agent = mysqli_query($conn,"select count(*) as activecount,HOUR(current_eventtime) as hours from user_callsummary where DATE(current_eventtime) = '$today' ".$camp_logged_condition." and currentevent !='LOGOUT' group by HOUR(current_eventtime)");

       $query_active_agent = mysqli_query($conn,"select count(*) as activecount,HOUR(modify_date) as hours from campaign_dial_status where DATE(modify_date) = '$today' ".$campcondition." group by HOUR(modify_date)");

        while($row_active_agent = mysqli_fetch_assoc($query_active_agent)){

		$active_agent_hours[] = $row_active_agent['hours'];
		$active_agent_count[] = $row_active_agent['activecount'];
	}
        
        $initialdata_active_agent = array_combine($initialhrs, $intialcalls);
        $finaldata_active_agent = array_combine($active_agent_hours, $active_agent_count);
        $mergedata_active_agent = array_replace($initialdata_active_agent,$finaldata_active_agent);
        $final_active_agent_values = array_values($mergedata_active_agent);


	$widget_contents = array(
			"chart_dispo"=>$chart_dispo,
			"chart_dispo_status"=>$chart_dispo_status,
                        "res_thirty"=>$res_thirty,
			"res_sixty"=>$res_sixty,
			"res_one_twenty"=>$res_one_twenty,
			"res_three_hundred"=>$res_three_hundred,
			"res_three_hundred_more"=>$res_three_hundred_more,
                        "time_distribution_hours"=>$initialhrs,
                        "time_distribution_totalcalls"=>$final_timedistribution_values,
                        "callduration_status"=> $callduration_status,
                        "callduration_count"=>$callduration_count,
                        "totaltalk_hours"=>$initialhrs,
                        "totaltalk_duration" =>$final_totaltalk_values,
                        "login_hours" =>$initialhrs,
                        "login_count"=>$final_login_values,
                        "logout_hours"=>$initialhrs,
                        "logout_count"=> $final_logout_values,
                        "active_agent_hours"=>$initialhrs,
                        "active_agent_count"=>$final_active_agent_values,
                        "linechart_hours" =>$initialhrs_xaxis,
                        "test" =>$timedisrtibution_totalcalls,
			);
	echo json_encode($widget_contents);	



?>
