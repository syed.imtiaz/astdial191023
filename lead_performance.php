<!-- header -->
<?php include 'analytics_header.php'; ?>

        <!-- Page Content  -->
        <div id="content">
            <div class="card">
              <div class="card-body">
                <h6 style="text-align: left; color: #800080;">LEAD PERFORMANCE</h6>
                <div class="row">
                    <div class="col-md-3">
                    <label>Date&nbsp;</label><input  type="text" name="date" id="reportrange" autocomplete="false">
                    </div>
                    <div class="col-md-3" >
                        <label>Campaign&nbsp;</label>
                        <select name="camapign" id="campaign">
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-primary btn-sm" id="submit_filter" style="margin-bottom: -10px;">Submit</button>
                    </div>
                    <div class="col-md-5">
                        <img src="login_logo.png" style="width: 25%; float:right; margin-top: -20px;">
                    </div>
                </div>
              </div>
            </div>

            <!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"> -->
        <!-- <div class="container"> -->
            <div class="row">
                <div class="col-md-2" style="margin-bottom: -20px; margin-right: -5px;">
                    <div class="card bg-c-blue order-card">
                        <div class="card-block">
                            <h6 class="m-b-20 text-center" >Total Leads Dialed</h6>
                            <h3 class="text-center"><span id="dialed"></span></h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-2" style="margin-right: -5px;">
                    <div class="card bg-c-green order-card">
                        <div class="card-block">
                            <h6 class="m-b-20 text-center" >Connected</h6>
                            <h3 class="text-center"><span id="connected"></span></h3>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-2" style="margin-right: -5px;">
                    <div class="card bg-c-pink order-card">
                        <div class="card-block">
                            <h6 class="m-b-20 text-center" >Not Connected</h6>
                            <h3 class="text-center"><span id="not_connected"></span></h3>
                        </div>
                    </div>
                </div>
                
        <!--         <div class="col-md-4 col-xl-3">
                    <div class="card bg-c-yellow order-card">
                        <div class="card-block">
                            <h6 class="m-b-20">Orders Received</h6>
                            <h2 class="text-center"><span>486</span></h2>
                        </div>
                    </div>
                </div> -->
        	</div>
        <!-- </div> -->

            <!-- <br> -->
            <!-- charts starts here -->
            <div class="row">
                <div class="col-md-6" id="col_border">
                    <div class="card">
                    	  <br>
                    	  <h6>Disposition</h6>
                        <div class="card-body" id="graph-disposition">
                            <canvas id="donut_sevenDispo" width="280" height="100"></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-md-6" id="col_border">
                    <div class="card">
                    	<br>
                    	<h6>Calls by Duration</h6>
                        <div class="card-body" id="graph-duration">                                
                            <canvas id="horizontalBarChartCanvas" width="280" height="100">
                            </canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script src="Chartjs/Chart.js"></script>

    <!-- daterange start -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <!-- daterange ends -->

    <script type="text/javascript">
        $(document).ready(function () {

          $("#submit_filter").click(function(){  

           // disposition chart
           document.getElementById("donut_sevenDispo").remove(); //canvas
			     div = document.querySelector("#graph-disposition"); //canvas parent element
			     div.insertAdjacentHTML("afterbegin", "<canvas id='donut_sevenDispo' width='280' height='100'></canvas>"); //adding the canvas again   

           // call by duration
           document.getElementById("horizontalBarChartCanvas").remove(); //canvas
           div = document.querySelector("#graph-duration"); //canvas parent element
           div.insertAdjacentHTML("afterbegin", "<canvas id='horizontalBarChartCanvas' width='280' height='100'>"); //adding the canvas again  

                var full_dates = $('#reportrange').val();
                var array = full_dates.split("-");
                var sdate = array[0];
                var edate = array[1];

                var sel_camp = $('#campaign').find(":selected").text();
                if(sel_camp == "ALL"){
                    var camp_sel = "";
                }else{
                    var camp_sel = sel_camp;
                }

                // fetching graph data
                $.ajax(
                {
                    type: "POST",
                    url: "lead_performance_widget.php",
                    dataType:'json',
                    data: {
                      'post_data': 1,
                      'sdate': sdate,
                      'edate': edate,
                      'camp_sel': camp_sel,
                    },
                    success: function(response)
                    {
                        console.log(response.edate);

                        $('#dialed').html(response.dial);
                        $('#connected').html(response.connect);
                        $('#not_connected').html(response.notconnect);

                        // last seven days dispo
                        var seven_dispo = response.chart_dispo_seven;
                        var seven_dispo_status = response.chart_dispo_status_seven;
                        lastSevenDispo(seven_dispo, seven_dispo_status);

                        // yesterday call by duration
                        var thiry = response.res_thirty;
                        var sixty = response.res_sixty;
                        var one_twenty = response.res_one_twenty;
                        var three_hundred = response.res_three_hundred;
                        var three_hundred_more = response.res_three_hundred_more;

                        yesterday_duration(thiry, sixty, one_twenty, three_hundred, three_hundred_more);
                    }
                });
            
            });

            // without filter
            $.ajax(
            {
                type: "POST",
                url: "lead_performance_widget.php",
                dataType:'json',
                data: {
                  'post_data': 0,
                },
                success: function(response)
                {
                    console.log(response.edate);
                    // console.log(response.notconnect);

                    $('#dialed').html(response.dial);
                    $('#connected').html(response.connect);
                    $('#not_connected').html(response.notconnect);
                    // last seven days dispo
                    var seven_dispo = response.chart_dispo_seven;
                    var seven_dispo_status = response.chart_dispo_status_seven;
                    lastSevenDispo(seven_dispo, seven_dispo_status);

                    // yesterday call by duration
                    var thiry = response.res_thirty;
                    var sixty = response.res_sixty;
                    var one_twenty = response.res_one_twenty;
                    var three_hundred = response.res_three_hundred;
                    var three_hundred_more = response.res_three_hundred_more;

                    yesterday_duration(thiry, sixty, one_twenty, three_hundred, three_hundred_more);
                }
            });


            var camp;
            $.ajax(
              {
              type: "POST",
              url: "fetch_dropdown.php",
              dataType:'json',
              data: {
                'postdata': 1,
              },
              success: function(response)
              {
                    // CAMPAIGN
                    camp += '<option value="ALL">ALL</option>';
                    for(i in response.campaigns){
                      camp += "<option value='"+response.campaigns[i]+"'>"+response.campaigns[i]+"</option>";
                    }
                     document.getElementById('campaign').innerHTML = camp;
               }
            });

        });


    function yesterday_duration(thiry, sixty, one_twenty, three_hundred, three_hundred_more){

                Chart.defaults.global.defaultFontFamily = "Lato";

                var ctxLine = document.getElementById("horizontalBarChartCanvas").getContext("2d");
                if(window.bar != undefined) 
                window.bar.destroy(); 
                // window.bar = new Chart(ctxLine, {});

                window.bar = new Chart(ctxLine, {
                   type: 'horizontalBar',
                   data: {
                      labels: ['<=30 Sec', '>30 and <=60 Sec', '>60 Sec and <=2 Min', '>2 Min and <=5 Min', '>5 Min'],
                      datasets: [{
                         data: [thiry, sixty, one_twenty, three_hundred, three_hundred_more],
                         backgroundColor: ["#9C27B0", "#1DE9B6", "#2196F3","#F50057","#5E35B1"], 
                      }]
                   },
                   options: {
                      tooltips: {
                        enabled: true
                      },
                      responsive: true,
                      legend: {
                         display: false,
                         position: 'bottom',
                         fullWidth: true,
                         labels: {
                           boxWidth: 10,
                           padding: 50
                         }
                      },
                      scales: {
                         yAxes: [{
                           barPercentage: 1,
                           gridLines: {
                             display: true,
                             drawTicks: true,
                             drawOnChartArea: false
                           },
                           ticks: {
                             fontColor: '#555759',
                             fontFamily: 'Lato',
                             fontSize: 11
                           }
                            
                         }],
                         xAxes: [{
                             gridLines: {
                               display: true,
                               drawTicks: false,
                               tickMarkLength: 5,
                               drawBorder: false
                             },
                           ticks: {
                             padding: 5,
                             beginAtZero: true,
                             fontColor: '#555759',
                             fontFamily: 'Lato',
                             fontSize: 11,
                             callback: function(label, index, labels) {
                                if (Math.floor(label) === label) {
                                    return label;
                                }
                             }
                               
                           },
                            scaleLabel: {
                              display: true,
                              padding: 10,
                              fontFamily: 'Lato',
                              fontColor: '#555759',
                              fontSize: 16,
                              fontStyle: 700,
                              // labelString: 'Calls by Duration (in seconds)'
                            },
                           
                         }]
                      }
                   }
                });


            }

            // chart functions starts here
            function lastSevenDispo(seven_dispo, seven_dispo_status){
                // colors
                var chart_colors = ["#1DE9B6","#2196F3","#F50057","#5E35B1","#B10358","#F1C40F","#7D3C98","#E74C3C","#FE3A06","#03D5E7","#04B014","#BE4B05","#BE0521","#EA7385"];
                var dynamic_colors = [];
                    for (var i = 0; i < seven_dispo_status.length; i++) {
                        dynamic_colors.push(chart_colors[i])
                    }

            Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

            var helpers = Chart.helpers;
            var defaults = Chart.defaults;

            Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
                updateElement: function(arc, index, reset) {
                var _this = this;
                var chart = _this.chart,
                    chartArea = chart.chartArea,
                    opts = chart.options,
                    animationOpts = opts.animation,
                    arcOpts = opts.elements.arc,
                    centerX = (chartArea.left + chartArea.right) / 2,
                    centerY = (chartArea.top + chartArea.bottom) / 2,
                    startAngle = opts.rotation, // non reset case handled later
                    endAngle = opts.rotation, // non reset case handled later
                    dataset = _this.getDataset(),
                    circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
                    innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
                    outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
                    custom = arc.custom || {},
                    valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

                helpers.extend(arc, {
                  // Utility
                  _datasetIndex: _this.index,
                  _index: index,

                  // Desired view properties
                  _model: {
                    x: centerX + chart.offsetX,
                    y: centerY + chart.offsetY,
                    startAngle: startAngle,
                    endAngle: endAngle,
                    circumference: circumference,
                    outerRadius: outerRadius,
                    innerRadius: innerRadius,
                    label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
                  },

                  draw: function () {
                    var ctx = this._chart.ctx,
                                    vm = this._view,
                                    sA = vm.startAngle,
                                    eA = vm.endAngle,
                                    opts = this._chart.config.options;
                            
                                var labelPos = this.tooltipPosition();
                                var segmentLabel = vm.circumference / opts.circumference * 100;
                                
                                ctx.beginPath();
                                
                                ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
                                ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);
                                
                                ctx.closePath();
                                ctx.strokeStyle = vm.borderColor;
                                ctx.lineWidth = vm.borderWidth;
                                
                                ctx.fillStyle = vm.backgroundColor;
                                
                                ctx.fill();
                                ctx.lineJoin = 'bevel';
                                
                                if (vm.borderWidth) {
                                    ctx.stroke();
                                }
                                
                                if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
                                    ctx.beginPath();
                                    ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
                                    ctx.fillStyle = "#fff";
                                    ctx.textBaseline = "top";
                                    ctx.textAlign = "center";
                        
                        // Round percentage in a way that it always adds up to 100%
                                    ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
                                }
                  }
                });

                var model = arc._model;
                model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
                model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
                model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
                model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

                // Set correct angles if not resetting
                if (!reset || !animationOpts.animateRotate) {
                  if (index === 0) {
                    model.startAngle = opts.rotation;
                  } else {
                    model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
                  }

                  model.endAngle = model.startAngle + model.circumference;
                }

                arc.pivot();
              }
            });

            var config = {
              type: 'doughnutLabels',
              data: {
                datasets: [{
                  data:seven_dispo,
                  backgroundColor: chart_colors,
                  label: 'Dataset 1'
                }],
                labels: seven_dispo_status
              },
              options: {
                responsive: true,
                legend: {
                  position: 'right',
                },
                title: {
                  display: false,
                  text: 'Chart.js Doughnut Chart'
                },
                animation: {
                  animateScale: true,
                  animateRotate: true
                }
              }
            };

            var ctx = document.getElementById("donut_sevenDispo").getContext("2d");
            new Chart(ctx, config);
            }
    </script>

    <script type="text/javascript">
        $(function() {
            var start = moment().subtract(6, 'days');
            var end = moment();
           
            function cb(start, end) {
                $('#reportrange span').html(start.format('MM-DD-YYYY') + '  ' + end.format('MM-DD-YYYY'));
            }
            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                   'Today': [moment(), moment()],
                   // 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                   // 'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   // 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                   '3 Months': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')]
                }
            }, cb);
            cb(start, end);
        });
    </script>
</body>

</html>