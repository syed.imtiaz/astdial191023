<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$con = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($con,$dbName);

if ($con->connect_error) {
 die("Connection failed: " . $con->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
$cur_date = date("Y-m-d");

$record_id = $_POST['record_id'];
// GET CAMPAIGN NAME
$get_camp_name = mysqli_query($con, "SELECT campaignname FROM vtiger_campaign WHERE campaignid = '".$record_id."' ");
$row_camp_name = mysqli_fetch_assoc($get_camp_name);
$from_campaign = $row_camp_name['campaignname'];

$to_campaign = $_POST['camp_select'];
$main_disp = $_POST['main_selected'];




if($main_disp == ""){
	$main_disps = " " ;
}else{
	$main_disps = "AND dispo  = '.$main_disp.'" ;
}

$get_from = mysqli_query($con, "SELECT campaignid FROM vtiger_campaign WHERE campaignname  =  '$from_campaign'");
$row_from = mysqli_fetch_assoc($get_from);
$from_campid = $row_from['campaignid'];



$get_contacts = mysqli_query($con, "SELECT contactid FROM vtiger_campaigncontrel WHERE campaignid = '$from_campid' AND dispo = '$main_disp'");
$count_leads = mysqli_num_rows($get_contacts);
// count leads

$f_value = round($count_leads * 0.2);
$s_value = round($count_leads * 0.3);

// CAMPAIGN
$select_camp = "SELECT campaignname,campaignid FROM vtiger_campaign INNER JOIN vtiger_crmentity ON vtiger_campaign.campaignid=vtiger_crmentity.crmid WHERE deleted='0' AND campaigntype='Auto-Dialling' OR campaigntype='Manual-Dialling' OR campaigntype='Preview-Dialling' AND campaignstatus='Active'";	
$query_camp = mysqli_query($con,$select_camp);				
while($row_camp = mysqli_fetch_assoc($query_camp)){
	$campaigns[] = $row_camp['campaignname'];
}

// MAINDISPO
$select_dispo = mysqli_query($con, "SELECT name, maindispoid FROM vtiger_maindispo INNER JOIN vtiger_crmentity ON vtiger_maindispo.maindispoid=vtiger_crmentity.crmid WHERE deleted='0'");
while($row_dispo = mysqli_fetch_assoc($select_dispo)){
	$main_dispo[] = $row_dispo['name'];
}

$realtime_contents = array(
		"campaigns"=>$campaigns,
		"main_dispo"=>$main_dispo,
		"campaign_name"=>$from_campaign,
		"count_leads"=>$count_leads,
		"f_value"=>$f_value,
		"s_value"=>$s_value,
		"from_cmp"=>$from_campid
		);
echo json_encode($realtime_contents);	

?>
