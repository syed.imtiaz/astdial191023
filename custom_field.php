<?php

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('CallLogs');
$callLogsBlock = Vtiger_Block::getInstance('LBL_CALLLOGS_INFORMATION', $module);

$contact = Vtiger_Field::getInstance('call_status', $module);
if (!$contact) {
    $contact = new Vtiger_Field();
    $contact->name = 'call_status';
    $contact->label = 'Status';
    $contact->table = $module->basetable;
    $contact->column = 'call_status';
    $contact->columntype = 'varchar(250)';
    $contact->uitype = 15;
    $contact->typeofdata = 'V~O';
    $contact->setPicklistValues(array('Connected', 'Not Connected'));
    $callLogsBlock->addField($contact);
    //$contact->setRelatedModules(array('Contacts'));
}

?>
