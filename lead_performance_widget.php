<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$conn = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($conn,$dbName);

if ($conn->connect_error) {
 die("Connection failed: " . $conn->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)

$yesterday = date('Y-m-d',strtotime("-1 days"));
$last_seventh_day = date('Y-m-d',strtotime("-7 days"));
$today = date('Y-m-d');
// $seventh_day = date('Y-m-d',strtotime("-7 days"));

	if($_POST['post_data'] == 1){
		$post = '1';

		$sdate = $_POST['sdate'];
		$edate = $_POST['edate'];
		$campaign = $_POST['camp_sel'];

		$start_date = date("Y-m-d", strtotime($sdate));
		$end_date = date("Y-m-d", strtotime($edate));

			if($campaign != ''){
				$camp = " AND campaign LIKE '%".$campaign."%' " ;
				$campaignname = " AND vtiger_campaign.campaignname LIKE '%".$campaign."%' " ;
			}else{
				$camp = "";
				$campaignname = "";
			}

	}else{
		$post = '0';
		$camp = "";
		$campaignname = "";

		$start_date = $last_seventh_day;
		$end_date = $today;

		$start_dates = date("d-m-Y", strtotime($last_seventh_day));
		$end_dates = date("d-m-Y", strtotime($today));
	}

	// LAST 7 DAYS DISPOSITION
	$query_dispoCount_seven = mysqli_query($conn, "SELECT COUNT(dispo) as total_dispo, CONCAT(UPPER(SUBSTRING(dispo,1,1)),LOWER(SUBSTRING(dispo,2))) as dispo_upper  FROM campaign_dial_status WHERE DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." GROUP BY dispo ");
	while($total_dispoCount_seven = mysqli_fetch_assoc($query_dispoCount_seven)) {
		$chart_dispo_seven[] = $total_dispoCount_seven['total_dispo'];
		$upper_first = $total_dispoCount_seven['dispo_upper'];
		$chart_dispo_status_seven[] = ucfirst($upper_first);
	}


	// YESTERDAYS CALL BY DURATION
	$qry_thirty = mysqli_query($conn, "SELECT COUNT(*) as total30 FROM `campaign_dial_status` WHERE `duration`<=30 and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." ");
	$row_thirty = mysqli_fetch_assoc($qry_thirty);
	$res_thirty = $row_thirty['total30'];

	$qry_sixty = mysqli_query($conn, "SELECT COUNT(*) as total60 FROM `campaign_dial_status` WHERE `duration`>30 and `duration`<=60 and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." ");
	$row_sixty = mysqli_fetch_assoc($qry_sixty);
	$res_sixty = $row_sixty['total60'];

	$qry_one_twenty = mysqli_query($conn, "SELECT COUNT(*) as total120 FROM `campaign_dial_status` WHERE `duration`>60 and `duration`<=120 and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." ");
	$row_one_twenty = mysqli_fetch_assoc($qry_one_twenty);
	$res_one_twenty = $row_one_twenty['total120'];

	$qry_three_hundred = mysqli_query($conn, "SELECT COUNT(*) as total300 FROM `campaign_dial_status` WHERE `duration`>120 and `duration`<=300 and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." ");
	$row_three_hundred = mysqli_fetch_assoc($qry_three_hundred);
	$res_three_hundred = $row_three_hundred['total300'];

	$qry_three_hundred_more = mysqli_query($conn, "SELECT COUNT(*) as total300_greater FROM `campaign_dial_status` WHERE `duration`>300 and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." ");
	$row_three_hundred_more = mysqli_fetch_assoc($qry_three_hundred_more);
	$res_three_hundred_more = $row_three_hundred_more['total300_greater'];
	//

	// TOTAL CONNECTED
	// $count_dial = mysqli_query($conn, "SELECT count(*) AS dial_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.status NOT IN ('') INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$campaignname." and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ");
	// $cnt_dial = mysqli_fetch_assoc($count_dial);
	// $dial = $cnt_dial['dial_cnt'];

	//connect
	$select_connect = "SELECT count(*) from campaign_dial_status where status='1' and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." ";
	$query_connect = mysqli_query($conn,$select_connect);
	$row_connect = mysqli_fetch_array($query_connect);
	$connect = $row_connect[0];

	//notconnect
	$select_notconnect = "SELECT count(*) from campaign_dial_status where status='2' and DATE(`modify_date`)<='$end_date' AND DATE(`modify_date`)>='$start_date' ".$camp." ";
	$query_notconnect = mysqli_query($conn,$select_notconnect);
	$row_notconnect = mysqli_fetch_array($query_notconnect);
	$notconnect = $row_notconnect[0];

	$dial = $connect + $notconnect;

	// total
	// $connect = $connects+$notconnects;
	// $notconnect = $dial - $connect;

$widget_contents = array(
		"chart_dispo_seven"=>$chart_dispo_seven,
		"chart_dispo_status_seven"=>$chart_dispo_status_seven,
		"res_thirty"=>$res_thirty,
		"res_sixty"=>$res_sixty,
		"res_one_twenty"=>$res_one_twenty,
		"res_three_hundred"=>$res_three_hundred,
		"res_three_hundred_more"=>$res_three_hundred_more,
		"dial"=>$dial,
		"connect"=>$connect,
		"notconnect"=>$notconnect,
		"start_dates"=>$start_dates,
		"end_dates"=>$end_dates,
		);
echo json_encode($widget_contents);	



?>