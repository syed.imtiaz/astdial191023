<?php


class Settings_DialStatus_DialStatusReport_View extends Settings_Vtiger_Index_View {

	public function process(Vtiger_Request $request) {



		$viewer = $this->getViewer($request);
		$qualifiedModuleName = $request->getModule(false);
		
	
		global $adb;

		$select_camp = "SELECT campaignname,campaignid from vtiger_campaign  INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaign.campaignid AND vtiger_crmentity.deleted='0' where campaigntype='Auto-Dialling' OR campaigntype='Preview-Dialling' OR campaigntype='Manual-Dialling' and campaignstatus='Active'";
			$res=$adb->pquery($select_camp);
			while($row = $adb->fetch_array($res))
			{
			$camp_name[] =$row['campaignname'];
			}
	
		$viewer->assign('campaigns',$camp_name);

		$viewer->view('DialStatusReport.tpl', $qualifiedModuleName);

	}


}
