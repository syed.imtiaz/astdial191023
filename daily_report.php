<?php

require_once("dbconnect.php");
require_once("phpmailer/class.phpmailer.php");

date_default_timezone_set("Asia/Kolkata");

$curdate     = date('Y-m-d');
$curdate     = date('Y-m-d',strtotime("-1 days"));
$curdate_yes = date('Y-m-d',strtotime("-1 days"));

$sel   = "select userid,sum(login_time),sum(pause_time),sum(break_time),sum( talk_time) from user_callsummary  where DATE(datetime) between '".$curdate."' and  '".$curdate."' group by(userid) order by DATE(datetime) DESC ";
$query = mysqli_query($con,$sel);  
$agent_performance = 
"<table border='1' style='border-collapse: collapse; border-color: black;' width='100%'>
            <thead style='background-color:#800080;color:#FFFFFF'>
                <tr >
                    <th>Sl.No.</th>
                    <th>Name</th>
                    <th>Total Login Time</th>
                    <th>Total Pause Time</th>
                    <th>Total Break Time</th>
                    <th>Total Talk Time</Th>
                    <th>Connected Calls</th>
                    <th>Not Connected Calls</th>
            </thead>    
            <tbody>";
                
                //$count = 0;
                $num2 = mysqli_num_rows($query);

                if($num2 > 0){
                    while($row = mysqli_fetch_array($query)){
                        $sl_No1 = $sl_No1 + 1;

                        $user = $row[0];
                        $sql_sucess    = "select count(*) from campaign_dial_status where user = '".$user."' and status = '1'and DATE(modify_date) between'".$curdate."' and  '".$curdate."' ";
                        $query_sucess  = mysqli_query($con,$sql_sucess);
                        $result_sucess = mysqli_fetch_array($query_sucess);
                        $sucess_calls  = $result_sucess[0];

                        $diff       = $row[1];
                        $hours      = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
                        $minuts     = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
                        $seconds    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
                        $login_time = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);
                       
                        $pause_time = $row[2];
                        //calculate pause time
                        $hours           = floor(($pause_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
                        $minuts          = floor(($pause_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
                        $seconds         = floor(($pause_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
                        $pause_time_hour = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);

                    
                        $break_time = $row[3];
                        //calculate break time
                        $hours           = floor(($break_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
                        $minuts          = floor(($break_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
                        $seconds         = floor(($break_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
                        $break_time_hour = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);

                    
                        $talk_time =  $row[4];
                        //calculate talk time
                        $hours          = floor(($talk_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
                        $minuts         = floor(($talk_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
                        $seconds        = floor(($talk_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
                        $talk_time_hour = sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);
                    
                        //end success
                            
                        //unsucess calls
                        $sql_unsucess    = "select count(*) from campaign_dial_status where user = '".$user."' and DATE(modify_date) between'".$curdate."' and  '".$curdate."'  and status = '2'";
                        $query_unsucess  = mysqli_query($con,$sql_unsucess);
                        $result_unsucess = mysqli_fetch_array($query_unsucess);
                        $unsucess_calls  = $result_unsucess[0];
                             
                        $get_name   = "select first_name,last_name from vtiger_users where user_name = '".$row[0]."' ";
                        $query_name = mysqli_query($con,$get_name);
                        $name       = mysqli_fetch_array($query_name);
                        //$display_name = $name[0]." ".$name[1];
                        $display_name = $name[0];
                        
                        $agent_performance .= "<tr><td>$sl_No1</td><td style='width:25%;' >$display_name</td><td>$login_time</td><td>$pause_time_hour</td><td>$break_time_hour</td><td>$talk_time_hour</td><td style='text-align:right;'>$sucess_calls</td><td style='text-align:right;'>$unsucess_calls</td>
                </tr>";
                    }
                } else {
                    $agent_performance .= "<tr style='text-align:right;'><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                    </td></tr>";
                }

                $agent_performance .= "</tbody></table>";
                
                $disposition  = 
                    "<table border='1' style='border-collapse: collapse; border-color: black;' width='100%'>
                        <thead style='background-color:#800080;color:#FFFFFF'>
                             <tr>
                                <th>Sl.No.</th>
                                <th>Disposition</th>
                                <th>Count</th>                   
                        </thead>
                            <tbody>";
                            //$count2 = 0;

                            $sel2   = "SELECT dispo,count(*) FROM `campaign_dial_status` WHERE date(`modify_date`)='$curdate' GROUP BY dispo ";
                            $query2 = mysqli_query($con,$sel2); 
                            $num    = mysqli_num_rows($query2);

                            if($num > 0) {
                                while($row2 = mysqli_fetch_array($query2)) {
                                    $sl_No2 = $sl_No2 + 1;
                                    $dispo  = $row2[0];
                                    $count  = $row2[1];
    
                                    if($dispo == ''){
                                        $dispo ='No Dispo';
                                    }

                                    $disposition .= "<tr><td style='width:10%;'>$sl_No2</td><td>$dispo</td><td style='text-align:right;'>$count</td></tr>";
                                }

                            } else {
                                $disposition .= "<tr><td>0</td><td>-</td><td>0</td></tr>";

                            }

                            $disposition .= "</tbody></table>";

                $duration  = 
                "<table border='1' style='border-collapse: collapse; border-color: black;' width='250px'>
                    <thead style='background-color:#800080;color:#FFFFFF'>
                          <tr> 
                            <th>Sl.No.</th>
                            <th>Duration</th>
                            <th>No. of Calls</th>
                    </thead>
                        <tbody>";

                        $count3 = 0;
                        $sel3   = "SELECT count(`user`) FROM `campaign_dial_status` WHERE `duration` between 0 and 10 and date(`modify_date`)='$curdate'";
                        $query3 = mysqli_query($con,$sel3); 
                        $num3    = mysqli_num_rows($query3);

                        if($num3 > 0) {
                            while($row3 = mysqli_fetch_array($query3)) {
                                $sl_No3     = $sl_No3 + 1;
                                $user_count = $row3[0];
                                $dur        = '0-10 sec';

                                $duration .= "<tr><td>$sl_No3</td><td>$dur</td><td style='text-align:right;'>$user_count</td></tr>";
                            }

                        } else {
                            $duration .= "<tr><td>0</td><td>-</td><td>0</td></tr>";

                        }

                        $sel3   = "SELECT count(`user`) FROM `campaign_dial_status` WHERE `duration` between 11 and 30 and date(`modify_date`)='$curdate'";
                        $query3 = mysqli_query($con,$sel3); 
                        $num3    = mysqli_num_rows($query3);

                        if($num3 > 0) {
                            while($row3 = mysqli_fetch_array($query3)) {
                                $sl_No3     = $sl_No3 + 1;
                                $user_count = $row3[0];
                                $dur        = '11-30 sec';

                                $duration .= "<tr><td>$sl_No3</td><td>$dur</td><td style='text-align:right;'>$user_count</td></tr>";
                            }

                        } else {
                            $duration .= "<tr><td>0</td><td>-</td><td>0</td></tr>";

                        }

                        $sel3   = "SELECT count(`user`) FROM `campaign_dial_status` WHERE `duration` between 31 and 60 and date(`modify_date`)='$curdate'";
                        $query3 = mysqli_query($con,$sel3); 
                        $num3    = mysqli_num_rows($query3);

                        if($num3 > 0) {
                            while($row3 = mysqli_fetch_array($query3)) {
                                $sl_No3     = $sl_No3 + 1;
                                $user_count = $row3[0];
                                $dur        = '31-60 sec';

                                $duration .= "<tr><td>$sl_No3</td><td>$dur</td><td style='text-align:right;'>$user_count</td></tr>";
                            }

                        } else {
                            $duration .= "<tr><td>0</td><td>-</td><td>0</td></tr>";

                        }

                        $sel3   = "SELECT count(`user`) FROM `campaign_dial_status` WHERE `duration` between 61 and 120 and date(`modify_date`)='$curdate'";
                        $query3 = mysqli_query($con,$sel3); 
                        $num3    = mysqli_num_rows($query3);

                        if($num3 > 0) {
                            while($row3 = mysqli_fetch_array($query3)) {
                                $sl_No3     = $sl_No3 + 1;
                                $user_count = $row3[0];
                                $dur        = '1-2 mins';

                                $duration .= "<tr><td>$sl_No3</td><td>$dur</td><td style='text-align:right;'>$user_count</td></tr>";
                            }

                        } else {
                            $duration .= "<tr><td>0</td><td>-</td><td>0</td></tr>";

                        }

                        $sel3   = "SELECT count(`user`) FROM `campaign_dial_status` WHERE `duration` between 121 and 240 and date(`modify_date`)='$curdate'";
                        $query3 = mysqli_query($con,$sel3); 
                        $num3    = mysqli_num_rows($query3);

                        if($num3 > 0) {
                            while($row3 = mysqli_fetch_array($query3)) {
                                $sl_No3     = $sl_No3 + 1;
                                $user_count = $row3[0];
                                $dur        = '2-4 mins';

                                $duration .= "<tr><td>$sl_No3</td><td>$dur</td><td style='text-align:right;'>$user_count</td></tr>";
                            }

                        } else {
                            $duration .= "<tr><td>0</td><td>-</td><td>0</td></tr>";

                        }

                        $sel3   = "SELECT count(`user`) FROM `campaign_dial_status` WHERE `duration` between 241 and 420 and date(`modify_date`)='$curdate'";
                        $query3 = mysqli_query($con,$sel3); 
                        $num3    = mysqli_num_rows($query3);

                        if($num3 > 0) {
                            while($row3 = mysqli_fetch_array($query3)) {
                                $sl_No3     = $sl_No3 + 1;
                                $user_count = $row3[0];
                                $dur        = '4-7 mins';

                                $duration .= "<tr><td>$sl_No3</td><td>$dur</td><td style='text-align:right;'>$user_count</td></tr>";
                            }

                        } else {
                            $duration .= "<tr><td>0</td><td>-</td><td>0</td></tr>";

                        }

                        $sel3   = "SELECT count(`user`) FROM `campaign_dial_status` WHERE `duration` between 421 and 600 and date(`modify_date`)='$curdate'";
                        $query3 = mysqli_query($con,$sel3); 
                        $num3    = mysqli_num_rows($query3);

                        if($num3 > 0) {
                            while($row3 = mysqli_fetch_array($query3)) {
                                $sl_No3     = $sl_No3 + 1;
                                $user_count = $row3[0];
                                $dur        = '7-10 mins';

                                $duration .= "<tr><td>$sl_No3</td><td>$dur</td><td style='text-align:right;'>$user_count</td></tr>";
                            }

                        } else {
                            $duration .= "<tr><td>0</td><td>-</td><td>0</td></tr>";

                        }
                        
                        $sel3   = "SELECT count(`user`) FROM `campaign_dial_status` WHERE `duration` > 600 and date(`modify_date`)='$curdate'";
                        $query3 = mysqli_query($con,$sel3); 
                        $num3    = mysqli_num_rows($query3);

                        if($num3 > 0) {
                            while($row3 = mysqli_fetch_array($query3)) {
                                $sl_No3     = $sl_No3 + 1;
                                $user_count = $row3[0];
                                $dur        = '> 10 mins';

                                $duration .= "<tr><td>$sl_No3</td><td>$dur</td><td style='text-align:right;'>$user_count</td></tr>";
                            }

                        } else {
                            $duration .= "<tr><td>0</td><td>-</td><td>0</td></tr>";

                        }
                        

                        $duration .= "</tbody></table>";
                                    //print_r($duration);exit;

$select_email="select email1,concat( first_name, ' ', last_name ) from vtiger_users where is_admin='on'";
$q_email=mysqli_query($con,$select_email);
// while($r_email= mysqli_fetch_array($q_email)) {
    // $email = $r_email[0];
    // $name  = $r_email[1];
    $email = "tibinktomy@gmail.com";
    $name  = "Tibin";
    $mail  = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug  = 0;
    $mail->SMTPAuth   = TRUE;
    $mail->SMTPSecure = "ssl";
    $mail->Port       = 465;  
    $mail->Username   = 'web23473492p140';//"abacrm1@abatecs.com";
    $mail->Password   = 'RKR8B7mR';//"@B@crm1@12#";
    $mail->Host       = 'alfa3214.alfahosting-server.de';//"abatecs.com";
    $mail->Mailer     = "smtp";//smtp
    $mail->SetFrom("reports@astcrm.com");
    $mail->AddReplyTo("reports@astcrm.com");
    $mail->AddAddress($email); 
    $mail->AddCC("k.tibin@astcrm.com");
    $mail->Subject    = "Daily Report *astDIAL7";
    $mail->WordWrap   = 80;
    $mail->MsgHTML("<!DOCTYPE html PUBLIC'-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <title>astDIAL Email</title>
        <style type='text/css'>
            body {
                margin: 0; 
                padding: 0; 
                min-width: 100%!important;
            }

            .content {
                width: 100%; 
                max-width: 600px;
            }

            .header {
                padding: 40px 30px 20px 30px;
            }

            .col425 {
                width: 525px!important;
            }

            .subhead {
                font-size: 24px; 
                color: #ffffff; 
                font-family: sans; 
                letter-spacing: 10px;
            }

            .h1 {
                font-size: 33px; 
                line-height: 38px; 
                font-weight: bold;
            }

            .h1, .h2, .bodycopy {
                color: #153643; 
                font-family: arial;
            }

            .innerpadding {
                padding: 30px 0px 0px 0px;
            }

            .borderbottom {
                border-bottom: 1px solid #f2eeed;
            }

            .h2 {
                padding: 0 0 15px 0; 
                font-size: 24px; 
                line-height: 28px; 
                font-weight: bold;
            }

            .bodycopy {
                font-size: 16px; 
                line-height: 22px; 
                text-align:justify;
            }

            .button {
                text-align: center; 
                font-size: 18px; 
                font-family: arial; 
                font-weight: bold; 
                padding: 0 30px 0 30px;
            }

            .button a {
                color: #ffffff; 
                text-decoration: none;
            }

            img {
                height: auto;
            }

            .footer {
                padding: 20px 30px 15px 30px;
            }

            .footercopy {
                font-family: sans-serif; 
                font-size: 14px; 
                color: #ffffff;
            }

            .footercopy a {
                color: #ffffff; 
                text-decoration: underline;
            }

            .table1 {
                

            @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
                body[yahoo] .buttonwrapper {background-color: transparent!important;}
                body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important; display: block!important;
            }
            
            </style>
        </head>
        
        <body yahoo bgcolor='#f6f8f1'>
        <!--[if (gte mso 9)|(IE)]>
        <table width='600' align='center' cellpadding='0' cellspacing='0' border='0'>
        <tr>
        <td>
        <![endif]-->
        
        <table class='content' align='center' cellpadding='0' cellspacing='0' border='0' style='border-left: 1px solid #f2eeed; border-right: 1px solid #f2eeed;'>
            <tr>
            <td class='header' bgcolor='#c7d8a7'>            
                <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                <tr>
                    <td style='font-family: arial black; font-style: italic; color: #800080; font-size: 43px; text-align: right; '>
                    *astDIAL
                    </td>
                </tr>                           
                <tr>
                    <td class='h1' style='padding: 5px 0 0 0; text-align:center;'>
                    Daily Report                  
                    </td>
                </tr>
                </table>
                
            </td>
            </tr>
            <tr>
            <td class='innerpadding borderbottom'>
                
                <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                    <tr>
                    <td class='bodycopy'>
                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                <tr><td>Dear $name,
    <p>Below is the daily report for the Cloud Service on $curdate_yes</p></td></tr>
                <tr><td class='bodycopy'>
        <p><br><b>Agent Performance Report</b></p>
        
    ".$agent_performance. "</td></tr><tr><td class='bodycopy'><b><br><p>Disposition Report</p></b>".

    $disposition."</td>
                </tr>
                <tr><td class='bodycopy'><b><br><p>Call Duration Report</p></b>".

    $duration."<br></td>
                </tr>
                </table>
                        
                    
                <!--[if (gte mso 9)|(IE)]>
                <table width='380' align='left' cellpadding='0' cellspacing='0' border='1' border-width: thick;>
                                    <tr>
                        <td style='padding: 20px 0 0 0;'>
                            <table class='buttonwrapper' bgcolor='#e05443' border='1' border-width: thick; cellspacing='0' cellpadding='0'>
                            <tr>
                                <td class='button' height='45'>
                                <a href='http://www.astcrm.com/contact-us'>Contact Us</a>
                                </td>
                            </tr>
                            </table>
                            
                        </td>
                        </tr>
                    </table>
                    
                    </td>
                </tr>
                </table>
                
                <!--[if (gte mso 9)|(IE)]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </td>       
            </tr>
        
            
            <tr>
            <td class='footer' bgcolor='#44525f'>
                <table width='100%' border='1' border-width: thick; cellspacing='0' cellpadding='0'>
                <tr>
                    <td align='center' class='footercopy'>
                    Copyright @ *astCRM 2019<br/>
                    <a href='mailto:support@astCRM.com?Subject=Unsubsribe' target='_top'><font color='#ffffff'>Unsubscribe</font></a>
                    </td>
                </tr>
                <tr>
                    <td align='center' class='footercopy'>
                    For Support
                    </td></tr>
                <tr>
                    <td align='center' style='padding: 10px 0 0 0;'>  
                    <a href='https://demo.asttecs.com/webcall/call.php?phone=6680'><img src='http://demo.asttecs.com/crm/asteazy/storage/Click-to-call.png' alt='call' title='call' style='display:block' width='272' height='80'></a>
                    </td>
                </tr>
                </table>
            </td>
            </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
        </body>

    ");
        //$mail->AddAttachment($file, $file);

        //print_r($mail);exit;

    $mail->IsHTML(true);

    if(!$mail->Send()) {
        echo "Problem in Sending Mail.";
    } else {
    //unlink($filename);
        echo "Mail Sent Successfully.";
    } 
// }
?>
