<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <title>Real-time</title>
    </head>
    <body style="margin: 10px;">

        <style type="text/css">
            .badge{
                width: 100%;
                font-size: 16px;
                padding: 5px;
                margin-top: 2px;
                border-radius: 2px;
            }
            /*refresh switch starts*/
            .switch-field {
                display: flex;
                margin-bottom: 36px;
                overflow: hidden;
            }

            .switch-field input {
                position: absolute !important;
                clip: rect(0, 0, 0, 0);
                height: 1px;
                width: 1px;
                border: 0;
                overflow: hidden;
            }

            .switch-field label {
                background-color: #ffffff;
                color: rgba(0, 0, 0, 0.6);
                font-size: 14px;
                line-height: 1;
                text-align: center;
                padding: 8px 16px;
                margin-right: -1px;
                border: 1px solid rgba(0, 0, 0, 0.2);
                transition: all 0.1s ease-in-out;
            }

            .switch-field label:hover {
                cursor: pointer;
            }

            .switch-field input:checked + label {
                background-color: #35aa47;
                box-shadow: none;
                color : #ffffff;
            }

            .switch-field label:first-of-type {
                border-radius: 4px 0 0 4px;
            }

            .switch-field label:last-of-type {
                border-radius: 0 4px 4px 0;
            }

            p {
                font-size: 16px;
                margin-bottom: 8px;
            }
            /*refresh switch ends*/

            /*countdown div starts*/

            #countdown {
                position: relative;
                /*margin: auto;*/
                margin-bottom: 15px;
                height: 40px;
                width: 40px;
                text-align: center;
            }

            #countdown-number {
                color: #17a2b8;
                display: inline-block;
                line-height: 40px;
                font-size: 18px;
            }

            svg {
                position: absolute;
                top: 0;
                right: 0;
                width: 40px;
                height: 40px;
                transform: rotateY(-180deg) rotateZ(-90deg);
            }

            /*    svg circle {
                  stroke-dasharray: 113px;
                  stroke-dashoffset: 0px;
                  stroke-linecap: round;
                  stroke-width: 4px;
                  stroke: white;
                  fill: none;
                  stroke:#17a2b8;
                  animation: countdown 10s linear infinite forwards;
                }*/

            @keyframes countdown {
                from {
                    stroke-dashoffset: 0px;
                }
                to {
                    stroke-dashoffset: 113px;
                }
            }
            /*countdown div ends*/

        </style>
        <div class="row">
            <div class="col-md-6">
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <!-- live status -->
                        <!-- <div class="col-md-4"> -->
                        <div class="card">
                            <div class="card-header" style="background-color: #E5E8E8; padding: 10px;">
                                <b>LIVE STATUS</b>
                            </div>
                            <span class="badge badge-primary" style="background-color: #007bff">Total : <span id="total_live"></span></span>
                            <span class="badge badge-success" style="background-color: #28a745">Talk : <span id="talk_live"></span></span>
                            <span class="badge badge-danger" style="background-color: #dc3545">Pause : <span id="pause_live"></span></span>
                            <span class="badge badge-secondary" style="background-color: #ffc107">Break : <span id="break_live"></span></span>
                            <span class="badge badge-info badge" style="background-color: #17a2b8">Logout : <span id="logout_live"></span></span>
                        </div>
                        <br>
                        <!-- countdown timer -->
                        <div class="form-group">
                            <!-- campaign filter -->
                            <label><b>SELECT CAMPAIGN</b></label>
                            <select id="campaign" name="camapign"  style="width: 70%; padding:5px 20px; display: inline-block; border: 1px solid #ccc; border-radius: 4px; box-sizing: border-box; background-color: #ffffff;">

                            </select>

                        </div>
                        <!-- </div> -->
                    </div>
                    <div class="col-md-6">
                        <div class="card" style="border: solid 1px #f7f7f7;">
                            <div class="card-header" style="background-color: #E5E8E8; padding: 10px;">
                                <b>LEAD STATUS</b>
                            </div>
                            <span class="badge badge-primary" style="background-color: #007bff">Total Leads : <span id="total_leads"></span></span>
                            <span class="badge badge-success" style="background-color: #28a745">Assigned : <span id="assign"></span></span>
                            <span class="badge badge-danger" style="background-color: #dc3545">Unassigned : <span id="notassign"></span></span>
                            <span class="badge badge-secondary" style="background-color: #ffc107">Dialed : <span id="dial_leads"></span></span>
                            <span class="badge badge-info badge" style="background-color: #17a2b8">Manual Dialed : <span id="manualdial_cont"></span></span>
                            <span class="badge badge-warning badge" style="background-color: #6c757d">Not Dialed : <span id="notdial"></span></span>
                        </div>
                        <br>
                        <div id="countdown">
                            <div id="countdown-number"></div>
                            <svg id="disp_timer">
                            <circle r="18" cx="20" cy="20" ></circle>
                            </svg>
                        </div>
                    </div>
                </div>

            </div>


            <div class="col-md-6" id="col_border">
                <div class="card">
                    <div class="card-body">
                        <br>
                        <h6>Time Distribution</h6>
                        <canvas id="time_distributionChart" width="280" height="100"></canvas>
                    </div>
                </div>
            </div>



        </div>

        <div class="card">
            <div class="card-header" style="background-color: #E5E8E8; padding: 10px;">
                <b>AGENT STATUS</b>
            </div>
            <br>
            <table id="myTable" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%"></table>
        </div>




        <div class="row">
            <div class="col-md-6" id="col_border">
                <div class="card">
                    <div class="card-body">
                        <br>
                        <h6>Talk Duration</h6>
                        <canvas id="today_duration" width="280" height="100"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-md-6" id="col_border">
                <div class="card">
                    <div class="card-body">
                        <br>
                        <h6>Disposition</h6>
                        <canvas id="pieChart-disposition" width="380" height="200"></canvas>
                    </div>
                </div>
            </div>



        </div>

        <div class="row">


            <div class="col-md-6" id="col_border">
                <div class="card">
                    <div class="card-body">
                        <br>
                        <h6>Total Talk</h6>
                        <canvas id="totaltalk_duration" width="280" height="100"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-md-6" id="col_border">
                <div class="card">
                    <div class="card-body">
                        <br>
                        <h6>Login Activity</h6>
                        <canvas id="today_loginactivity" width="280" height="100"></canvas>
                    </div>
                </div>
            </div>

        </div>


        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
        <script src="Chartjs/Chart.js"></script>

        <!-- javascript here -->
        <script type="text/javascript">

            $(document).ready(function () {
                var i, assign, table_row;
                var countdown_time = 10;
                var in_seconds = countdown_time + '000';

                document.getElementById('disp_timer').innerHTML = '<circle r="18" cx="20" cy="20" style="stroke-dasharray: 113px; stroke-dashoffset: 0px; stroke-linecap: round; stroke-width: 4px; stroke: white; fill: none; stroke:#17a2b8; animation: countdown ' + countdown_time + 's linear infinite forwards;"></circle>';

                $("#campaign").change(function (event) {
                    var camp_selected = $(this).val();
                    // filter load
                    $.ajax(
                            {
                                type: "POST",
                                url: "fetch_realtime.php",
                                dataType: 'json',
                                data: {
                                    'camp_selected': camp_selected,
                                },
                                success: function (response)
                                {
                                    document.getElementById('total_live').innerHTML = response.total;
                                    document.getElementById('talk_live').innerHTML = response.dial;
                                    document.getElementById('pause_live').innerHTML = response.pause;
                                    document.getElementById('break_live').innerHTML = response.break;
                                    document.getElementById('logout_live').innerHTML = response.logout;

                                    document.getElementById('total_leads').innerHTML = response.total_leads;
                                    document.getElementById('assign').innerHTML = response.assign;
                                    document.getElementById('notassign').innerHTML = response.notassign;
                                    document.getElementById('dial_leads').innerHTML = response.dial_leads;
                                    document.getElementById('notdial').innerHTML = response.notdial;
                                    document.getElementById('manualdial_cont').innerHTML = response.manualdial_cont;


                                    // table data starts
                                    table_row = '<thead style="background-color: #2471A3; color: #ffffff;"> <tr> <th class="th-sm">Name</th> <th class="th-sm">Campaign</th> <th class="th-sm">Success Calls</th> <th class="th-sm">Failed Calls</th> <th class="th-sm">Current Event</th> <th class="th-sm">Current Event Duration</th> <th class="th-sm">Info</th> <th class="th-sm">Login Time</th> <th class="th-sm"><span class="badge badge-success" style="width: 30%; margin-right: 2px; background-color: #28a745; padding: 1px;">Talk</span><span class="badge badge-danger" style="width: 30%; margin-right: 2px; background-color: #dc3545; padding: 1px;">Pause</span><span class="badge badge-secondary" style="width: 30%; margin-right: 2px; background-color: #ffc107; padding: 1px;">Break</span></th> </tr> </thead><tbody>';
                                    for (i in response.username) {
                                        var td_data, evt_time, status_table;

                                        status_table = "<table class='table table-striped table-bordered'><tr  style='width:" + response.total_percentage[i] + "%'>";
                                        if (response.talk_percentage[i] > 0) {
                                            status_table += "<td style='width:" + response.talk_percentage[i] + "%;background-color: #28a745;' title=" + response.show_talk_time[i] + "></td>";
                                        }

                                        if (response.pause_time_per[i] > 0) {
                                            status_table += "<td style='width:" + response.pause_time_per[i] + "%;background-color: #dc3545;' title=" + response.show_pause_time[i] + "></td>";
                                        }

                                        if (response.break_time_per[i] > 0) {
                                            status_table += "<td style='width:" + response.break_time_per[i] + "%;background-color: #ffc107;' title=" + response.show_break_time[i] + "></td>";
                                        }
                                        status_table += "</tr></table>";

                                        // dispo check
                                        if (response.dispo_status[i] == 'PAUSE') {
                                            td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545;">PAUSE</span></td>';
                                        } else if (response.dispo_status[i] == 'DIAL') {
                                            td_data = '<td><span class="badge badge-success" style="background-color: #28a745">INCALL</span></td>';
                                        } else if (response.dispo_status[i] == 'LOGOUT') {
                                            td_data = '<td><span class="badge badge-info badge" style="background-color: #17a2b8">LOGOUT</span></td>';
                                        } else if (response.dispo_status[i] == 'LOGIN') {
                                            td_data = '<td><span class="badge badge-primary" style="background-color: #007bff;">LOGIN</span></td>';
                                        } else if (response.dispo_status[i] == 'BREAK') {
                                            td_data = '<td><span class="badge badge-secondary" style="background-color: #ffc107">BREAK</span></td>';
                                        } else if (response.dispo_status[i] == 'userdispo') {
                                            td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545">PAUSE</span></td>';
                                        } else {
                                            td_data = '<td><span class="badge badge-primary" style="background-color: #007bff">' + response.dispo_status[i] + '</span></td>';
                                        }
                                        // check event time greater than 1 hour
                                        if (response.event_check[i] > response.diff_check[i]) {
                                            evt_time = '<td style="background-color:red;">' + response.event_time[i] + '</td>';
                                        } else {
                                            evt_time = '<td>' + response.event_time[i] + '</td>';
                                        }

                                        table_row += '<tr><td style="background-color:' + response.column_background[i] + '">' + response.username[i] + '</td><td>' + response.campaign_name[i] + '</td><td>' + response.successful_calls[i] + '</td><td>' + response.failed_calls[i] + '</td>' + td_data + evt_time + '<td>' + response.info[i] + '</td><td>' + response.login_total[i] + '</td><td>' + status_table + '</td></tr>';
                                    }
                                    // table data ends
                                    table_row += '</tbody><tfoot> <tr> <th></th> <th></th> <th>Total: ' + response.success_total_call + '</th> <th>Total: ' + response.unsuccess_total_call + '</th> <th></th> <th></th> <th></th> <th></th> <th></th> </tr> </tfoot>';

                                    document.getElementById('myTable').innerHTML = table_row;
                                }
                            });
                });

                function refresh_div() {
                    var e = document.getElementById("campaign");
                    var sel_value = e.options[e.selectedIndex].value;

                    // refresh load
                    $.ajax(
                            {
                                type: "POST",
                                url: "fetch_realtime.php",
                                dataType: 'json',
                                data: {
                                    'camp_selected': sel_value,
                                },
                                success: function (response)
                                {
                                    document.getElementById('total_live').innerHTML = response.total;
                                    document.getElementById('talk_live').innerHTML = response.dial;
                                    document.getElementById('pause_live').innerHTML = response.pause;
                                    document.getElementById('break_live').innerHTML = response.break;
                                    document.getElementById('logout_live').innerHTML = response.logout;

                                    document.getElementById('total_leads').innerHTML = response.total_leads;
                                    document.getElementById('assign').innerHTML = response.assign;
                                    document.getElementById('notassign').innerHTML = response.notassign;
                                    document.getElementById('dial_leads').innerHTML = response.dial_leads;
                                    document.getElementById('notdial').innerHTML = response.notdial;
                                    document.getElementById('manualdial_cont').innerHTML = response.manualdial_cont;


                                    // table data starts
                                    table_row = '<thead style="background-color: #2471A3; color: #ffffff;"> <tr> <th class="th-sm">Name</th> <th class="th-sm">Campaign</th> <th class="th-sm">Success Calls</th> <th class="th-sm">Failed Calls</th> <th class="th-sm">Current Event</th> <th class="th-sm">Current Event Duration</th> <th class="th-sm">Info</th> <th class="th-sm">Login Time</th> <th class="th-sm"><span class="badge badge-success" style="width: 30%; margin-right: 2px; background-color: #28a745; padding: 1px;">Talk</span><span class="badge badge-danger" style="width: 30%; margin-right: 2px; background-color: #dc3545; padding: 1px;">Pause</span><span class="badge badge-secondary" style="width: 30%; margin-right: 2px; background-color: #ffc107; padding: 1px;">Break</span></th> </tr> </thead><tbody>';
                                    for (i in response.username) {
                                        var td_data, evt_time, status_table;

                                        status_table = "<table class='table table-striped table-bordered'><tr  style='width:" + response.total_percentage[i] + "%'>";
                                        if (response.talk_percentage[i] > 0) {
                                            status_table += "<td style='width:" + response.talk_percentage[i] + "%;background-color: #28a745;' title=" + response.show_talk_time[i] + "></td>";
                                        }

                                        if (response.pause_time_per[i] > 0) {
                                            status_table += "<td style='width:" + response.pause_time_per[i] + "%;background-color: #dc3545;' title=" + response.show_pause_time[i] + "></td>";
                                        }

                                        if (response.break_time_per[i] > 0) {
                                            status_table += "<td style='width:" + response.break_time_per[i] + "%;background-color: #ffc107;' title=" + response.show_break_time[i] + "></td>";
                                        }
                                        status_table += "</tr></table>";

                                        // dispo check
                                        if (response.dispo_status[i] == 'PAUSE') {
                                            td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545;">PAUSE</span></td>';
                                        } else if (response.dispo_status[i] == 'DIAL') {
                                            td_data = '<td><span class="badge badge-success" style="background-color: #28a745">INCALL</span></td>';
                                        } else if (response.dispo_status[i] == 'LOGOUT') {
                                            td_data = '<td><span class="badge badge-info badge" style="background-color: #17a2b8">LOGOUT</span></td>';
                                        } else if (response.dispo_status[i] == 'LOGIN') {
                                            td_data = '<td><span class="badge badge-primary" style="background-color: #007bff;">LOGIN</span></td>';
                                        } else if (response.dispo_status[i] == 'BREAK') {
                                            td_data = '<td><span class="badge badge-secondary" style="background-color: #ffc107">BREAK</span></td>';
                                        } else if (response.dispo_status[i] == 'userdispo') {
                                            td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545">PAUSE</span></td>';
                                        } else {
                                            td_data = '<td><span class="badge badge-primary" style="background-color: #007bff">' + response.dispo_status[i] + '</span></td>';
                                        }
                                        // check event time greater than 1 hour
                                        if (response.event_check[i] > response.diff_check[i]) {
                                            evt_time = '<td style="background-color:red;">' + response.event_time[i] + '</td>';
                                        } else {
                                            evt_time = '<td>' + response.event_time[i] + '</td>';
                                        }

                                        table_row += '<tr><td style="background-color:' + response.column_background[i] + '">' + response.username[i] + '</td><td>' + response.campaign_name[i] + '</td><td>' + response.successful_calls[i] + '</td><td>' + response.failed_calls[i] + '</td>' + td_data + evt_time + '<td>' + response.info[i] + '</td><td>' + response.login_total[i] + '</td><td>' + status_table + '</td></tr>';
                                    }
                                    // table data ends
                                    table_row += '</tbody><tfoot> <tr> <th></th> <th></th> <th>Total: ' + response.success_total_call + '</th> <th>Total: ' + response.unsuccess_total_call + '</th> <th></th> <th></th> <th></th> <th></th> <th></th> </tr> </tfoot>';

                                    document.getElementById('myTable').innerHTML = table_row;

                                }
                            });
                }
                t = setInterval(refresh_div, in_seconds);

                // default load
                $.ajax(
                        {
                            type: "POST",
                            url: "fetch_realtime.php",
                            dataType: 'json',
                            data: {
                                'postdata': 1,
                            },
                            success: function (response)
                            {
                                console.log(response.test);
                                assign += '<option value="ALL">All</option>';

                                for (i in response.campaigns) {

                                    assign += "<option value='" + response.campaigns[i] + "'>" + response.campaigns[i] + "</option>";
                                }
                                document.getElementById('campaign').innerHTML = assign;

                                document.getElementById('total_live').innerHTML = response.total;
                                document.getElementById('talk_live').innerHTML = response.dial;
                                document.getElementById('pause_live').innerHTML = response.pause;
                                document.getElementById('break_live').innerHTML = response.break;
                                document.getElementById('logout_live').innerHTML = response.logout;

                                document.getElementById('total_leads').innerHTML = response.total_leads;
                                document.getElementById('assign').innerHTML = response.assign;
                                document.getElementById('notassign').innerHTML = response.notassign;
                                document.getElementById('dial_leads').innerHTML = response.dial_leads;
                                document.getElementById('notdial').innerHTML = response.notdial;
                                document.getElementById('manualdial_cont').innerHTML = response.manualdial_cont;


                                // table data starts
                                table_row = '<thead style="background-color: #2471A3; color: #ffffff;"> <tr> <th class="th-sm">Name</th> <th class="th-sm">Campaign</th> <th class="th-sm">Success Calls</th> <th class="th-sm">Failed Calls</th> <th class="th-sm">Current Event</th> <th class="th-sm">Current Event Duration</th> <th class="th-sm">Info</th> <th class="th-sm">Login Time</th> <th class="th-sm"><span class="badge badge-success" style="width: 30%; margin-right: 2px; background-color: #28a745; padding: 1px;">Talk</span><span class="badge badge-danger" style="width: 30%; margin-right: 2px; background-color: #dc3545; padding: 1px;">Pause</span><span class="badge badge-secondary" style="width: 30%; margin-right: 2px; background-color: #ffc107; padding: 1px;">Break</span></th> </tr> </thead><tbody>';

                                for (i in response.username) {
                                    var td_data, evt_time, status_table;

                                    status_table = "<table class='table table-striped table-bordered'><tr  style='width:" + response.total_percentage[i] + "%'>";
                                    if (response.talk_percentage[i] > 0) {
                                        status_table += "<td style='width:" + response.talk_percentage[i] + "%;background-color: #28a745;' title=" + response.show_talk_time[i] + "></td>";
                                    }

                                    if (response.pause_time_per[i] > 0) {
                                        status_table += "<td style='width:" + response.pause_time_per[i] + "%;background-color: #dc3545;' title=" + response.show_pause_time[i] + "></td>";
                                    }

                                    if (response.break_time_per[i] > 0) {
                                        status_table += "<td style='width:" + response.break_time_per[i] + "%;background-color: #ffc107;' title=" + response.show_break_time[i] + "></td>";
                                    }
                                    status_table += "</tr></table>";

                                    // dispo check
                                    if (response.dispo_status[i] == 'PAUSE') {
                                        td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545;">PAUSE</span></td>';
                                    } else if (response.dispo_status[i] == 'DIAL') {
                                        td_data = '<td><span class="badge badge-success" style="background-color: #28a745">INCALL</span></td>';
                                    } else if (response.dispo_status[i] == 'LOGOUT') {
                                        td_data = '<td><span class="badge badge-info badge" style="background-color: #17a2b8">LOGOUT</span></td>';
                                    } else if (response.dispo_status[i] == 'LOGIN') {
                                        td_data = '<td><span class="badge badge-primary" style="background-color: #007bff;">LOGIN</span></td>';
                                    } else if (response.dispo_status[i] == 'BREAK') {
                                        td_data = '<td><span class="badge badge-secondary" style="background-color: #ffc107">BREAK</span></td>';
                                    } else if (response.dispo_status[i] == 'userdispo') {
                                        td_data = '<td><span class="badge badge-danger" style="background-color: #dc3545">PAUSE</span></td>';
                                    } else {
                                        td_data = '<td><span class="badge badge-primary" style="background-color: #007bff">' + response.dispo_status[i] + '</span></td>';
                                    }
                                    // check event time greater than 1 hour
                                    if (response.event_check[i] > response.diff_check[i]) {
                                        evt_time = '<td style="background-color:red;">' + response.event_time[i] + '</td>';
                                    } else {
                                        evt_time = '<td>' + response.event_time[i] + '</td>';
                                    }

                                    table_row += '<tr><td style="background-color:' + response.column_background[i] + '">' + response.username[i] + '</td><td>' + response.campaign_name[i] + '</td><td>' + response.successful_calls[i] + '</td><td>' + response.failed_calls[i] + '</td>' + td_data + evt_time + '<td>' + response.info[i] + '</td><td>' + response.login_total[i] + '</td><td>' + status_table + '</td></tr>';
                                }
                                // table data ends
                                table_row += '</tbody><tfoot> <tr> <th></th> <th></th> <th>Total: ' + response.success_total_call + '</th> <th>Total: ' + response.unsuccess_total_call + '</th> <th></th> <th></th> <th></th> <th></th> <th></th> </tr> </tfoot>';
                                document.getElementById('myTable').innerHTML = table_row;
                            }
                        });

                // countdown
                var countdownNumberEl = document.getElementById('countdown-number');
                var countdown = countdown_time;

                countdownNumberEl.textContent = countdown;

                setInterval(function () {
                    countdown = --countdown <= 0 ? 10 : countdown;

                    countdownNumberEl.textContent = countdown;
                }, 1000);
                // 

            });
        </script>
        <!-- charts -->
        <script type="text/javascript">
            $(document).ready(function ()
            {
                // fetching graph data
                $.ajax(
                        {
                            type: "POST",
                            url: "realtime_custom_widget.php",
                            dataType: 'json',
                            data: {
                                'post_data': 1,
                            },
                            success: function (response)
                            {

                                var dispo_cnt = response.chart_dispo;
                                var dispo_status = response.chart_dispo_status;
                                // Today call by duration
                                var thiry = response.res_thirty;
                                var sixty = response.res_sixty;
                                var one_twenty = response.res_one_twenty;
                                var three_hundred = response.res_three_hundred;
                                var three_hundred_more = response.res_three_hundred_more;

                                var callduration_status = response.callduration_status;
                                var callduration_count = response.callduration_count;

                                var time_distribution_hours = response.time_distribution_hours;
                                var timedistribution_calls = response.time_distribution_totalcalls;

                                var totaltalk_hours = response.totaltalk_hours;
                                var totaltalk_duration = response.totaltalk_duration;


                                var login_hours = response.login_hours;
                                var login_count = response.login_count;
                                
                                var logout_hours = response.logout_hours;
                                var logout_count = response.logout_count;
                                
                                var active_agent_hours = response.active_agent_hours;
                                var active_agent_count = response.active_agent_count;



                                todayDispo(dispo_cnt, dispo_status);
                                todayCallsduration(callduration_count, callduration_status);
                                timeDistribution(time_distribution_hours, timedistribution_calls);
                                totalTalkDuration(totaltalk_hours, totaltalk_duration);
                                todayLoginActivity(login_hours, login_count,logout_hours,logout_count,active_agent_hours,active_agent_count);

                            }
                        });



                function todayCallsduration(callduration_count, callduration_status) {

                    console.log(callduration_count);
                    // colors
                    var chart_colors = ["#1DE9B6", "#2196F3", "#F50057", "#5E35B1", "#B10358", "#F1C40F", "#7D3C98", "#E74C3C", "#FE3A06", "#03D5E7", "#04B014", "#BE4B05", "#BE0521", "#EA7385"];
                    var dynamic_colors = [];
                    for (var i = 0; i < callduration_status.length; i++) {
                        dynamic_colors.push(chart_colors[i])
                    }

                    Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

                    var helpers = Chart.helpers;
                    var defaults = Chart.defaults;

                    Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
                        updateElement: function (arc, index, reset) {
                            var _this = this;
                            var chart = _this.chart,
                                    chartArea = chart.chartArea,
                                    opts = chart.options,
                                    animationOpts = opts.animation,
                                    arcOpts = opts.elements.arc,
                                    centerX = (chartArea.left + chartArea.right) / 2,
                                    centerY = (chartArea.top + chartArea.bottom) / 2,
                                    startAngle = opts.rotation, // non reset case handled later
                                    endAngle = opts.rotation, // non reset case handled later
                                    dataset = _this.getDataset(),
                                    circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
                                    innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
                                    outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
                                    custom = arc.custom || {},
                                    valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

                            helpers.extend(arc, {
                                // Utility
                                _datasetIndex: _this.index,
                                _index: index,
                                // Desired view properties
                                _model: {
                                    x: centerX + chart.offsetX,
                                    y: centerY + chart.offsetY,
                                    startAngle: startAngle,
                                    endAngle: endAngle,
                                    circumference: circumference,
                                    outerRadius: outerRadius,
                                    innerRadius: innerRadius,
                                    label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
                                },
                                draw: function () {
                                    var ctx = this._chart.ctx,
                                            vm = this._view,
                                            sA = vm.startAngle,
                                            eA = vm.endAngle,
                                            opts = this._chart.config.options;

                                    var labelPos = this.tooltipPosition();
                                    var segmentLabel = vm.circumference / opts.circumference * 100;

                                    ctx.beginPath();

                                    ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
                                    ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);

                                    ctx.closePath();
                                    ctx.strokeStyle = vm.borderColor;
                                    ctx.lineWidth = vm.borderWidth;

                                    ctx.fillStyle = vm.backgroundColor;

                                    ctx.fill();
                                    ctx.lineJoin = 'bevel';

                                    if (vm.borderWidth) {
                                        ctx.stroke();
                                    }

                                    if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
                                        ctx.beginPath();
                                        ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
                                        ctx.fillStyle = "#fff";
                                        ctx.textBaseline = "top";
                                        ctx.textAlign = "center";

                                        // Round percentage in a way that it always adds up to 100%
                                        ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
                                    }
                                }
                            });

                            var model = arc._model;
                            model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
                            model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
                            model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
                            model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

                            // Set correct angles if not resetting
                            if (!reset || !animationOpts.animateRotate) {
                                if (index === 0) {
                                    model.startAngle = opts.rotation;
                                } else {
                                    model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
                                }

                                model.endAngle = model.startAngle + model.circumference;
                            }

                            arc.pivot();
                        }
                    });

                    var config = {
                        type: 'doughnutLabels',
                        data: {
                            datasets: [{
                                    data: callduration_count,
                                    backgroundColor: chart_colors,
                                    label: 'Dataset 1'
                                }],
                            labels: callduration_status
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'right',
                            },
                            title: {
                                display: false,
                                text: 'Chart.js Doughnut Chart'
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            }
                        }
                    };

                    var ctx = document.getElementById("today_duration").getContext("2d");
                    new Chart(ctx, config);

                }


                function todayDispo(dispo_cnt, dispo_status) {
                    // pie chart Today call duration 
                    var ctx = document.getElementById("pieChart-disposition");
                    var myChart = new Chart(ctx, {
                        type: 'pie',
                        data: {
                            labels: dispo_status,
                            datasets: [{
                                    label: '# of Tomatoes',
                                    data: dispo_cnt,
                                    backgroundColor: [
                                        "#9C27B0", "#1DE9B6", "#2196F3", "#F50057", "#5E35B1"
                                    ],
                                    borderColor: [
                                        "#9C27B0", "#1DE9B6", "#2196F3", "#F50057", "#5E35B1"
                                    ],
                                    borderWidth: 1
                                }]
                        },
                        options: {
                            title: {
                                display: true,
                                position: 'top'
                            },
                            //cutoutPercentage: 40,
                            responsive: false,
                            legend: {
                                position: "right"
                            },
                            tooltips: {
                                enabled: true
                            }

                        }
                    });
                    //pie chart Today call duration ends
                }


                function timeDistribution(time_distribution_hours, timedistribution_calls) {

                    var canvas = document.getElementById("time_distributionChart");
                    var ctx = canvas.getContext('2d');

                    // Data with datasets options
                    var data = {
                        labels: time_distribution_hours,
                        datasets: [
                            {
                                label: "No Of Calls",
                                fill: false,
                                backgroundColor: "#2196F3",
                                borderColor: "#2196F3",
                                data: timedistribution_calls
                            }
                        ]
                    };

                    // Notice how nested the beginAtZero is
                    var options = {
                        title: {
                            display: true,
                            position: 'top'
                        },
                        scales: {
                            yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                        },
                        legend: {position: 'bottom'},
                    };

                    // Chart declaration:
                    var myBarChart = new Chart(ctx, {
                        type: 'line',
                        data: data,
                        options: options
                    });
                }

                function totalTalkDuration(totaltalk_hours, totaltalk_duration) {

                    var canvas = document.getElementById("totaltalk_duration");
                    var ctx = canvas.getContext('2d');

                    // Data with datasets options
                    var data = {
                        labels: totaltalk_hours,
                        datasets: [
                            {
                                label: "No Of Calls",
                                fill: false,
                                backgroundColor: "#2196F3",
                                borderColor: "#2196F3",
                                data: totaltalk_duration
                            }
                        ]
                    };

                    // Notice how nested the beginAtZero is
                    var options = {
                        title: {
                            display: true,
                            position: 'top'
                        },
                        scales: {
                            yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                        },
                        legend: {position: 'bottom'},
                    };

                    // Chart declaration:
                    var myBarChart = new Chart(ctx, {
                        type: 'line',
                        data: data,
                        options: options
                    });

                }

              function  todayLoginActivity(login_hours, login_count,logout_hours,logout_count,active_agent_hours,active_agent_count){
                  
                   var canvas = document.getElementById("today_loginactivity");
                    var ctx = canvas.getContext('2d');


                    var dataFirst = {
                        label: "Login",
                        data: login_count,
                        lineTension: 0,
                        fill: false,
                        borderColor: 'blue'
                    };

                    var dataSecond = {
                        label: "Logout",
                        data: logout_count,
                        lineTension: 0,
                        fill: false,
                        borderColor: 'red'
                    };


                    var dataThird = {
                        label: "Active",
                        data: active_agent_count,
                        lineTension: 0,
                        fill: false,
                        borderColor: 'green'
                    };

                    
                    // Data with datasets options
                    var data = {
                        labels: login_hours,
                        datasets: [
                            dataFirst, dataSecond, dataThird
                        ]
                    };

                    // Notice how nested the beginAtZero is
                    var options = {
                        title: {
                            display: true,
                            position: 'top'
                        },
                        scales: {
                            yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                        },
                        legend: {position: 'bottom'},
                    };

                    // Chart declaration:
                    var myBarChart = new Chart(ctx, {
                        type: 'line',
                        data: data,
                        options: options
                    });
                  

                }

            });
        </script>
    </body>
</html>
