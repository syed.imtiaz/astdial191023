<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$con = mysqli_connect($hostname,$username,$password);
mysqli_select_db($con,$dbName);

if ($con->connect_error) {
 die("Connection failed: " . $con->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
$cur_date = date("Y-m-d H:i:s");

$record_id = $_POST['record_id'];
$loggedin_user = $_POST['loggedin_user'];
$main_disp = $_POST['main_selected'];

$get_name = mysqli_query($con, "SELECT campaignname FROM vtiger_campaign WHERE campaignid = '$record_id' ");
$row_name = mysqli_fetch_assoc($get_name);
$campaignName = $row_name['campaignname'];

$camp = " AND vtiger_campaign.campaignname = '".$campaignName."' " ;

$mod_dupli = mysqli_query($con, "SELECT crmid FROM vtiger_modtracker_basic WHERE crmid = '$record_id' ");
$row_dupli = mysqli_num_rows($mod_dupli);

// if($row_dupli == 0){

	$sel_id1 = mysqli_query($con, "SELECT id FROM vtiger_modtracker_basic_seq ");
	$row_id1 = mysqli_fetch_assoc($sel_id1);
	$seq_id1 = $row_id1['id'];
	$seq_id_plus1 = $row_id1['id']+1;

	$insert_camp = "INSERT INTO `vtiger_modtracker_basic` ( `id` , `crmid` , `module` , `whodid` , `changedon` , `status` )
	VALUES (
	'$seq_id_plus1', '$record_id', 'Campaigns', '$loggedin_user', '$cur_date', '0'
	)";
	if($con->query($insert_camp) === TRUE){

		$insert_camp_detail = "INSERT INTO `vtiger_modtracker_detail` SET id = '$seq_id_plus1',`fieldname` = 'description',
		`prevalue` = 'Event: Recycled | Campaign : ".$campaignName." | Dispo : ".$main_disp." ',
		`postvalue` = 'Campaign : ".$campaignName."' ";
		$con->query($insert_camp_detail);

		$update_seq1 = "UPDATE vtiger_modtracker_basic_seq SET id = '$seq_id_plus1' ";
		mysqli_query($con, $update_seq1);

	}

// }


// RECYCLING LEADS WITH DISPOSITION
if($_POST['post_filter'] == 'post_recycle'){

	// $get_contacts = mysqli_query($con, "SELECT contactid FROM vtiger_campaigncontrel WHERE campaignid = '$record_id' AND dispo = '$main_disp' AND ast_update = 1 ");
	// while($row_contacts = mysqli_fetch_assoc($get_contacts)){
	// 	$contact_id = $row_contacts['contactid'];

	// 	$sel_id = mysqli_query($con, "SELECT id FROM vtiger_modtracker_basic_seq ");
	// 	$row_id = mysqli_fetch_assoc($sel_id);
	// 	$seq_id = $row_id['id'];
	// 	$seq_id_plus = $row_id['id']+1;

	// 	$callstatus = "INSERT into vtiger_modtracker_basic(id,crmid,module,whodid,changedon,status) 
	// 	values('$seq_id_plus','$contact_id','Contacts','$loggedin_user','".$cur_date."','0')";
	// 	if(mysqli_query($con, $callstatus)===TRUE){

	// 		$update_details = " INSERT INTO vtiger_modtracker_detail SET id = '$seq_id_plus', fieldname = 'lastcall_status', prevalue = 'Event: Recycled | Campaign : ".$campaignName." | Dispo : ".$main_disp."',postvalue = 'Campaign : ".$campaignName."' ";
	// 		if(mysqli_query($con,$update_details) === TRUE){
	// 			$seq_id_plus = $seq_id_plus + 1;
	// 			$update_seq = "UPDATE vtiger_modtracker_basic_seq SET id = '$seq_id_plus' ";
	// 			mysqli_query($con, $update_seq);
	// 		}
	// 	}

	// }


	if($main_disp == "Auto Disposed"){

		$get_not_dialed = mysqli_query($con, "SELECT contactid FROM vtiger_campaigncontrel WHERE ast_update = '1' AND (dispo = '0' OR dispo = '' OR dispo = 'Auto Disposed') AND campaignid = '".$record_id."' ");
		
		while($row_not_dialed = mysqli_fetch_assoc($get_not_dialed)){

			$not_dialed_con = $row_not_dialed['contactid'];
			echo $not_dialed_con;
			$query_update = "UPDATE vtiger_campaigncontrel INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid set ast_update='0',status='0',user='0',customer_name='0',dispo='0',subdispo='0',camp_name = '0', camp_status = '0' WHERE contactid = '".$not_dialed_con."' ".$camp." ";

			echo $query_update;
			$num_recycle = mysqli_num_rows($query_update);

			if ($con->query($query_update) === TRUE) {
				$msg = 'Recycled Successfully';
				$status  = 1;
			}
		}

		}else{

		$query_update = "UPDATE vtiger_campaigncontrel 
		SET campaignid = '$record_id', campaignrelstatusid = '1', ast_update = 0, user = 0, customer_name = 0, status = 0, dispo = 0, subdispo = 0, callback = 0, entry_date = '$cur_date', modify_date = '$cur_date', comments = 0, camp_name = 0, camp_status = 0 WHERE campaignid = '$record_id' AND dispo = '$main_disp' AND ast_update = 1 ";
		if ($con->query($query_update) === TRUE) {
			$msg = 'Recycled Successfully';
			$status  = 1;
		}

	}




}else{

	$to_campaign = $_POST['camp_select'];
	$main_disp = $_POST['main_selected'];

	// TRANSFERRING LEADS FROM ONE CAMPAIGN TO ANOTHER WITH DISPOSITON

	$mod_dupli = mysqli_query($con, "SELECT crmid FROM vtiger_modtracker_basic WHERE crmid = '$record_id' ");
	$row_dupli = mysqli_num_rows($mod_dupli);

	$sel_id1 = mysqli_query($con, "SELECT id FROM vtiger_modtracker_basic_seq ");
	$row_id1 = mysqli_fetch_assoc($sel_id1);
	$seq_id1 = $row_id1['id'];
	$seq_id_plus1 = $row_id1['id']+1;

	$insert_camp = "INSERT INTO `vtiger_modtracker_basic` ( `id` , `crmid` , `module` , `whodid` , `changedon` , `status` )
	VALUES (
	'$seq_id_plus1', '$record_id', 'Campaigns', '$loggedin_user', '$cur_date', '0'
	)";
	if($con->query($insert_camp) === TRUE){

		$insert_camp_detail = "INSERT INTO `vtiger_modtracker_detail` SET id = '$seq_id_plus1',`fieldname` = 'description',
		`prevalue` = 'Event: Transfer Leads | From Campaign : ".$campaignName." | Dispo : ".$main_disp." ',
		`postvalue` = 'Campaign : ".$to_campaign."' ";
		$con->query($insert_camp_detail);

		$update_seq1 = "UPDATE vtiger_modtracker_basic_seq SET id = '$seq_id_plus1' ";
		mysqli_query($con, $update_seq1);

	}


	if($main_disp == ""){
		$main_disps = " " ;
	}else{
		$main_disps = "AND dispo = '".$main_disp."'  " ;
	}
	
	$get_camp_name = mysqli_query($con, "SELECT campaignname FROM vtiger_campaign WHERE campaignid = '".$record_id."' ");
	$row_camp_name = mysqli_fetch_assoc($get_camp_name);
	$from_campaign = $row_camp_name['campaignname'];

	$get_from = mysqli_query($con, "SELECT campaignid FROM vtiger_campaign WHERE campaignname = '$from_campaign'");
	$row_from = mysqli_fetch_assoc($get_from);
	$from_campid = $row_from['campaignid'];

	$get_to = mysqli_query($con, "SELECT campaignid FROM vtiger_campaign WHERE campaignname ='$to_campaign'");
	$row_to = mysqli_fetch_assoc($get_to);
	$to_campid = $row_to['campaignid'];

	$get_contacts = mysqli_query($con, "SELECT contactid FROM vtiger_campaigncontrel WHERE campaignid = '.$from_campid.' AND dispo = '$main_disp'");
	// $count_leads = mysqli_num_rows($get_contacts);
	$count_leads = 0;
	while($row_contacts = mysqli_fetch_assoc($get_contacts)){

		$contact_ids = $row_contacts['contactid'];

		$insert_leads = "INSERT INTO vtiger_campaigncontrel SET campaignid = '".$to_campid."', contactid = '".$contact_ids."', campaignrelstatusid = '1', ast_update = '0', user = '0', customer_name = '0', status = '0', dispo = '0', subdispo = '0', callback = '0', callback_time = '0000-00-00 00:00:00', entry_date = '$cur_date', modify_date='$cur_date', comments='' ";
			if ($con->query($insert_leads) === TRUE) {
		    	$msg = 'Leads Transferred Successfully';
				$status  = 1;
				$count_leads++;
			}
	}



}
	$leads_data = array(
		"msg"=>$msg,
		"status"=>$status,
		"count_leads"=>$count_leads,
		"from_campaign"=>$from_campaign,
		"to_campaign"=>$to_campaign,
		"main_disp"=>$main_disp,
		"camp_id"=>$from_campid,
		);
	echo json_encode($leads_data);

?>
