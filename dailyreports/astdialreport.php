<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once("../config.inc.php");
require_once("class.phpmailer.php");

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName = $dbconfig['db_name'];
$conn = @mysqli_connect($hostname, $username, $password);
mysqli_select_db($conn, $dbName);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    // echo "connected";
}

global $site_URL;
date_default_timezone_set("Asia/Kolkata");
$report_type = $_REQUEST['type']; //Define Report Type
$today = date('Y-m-d');
$yesterday = date('Y-m-d', strtotime("-1 days"));


if ($report_type == 'monthly') {
    $report_name = 'Monthly Report';
    //$first_date = date("Y-m-d", strtotime("first day of previous month"));
    //$last_date = date("Y-m-d", strtotime("last day of previous month"));
    $report_startdate = date("Y-m-d", strtotime("first day of this month"));
    $report_enddate = date("Y-m-d", strtotime("last day of this month"));
    $call_report_name = 'Last Month Calls';
    $login_report_name ='Last Month Login';
} else {
    $report_name = 'Weekly Report';
    $report_startdate = date('Y-m-d', strtotime('Last Monday', time()));
    $report_enddate = date('Y-m-d', strtotime('Last Sunday', time()));
    $call_report_name = 'Last 7 Days Calls';
    $login_report_name ='Last 7 Days Login';
}


$logindata = getLoginDetails($conn, $report_enddate, $report_startdate);
$hours_labels = $logindata['date_last_login'];
$login_data = $logindata['cnt_last_login'];
$login_activity_link = getLoginActivityData($hours_labels, $login_data,$login_report_name);


$call_trend_xaxis = getCallTrendsXAXIS($conn, $report_startdate, $report_enddate);
$calltrends_count = getCallTrendsYAXIS($conn, $call_trend_xaxis);
$connected_calls = $calltrends_count['connected'];
$notconnected_calls = $calltrends_count['notconnected'];

// LAST 7 DAYS TOTAL CALLS

$calltrends_link = getLast7DaysCalls($connected_calls, $notconnected_calls, $call_trend_xaxis,$call_report_name);

function getCallTrendsXAXIS($conn, $report_startdate, $report_enddate) {
    $query_lastdaterangeCalls = mysqli_query($conn, "SELECT COUNT(*) as total_lastweekcalls, modify_date FROM campaign_dial_status WHERE DATE(modify_date) <= '" . $report_enddate . "' AND DATE(modify_date) >= '" . $report_startdate . "' GROUP BY DATE(modify_date) ORDER BY modify_date ASC");
    while ($total_calls = mysqli_fetch_assoc($query_lastdaterangeCalls)) {
        $chart_lastweekcall[] = $total_calls['total_lastweekcalls'];
        $call_trend_xaxis[] = 	date('d-m-Y', strtotime($total_calls['modify_date']));
    }
    return $call_trend_xaxis;
}

function getCallTrendsYAXIS($conn, $call_trend_xaxis) {
    foreach ($call_trend_xaxis as $key) {
   $key = date('Y-m-d', strtotime($key));
        // NOT CONNNECTED CALLS
        $sql_notconnected = mysqli_query($conn, "SELECT COUNT(*) as notconnected FROM campaign_dial_status WHERE DATE(modify_date) = '" . $key . "' AND status = '2'");
        $row_notconnected = mysqli_fetch_assoc($sql_notconnected);
        $not_connected[] = $row_notconnected['notconnected'];
        //CONNNECTED CALLS
        $sql_connected = mysqli_query($conn, "SELECT COUNT(*) as connected FROM campaign_dial_status WHERE DATE(modify_date) = '" . $key . "' AND status = '1' GROUP BY DATE(modify_date)");
        $row_connected = mysqli_fetch_assoc($sql_connected);
        $connected[] = $row_connected['connected'];
    }
    $result_data = array('notconnected'=>$not_connected,'connected'=>$connected);
    return $result_data;
    
}

function getLoginDetails($conn, $report_enddate, $report_startdate) {
    $query_last_login = mysqli_query($conn, "SELECT COUNT(id) as login_cnt_seven, DATE(datetime) as login_date FROM user_callsummary WHERE DATE(datetime) <= '" . $report_enddate . "' AND DATE(datetime) >= '" . $report_startdate . "' group by DATE(datetime) order by DATE(datetime) ASC ");
    while ($row_last_login = mysqli_fetch_assoc($query_last_login)) {

        $date_last_login[] = date('d-m-Y', strtotime($row_last_login['login_date']));
        $cnt_last_login[] = $row_last_login['login_cnt_seven'];
    }
    $login_details = array("date_last_login" => $date_last_login,
        "cnt_last_login" => $cnt_last_login);
    return $login_details;
}

function getLast7DaysCalls($connected_calls, $notconnected_calls, $call_trend_xaxis,$call_report_name) {



    $not_connected_calls = ConvertString2Integer($notconnected_calls);
    $connected_calls = ConvertString2Integer($connected_calls);

    $last7daysCalls_data = [
        "type" => "bar",
        "data" => [
            "labels" => $call_trend_xaxis,
            "datasets" => [
                [
                    "label" => "Connected",
                    "backgroundColor" => "lightgreen",
                    "data" => $connected_calls
                ],
                [
                    "label" => "Not Connected",
                    "backgroundColor" => "skyblue",
                    "data" => $not_connected_calls
                ]
            ]
        ],
        "options" => [
            "title" => [
                "display" => true,
                "text" => $call_report_name,
            ],
            "legend" => [
                "position" => "bottom"
            ],
            "scales" => [
                "xAxes" => [
                    [
                        "stacked" => true
                    ]
                ],
                "yAxes" => [
                    [
                        "stacked" => true
                    ]
                ]
            ],
            "plugins" => [
                "datalabels" => [
                    "display" => true,
                    "font" => [
                        "style" => "bold"
                    ]
                ]
            ]
        ]
    ];


    $last7daysCalls_config = json_encode($last7daysCalls_data);
    $last7daysCalls_url = 'https://quickchart.io/chart?w=400&h=250&c=' . urlencode($last7daysCalls_config);
    return $last7daysCalls_url;
}

function ConvertString2Integer($inputarray) {
    $output_int_array = array_map(
            function($value) {
        return (int) $value;
    }, $inputarray
    );

    return $output_int_array;
}

function getLoginActivityData($hours_labels, $login_data,$login_report_name) {

    $logindata1 = ConvertString2Integer($login_data);
    $chartConfigArr = array(
        'type' => 'line',
        'data' => array(
            'labels' => $hours_labels,
            'datasets' => array(
                array(
                    'label' => 'Login',
                    'data' => $logindata1,
                    'fill' => false,
                    'borderColor' => 'blue',
                ),
            )
        ),
        'options' => array(
            'title' => [
                'display' => true,
                'text' => $login_report_name,
            ],
            'legend' => array(
                'position' => 'bottom',
            ),
            'plugins' => array(
                'datalabels' => array(
                    'display' => true,
                    'align' => 'left',
                    'backgroundColor' => '#e1abee',
                )
            ),
        )
    );

    $login_activity_config = json_encode($chartConfigArr);
    $login_activity_url = 'https://quickchart.io/chart?c=' . urlencode($login_activity_config);
    return $login_activity_url;
}

$htmlcontent = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>' . $report_name . '</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<style>
body{
            color: #090004;
            background-color: #f2f2f2;
            margin-left: 20%;
            margin-right: 20%;
            font-family: "Open Sans", sans-serif;
            font-weight: normal;
            font-size: 14px;
        }

        .card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            background-color: #ffffff;
            margin-bottom: 10px;
        }

        .container {
            padding: 2px 16px;
        }

        .grid-container {
            display: grid;
            grid-template-columns: auto auto auto;
           
        }

        p{
            text-align: left;
        }

        th, td {
            padding: 5px;
            
        }

        .button {
            background-color: #0ebcce;
            border: none;
            color: white;
            padding: 8px 16px;
            border-radius:20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            font-weight: bold;
            cursor: pointer;
        }

       
        ul.social-media-list {
            list-style-type: none;
            margin: 0;
            padding: 0;
            font-size: 0px; 
        }

        ul.social-media-list li {
            display: inline-block;
            margin: 2px;
        }

        ul.social-media-list img {
            padding: 5px;
            border-radius: 5px;
            width: 24px;
            height: 24px;
        }

        ul.social-media-list img:hover {
            background-color: #0ebcce;
        }
       

        img #meetn_graph, #pie_chart {
            width: 100%;
            height: auto;
        }

        .img-example {
            max-width: 100%;
        }

        .img-example__sparkline {
            max-width: 10em;
        }
        
div.polaroid {
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  text-align: center;
}

</style>
</head>

<body style="margin: 0; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
        <tr>
            <td style="padding: 10px 0 30px 0;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="750" style="border: 1px solid #cccccc; border-collapse: collapse;">
                   
     

<tr>
<td class="card"><div class="card" style="width: 100%;background-color: #ffffff">
                    <div class="container">
                        <p style="font-family:arial black;font-style:italic;color:#800080;font-size:35px;text-align:right">*astDIAL</p>
                        <h2 style="text-align: center; color:black ">' . $report_name . '</h2>
                        <h3 style="text-align: center; color:black ">Duration :  <span><b style="color: #339933;">' .$report_startdate. '</b> to <b style="color: #339933;">' . $report_enddate . '</span></h3>

                    </div>
                </div></td></tr>



<tr>
<td class="card">
<div class="polaroid" style="width: 100%;background-color: #ffffff">
<h3 style="text-align: left; padding-top: 20px; padding-left:20px;color:black">Call Trends </h3>
                     
                    <img class="img-example"  src="' . $calltrends_link . '">
                     
                </div> 
                </td>
</tr>


<tr>
<td class="card">
<div class="polaroid" style="width: 100%;background-color: #ffffff">

                     
                    <img class="img-example"  src="' . $login_activity_link . '">
                     
                </div> 
                </td>
</tr>
';




$htmlcontent .='

                    <tr>
                        <td bgcolor="#425767" style="padding: 30px 30px 30px 30px;">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
	
		<tr>
			
			<td style="width: 100%; text-align: center;">
			<h1 style="color: #ffffff; text-align: center;">Hey Admin!</h1>
			<h3 style="color: #ffffff; text-align: center;">Refer us and get amazing prices!</h3>
	
                        </tr>
                        <tr align="center">


<td style="width: 20%; align="center">
<table border="0" cellpadding="0" cellspacing="0">
<tr>

<td><!--Facebook icon-->
<a href="https://www.facebook.com/astcrm/" target="_blank">
<img alt="FaceBook|astCRM" src="https://www.astcrm.com/daily_report_icons/facebook_icon.png" width="24" height="24"/>
</a></td>&nbsp;

<td><!--LinkedIn icon-->
<a href="https://in.linkedin.com/company/astcrm/" target="_blank">
<img alt="LinkedIn|astCRM" src="https://www.astcrm.com/daily_report_icons/linkedin_icon.png" width="24" height="24"/>
</a></td>

</tr>
</table>
</td>



			
</tr>
</thead>
</table>


                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>

';

function getAdminUsersList($conn) {
    $emaillist = array();
    $select_email = "select email1,concat( first_name, ' ', last_name ) as username from vtiger_users where is_admin='on' and status='Active'";
    $q_email = mysqli_query($conn, $select_email);
    while ($row = mysqli_fetch_assoc($q_email)) {
        $username[] = $row['username'];
        $emaillist[] = $row['email1'];
    }

    return $emaillist;
}

$adminlist = getAdminUsersList($conn);

$crm_name = 'demo1-' . $report_name;

foreach ($adminlist as $email) {
    sendastReport($htmlcontent, $email, $crm_name);
}

function sendastReport($htmlcontent, $email, $crm_name) {
    echo $htmlcontent;


    /* $mail = new PHPMailer();
      $mail->SMTPDebug = 2;

      $mail->IsSMTP();                                      // set mailer to use SMTP
      $mail->Host = "ssl://smtp.gmail.com:465";  // specify main and backup server
      $mail->SMTPAuth = true;     // turn on SMTP authentication
      $mail->Username = "testrizwanc6m@gmail.com";  // SMTP username
      $mail->Password = "Test@c6m"; // SMTP password

      $mail->From = "testrizwanc6m@gmail.com";
      $mail->FromName = "TestRizwan";
      $mail->AddAddress("rizwan.cse7@gmail.com", "Rizwan");
      //$mail->AddAddress("rizwan.cse7@gmail.com");                  // name is optional
      $mail->AddReplyTo("testrizwanc6m@gmail.com", "ast");

      $mail->WordWrap = 50;                                 // set word wrap to 50 characters
      //$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
      //$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
      $mail->IsHTML(true);                                  // set email format to HTML

      $mail->Subject = $crm_name;

      $mail->Body = $htmlcontent;
      $mail->AltBody = "This is the body in plain text for non-HTML mail clients";

      if (!$mail->Send()) {
      echo "Message could not be sent.

      ";
      echo "Mailer Error: " . $mail->ErrorInfo;
      exit;
      }

      echo "Message has been sent"; */
}

