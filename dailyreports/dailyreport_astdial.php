<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


require_once("../config.inc.php");
require_once("class.phpmailer.php");

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName = $dbconfig['db_name'];

$conn = @mysqli_connect($hostname, $username, $password);
mysqli_select_db($conn, $dbName);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    // echo "connected";
}

global $site_URL;
date_default_timezone_set("Asia/Kolkata");


//for date_disply
$date_display = date('Y-m-d', strtotime("-1 days"));
$date_to_str = strtotime($date_display);
$date_formate  = date("d M Y",$date_to_str);



$today = date('Y-m-d');

$yesterday = date('Y-m-d', strtotime("-1 days"));
$last_seventh_day = date('Y-m-d', strtotime("-7 days"));
$yesterday_date = date('Y-m-d', strtotime("-1 days"));

// YESTERDAY NOT CONNNECTED CALLS
$sql_connected_seven = mysqli_query($conn, "SELECT COUNT(*) as notconnected FROM campaign_dial_status WHERE DATE(modify_date) = '$yesterday' AND status = '2'");
$row_connected_seven = mysqli_fetch_assoc($sql_connected_seven);
$notconnected_calls = $row_connected_seven['notconnected'];

// YESTERDAY CONNNECTED CALLS
$sql_connected_seven1 = mysqli_query($conn, "SELECT COUNT(*) as connected FROM campaign_dial_status WHERE DATE(modify_date) = '$yesterday' AND status = '1' ");
$row_connected_seven1 = mysqli_fetch_assoc($sql_connected_seven1);
$connected_calls = $row_connected_seven1['connected'];



$total_calls = $connected_calls + $notconnected_calls;

// TOTAL USERS
$query_users = mysqli_query($conn, "SELECT COUNT(id) as user_cnt FROM vtiger_users WHERE status = 'Active' ");
$row_users = mysqli_fetch_assoc($query_users);
$total_attendance = $row_users['user_cnt'];

// YESTERDAY LOGIN
$query_last_login = mysqli_query($conn, "SELECT COUNT(id) as login_cnt_seven FROM user_callsummary WHERE DATE(datetime) = '$yesterday' ");
$row_last_login = mysqli_fetch_assoc($query_last_login);

$loggedin_count = $row_last_login['login_cnt_seven'];
$not_loggedin_count = $total_attendance - $loggedin_count;

// get report userwise
$user_report = mysqli_query($conn, "SELECT a.login_time, a.pause_time, a.break_time, a.talk_time, CONCAT( b.first_name, ' ', b.last_name ) AS user_full_name,b.user_name"
        . " FROM user_callsummary a LEFT JOIN vtiger_users b ON a.userid = b.user_name "
        . " WHERE b.status = 'Active' AND DATE( a.current_eventtime ) = '$yesterday'  order by a.login_time DESC");
$report_count = mysqli_num_rows($user_report);


// FETCH YESTERDAY DISPOSITION
$chart_dispo = array();
$chart_dispo_status = array();
$query_dispoCount = mysqli_query($conn, "SELECT COUNT(dispo) as total_dispo, dispo FROM vtiger_calllogs WHERE date = '" . $yesterday . "' AND dispo <> '' GROUP BY dispo");
while ($total_dispoCount = mysqli_fetch_assoc($query_dispoCount)) {
    $chart_dispo[] = $total_dispoCount['total_dispo'];
    $chart_dispo_status[] = $total_dispoCount['dispo'];
}

$total_dispo_yesterday = getTotalYesterdayDispostion($conn, $yesterday);
$yesterday_disposition_link = getYesterdayDispositionUrl($chart_dispo, $chart_dispo_status, $total_dispo_yesterday);


//miss callbacks//


// $sql_misscallback = "SELECT * FROM vtiger_calllogs t1 INNER JOIN vtiger_users t2
// ON t1.source = t2.user_name GROUP BY t2.user_name";

$sql_misscallback = "SELECT t1.callback,t2.first_name,t2.last_name, 
CONCAT(t2.first_name, ' ', t2.last_name) AS full_name, COUNT(t1.callback) as total_callback FROM 
vtiger_calllogs t1 INNER JOIN vtiger_users t2 ON t1.source = t2.user_name 
WHERE t1.date = '$yesterday' AND t1.callback!='' group by t2.user_name";

$res_misscallback = $conn->query($sql_misscallback);
$count_misscallback = mysqli_num_rows($res_misscallback);

$sql_miss_cnt = mysqli_query($conn, "SELECT COUNT(*) cb_count FROM vtiger_calllogs 
WHERE callback != '' AND date = '$yesterday' ");
$res_miss_cnt = mysqli_fetch_assoc($sql_miss_cnt);
$cb_cnt = $res_miss_cnt['cb_count'];


// LAST 7 DAYS DISPOSITION
$chart_dispo_seven = array();
$chart_dispo_status_seven = array();
$query_dispoCount_seven = mysqli_query($conn, "SELECT COUNT(dispo) as total_dispo, dispo FROM vtiger_calllogs WHERE date <= '" . $yesterday . "' AND date >= '" . $last_seventh_day . "' AND dispo <> '' GROUP BY dispo ORDER BY date asc");
while ($total_dispoCount_seven = mysqli_fetch_assoc($query_dispoCount_seven)) {
    $chart_dispo_seven[] = $total_dispoCount_seven['total_dispo'];
    $chart_dispo_status_seven[] = $total_dispoCount_seven['dispo'];
}

$total_dispo_last7days = getTotalLast7daysDisposition($conn, $yesterday, $last_seventh_day);

$last_sevenday_disposition_link = getLast7DayDispositionUrl($chart_dispo_seven, $chart_dispo_status_seven, $total_dispo_last7days);




$agentLogDetails = FetchAgentLoginDetails($conn, $yesterday);
$hours_labels = array();
$login_data = array();
$logout_data = array();
$active_hrs_data = array();
foreach ($agentLogDetails as $key => $value) {
    array_push($hours_labels, $value['eventTime']);
    array_push($login_data, $value['loginCount']);
    array_push($logout_data, $value['logoutCount']);
    array_push($active_hrs_data, $value['netUserCount']);
}
$login_activity_link = getLoginActivityData($hours_labels, $login_data, $logout_data, $active_hrs_data);


$sql = "SELECT sum( duration ) AS duration, HOUR( modify_date ) AS hours
FROM `campaign_dial_status`
WHERE DATE( `modify_date` ) = '$yesterday'
AND dispo != 'NO Contact'
GROUP BY HOUR( modify_date)";

$query_totaltalk = mysqli_query($conn, $sql);
while ($row = mysqli_fetch_assoc($query_totaltalk)) {
    $duration[] = $row['duration'];
    $hours[] = $row['hours'] . ':00';
}


$talk_activity_link = getTalkActivity($duration, $hours);

// LAST 7 DAYS TOTAL CALLS
$query_lastweekcalls = mysqli_query($conn, "SELECT COUNT(*) as total_lastweekcalls, modify_date FROM campaign_dial_status WHERE DATE(modify_date) <= '" . $yesterday . "' AND DATE(modify_date) >= '" . $last_seventh_day . "' GROUP BY DATE(modify_date) ORDER BY modify_date ASC");
while ($total_lastweekcalls = mysqli_fetch_assoc($query_lastweekcalls)) {
    $chart_lastweekcall[] = $total_lastweekcalls['total_lastweekcalls'];
    $chart_lastweekdate1[] = date('Y-m-d', strtotime($total_lastweekcalls['modify_date']));
    $chart_lastweekdate[] = date('d-m-Y', strtotime($total_lastweekcalls['modify_date']));
}

foreach ($chart_lastweekdate1 as $key) {

    // LAST SEVEN DAYS NOT CONNNECTED CALLS
    $sql_connected_seven = mysqli_query($conn, "SELECT COUNT(*) as notconnected_seven FROM campaign_dial_status WHERE DATE(modify_date) = '" . $key . "' AND status = '2'");
    $row_connected_seven = mysqli_fetch_assoc($sql_connected_seven);
    $not_connected_lastseven[] = $row_connected_seven['notconnected_seven'];

    // LAST SEVEN DAYS CONNNECTED CALLS
    $sql_connected_seven1 = mysqli_query($conn, "SELECT COUNT(*) as connected_seven FROM campaign_dial_status WHERE DATE(modify_date) = '" . $key . "' AND status = '1' GROUP BY DATE(modify_date)");
    $row_connected_seven1 = mysqli_fetch_assoc($sql_connected_seven1);
    $connected_last_seven[] = $row_connected_seven1['connected_seven'];
}

$last7dayscalls_link = getLast7DaysCalls($yesterday, $last_seventh_day, $connected_last_seven, $not_connected_lastseven, $chart_lastweekdate);


$callback_today = mysqli_query($conn, "SELECT COUNT(*) AS callback_count FROM `vtiger_contactdetails` as a join vtiger_crmentity as b on (b.crmid=a.contactid) WHERE callback LIKE   '%$today%' and b.deleted=0");
$res_callback = mysqli_fetch_assoc($callback_today);
$callback_count = $res_callback['callback_count'];

function ConnectedCallsbyUserId($conn, $user_name) {
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $sql_connected_seven1 = mysqli_query($conn, "SELECT COUNT(*) as connected FROM campaign_dial_status WHERE DATE(modify_date) = '$yesterday' AND status = '1' and user='$user_name' ");
    $row_connected_seven1 = mysqli_fetch_assoc($sql_connected_seven1);
    $connected_calls = $row_connected_seven1['connected'];
    return $connected_calls;
}

function NotConnectedCallsbyUserId($conn, $user_name) {
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $sql_connected_seven = mysqli_query($conn, "SELECT COUNT(*) as notconnected FROM campaign_dial_status WHERE DATE(modify_date) = '$yesterday' AND status = '2' and user='$user_name' ");
    $row_connected_seven = mysqli_fetch_assoc($sql_connected_seven);
    $notconnected_calls = $row_connected_seven['notconnected'];
    return $notconnected_calls;
}

function getTotalYesterdayDispostion($conn, $yesterday) {
    $query_dispoCount = mysqli_query($conn, "SELECT COUNT(dispo) as total_dispo, dispo FROM vtiger_calllogs WHERE date = '" . $yesterday . "' AND dispo <> '' ");
    while ($total_dispoCount = mysqli_fetch_assoc($query_dispoCount)) {
        $total_dispo[] = $total_dispoCount['total_dispo'];
    }
    return $total_dispo;
}

function getTotalLast7daysDisposition($conn, $yesterday, $last_seventh_day) {
    $query_dispoCount_seven = mysqli_query($conn, "SELECT COUNT(dispo) as total_dispo, dispo FROM vtiger_calllogs WHERE date <= '" . $yesterday . "' AND date >= '" . $last_seventh_day . "' AND dispo <> '' ");
    while ($total_dispoCount_seven = mysqli_fetch_assoc($query_dispoCount_seven)) {
        $total_dispo[] = $total_dispoCount_seven['total_dispo'];
    }
    return $total_dispo;
}

function FetchAgentLoginDetails($conn, $yesterday) {

    $agentLogDetails = array();
    $startTime = "00:00:00";
    $cummalativeLogin = 0;
    $cummalativeLogout = 0;
    for ($i = 0; $i < 23; $i++) {
        $minTime = strtotime($startTime) + ($i * 60 * 60);
        $maxTime = strtotime($startTime) + (($i + 1) * 60 * 60);
        $minTime = date('H:i', $minTime);
        $maxTime = date('H:i', $maxTime);
        $loginCount = 0;
        $logoutCount = 0;
        $sql = "SELECT COUNT(*) as total FROM `user_callsummary` WHERE DATE(`first_login`)='$yesterday' and (TIME(`first_login`) between '$minTime' and '$maxTime')";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $loginCount = $row['total'];
                $cummalativeLogin = $cummalativeLogin + $loginCount;
            }
        }
        $sql = "SELECT COUNT(*) as total FROM `user_callsummary` WHERE DATE(`last_logout`)='$yesterday' and (TIME(`last_logout`) between '$minTime' and '$maxTime')";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $logoutCount = $row['total'];
                $cummalativeLogout = $cummalativeLogout + $logoutCount;
            }
        }


        $netUserCount = $cummalativeLogin - $cummalativeLogout;
        if (($loginCount > 0) or ( $logoutCount > 0)) {
            array_push($agentLogDetails, array("eventTime" => $minTime, "loginCount" => $loginCount, "logoutCount" => $logoutCount, "netUserCount" => $netUserCount));
        }
    }

    return $agentLogDetails;
}

function getLast7DaysCalls($yesterday, $last_seventh_day, $connected_last_seven, $not_connected_lastseven, $chart_lastweekdate) {


    $not_connected_calls = ConvertString2Integer($not_connected_lastseven);
    $connected_calls = ConvertString2Integer($connected_last_seven);
    $last7daysCalls_data = [
        "type" => "bar",
        "data" => [
            "labels" => $chart_lastweekdate,
            "datasets" => [
                [
                    "label" => "Not Connected",
                    "backgroundColor" => "#1DE9B6",
                    "data" => $not_connected_calls
                ],
                [
                    "label" => "Connected",
                    "backgroundColor" => "#2196F3",
                    "data" => $connected_calls
                ]
            ]
        ],
        "options" => [
            "title" => [
                "display" => true,
                "text" => "Last 7 Days Calls",
            ],
            "legend" => [
                "position" => "bottom"
            ],
            "scales" => [
                "xAxes" => [
                    [
                        "stacked" => true
                    ]
                ],
                "yAxes" => [
                    [
                        "stacked" => true
                    ]
                ]
            ],
            "plugins" => [
                "datalabels" => [
                    "display" => true,
                    "font" => [
                        "style" => "normal",
                        "size" => 8,
                    ]
                ]
            ]
        ]
    ];


    $last7daysCalls_config = json_encode($last7daysCalls_data);
    $last7daysCalls_url = 'https://quickchart.io/chart?w=400&h=250&c=' . urlencode($last7daysCalls_config);
    return $last7daysCalls_url;
}

function getTalkActivity($duration, $hours) {

    
$duration1 = ConvertString2Integer($duration);
$chartConfigArr = array(
    'type' => 'line',
    'data' => array(
        'labels' => $hours,
        'datasets' => array(
            array(
                'label' => 'Total Talk (seconds)',
                'data' => $duration1,
                'fill' => false,
                'borderColor' => 'blue',
            ),
        )
    ),
    'options' => array(
        'legend' => array(
             'position' => 'bottom',
        ),
        /* 'title' => array(
             'display' => TRUE,
                'text' => 'Talk Activty'
        ),*/
        'plugins' => array(
            'datalabels' => array(
                 'display' => true,
                'align' => 'left',
                'backgroundColor' => '#e1abee',
            )
        ),
    )
);


    $talk_data_config = json_encode($chartConfigArr);
    $talk_activity_url = 'https://quickchart.io/chart?c=' . urlencode($talk_data_config);
    return $talk_activity_url;
}

function ConvertString2Integer($inputarray) {
    $output_int_array = array_map(
            function($value) {
        return (int) $value;
    }, $inputarray
    );
    
    return $output_int_array ;
}


function getLoginActivityData($hours_labels, $login_data, $logout_data, $active_hrs_data) {

   
$logindata1 = ConvertString2Integer($login_data);
$logoutdata1 =ConvertString2Integer($logout_data);
$agentdata1 = ConvertString2Integer($active_hrs_data);

$chartConfigArr = array(
    'type' => 'line',
    'data' => array(
        'labels' => $hours_labels,
        'datasets' => array(
            array(
                'label' => 'Login',
                'data' => $logindata1,
                'fill' => false,
                'borderColor' => 'blue',
            ),
            array(
                'label' => 'Logout',
                'data' => $logoutdata1,
                'fill' => false,
                'borderColor' => 'red',
            ),
            array(
                'label' => 'Active',
                'data' => $agentdata1,
                'fill' => false,
                'borderColor' => 'green',
            ),
        )
    ),
    'options' => array(
        'legend' => array(
             'position' => 'bottom',
        ),
        'plugins' => array(
            'datalabels' => array(
                 'display' => true,
                'align' => 'left',
                'backgroundColor' => '#e1abee',
            )
        ),
    )
);

    $login_activity_config = json_encode($chartConfigArr);
    $login_activity_url = 'https://quickchart.io/chart?c=' . urlencode($login_activity_config);
    return $login_activity_url;
}

function getYesterdayDispositionUrl($chart_dispo, $chart_dispo_status, $total_dispo_yesterday) {
    $yesterday_disposition_configArr = [
        "type" => "doughnut",
        "data" => [
            "labels" => $chart_dispo_status,
            "datasets" => [
                [
                    "data" => $chart_dispo
                ]
            ]
        ],
        "options" => [
            "responsive" => true,
            "legend" => [
                "position" => 'right',
            ],
//            "title" => [
//                "display" => TRUE,
//                "text" => 'Yesterday Disposition'
//            ],
            "plugins" => [
                "doughnutlabel" => [
                    "labels" => [
                        [
                            "text" => $total_dispo_yesterday,
                        ],
                        [
                            "text" => "total"
                        ],
                    ]
                ]
            ]
        ]
    ];



    $yesterday_disposition_config = json_encode($yesterday_disposition_configArr);
    $yesterday_disposition_url = 'https://quickchart.io/chart?c=' . urlencode($yesterday_disposition_config);

    return $yesterday_disposition_url;
}

function getLast7DayDispositionUrl($chart_dispo_seven, $chart_dispo_status_seven, $total_dispo_last7days) {

    $last7day_disposition_configArr = [
        "type" => "doughnut",
        "data" => [
            "labels" => $chart_dispo_status_seven,
            "datasets" => [
                [
                    "data" => $chart_dispo_seven
                ]
            ]
        ],
        "options" => [
            "responsive" => true,
            "legend" => [
                "position" => 'right',
            ],
//            "title" => [
//                "display" => TRUE,
//                "text" => 'Last 7 days Disposition'
//            ],
            "plugins" => [
                "doughnutlabel" => [
                    "labels" => [
                        [
                            "text" => $total_dispo_last7days,
                        ],
                        [
                            "text" => "total"
                        ],
                    ]
                ]
            ]
        ]
    ];



    $last7days_disposition_config = json_encode($last7day_disposition_configArr);
    $last7days_disposition_url = 'https://quickchart.io/chart?c=' . urlencode($last7days_disposition_config);

    return $last7days_disposition_url;
}

$htmlcontent = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Daily Report</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<style>
body{
            color: #090004;
            background-color: #f2f2f2;
            margin-left: 20%;
            margin-right: 20%;
            font-family: "Open Sans", sans-serif;
            font-weight: normal;
            font-size: 14px;
        }

        .card {
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            background-color: #ffffff;
            margin-bottom: 10px;
        }

        .container {
            padding: 2px 16px;
        }

        .grid-container {
            display: grid;
            grid-template-columns: auto auto auto;
           
        }

        p{
            text-align: left;
        }

        th, td {
            padding: 5px;
            
        }

        .button {
            background-color: #0ebcce;
            border: none;
            color: white;
            padding: 8px 16px;
            border-radius:20px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 12px;
            font-weight: bold;
            cursor: pointer;
        }

       
        ul.social-media-list {
            list-style-type: none;
            margin: 0;
            padding: 0;
            font-size: 0px; 
        }

        ul.social-media-list li {
            display: inline-block;
            margin: 2px;
        }

        ul.social-media-list img {
            padding: 5px;
            border-radius: 5px;
            width: 24px;
            height: 24px;
        }

        ul.social-media-list img:hover {
            background-color: #0ebcce;
        }
       

        img #meetn_graph, #pie_chart {
            width: 100%;
            height: auto;
        }

        .img-example {
            max-width: 100%;
        }

        .img-example__sparkline {
            max-width: 10em;
        }
</style>
</head>

<body style="margin: 0; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
        <tr>
            <td style="padding: 10px 0 30px 0;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                   
     

<tr>
<td><div class="card" style="width: 100%;background-color: #ffffff">
                    <div class="container">
                        <p style="font-family:arial black;font-style:italic;color:#800080;font-size:35px;text-align:right">*astDIAL</p>
                    <h3 style="text-align: left; color:black ">Daily Report :  <span><b style="color: #92e600;">' . $date_formate . '</b></span></h3>

                    </div>
                </div></td></tr>

<tr>
<td>
<div class="card" style="width: 100%;background-color: #ffffff">
  
                    <table style="width:100%;">
                    <thead>
                    <td> 
                        <div class="card" style="width: 100%; margin-left: 2%;background-color: #f0ffe3;" >
                            <div class="container" style="height: 100px; ">
                                <p style="color:black;"><b>Total Calls</b></p>
                                <h2 style="color: black; text-align: center;">' . $total_calls . '</h2>
                            </div>
                            <br>
                        </div>
                    </td>
                    <td> 
                        <div class="card" style="width: 100%; margin-right: 2%;background-color: #e4f8fa;" >
                            <div class="container" style="height: 100px; ">
                                <p style="color:cc447e;"><b>Connected Calls</b></p>
                                <h2 style="color: #0ebcce; text-align: center;">' . $connected_calls . '</h2>
                            </div>
                            <br>
                        </div>
                    </td>
                    <td> 
                        <div class="card" style="width: 100%; margin-right: 2%;background-color: #ffede3;" >
                            <div class="container" style="height: 100px; ">
                                <p style="color:cc447e;"><b>Not Connected Calls</b></p>
                                <h2 style="color: #ff5e00; text-align: center;">' . $notconnected_calls . '</h2>
                            </div>
                            <br>
                        </div>
                    </td>

                    </thead>
                </table>
            </div>
</td>
</tr>

<tr>
<td><div class="card" style="width: 100%;background-color: #ffffff">
<h3 style="text-align: left; padding-top: 20px; padding-left:20px;color:black">Call Trends </h3>
                     <div class="container" >
                    <img class="img-example"  src="' . $last7dayscalls_link . '">
                     </div>
                </div> </td>
</tr>

<tr>
<td>

                <div class="card" style="width: 100%;background-color: #ffffff">
                    
                    <table style="width: 100%;">
                    <thead>
                   <td> 
          <div class="card" style="width: 100%; margin-left: -2%;">
            <div class="container" style="height: 119px;">
             <h3 style="text-align: center; padding-top: 40px;">Attendance</h3>
            </div>
            <br>
          </div>
        </td>

                    <td> 
                        <div class="card" style="width: 100%;margin-right: 2%;background-color: #ffffff;">
                            <div class="container" style="height: 119px;">
                                <p>Logged In</p>
                                <span><b style="color: #0ebcce; font-size: 50px;">' . $loggedin_count . '</b><b style="color: #6e6e6e; font-size: 20px;">/' . $total_attendance . '</b></span>
                            </div>
                            <br>
                        </div>
                    </td>
                    <td> 
                        <div class="card" style="width: 100%;margin-right: 2%;background-color: #ffffff;">
                            <div class="container" style="height: 119px;">
                                <p>Not Logged In</p>
                                <span><b style="color: #ff5e00; font-size: 50px;">' . $not_loggedin_count . '</b><b style="color: #6e6e6e; font-size: 20px;">/' . $total_attendance . '</b></span>
                            </div>
                            <br>
                        </div>
                    </td>
                    
                    </thead>
                </table>
            </div>
</td>

</tr>
                     <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%";>
                                        <tbody style="background-color: #ffffff;">
                                            <tr>
                                                <td width="260" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td><p style="text-align:center;color:#777171;"><b>Last 7 Days Disposition</b></p>
                                                                <img src=' . $last_sevenday_disposition_link . ' alt="" width="100%" height="150" style="display: block;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="font-size: 0; line-height: 0;" width="20">
                                                    &nbsp;
                                                </td>
                                                <td width="260" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td><p style="text-align:center;color:#777171;"><b>Yesterday Disposition</b></p>
                                                                <img src=' . $yesterday_disposition_link . ' alt="" width="100%" height="150" style="display: block;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                            </table>
                                            </td>
                                            </tr>
                      
                    <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%";>
                                        <tbody style="background-color: #ffffff;">
                                            <tr>
                                                <td width="260" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td><p style="text-align:center;color:#777171;"><b>Talk Activity</b></p>
                                                                <img src=' . $talk_activity_link . ' alt="" width="100%" height="150" style="display: block;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="font-size: 0; line-height: 0;" width="20">
                                                    &nbsp;
                                                </td>
                                                <td width="260" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td><p style="text-align:center;color:#777171;"><b>Login Activity</b></p>
                                                                 <img src=' . $login_activity_link . ' alt="" width="100%" height="150" style="display: block;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                            </table>
                                            </td>
                                            </tr>
                    
<tr>
<td>
<div class="card" style="width: 100%;background-color: #ffffff;">
                <div class="container">

                    <h3 style="text-align: center; color:black ">CallBack for Today : <span><b style="color: #0ebcce;font-size: 25px;">' . $callback_count . '</b></span> </h3>

                    </div>
                </div>
</td>
</tr>
                    
<tr>


<td>
<div class="card" style="width: 100%;background-color: #425767;">
<div class="container">
<h3 style="text-align: left; color:#ffffff ">Agent Performance</h3>
</div>
<table class="table1" style="width: 100%;background-color: #ffffff; color: #535353; font-size: 13px;">
	<thead style="background-color: #d0d0d0;">
		<tr>
			<th>S.No</th>
			<th>Name</th>
			<th>Total Login Time</th>
            <th>Total Pause Time</th>
            <th>Total Break Time</th>
            <th>Total Talk Time</th>
			<th>Connected Calls</th>
			<th>Not Connected Calls</th>
			
		</tr>
	</thead>
	<tbody>';

if ($report_count > 0) {

    $sno = 0;
    while ($user_rows = mysqli_fetch_assoc($user_report)) {
        $sno++;
        $login_time = $user_rows['login_time'];
        $pause_time = $user_rows['pause_time'];
        $break_time = $user_rows['break_time'];
        $talk_time = $user_rows['talk_time'];
        $user_name = $user_rows['user_name'];
        $user_full_name = $user_rows['user_full_name'];

        $htmlcontent .= '<tr>
                <td style="text-align: center; border-bottom: solid 1px #d0d0d0;color: #0c0b0b;"> ' . $sno . '</td>
                <td style="text-align: left; border-bottom: solid 1px #d0d0d0;color: #0c0b0b;width:25%">' . $user_full_name . '</td>
                <td style="text-align: center; border-bottom: solid 1px #d0d0d0;color: #0c0b0b;">' . gmdate('H:i:s', $login_time) . '</td>
                <td style="text-align: right; border-bottom: solid 1px #d0d0d0; color: #0c0b0b; padding-right: 5px;">' . gmdate('H:i:s', $pause_time) . '</td>
                <td style="text-align: right; border-bottom: solid 1px #d0d0d0; color: #0c0b0b; padding-right: 5px;">' . gmdate('H:i:s', $break_time) . '</td>
                <td style="text-align: right; border-bottom: solid 1px #d0d0d0; color: #0c0b0b; padding-right: 5px;">' . gmdate('H:i:s', $talk_time) . '</td>
                <td style="text-align: right; border-bottom: solid 1px #d0d0d0; color: #92e600; padding-right: 5px;"> ' . ConnectedCallsbyUserId($conn, $user_name) . ' </td>
                <td style="text-align: right; border-bottom: solid 1px #d0d0d0; color: #0ebcce; padding-right: 5px;"> ' . NotConnectedCallsbyUserId($conn, $user_name) . ' </td>
              </tr>';
    }
} else {
    $htmlcontent .= '<td style="align-content: center;"><h3>data not available</h3><td>';
}



/***********************/
$htmlcontent .='</tbody>
</table></td>
</tr>





<tr>
<td>
<div class="card" style="width: 100%;background-color: #ffffff;">
                <div class="container">

                    <h3 style="text-align: center; color:black ">Un-Attempted CallBack : <span><b style="color: #0ebcce;font-size: 25px;">' . $cb_cnt . '</b></span> </h3>

                    </div>
                </div>
</td>
</tr>

<tr>
<td>
<div class="card" style="width: 100%;background-color: #425767;">
<div class="container">
<h3 style="text-align: left; color:#ffffff ">Un-Attempted CallBack (Agent Wise)</h3>
</div>
<table class="table1" style="width: 100%;background-color: #ffffff; color: #535353; font-size: 13px;">
    <thead style="background-color: #d0d0d0;">
        <tr>
            <th>S.No</th>
            <th>Name</th>
            <th>CallBack Count</th>
          
        </tr>
    </thead>
    <tbody>';


    if($count_misscallback > 0){

        $sl = 0;
        while($row = $res_misscallback->fetch_assoc()){
            $sl++;
            

            $f_name = $row['first_name'];
            $l_name = $row['last_name'];
            
            $c = $row['total_callback'];
           
            $full_name = $row['full_name'];
       
           // $full_name = $f_name. ' ' .$l_name; 
            
           echo $full_name;
           echo "\n";
           echo "sl-.$sl";
           echo "\n";
           echo "count-.$c";
           echo "\n";

            $htmlcontent .= '<tr>
                   <td style="text-align: center; border-bottom: solid 1px #d0d0d0;color: #0c0b0b;"> ' . $sl . '</td>
                    <td style="text-align: center; border-bottom: solid 1px #d0d0d0;color: #0c0b0b;width:50%">' . $full_name . '</td>
                     <td style="text-align: center; border-bottom: solid 1px #d0d0d0;color: #0c0b0b;width:50%">' . $c . '</td>
                   
                  </tr>';
    
        }
    }else{
        $htmlcontent .= '<td style="align-content: center;"><h3>data not available</h3><td>';
    }
/*********************/


$htmlcontent .='</tbody>
</table></td>
</tr>
                    <tr>
                        <td bgcolor="#425767" style="padding: 30px 30px 30px 30px;">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
	
		<tr>
			
			<td style="width: 100%; text-align: center;">
			<h1 style="color: #ffffff; text-align: center;">Hey Admin!</h1>
			<h3 style="color: #ffffff; text-align: center;">Refer us and get amazing prices!</h3>
	
                        </tr>
                        <tr align="center">


<td style="width: 20%; align="center">
<table border="0" cellpadding="0" cellspacing="0">
<tr>

<td><!--Facebook icon-->
<a href="https://www.facebook.com/astcrm/" target="_blank">
<img alt="FaceBook|astCRM" src="https://www.astcrm.com/daily_report_icons/facebook_icon.png" width="24" height="24"/>
</a></td>&nbsp;

<td><!--LinkedIn icon-->
<a href="https://in.linkedin.com/company/astcrm/" target="_blank">
<img alt="LinkedIn|astCRM" src="https://www.astcrm.com/daily_report_icons/linkedin_icon.png" width="24" height="24"/>
</a></td>

</tr>
</table>
</td>



			
</tr>
</thead>
</table>


                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>

';


function getAdminUsersList($conn) {
    $emaillist = array();
    $select_email = "select email1,concat( first_name, ' ', last_name ) as username from vtiger_users where is_admin='on' and status='Active'";
    $q_email = mysqli_query($conn, $select_email);
    while ($row = mysqli_fetch_assoc($q_email)) {
        $username[] = $row['username'];
        $emaillist[] = $row['email1'];
    }

    return $emaillist;
}

$adminlist = getAdminUsersList($conn);

$crm_name = 'astdialv2';

foreach ($adminlist as $email) {
    sendastReport($htmlcontent, $email,$crm_name);
}

function sendastReport($htmlcontent, $email,$crm_name) {
    echo $htmlcontent;
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $mail = new PHPMailer();
    $mail->SMTPDebug = 2;
    $mail->IsSMTP();                                      // set mailer to use SMTP
    $mail->Host = "ssl://alfa3214.alfahosting-server.de:465";  // specify main and backup server
    $mail->SMTPAuth = true;     // turn on SMTP authentication
    $mail->Username = "web23473492p140";  // SMTP username
    $mail->Password = "RKR8B7mR"; // SMTP password
    $mail->From = "reports@astcrm.com";
    $mail->FromName = "Daily Report";
    $mail->AddAddress($email);
    $mail->AddAddress('support@astcrm.com');
    // $mail->AddCC('support@astcrm.com');
    $mail->AddCC('sales@astcrm.com');
    $mail->AddReplyTo("reports@astcrm.com", "reports");                              // set word wrap to 50 characters
    $mail->IsHTML(true);                                  // set email format to HTML
    $mail->Subject = "Daily Report $crm_name *astDIAL";
    $mail->Body = $htmlcontent;
    if (!$mail->Send()) {
        echo "Message could not be sent";
        echo "Mailer Error: " . $mail->ErrorInfo;
        exit;
    } else {
        echo "Message has been sent";
    }
}


//test test file to git
