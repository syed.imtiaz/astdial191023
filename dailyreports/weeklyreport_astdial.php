<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once("../config.inc.php");
require_once("class.phpmailer.php");

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName = $dbconfig['db_name'];

$conn = mysqli_connect($hostname, $username, $password);
mysqli_select_db($conn, $dbName);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    // echo "connected";
}

global $site_URL;
date_default_timezone_set("Asia/Kolkata");

$today = date('Y-m-d');
$yesterday = date('Y-m-d', strtotime("-1 days"));
$last_seventh_day = date('Y-m-d', strtotime("-7 days"));
$yesterday_date = date('Y-m-d', strtotime("-1 days"));


//chetan

$previous_week = strtotime("-1 week +1 day");
$start_week = strtotime("last sunday midnight",$previous_week);
$end_week = strtotime("next saturday",$start_week);
  
$start_week = date("Y-m-d",$start_week);
$end_week = date("Y-m-d",$end_week);

$start_week_1 = date("d-m-Y",strtotime($start_week));
$end_week_1 = date("d-m-Y",strtotime($end_week));
//



// connected calls
$sql_connected = mysqli_query($conn, "SELECT a.date, a.dispo, b.name, c.cf_918 as call_status FROM vtiger_calllogs a LEFT JOIN vtiger_maindispo b ON a.dispo = b.name LEFT JOIN vtiger_maindispocf c ON b.maindispoid = c.maindispoid WHERE c.cf_918 = 'Connected' and a.date <= '$end_week' AND date >= '$start_week'");
$count_connected = mysqli_num_rows($sql_connected);

//total calls
$sql_total_connected = mysqli_query($conn, "SELECT a.date, a.dispo, b.name, c.cf_918 as call_status FROM vtiger_calllogs a LEFT JOIN vtiger_maindispo b ON a.dispo = b.name LEFT JOIN vtiger_maindispocf c ON b.maindispoid = c.maindispoid WHERE a.date <= '$end_week' AND date >= '$start_week'");
$count_total_connected = mysqli_num_rows($sql_total_connected);

$count_not_connected = $count_total_connected - $count_connected;

//calls by Options
$chart_total_seven = array();
$date = array();
$query_calls_seven = "SELECT date, COUNT(calllogsid) as total_calls_seven FROM vtiger_calllogs WHERE date <= '".$yesterday."' AND date >= '".$last_seventh_day."' GROUP BY date";

$res = $conn->query($query_calls_seven);
if($res->num_rows > 0){
    while($row = $res->fetch_assoc()){

        $month_day = date('M d',strtotime($row['date']));
        array_push($chart_total_seven, $row['total_calls_seven']);
        array_push($date,$month_day);
    }
}


$chart_total_seven = array(

    "type" =>  "bar",
    "data" => array(
        "labels" => $date,
        "datasets" => array(
            array(
                "label" => "Call Trends",
                "backgroundColor" => "#1DE9",
                "data" => $chart_total_seven
            )
           
    )
            ),
            "options" => array(
                "legend" => array(
                    "position" => "bottom"
                ),
             
                "plugins" => array(
                    "datalabels" => array(
                        "display" => true,
                        "font" => array(
                            "style" => "normal",
                            "size" => 10,
                           
                        )
                    )
                )
        )
);
$call_type = json_encode($chart_total_seven);
$call_type_chart_chartUrl = 'https://quickchart.io/chart?c='.urlencode($call_type);


//calls by agents

$username = array();
$total_calls_by_agents = array();


 $sql = "SELECT a.destination, COUNT(a.destination) AS total, CONCAT(b.first_name,' ',b.last_name) AS agent_name FROM vtiger_calllogs a LEFT JOIN vtiger_users b ON a.source = b.user_name WHERE a.date <= '".$yesterday."' AND a.date >= '".$last_seventh_day."' GROUP BY a.source";


$res  = $conn->query($sql);
if($res->num_rows > 0){
    while($row = $res->fetch_assoc()){

        if($row['agent_name'] != ''){
         array_push($username, $row['agent_name']);
         array_push($total_calls_by_agents, $row['total']);
        }
       
    }
}

//print_r($username);

$CallByAgentsChart = array(
    "type" =>  "bar",
    "data" => array(
        "labels" => $username,
        "datasets" => array(
            array(
                "label" => "Call by agents",
                "backgroundColor" => 
                "#46BFBD",
                "data" => $total_calls_by_agents
            )
           
    )
            ),
    "options" => array(
        "legend" => array(
            "position" => "bottom"
        ),
     
        "plugins" => array(
            "datalabels" => array(
                "display" => true,
                "font" => array(
                    "style" => "normal",
                    "size" => 14,
                   
                )
            )
        )
)

);
$Agents_call_type = json_encode($CallByAgentsChart);
$Agents_call_type_chart_chartUrl = 'https://quickchart.io/chart?c='.urlencode($Agents_call_type);


//time function loop

// $start_time = new DateTime("00:00");
// $end_time   = new DateTime("24:00");

// $interval = DateInterval::createFromDateString('240 min');

// $times    = new DatePeriod($start_time, $interval, $end_time);

// foreach ($times as $time) {
//     echo $time->format('h:i:sa'), '-', 
//          $time->add($interval)->format('h:i:sa'), "\n";
//          echo "<br>";
// }


//disposition
$chart_dispo = array();
$chart_dispo_status = array();
$query_dispoCount = mysqli_query($conn, "SELECT COUNT(dispo) as total_dispo, dispo FROM vtiger_calllogs WHERE date <= '".$yesterday."' AND date >= '".$last_seventh_day."'  AND dispo <> '' GROUP BY dispo");
while ($total_dispoCount = mysqli_fetch_assoc($query_dispoCount)) {
    $chart_dispo[] = $total_dispoCount['total_dispo'];
    $chart_dispo_status[] = $total_dispoCount['dispo'];
}

// $total_dispo_yesterday = getTotalYesterdayDispostion($conn, $yesterday);
// $yesterday_disposition_link = getYesterdayDispositionUrl($chart_dispo, $chart_dispo_status, $total_dispo_yesterday);



    $yesterday_disposition_configArr = array(
        "type" => "doughnut",
        "data" => array(
            "labels" => $chart_dispo_status,
            "datasets" => array(
                array(
                    "data" => $chart_dispo
                )
            )
        ),
        "options" => array(
            "responsive" => true,
            "legend" => array(
                "position" => 'right',
            ),
            "plugins" => array(
                "doughnutlabel" => array(
                    "labels" => array(
                        
                            "text" => $total_dispo_yesterday,
                            "text" => "total"
                        
                )
            )
            )
        )
    );



    $yesterday_disposition_config = json_encode($yesterday_disposition_configArr);
    $yesterday_disposition_url = 'https://quickchart.io/chart?c=' . urlencode($yesterday_disposition_config);

    




//calls by Duration
$duration_arr = array("0-30","30-60","60-120","120-300","300-3000");
foreach ($duration_arr as $duration) {
$arr_duration = explode('-', $duration);
$duration_start = $arr_duration[0];
$duration_end = $arr_duration[1]; 

$sql_duration = "SELECT COUNT(*) AS duration_call_count FROM campaign_dial_status WHERE duration >= '$duration_start' AND duration <= '$duration_end' AND modify_date <= '$yesterday' AND modify_date >= '$last_seventh_day'";


$res_duration = $conn->query($sql_duration);
if($res->num_rows > 0){
    while($row_duration = $res_duration->fetch_assoc()){

        $calls_by_duration[] = $row_duration['duration_call_count']; 
    }
}
}

$CallByDurationChart = array(
    "type" =>  "pie",
    "data" => array(
        "labels" => array('<= 30 Sec', '> 30 to <= 60 Sec', '> 60 to <= 2 Min', '> 2 Min to <= 5 Min', '> 5 Min'),
        "datasets" => array(
            array(
                "label" => "Call by duration",
                "backgroundColor" => array(
                    "#F1C40F",
                    "#46BFBD",
                    "#FDB45C",
                    "#F17F19",
                    "#E74C3C"
                ),
                "data" => $calls_by_duration
            )
           
    )
            ),
    "options" => array(
        "legend" => array(
            "position" => "right"
        ),
     
        "plugins" => array(
            "datalabels" => array(
                "display" => true,
                "font" => array(
                    "style" => "normal",
                    "size" => 14,
                )
            )
        )
)

);
$calls_by_duration = json_encode($CallByDurationChart);
$calls_by_duration_chart_chartUrl = 'https://quickchart.io/chart?c='.urlencode($calls_by_duration);


//calls by campaign
$camp_name = array();
$total_calls_by_camp = array();
$sql_comp = "SELECT campaign, COUNT(calllogsid) AS total_calls_by_comp FROM vtiger_calllogs WHERE date <= '$yesterday' AND date >='$last_seventh_day' 
GROUP BY campaign";
$res_comp = $conn->query($sql_comp);
if($res_comp->num_rows > 0){
    while($row_comp = $res_comp->fetch_assoc()){

        array_push($camp_name, $row_comp['campaign']);
        array_push($total_calls_by_camp, $row_comp['total_calls_by_comp']);

    }
}


$call_by_camp = array(
    "type" =>  "bar",
    "data" => array(
        "labels" => $camp_name,
        "datasets" => array(
            array(
                "label" => "Call by agents",
                "backgroundColor" => 
                    "#46BFBD"
                ,
                "data" => $total_calls_by_camp
            )
           
    )
            ),
            "options" => array(
                "legend" => array(
                    "position" => "bottom"
                ),
             
                "plugins" => array(
                    "datalabels" => array(
                        "display" => true,
                        "font" => array(
                            "style" => "normal",
                            "size" => 10,
                           
                        )
                    )
                )
        )

);
$call_by_camp = json_encode($call_by_camp);
$call_by_camp_url = 'https://quickchart.io/chart?c='.urlencode($call_by_camp);

//table date


function ConnectedCallsbyUserId($conn, $user_name) {
    $yesterday = date('Y-m-d', strtotime("-1 days"));
$last_seventh_day = date('Y-m-d', strtotime("-7 days"));
   // $yesterday = date('Y-m-d', strtotime("-1 days"));
    $sql_connected_seven1 = mysqli_query($conn, "SELECT COUNT(*) as connected FROM campaign_dial_status WHERE DATE(modify_date) <= '$yesterday' AND DATE(modify_date) >='$last_seventh_day'  AND status = '1' and user='$user_name' ");
    $row_connected_seven1 = mysqli_fetch_assoc($sql_connected_seven1);
    $connected_calls = $row_connected_seven1['connected'];
   
    return $connected_calls;
}

function NotConnectedCallsbyUserId($conn, $user_name) {
   // $yesterday = date('Y-m-d', strtotime("-1 days"));
   $yesterday = date('Y-m-d', strtotime("-1 days"));
$last_seventh_day = date('Y-m-d', strtotime("-7 days"));
    $sql_connected_seven = mysqli_query($conn, "SELECT COUNT(*) as notconnected FROM campaign_dial_status WHERE DATE(modify_date) <= '$yesterday' AND DATE(modify_date) >='$last_seventh_day' AND status = '2' and user='$user_name' ");
    $row_connected_seven = mysqli_fetch_assoc($sql_connected_seven);
    $notconnected_calls = $row_connected_seven['notconnected'];
    return $notconnected_calls;
}


$missed_call_table = array();
$user_report = "SELECT a.login_time, a.pause_time, a.break_time, a.talk_time, CONCAT( b.first_name, ' ', b.last_name ) AS user_full_name,b.user_name"
        . " FROM user_callsummary a LEFT JOIN vtiger_users b ON a.userid = b.user_name "
        . " WHERE b.status = 'Active' AND DATE( a.current_eventtime ) <= '$yesterday' AND DATE( a.current_eventtime ) >= '$last_seventh_day' GROUP BY user_full_name order by a.login_time DESC ";

     


$res = $conn->query($user_report);
if($res->num_rows > 0){

    $sn = 1;
    while($user_rows = $res->fetch_assoc()){

       
        $login_time = $user_rows['login_time'];
        $pause_time = $user_rows['pause_time'];
        $break_time = $user_rows['break_time'];
        $talk_time = $user_rows['talk_time'];
        $user_name = $user_rows['user_name'];
        $user_full_name = $user_rows['user_full_name'];

         //  echo $time->format('h:i:sa');

           array_push($missed_call_table, array($user_full_name,$login_time,$pause_time,$break_time,$talk_time));

           $table_data .= '<tr>
        <td style="text-align: left; border-bottom: solid 1px #d0d0d0;">'.$sn++.'</td>
        <td style="text-align: left; border-bottom: solid 1px #d0d0d0;">'.$user_full_name.'</td>
        <td style="text-align:left; border-bottom: solid 1px #d0d0d0;">' . gmdate('H:i:s', $login_time) . '</td>
        <td style="text-align:left; border-bottom: solid 1px #d0d0d0;">' . gmdate('H:i:s', $pause_time) . '</td>
        <td style="text-align:left; border-bottom: solid 1px #d0d0d0;">' . gmdate('H:i:s', $break_time) . '</td>
        <td style="text-align:left; border-bottom: solid 1px #d0d0d0;">' . gmdate('H:i:s', $talk_time) . '</td>
        <td style="text-align:center; border-bottom: solid 1px #d0d0d0;">' . ConnectedCallsbyUserId($conn, $user_name) . '</td>
        <td style="text-align:center; border-bottom: solid 1px #d0d0d0;">' . NotConnectedCallsbyUserId($conn, $user_name) . '</td>

       
</tr>';

    }
}








$htmlcontent = '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Weekly Report</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<style>
body{
    color: #090004;
    background-color: #f2f2f2;
    margin-left: 20%;
    margin-right: 20%;
    font-family: "Open Sans", sans-serif;
    font-weight: normal;
    font-size: 14px;
}

.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    background-color: #ffffff;
    margin-bottom: 5px;
}

.container {
    padding: 2px 14px;
}

.grid-container {
    display: grid;
    grid-template-columns: auto auto auto;
   
}

p{
    text-align: left;
}

th, td {
    padding: 5px;
    
}

.button {
    background-color: #0ebcce;
    border: none;
    color: white;
    padding: 8px 16px;
    border-radius:20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 12px;
    font-weight: bold;
    cursor: pointer;
}

img #meetn_graph, #pie_chart {
    width: 100%;
    height: auto;
}

.img-example {
    max-width: 100%;
}

.img-example__sparkline {
    max-width: 10em;
}
</style>
</head> 

<body style="margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" width="100%"> 

<tr>
    <td style="padding: 10px 0 30px 0;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
        </td>
           
</tr>



<tr>
    <td>
        <div class="card" style="width: 100%;background-color: #ffffff">
            <div class="container">
            <p style="font-family:arial black;font-style:italic;color:#800080;font-size:35px;text-align:right">
            *astDIAL</p>

            <h1>Weekly Report</h1>
            <h3 style="text-align: left; color:black ">Duration :  <span><b style="color: #339933;">' . $start_week_1 . '</b> to <b style="color: #339933;">' . $end_week_1 . '</span></h3>

        </div>
    </td>
</tr>


<tr>
<td>
<div class="card" style="width: 100%;background-color: #ffffff">
  
                    <table style="width:100%;">
                    <thead>
                    <td> 
                        <div class="card" style="width: 100%; margin-left: 2%;background-color: #f0ffe3;" >
                            <div class="container" style="height: 100px; ">
                                <p style="color:black;text-align: center;"><b>Total Calls</b></p>
                                <h2 style="color: black; text-align: center;">'.$count_total_connected.'</h2>
                            </div>
                            <br>
                        </div>
                    </td>
                    <td> 
                        <div class="card" style="width: 100%; margin-right: 2%;background-color: #e4f8fa;" >
                            <div class="container" style="height: 100px; ">
                                <p style="color:cc447e;text-align: center;"><b>Connected</b></p>
                                <h2 style="color: #0ebcce; text-align: center;">'.$count_connected.'</h2>
                            </div>
                            <br>
                        </div>
                    </td>
                    <td> 
                        <div class="card" style="width: 100%; margin-right: 2%;background-color: #ffede3;" >
                            <div class="container" style="height: 100px; ">
                                <p style="color:cc447e;text-align: center;"><b>Not Connected</b></p>
                                <h2 style="color: #ff5e00; text-align: center;">'.$count_not_connected.'</h2>
                            </div>
                            <br>
                        </div>
                    </td>

                    </thead>
                </table>
            </div>
</td>
</tr>




<tr>
    <td>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" ;>
            <tbody style="background-color: #ffffff;">
                <tr>
                    <td width="260" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p style="text-align:center;color:#777171;"><b>Calls Trends</b></p>
                                    <img src='.$call_type_chart_chartUrl.' alt="" width="100%" height="auto"
                                        style="display: block;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">
                        &nbsp;
                    </td>

                </tr>
            </tbody>
        </table>
    </td>
</tr>



<tr>
    <td>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" ;>
            <tbody style="background-color: #ffffff;">
                <tr>
                    <td width="260" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p style="text-align:center;color:#777171;"><b>Calls By Disposition</b></p>
                                    <img src='.$yesterday_disposition_url.' alt="" width="100%" height="auto"
                                        style="display: block;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">
                        &nbsp;
                    </td>
                    <td width="260" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p style="text-align:center;color:#777171;"><b>Calls By Duration</b></p>
                                    <img src='.$calls_by_duration_chart_chartUrl.' alt="" width="100%" height="auto"
                                        style="display: block;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>



<tr>
    <td>
        <table border="0" cellpadding="0" cellspacing="0" width="100%" ;>
            <tbody style="background-color: #ffffff;">
                <tr>
                    <td width="260" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p style="text-align:center;color:#777171;"><b>Calls By Campaigns</b></p>
                                    <img src="'.$call_by_camp_url.'" alt="" width="100%" height="auto"
                                        style="display: block;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">
                        &nbsp;
                    </td>
                    <td width="260" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <p style="text-align:center;color:#777171;"><b>Calls By Users</b></p>
                                    <img src='.$Agents_call_type_chart_chartUrl.' alt="" width="100%" height="auto"
                                        style="display: block;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </td>
</tr>

<tr>
<td>


    <div class="card" style="width: 100%;background-color: #425767;">
    <div class="container">
    <h3 style="text-align: left; color:#ffffff ">Agent performance</h3>
    </div>
    <table class="table1" style="width: 100%;background-color: #ffffff; color: #535353; font-size: 13px;">
        <thead style="background-color: #d0d0d0;">
            <tr>
            <th style="text-align: left;">#</th>
            <th style="text-align: left;">Name</th>
            <th>Login Time</th>
            <th>Pause Time</th>
            <th>Break Time</th>
            <th>Talk Time</th>
            <th>Connected</th>
            <th>Not Connected</th>
                
            </tr>
        </thead>

    <tbody>
        '.$table_data.'
    </tbody>
</table>


</td>
</tr>


</table>
</body>
</html> 
';




function getAdminUsersList($conn) {
    $emaillist = array();
    $select_email = "select email1,concat( first_name, ' ', last_name ) as username from vtiger_users where is_admin='on' and status='Active'";
    $q_email = mysqli_query($conn, $select_email);
    while ($row = mysqli_fetch_assoc($q_email)) {
        $username[] = $row['username'];
        $emaillist[] = $row['email1'];
    }

    return $emaillist;
}

$adminlist = getAdminUsersList($conn);

$crm_name = 'astdialv2';

foreach ($adminlist as $email) {
    sendastReport($htmlcontent,$email,$crm_name);
}



//echo $htmlcontent;
function sendastReport($htmlcontent,$email,$crm_name) {
    echo $htmlcontent;
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $mail = new PHPMailer();
    $mail->SMTPDebug = 2;
    $mail->IsSMTP();                                      // set mailer to use SMTP
    $mail->Host = "ssl://alfa3214.alfahosting-server.de:465";  // specify main and backup server
    $mail->SMTPAuth = true;     // turn on SMTP authentication
    $mail->Username = "web23473492p140";  // SMTP username
    $mail->Password = "RKR8B7mR"; // SMTP password
    $mail->From = "reports@astcrm.com";
    $mail->FromName = "Weekly Report";
    $mail->AddAddress($email);
    $mail->AddCC('sales@astcrm.com');
    $mail->AddCC('support@astcrm.com');    
    $mail->AddReplyTo("reports@astcrm.com", "reports");    // set word wrap to 50 characters
    $mail->IsHTML(true);                                  // set email format to HTML
    $mail->Subject = "Weekly Report $crm_name *astDIAL";
    $mail->Body = $htmlcontent;
    if (!$mail->Send()) {
        echo "Message could not be sent";
        echo "Mailer Error: " . $mail->ErrorInfo;
        exit;
    } else {
        echo "Message has been sent";
    }
}

?>
