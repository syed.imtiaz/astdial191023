<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$conn = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($conn,$dbName);

if ($conn->connect_error) {
 die("Connection failed: " . $conn->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
$check_today = date('d-m-Y');
$yesterday = date('Y-m-d',strtotime("-1 days"));
// $yesterday = '2021-01-06';
$last_seventh_day = date('Y-m-d',strtotime("-7 days"));
$today = date('Y-m-d');
$seventh_day = date('Y-m-d',strtotime("-7 days"));
$yesterDate = date('d-m-Y', strtotime($yesterday));


//get last day calls date function
$last_day = get_yesterday($conn, $today);
$l_d = $last_day['last_day'];
//end last days calls function

// total calls
$query_calls = mysqli_query($conn, "SELECT COUNT(calllogsid) as total_calls FROM vtiger_calllogs WHERE date = '".$yesterday."' ");
$total_calls = mysqli_fetch_assoc($query_calls);
$chart_total = $total_calls['total_calls'];

// total calls last seven days
$query_calls_seven = mysqli_query($conn, "SELECT COUNT(calllogsid) as total_calls_seven FROM vtiger_calllogs WHERE date <= '".$yesterday."' AND date >= '".$last_seventh_day."' ");
$total_calls_seven = mysqli_fetch_assoc($query_calls_seven);
$chart_total_seven = $total_calls_seven['total_calls_seven'];

// FETCH YESTERDAY DISPOSITION
$query_dispoCount = mysqli_query($conn, "SELECT COUNT(dispo) as total_dispo, dispo FROM vtiger_calllogs WHERE date = '".$l_d."' AND dispo <> '' GROUP BY dispo ORDER BY date asc");
while($total_dispoCount = mysqli_fetch_assoc($query_dispoCount)) {
	$chart_dispo[] = $total_dispoCount['total_dispo'];
	$chart_dispo_status[] = $total_dispoCount['dispo'];
}

// LAST 7 DAYS DISPOSITION
$query_dispoCount_seven = mysqli_query($conn, "SELECT COUNT(dispo) as total_dispo, dispo FROM vtiger_calllogs WHERE date <= '".$yesterday."' AND date >= '".$last_seventh_day."' AND dispo <> '' GROUP BY dispo ORDER BY date asc");
while($total_dispoCount_seven = mysqli_fetch_assoc($query_dispoCount_seven)) {
	$chart_dispo_seven[] = $total_dispoCount_seven['total_dispo'];
	$chart_dispo_status_seven[] = $total_dispoCount_seven['dispo'];
}

// LAST 7 DAYS TOTAL CALLS
$query_lastweekcalls = mysqli_query($conn, "SELECT COUNT(*) as total_lastweekcalls, modify_date FROM campaign_dial_status WHERE DATE(modify_date) <= '".$today."' AND DATE(modify_date) >= '".$last_seventh_day."' GROUP BY DATE(modify_date) ORDER BY modify_date ASC");
while($total_lastweekcalls = mysqli_fetch_assoc($query_lastweekcalls)) {

		$chart_lastweekcall[] = $total_lastweekcalls['total_lastweekcalls'];	
		// $seventh_day = date('Y-m-d',strtotime("-7 days"));
		$chart_lastweekdate1[] = date('Y-m-d',strtotime($total_lastweekcalls['modify_date']));
		$chart_lastweekdate[] = date('d-m-Y',strtotime($total_lastweekcalls['modify_date']));
	}

	foreach ($chart_lastweekdate1 as $key) {

	// LAST SEVEN DAYS NOT CONNNECTED CALLS
	$sql_connected_seven = mysqli_query($conn, "SELECT COUNT(*) as notconnected_seven FROM campaign_dial_status WHERE DATE(modify_date) = '".$key."' AND status = '2'");
	$row_connected_seven = mysqli_fetch_assoc($sql_connected_seven);
	$not_connected_lastseven[] = $row_connected_seven['notconnected_seven'];

	// LAST SEVEN DAYS CONNNECTED CALLS
	$sql_connected_seven1 = mysqli_query($conn, "SELECT COUNT(*) as connected_seven FROM campaign_dial_status WHERE DATE(modify_date) = '".$key."' AND status = '1' GROUP BY DATE(modify_date)");
	$row_connected_seven1 = mysqli_fetch_assoc($sql_connected_seven1);
	$connected_last_seven[] = $row_connected_seven1['connected_seven'];

	}

// YESTERDAY TOTAL CALLS
$call_yesterday_sql = mysqli_query($conn, "SELECT COUNT(id) as total_yesterday_calls, user_name, user FROM campaign_dial_status WHERE DATE(modify_date) = '".$l_d."' GROUP BY user");
while($call_yesterday_row = mysqli_fetch_assoc($call_yesterday_sql)){
	$yes_total[] = $call_yesterday_row['total_yesterday_calls'];
	$yes_source[] = $call_yesterday_row['user'];
	$yes_user[] = $call_yesterday_row['user_name'];
}

// YESTERDAY CONNECTED CALLS
$yes_conn_sql = mysqli_query($conn, "SELECT COUNT(id) as conn_yesterday_calls, user_name, user FROM campaign_dial_status WHERE DATE(modify_date) = '".$l_d."' AND status = '1' GROUP BY user");
while($row_yes_con = mysqli_fetch_assoc($yes_conn_sql)){
	$yes_call_conn[] = $row_yes_con['conn_yesterday_calls'];
}

// YESTERDAY NOT CONNECTED CALLS
$yes_conn_sqln = mysqli_query($conn, "SELECT COUNT(id) as nconn_yesterday_calls, user_name, user FROM campaign_dial_status WHERE DATE(modify_date) = '".$l_d."' AND status = '2' GROUP BY user");
while($row_yes_conn = mysqli_fetch_assoc($yes_conn_sqln)){
	$yes_call_nconn[] = $row_yes_conn['nconn_yesterday_calls'];
}

$query_lastweekperformance = mysqli_query($conn, "SELECT sum(pause_time) as pause_time, sum(break_time) as break_time , sum(talk_time) as talk_time from user_callsummary  where datetime  <= '".$yesterday."' AND  datetime >= '".$last_seventh_day."' group by datetime order by datetime DESC");
$total_lastweekperformance = mysqli_fetch_assoc($query_lastweekperformance);

	$chart_lastweekpause_time = $total_lastweekperformance['pause_time'];
	$chart_lastweekbreak_time = $total_lastweekperformance['break_time'];
	$chart_lastweektalk_time = $total_lastweekperformance['talk_time'];

// connected calls
	$sql_connected = mysqli_query($conn, "SELECT a.date, a.dispo, b.name, c.cf_918 as call_status FROM vtiger_calllogs a LEFT JOIN vtiger_maindispo b ON a.dispo = b.name LEFT JOIN vtiger_maindispocf c ON b.maindispoid = c.maindispoid WHERE c.cf_918 = 'Connected' and a.date = '".$today."' ");
	$count_connected = mysqli_num_rows($sql_connected);

// not connected calls
	$sql_not_connected = mysqli_query($conn, "SELECT a.date, a.dispo, b.name, c.cf_918 as call_status FROM vtiger_calllogs a LEFT JOIN vtiger_maindispo b ON a.dispo = b.name LEFT JOIN vtiger_maindispocf c ON b.maindispoid = c.maindispoid WHERE c.cf_918 = 'Not Connected' and a.date = '".$today."' ");
	$count_not_connected = mysqli_num_rows($sql_not_connected);

//total calls
	$sql_total_connected = mysqli_query($conn, "SELECT a.date, a.dispo, b.name, c.cf_918 as call_status FROM vtiger_calllogs a LEFT JOIN vtiger_maindispo b ON a.dispo = b.name LEFT JOIN vtiger_maindispocf c ON b.maindispoid = c.maindispoid WHERE a.date = '".$today."' ");
	$count_total_connected = mysqli_num_rows($sql_total_connected);


// LAST 7 DAYS LOGIN
	$query_last_login = mysqli_query($conn, "SELECT COUNT(id) as login_cnt_seven, DATE(datetime) as login_date FROM user_callsummary WHERE DATE(datetime) <= '".$yesterday."' AND DATE(datetime) >= '".$last_seventh_day."' group by DATE(datetime) order by DATE(datetime) ASC ");
	while($row_last_login = mysqli_fetch_assoc($query_last_login)){

		$date_last_login[] = date('d-m-Y',strtotime($row_last_login['login_date']));
		$cnt_last_login[] = $row_last_login['login_cnt_seven'];
	}

	// TOTAL USERS
	$query_users = mysqli_query($conn, "SELECT COUNT(id) as user_cnt FROM vtiger_users WHERE status = 'Active' ");
	$row_users = mysqli_fetch_assoc($query_users);
	$res_users = $row_users['user_cnt'];


	// YESTERDAYS CALL BY DURATION
	$qry_thirty = mysqli_query($conn, "SELECT COUNT(*) as total30 FROM `campaign_dial_status` WHERE `duration`<=30 and DATE(`modify_date`)='$yesterday' ");
	$row_thirty = mysqli_fetch_assoc($qry_thirty);
	$res_thirty = $row_thirty['total30'];

	$qry_sixty = mysqli_query($conn, "SELECT COUNT(*) as total60 FROM `campaign_dial_status` WHERE `duration`>30 and `duration`<=60 and DATE(`modify_date`)='$yesterday'");
	$row_sixty = mysqli_fetch_assoc($qry_sixty);
	$res_sixty = $row_sixty['total60'];

	$qry_one_twenty = mysqli_query($conn, "SELECT COUNT(*) as total120 FROM `campaign_dial_status` WHERE `duration`>60 and `duration`<=120 and DATE(`modify_date`)='$yesterday'");
	$row_one_twenty = mysqli_fetch_assoc($qry_one_twenty);
	$res_one_twenty = $row_one_twenty['total120'];

	$qry_three_hundred = mysqli_query($conn, "SELECT COUNT(*) as total300 FROM `campaign_dial_status` WHERE `duration`>120 and `duration`<=300 and DATE(`modify_date`)='$yesterday'");
	$row_three_hundred = mysqli_fetch_assoc($qry_three_hundred);
	$res_three_hundred = $row_three_hundred['total300'];

	$qry_three_hundred_more = mysqli_query($conn, "SELECT COUNT(*) as total300_greater FROM `campaign_dial_status` WHERE `duration`>300 and DATE(`modify_date`)='$yesterday'");
	$row_three_hundred_more = mysqli_fetch_assoc($qry_three_hundred_more);
	$res_three_hundred_more = $row_three_hundred_more['total300_greater'];
	// 

	$yesterday_dmy = date('d-m-Y', strtotime($yesterday));
	$last_seventh_day_dmy = date('d-m-Y', strtotime($last_seventh_day));





$last_day = get_yesterday($conn, $today);
$l_d = $last_day['last_day'];
$l_d = date("d-m-Y", strtotime($l_d));

//echo $l_d;
function get_yesterday($conn, $today)
{
	
	$limit = 1;
 	$qry_log_dates_1 = $conn->query("SELECT COUNT( calllogsid ) AS total_calls, date FROM vtiger_calllogs WHERE DATE != '' AND DATE != '$today' GROUP BY date ORDER BY date DESC");
while (($row_log_dates_1 = $qry_log_dates_1->fetch_assoc()) && ($limit <= 1)) {
		if ($row_log_dates_1['total_calls'] > 0) {

			$last_day = $row_log_dates_1['date'];
			// echo "last day".$last_day;
			// echo "<br>";
			$limit++;
		}
	}
	$last_day = array('last_day' => $last_day);
	return $last_day;
}


	$widget_contents = array(
			"chart_total_seven"=>$chart_total_seven,
			"chart_dispo_seven"=>$chart_dispo_seven,
			"chart_dispo_status_seven"=>$chart_dispo_status_seven,
			"res_thirty"=>$res_thirty,
			"res_sixty"=>$res_sixty,
			"res_one_twenty"=>$res_one_twenty,
			"res_three_hundred"=>$res_three_hundred,
			"res_three_hundred_more"=>$res_three_hundred_more,
			"res_users"=>$res_users,
			"date_last_login"=>$date_last_login,
			"cnt_last_login"=>$cnt_last_login,
			"not_conn_seven"=>$not_connected_lastseven,
			"conn_lastweek"=>$connected_last_seven,
			"chart_total"=>$chart_total,
			"chart_dispo"=>$chart_dispo,
			"chart_dispo_status"=>$chart_dispo_status,
			"chart_lastweekcall"=>$chart_lastweekcall,
			"chart_lastweekdate"=>$chart_lastweekdate,
			"chart_lastweekpause_time"=>$chart_lastweekpause_time,
			"chart_lastweekbreak_time"=>$chart_lastweekbreak_time,
			"chart_lastweektalk_time"=>$chart_lastweektalk_time,
			"count_connected"=>$count_connected,
			"count_not_connected"=>$count_not_connected,
			"count_total_connected"=>$count_total_connected,
			"yes_user"=>$yes_user ,
			"yes_totconn_call"=>$yes_total ,
			"yes_conn_call"=>$yes_call_conn ,
			"yes_nconn_call"=>$yes_call_nconn,
			"check_today"=>$check_today,  
			"yesterDate"=>$yesterDate,  
			"yesterday_dmy"=>$yesterday_dmy,  
			"last_seventh_day_dmy"=>$last_seventh_day_dmy,
			"l_d"=>$l_d,
			);
	echo json_encode($widget_contents);	



?>
