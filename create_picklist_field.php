<?php
$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('CallLogs');

$infoBlock = Vtiger_Block::getInstance('LBL_CALLLOGS_INFORMATION', $module);

$field1 = Vtiger_Field::getInstance('call_type', $module);
if (!$field1) {
    $field1 = new Vtiger_Field();
    $field1->name = 'calltype';
    $field1->label = 'Call Type';
    $field1->table = $module->basetable;
    $field1->column = 'call_type';
    $field1->columntype = 'varchar(50)';
    $field1->uitype = 15;
    $field1->typeofdata = 'V~M~LE~128';
    $field1->setPicklistValues(array('Regular', 'Manual','Incoming'));
    $infoBlock->addField($field1);
}


// $field2 = Vtiger_Field::getInstance('duration', $module);
// if (!$field2) {
//     $field2 = new Vtiger_Field();
//     $field2->name = 'duration';
//     $field2->label = 'Duration';
//     $field2->table = $module->basetable;
//     $field2->uitype = 2;
//     $field2->column = $field2->name;
//     $field2->columntype = 'VARCHAR(50)';
//     $field2->typeofdata = 'V~O';
//     $infoBlock->addField($field2);
// }



// $field3 = Vtiger_Field::getInstance('uniqueid', $module);
// if (!$field3) {
//     $field3 = new Vtiger_Field();
//     $field3->name = 'uniqueid';
//     $field3->label = 'Unique Id';
//     $field3->table = $module->basetable;
//     $field3->uitype = 2;
//     $field3->column = $field3->name;
//     $field3->columntype = 'VARCHAR(50)';
//     $field3->typeofdata = 'V~O';
//     $infoBlock->addField($field3);
// }



// $field4 = Vtiger_Field::getInstance('voicefiles', $module);
// if (!$field4) {
//     $field4 = new Vtiger_Field();
//     $field4->name = 'voicefiles';
//     $field4->label = 'Voice Files';
//     $field4->table = $module->basetable;
//     $field4->uitype = 2;
//     $field4->column = $field4->name;
//     $field4->columntype = 'VARCHAR(255)';
//     $field4->typeofdata = 'V~O';
//     $infoBlock->addField($field4);
// }
