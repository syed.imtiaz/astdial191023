<?php
include 'config.inc.php';

$hostname = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbName   = $dbconfig['db_name'];

//DB connection
$con = @mysqli_connect($hostname,$username,$password);
mysqli_select_db($con,$dbName);

if ($conn->connect_error) {
 die("Connection failed: " . $con->connect_error);
}else{
	 // echo "connected";
} 

date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
$cur_date = date("Y-m-d");

$today_date = " AND modify_date LIKE '".$cur_date."%' ";
$today_date_man = " AND modify_date LIKE '".$cur_date."%' ";

$camp_selected = $_POST['camp_selected'];

if($_POST['camp_selected'] == '' || $_POST['camp_selected'] == 'ALL'){

	// agent report starts
		$sel = "SELECT a.* from user_callsummary a LEFT JOIN vtiger_users b ON a.userid = b.user_name where DATE(a.datetime)='".$cur_date."' order by b.first_name";	
		$sel_count = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."' ";	
		$sel_succ="SELECT count(*) from campaign_dial_status where DATE(modify_date)='".$cur_date."' and status = '1'";
		$sel_num="SELECT count(*)  from  user_callsummary  where DATE(datetime) ='".$cur_date."'";
	// agent report ends

	// live status report
	// total count
	$sel_count = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."'";
	$query_count = mysqli_query($con,$sel_count);
	$row_count = mysqli_fetch_array($query_count);
	$total = $row_count[0];
		
	//count dial
	$sel_dial = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."' and currentevent = 'DIAL'  ";	
	$query_dial = mysqli_query($con,$sel_dial);
	$row_dial = mysqli_fetch_array($query_dial);
	$dial = $row_dial[0];
			
	//count break
	$sel_break = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."'and currentevent = 'BREAK'  ";	
	$query_break = mysqli_query($con,$sel_break);
	$row_break = mysqli_fetch_array($query_break);
	$break = $row_break[0];
		
	//count pause
	$sel_pause = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."' and currentevent = 'PAUSE'  ";	
	$query_pause = mysqli_query($con,$sel_pause);
	$row_pause = mysqli_fetch_array($query_pause);
	$pause = $row_pause[0];
	
	//count logout
	$sel_logout = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."' and currentevent = 'LOGOUT'  ";	
	$query_logout = mysqli_query($con,$sel_logout);
	$row_logout = mysqli_fetch_array($query_logout);
	$logout = $row_logout[0];
	// live status ends

	// count total
	$sql_total=mysqli_query($con, "SELECT count(*) AS total_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$today_date." ");
	$cnt_total = mysqli_fetch_assoc($sql_total);
	$total_leads = $cnt_total['total_cnt'];

	//count dialed
	$count_dial = mysqli_query($con, "SELECT count(*) AS dial_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='1' AND vtiger_campaigncontrel.status NOT IN ('') INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$today_date." ");
	$cnt_dial = mysqli_fetch_assoc($count_dial);
	$dial_leads = $cnt_dial['dial_cnt'];

	$not_dialed = $total_leads-$dial_leads;

	if ($not_dialed == '') {
		$not_dialed = 0;
	}

	// assigned
	$assign_sql= mysqli_query($con, "SELECT count(*) AS asn_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='1'  INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$today_date." ") ;
	$cnt_assign = mysqli_fetch_assoc($assign_sql);
	$assign_leads = $cnt_assign['asn_cnt'];

	// unassigned
	$unassign_sql= mysqli_query($con, "SELECT count(*) AS unasn_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='0'  INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$today_date." ") ;
	$cnt_unassign  = mysqli_fetch_assoc($unassign_sql);
	$unassign_leads = $cnt_unassign['unasn_cnt'];

	// manual dial
	$sel_manualdial = mysqli_query($con, "SELECT count(*) AS manl_cnt FROM campaign_dial_status WHERE dial_id='-1' AND status = '1' ".$today_date_man." ");	
	$cnt_manual = mysqli_fetch_assoc($sel_manualdial);
	$manual_dialed = $cnt_manual['manl_cnt'];

	//regular call count
	$sql_regular_call = mysqli_query($con, "SELECT count(*) as regular_call FROM vtiger_calllogs WHERE call_type='Regular' " . $camp_man . " AND date = '$cur_date'");
	$cnt_regular = mysqli_fetch_assoc($sql_regular_call);
	$regular_call = $cnt_regular['regular_call'];

	//manual call
	$sql_manual_call = mysqli_query($con, "SELECT count(*) as manual_call FROM vtiger_calllogs WHERE call_type='Manual' " . $camp_man . " AND date = '$cur_date'");
	$cnt_manual = mysqli_fetch_assoc($sql_manual_call);
	$manual_call = $cnt_manual['manual_call'];
	//

	//Incoming call
	$sql_incoming_call = mysqli_query($con, "SELECT count(*) as incoming_call FROM vtiger_calllogs WHERE call_type='Incoming' " . $camp_man . " AND date = '$cur_date'");
	$cnt_manual = mysqli_fetch_assoc($sql_incoming_call);
	$incoming_call = $cnt_manual['incoming_call'];
	//

}else{
	$camp_selected = $_POST['camp_selected'];

	if($camp_selected != 'ALL'){
		$camp = " AND vtiger_campaign.campaignname LIKE '%".$camp_selected."%' " ;
		$camp_man = " AND campaign LIKE '%".$camp_selected."%' " ;
	}else{
		$camp = "" ;
		$camp_man = "";
	}
	// agent report starts
	$sel = "SELECT a.* from user_callsummary a LEFT JOIN vtiger_users b ON a.userid = b.user_name where DATE(a.datetime)='".$cur_date."' and a.campaign_logged_in='".$camp_selected."'  order by b.first_name";
	$sel_count = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."' and campaign_logged_in='".$camp_selected."' ";		
	$sel_succ="SELECT count(*) from campaign_dial_status where DATE(modify_date)='".$cur_date."' and status = '1' and campaign = '".$camp_selected."'";
	$sel_num="SELECT count(distinct(userid))  from  user_callsummary  where campaign_logged_in='".$camp_selected."' and DATE(datetime) = '".$cur_date."'";
	// agent report ends


	// live status report
	// total count
	$sel_count = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."' and campaign_logged_in='".$camp_selected."' ";	
	$query_count = mysqli_query($con,$sel_count);
	$row_count = mysqli_fetch_array($query_count);
	$total = $row_count[0];
	
	//count dial
	$sel_dial = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."' and currentevent = 'DIAL' and campaign_logged_in='".$camp_selected."'  ";	
	$query_dial = mysqli_query($con,$sel_dial);
	$row_dial = mysqli_fetch_array($query_dial);
	$dial = $row_dial[0];
			
	//count break
	$sel_break = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."'and currentevent = 'BREAK' and campaign_logged_in='".$camp_selected."' ";	
	$query_break = mysqli_query($con,$sel_break);
	$row_break = mysqli_fetch_array($query_break);
	$break = $row_break[0];
			
	//count pause
	$sel_pause = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."' and currentevent = 'PAUSE' and campaign_logged_in='".$camp_selected."' ";	
	$query_pause = mysqli_query($con,$sel_pause);
	$row_pause = mysqli_fetch_array($query_pause);
	$pause = $row_pause[0];
		
	//count logout
	$sel_logout = "SELECT count(*) from user_callsummary  where DATE(datetime)='".$cur_date."' and currentevent = 'LOGOUT' and campaign_logged_in='".$camp_selected."'  ";	
	$query_logout = mysqli_query($con,$sel_logout);
	$row_logout = mysqli_fetch_array($query_logout);
	$logout = $row_logout[0];
	// live status ends

	// lead status
	$cmp = "SELECT campaignid FROM vtiger_campaign WHERE campaignname = '".$camp_selected."' ";
	$get_campaignid = mysqli_query($con,$cmp);
	$row_campaignid = mysqli_fetch_array($get_campaignid);

	$campaign = $row_campaignid[0];

	// count total
	$sql_total=mysqli_query($con, "SELECT count(*) AS total_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$today_date.$camp." ");
	$cnt_total = mysqli_fetch_assoc($sql_total);
	$total_leads = $cnt_total['total_cnt'];

	//count dialed
	$count_dial = mysqli_query($con, "SELECT count(*) AS dial_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='1' AND vtiger_campaigncontrel.status NOT IN ('') INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$today_date.$camp." ");
	$cnt_dial = mysqli_fetch_assoc($count_dial);
	$dial_leads = $cnt_dial['dial_cnt'];

	$not_dialed = $total_leads-$dial_leads;

	if ($not_dialed == '') {
		$not_dialed = 0;
	}

	// assigned
	$assign_sql= mysqli_query($con, "SELECT count(*) AS asn_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='1'  INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$today_date.$camp." ") ;
	$cnt_assign = mysqli_fetch_assoc($assign_sql);
	$assign_leads = $cnt_assign['asn_cnt'];

	// unassigned
	$unassign_sql= mysqli_query($con, "SELECT count(*) AS unasn_cnt  FROM vtiger_campaigncontrel INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_campaigncontrel.contactid AND vtiger_crmentity.deleted='0' AND vtiger_campaigncontrel.ast_update='0'  INNER JOIN vtiger_campaign ON vtiger_campaign.campaignid=vtiger_campaigncontrel.campaignid ".$today_date.$camp." ") ;
	$cnt_unassign  = mysqli_fetch_assoc($unassign_sql);
	$unassign_leads = $cnt_unassign['unasn_cnt'];

	// manual dial
	$sel_manualdial = mysqli_query($con, "SELECT count(*) AS manl_cnt FROM campaign_dial_status WHERE dial_id='-1' AND status = '1' ".$today_date_man.$camp_man." ");	
	$cnt_manual = mysqli_fetch_assoc($sel_manualdial);
	$manual_dialed = $cnt_manual['manl_cnt'];

	//regular call count
	$sql_regular_call = mysqli_query($con, "SELECT count(*) as regular_call FROM vtiger_calllogs WHERE call_type='Regular' ".$camp_man." AND date = '$cur_date'");
	$cnt_regular = mysqli_fetch_assoc($sql_regular_call);
	$regular_call = $cnt_regular['regular_call'];

	//

	//manual call
	$sql_manual_call = mysqli_query($con, "SELECT count(*) as manual_call FROM vtiger_calllogs WHERE call_type='Manual' " . $camp_man . " AND date = '$cur_date'");
	$cnt_manual = mysqli_fetch_assoc($sql_manual_call);
	$manual_call = $cnt_manual['manual_call'];
	//

	//Incoming call
	$sql_incoming_call = mysqli_query($con, "SELECT count(*) as incoming_call FROM vtiger_calllogs WHERE call_type='Incoming' " . $camp_man . " AND date = '$cur_date'");
	$cnt_manual = mysqli_fetch_assoc($sql_incoming_call);
	$incoming_call = $cnt_manual['incoming_call'];
	//

}
// get campaigns
$campid=array();
$select_camp = "SELECT campaignname,campaignid FROM vtiger_campaign INNER JOIN vtiger_crmentity ON vtiger_campaign.campaignid=vtiger_crmentity.crmid WHERE deleted='0' AND campaignstatus='Active'";	
$query_camp = mysqli_query($con,$select_camp);				
while($row_camp = mysqli_fetch_assoc($query_camp)){
	$campaigns[] = $row_camp['campaignname'];
	$campid[] = $row_camp['campaignid'];
}
// end campaigns
// print_r($campid);

$campdialarray=array();
foreach($campid as $campaignid){
	//$totcamplist="SELECT * FROM `vtiger_campaigncontrel` WHERE `campaignrelstatusid` = '1' AND `campaignid` = '$campaignid'";
	//$res_totlist=mysqli_query($con,$totcamplist);
	//$totlist=mysqli_num_rows($res_totlist);

/*$totcamplist = "SELECT COUNT(DISTINCT vtiger_crmentity.crmid) AS count FROM vtiger_contactdetails INNER JOIN vtiger_campaigncontrel ON vtiger_campaigncontrel.contactid = vtiger_contactdetails.contactid INNER JOIN vtiger_contactaddress ON vtiger_contactdetails.contactid = vtiger_contactaddress.contactaddressid INNER JOIN vtiger_contactsubdetails ON vtiger_contactdetails.contactid = vtiger_contactsubdetails.contactsubscriptionid INNER JOIN vtiger_customerdetails ON vtiger_contactdetails.contactid = vtiger_customerdetails.customerid INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = vtiger_contactdetails.contactid LEFT JOIN vtiger_contactscf ON vtiger_contactdetails.contactid = vtiger_contactscf.contactid LEFT JOIN vtiger_groups ON vtiger_groups.groupid=vtiger_crmentity.smownerid LEFT JOIN vtiger_users ON vtiger_crmentity.smownerid=vtiger_users.id LEFT JOIN vtiger_account ON vtiger_account.accountid = vtiger_contactdetails.accountid LEFT JOIN vtiger_campaignrelstatus ON vtiger_campaignrelstatus.campaignrelstatusid = vtiger_campaigncontrel.campaignrelstatusid WHERE vtiger_campaigncontrel.campaignid = '$campaignid' AND vtiger_crmentity.deleted=0";
	
	$res_totlist=mysqli_query($con,$totcamplist);
	$totlist_row=mysqli_fetch_assoc($res_totlist);
	$totlist=$totlist_row['count'];

	$totcampdiallist="SELECT * FROM `vtiger_campaigncontrel` WHERE `campaignrelstatusid` = '1' AND (status !='0' or dispo !='0') AND `campaignid` = '$campaignid'";
	$res_totdiallist=mysqli_query($con,$totcampdiallist);
	$totdiallist=mysqli_num_rows($res_totdiallist);

	$totnondiallist=$totlist - $totdiallist;

	$campname="select campaignname FROM `vtiger_campaign` where campaignid='$campaignid'";
	$res_campname=mysqli_query($con,$campname);
	while($rowcamp=mysqli_fetch_array($res_campname)){
		$cName=$rowcamp['campaignname'];
	}


	$campdialarray[]=$cName.'@_@'.$totlist.'@_@'.$totdiallist.'@_@'.$totnondiallist.'@_@'.$campaignid;*/
};

// agent report data
	$q_succ=mysqli_query($con,$sel_succ);
	$r_succ=mysqli_fetch_array($q_succ);	
				
	$q_count=mysqli_query($con,$sel_count);
	$row_count=mysqli_fetch_array($q_count);

	$count_total = $row_count[0];
	if($r_succ[0] > 0){
		$suc_avg=$r_succ[0]/$count_total;
	}

	$q=mysqli_query($con,$sel);
	$num_cnt = mysqli_num_rows($q);

	while($row=mysqli_fetch_array($q)){

		// login time
		$diff=$row[2];
		$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
		$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
		$login_time= sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);

		// event time
		$cur_datetime = date('Y-m-d H:i:s');
		$eventtime = $row[9];
		$cur_eventtimesec = abs(strtotime($cur_datetime) - strtotime($eventtime));
		$hours   = floor(($cur_eventtimesec - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts  = floor(($cur_eventtimesec - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
		$seconds = floor(($cur_eventtimesec - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
		$cur_eventtime= sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);

		// pause time
		$activity = "select pause_time,break_time,talk_time from user_callsummary  where DATE(datetime)='".$cur_date."' and (userid='".$row[1]."')";	
		$query_activity = mysqli_query($con,$activity);
		$row_activity = mysqli_fetch_array($query_activity);
		if($row[8]=='PAUSE')
		{
			$pause_time = $row_activity['pause_time']+$cur_eventtimesec;
		}
		else{
			$pause_time = $row_activity['pause_time'];
		}
		$hours   = floor(($pause_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts  = floor(($pause_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
		$seconds = floor(($pause_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
		$pause_time_hour= sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);


		// break time
		if($row[8]=='BREAK')
		{
			$break_time = $row_activity['break_time']+$cur_eventtimesec;
		}
		$break_time = $row_activity['break_time'];
		//calculate break time
		$hours   = floor(($break_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts  = floor(($break_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
		$seconds = floor(($break_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
		$break_time_hour= sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);

		// talk time
		if($row[8]=='DIAL')
		{
			$talk_time = $row_activity['talk_time']+$cur_eventtimesec;
		}
		else{
		$talk_time = $row_activity['talk_time'];
		}
		//calculate talk time
		$hours   = floor(($talk_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts  = floor(($talk_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
		$seconds = floor(($talk_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
		$talk_time_hour= sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);

		// total time
	   $total_work_time = $talk_time + $break_time + $pause_time;
	   $hours   = floor(($total_work_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
	   $minuts  = floor(($total_work_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
	   $seconds = floor(($total_work_time - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
	   $totalwork_time_hour= sprintf('%02d:%02d:%02d', $hours, $minuts, $seconds);


	   $max_total_time = 480 * 60;

	   // check the condition for login time greater than 8 hours
	   $total_activity = $pause_time + $break_time + $talk_time;
	   if( $total_activity >= $max_total_time)
	   {
		$max_total_time = $total_activity;
		$idddddd= 'no';
		$total_per = ceil(($total_activity/$max_total_time)*100);
		$pause_time_per = ceil(( $pause_time/$max_total_time)*100);
		$talk_time_per = ceil(( $talk_time/$max_total_time)*100);
		$break_time_per = ceil(( $break_time/$max_total_time)*100);
	   }
	   else
	   {
	   	$idddddd= 'yes';
		$total_activity = ceil( $pause_time + $break_time + $talk_time);
		$total_per = ceil(($total_activity/$max_total_time)*100);
		$pause_time_per = ceil(( $pause_time/$max_total_time)*100);
		$talk_time_per = ceil(( $talk_time/$max_total_time)*100);
		$break_time_per = ceil(( $break_time/$max_total_time)*100);
		}


		if($_POST['camp_selected'] == '' || $_POST['camp_selected'] == 'ALL'){
			//sucess calls
			$user = $row[1];
			$sql_sucess = "SELECT id from campaign_dial_status where user = '".$user."' and DATE(modify_date)='".$cur_date."' and status = 1";
			$query_sucess = mysqli_query($con,$sql_sucess);
			$sucess_calls = mysqli_num_rows($query_sucess);
			//end success

			//unsucess calls
			$sql_unsucess = "SELECT id from campaign_dial_status where user = '".$user."' and DATE(modify_date)='".$cur_date."' and status = '2'";
			$query_unsucess = mysqli_query($con,$sql_unsucess);
			$unsucess_calls = mysqli_num_rows($query_unsucess);
			//end unsucess
		}else{
			$camp_selected = $_POST['camp_selected'];
			//sucess calls
			$user = $row[1];

			$sql_sucess = "SELECT id from campaign_dial_status where user = '".$user."' and DATE(modify_date)='".$cur_date."' and status = '1' and campaign = '".$camp_selected."' ";
			$query_sucess = mysqli_query($con,$sql_sucess);
			$sucess_calls = mysqli_num_rows($query_sucess);
			//end success

			//unsucess calls
			$sql_unsucess = "SELECT id from campaign_dial_status where user = '".$user."' and DATE(modify_date)='".$cur_date."' and status = '2' and campaign = '".$camp_selected."' ";
			$query_unsucess = mysqli_query($con,$sql_unsucess);
			$unsucess_calls = mysqli_num_rows($query_unsucess);
			//end unsucess
		}		

		$display_name = $row[1];
		$get_name = "SELECT first_name,last_name from vtiger_users where user_name = '".$row[1]."' ";
		$query_name = mysqli_query($con,$get_name);
		$name1 = mysqli_fetch_array($query_name);
		$u_name[]=$name1[0]." ".$name1[1];
		
		if($sucess_calls >= $suc_avg){
			$suc_80=round($suc_avg*(80/100));
			$suc_50=round($suc_avg*(50/100));

			if($sucess_calls >= $suc_80){
				$col[]="#58b676";
			}elseif($sucess_calls >= $suc_50 && $sucess_calls < $suc_80){
				$col[]="#71c18a";
			}else{
				$col[]="#95d0a8";
			}

		}else{
			$suc_80=round($suc_avg*(80/100));
			$suc_50=round($suc_avg*(50/100));

			if($sucess_calls >= $suc_80){
				$col[]="#f36d59";
			}elseif($sucess_calls >= $suc_50 && $sucess_calls < $suc_80){
				$col[]="#f58a79";
			}else{
				$col[]="#f79788";
			}
		}


		//set red color for cut_time > one hour
		$current_eventtime[] = strtotime($cur_eventtime);
		//echo strtotime($cur_eventtime)."===";
		$time_diff[] = strtotime('01:00:00');

		$campaign_name[] = $row[6];
		$successful_calls[] = $sucess_calls;
		$failed_calls[] = $unsucess_calls;
		$dispo_status[] = $row[8];
		$current_evt_time[] = $cur_eventtime;
		$info[] = $row[10];
		$login_total[] = $totalwork_time_hour; 

		$total_percentage[] =$total_per; 
		$talk_percentage[] =$talk_time_per; 
		$pause_percentage[] =$pause_time_per; 
		$break_percentage[] =$break_time_per; 

		$show_pause_time[] = $pause_time_hour;
		$show_break_time[] = $break_time_hour;
		$show_talk_time[] = $talk_time_hour;

	}
	$success_total_call = array_sum($successful_calls);
	$unsuccess_total_call = array_sum($failed_calls);
// agent report data ends

$realtime_contents = array(
		"test" => $pause_percentage,
		"campaigns"=>$campaigns,
		"campaignDetails"=>$campdialarray,
		"camp_selected"=>$camp_selected,
		"total"=>$total,
		"dial"=>$dial,
		"break"=>$break,
		"pause"=>$pause,
		"logout"=>$logout,
		"total_leads"=>$total_leads,
		"assign"=>$assign_leads,
		"notassign"=>$unassign_leads,
		"dial_leads"=>$dial_leads,
		"notdial"=>$not_dialed,
		"manualdial_cont"=>$manual_dialed,
		"username"=>$u_name,
		"campaign_name"=>$campaign_name,
		"column_background"=>$col,
		"successful_calls"=>$successful_calls,
		"failed_calls"=>$failed_calls,
		"dispo_status"=>$dispo_status,
		"event_check"=>$current_eventtime,
		"diff_check"=>$time_diff,
		"event_time"=>$current_evt_time,
		"info"=>$info,
		"login_total"=>$login_total,
		"total_percentage"=>$total_percentage,
		"talk_percentage"=>$talk_percentage,
		"pause_time_per"=>$pause_percentage,
		"break_time_per"=>$break_percentage,
		"success_total_call"=>$success_total_call,
		"unsuccess_total_call"=>$unsuccess_total_call,
		"num_cnt"=>$num_cnt,
		"show_pause_time"=>$show_pause_time,
		"show_break_time"=>$show_break_time,
		"show_talk_time"=>$show_talk_time,
		"regular_call"=> $regular_call,
		"manual_call"=> $manual_call,
		"incoming_call"=> $incoming_call

		);
echo json_encode($realtime_contents);	

?>
